﻿using Owin;
using System;
using VehicleQueueApp.Models;

namespace VehicleQueueApp
{
    public partial class Startup
    {
        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
        }
    }
}