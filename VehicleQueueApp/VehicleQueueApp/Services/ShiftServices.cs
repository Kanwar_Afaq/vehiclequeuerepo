﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VehicleQueueApp.Models;

namespace VehicleQueueApp.Services
{
    public class ShiftServices
    {
        public ShiftRepository shiftRepository = new ShiftRepository();
        public void deleteShift(int id)
        {
            shiftRepository.deleteShift(id);

        }
        public Shift getShiftbyId(int id)
        {
            Shift shift = shiftRepository.getShiftbyId(id);
            return shift;
        }
        public IEnumerable<Shift> GetShifts()
        {
            return shiftRepository.GetShifts();
        }
        public void insertDay(Shift shift)
        {
            shiftRepository.insertShift(shift);
        }
        public void updateShift(Shift shift)
        {
            shiftRepository.updateShift(shift);
        }
        public void StartShift(int userId,int dayId)
        {
            shiftRepository.StartShift(userId,dayId);
        }
        public void CLoseShift(int shiftId, int dayId, int userId)
        {
            shiftRepository.CloseShift(shiftId, dayId, userId);
        }
        public int getActiveDay()
        {
            return shiftRepository.GetActiveDay();
        }
        public int getCloseDay()
        {
           return shiftRepository.GetCLoseDay();
        }
        public int getActiveShift()
        {
            return shiftRepository.GetActiveShift();
        }
        public void AddShiftDetail()
        {
            shiftRepository.AddShiftdetail();
        }
        public string checkShiftButtonVisibility()
        {
        
                int activeShift = getActiveShift();
                if (activeShift > 0)
                {
                    return "0";//if any shif has open then visible close button on view
                }
                else
                {
                    return "1";
                }
         
          
        }
    }
}