﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VehicleQueueApp.Models;
using VehicleQueueApp.Models.OperationModel;

namespace VehicleQueueApp.Services
{
    public class OperationServices
    {
        public OperationRepository operationrepository = new OperationRepository();
        public void deleteOpeartion(int id)
        {
            operationrepository.deleteOpeartion(id);

        }

        public List<Shift> getLiveAmountRecordsByDay()
        {
            return operationrepository.getLiveAmountRecordsByDay();
        }
        public Shift getLiveAmountRecordsByShift()
        {
            return operationrepository.getLiveAmountRecordsByShift();
        }
        public Operation getOpeartionbyId(int id)
        {
            Operation Opeartion = operationrepository.getOpeartion(id);
            return Opeartion;
        }
        public IEnumerable<Operation> GetOperations()
        {
            return operationrepository.GetOpeartions();
        }
        public void insertOperation(Operation operation)
        {
            operationrepository.insertOpeartion(operation);
        }
        public decimal? getLiveTokenAmountByDay()
        {
            return operationrepository.getLiveTokenAmountByDay();
        }
        public void updateOperation(Operation operation)
        {
            operationrepository.updateOperation(operation);
        }
        public void StartOperation(int userId, int shiftId)
        {
            operationrepository.StartOpeartion(userId, shiftId);
        }
        public int GetActiveOperation()
        {
            return operationrepository.GetActiveOperation();
        }
        public IEnumerable<Operation> getOpeartionByUserId(int userId)
        {
            return operationrepository.getOpeartionByUserId(userId);
        }

        public decimal? getLiveTokenAmountperUser(int userId, int operationId)
        {
            return operationrepository.getLiveTokenAmountperUser(userId, operationId);
        }
        public decimal? getLiveTokenAmountByOperation(int userId, int shiftId)
        {
            return operationrepository.getLiveTokenAmountByOperation(userId, shiftId);
        }
        public int getLiveOpeartionId(int userId, int shiftId)
        {
            return operationrepository.getLiveOpeartionId(userId, shiftId);
        }
        public void CloseOperation(int userId)
        {
            operationrepository.CloseOperation(userId);

        }
        public decimal? getLiveTokenAmountByShift()
        {
            return operationrepository.getLiveTokenAmountByShift();
        }
    }
}