﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VehicleQueueApp.Models;

namespace VehicleQueueApp.Services
{
    public class BankChallanServices
    {
        BankChallanRepository bankChallanRepository = new BankChallanRepository();
        public bool AddChallan(int bankId)
        {
            bool isBankChallanAdded = bankChallanRepository.AddChallan(bankId);
            return isBankChallanAdded;
        }
        public List<ReportVM> getPdfreportOfBankChallan(MainVM mainvm)

        {
            return bankChallanRepository.getPdfreportOfBankChallan(mainvm);
        }
        public List<ReportVM> getPdfreportByBankChallanId(int BankchallanId)
        {
            return bankChallanRepository.getPdfreportByBankChallanId(BankchallanId);
        }

        public bool SubmittedbankChallan(int challanId, string fileName)
        {
            return bankChallanRepository.SubmittedbankChallan(challanId, fileName);

        }
        public List<BankChallan> GetAllBankChallans()
        {
            return bankChallanRepository.GetAllBankChallans();
        }
        public List<BankChallanDetail> GetChallanDetailByID(int bankchallanid)
        {
            return bankChallanRepository.GetChallanDetailByID(bankchallanid);
        }
        public MainVM GetAllBankChallanByVM()
        {
            return bankChallanRepository.GetAllBankChallanByVM();
        }
        public bool ChallanDepositToBank(int? id, bool value)
        {
            return bankChallanRepository.ChallanDepositToBank(id, value);
        }
    }
}