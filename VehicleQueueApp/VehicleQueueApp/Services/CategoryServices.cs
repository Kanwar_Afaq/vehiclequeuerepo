﻿using System.Collections.Generic;
using System.Linq;
using VehicleQueueApp.Models;

namespace VehicleQueueApp.Services
{
    public class CategoryServices 
    {
        public CategoriesRepository categoryRepository = new CategoriesRepository();
        //VehicleManagementEntities _context=new VehicleManagementEntities();
        //public CategoryServices(VehicleManagementEntities _context) : base(_context)
        //{
        //   // this._context = _context;
        //}
        public  void deleteCategory(int id)
        {
            categoryRepository.deleteCategory(id);

        }
        public void deleteCategoryFeeId(int id)
        {
            categoryRepository.deleteCategoryFee(id);
        }
        public  void Dispose()
        {

        }

        public  Category getCategorybyId(int id)
        {
            Category cat = categoryRepository.getCategorybyId(id);
            return cat;
        }
        public CategoryFee getCategoryfeebyId(int id)
        {
            CategoryFee catFee = categoryRepository.getCategoryFeebyId(id);
            return catFee;
        }

        public CategoryFee getCategoryfeebyCatId(int id)
        {
            CategoryFee catFee = categoryRepository.getCategoryfeebyCatId(id);
            return catFee;
        }

        public  IEnumerable<Category> GetCategorys()
        {
            return categoryRepository.GetCategorys().ToList();
        }

        public  void insertCategory(Category Category)
        {
            categoryRepository.insertCategory(Category);
        }

        public void insertCategoryFee(CategoryFee categoryFee)
        {
            categoryRepository.insertCategoryFee(categoryFee);
        }

        public  void updateCategory(Category Category)
        {
            categoryRepository.updateCategory(Category);
        }
        public void updateCategoryFee(CategoryFee CategoryFee)
        {
            categoryRepository.updateCategoryFee(CategoryFee);
        }
        public IEnumerable<CategoryFee> GetCategoryFee()
        {
            return categoryRepository.GetCategoryFee();
        }
    }
}