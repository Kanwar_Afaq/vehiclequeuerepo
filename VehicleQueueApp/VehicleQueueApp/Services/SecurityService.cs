﻿using VehicleQueueApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace VehicleQueueApp.Services
{
	public class SecurityService
	{
		public SecurityRepository _rep = new SecurityRepository();



		public AppRolesModule VerifyUserAccess(string controllerName, string IndexPage, string Param)
		{
			IEnumerable<AppRolesModule> _data = _rep.VerifyUserAccess(controllerName);

            if (_data != null && _data.Count() > 1)
            {

                //*******************Sub Vendor Condition*****************************///
                if (!string.IsNullOrEmpty(Param))
                {
                    var query = _data.Where(e => !string.IsNullOrEmpty(e.AppModule.ModuleIndex)
                        && e.AppModule.ModuleParam != null && e.AppModule.ModuleParam.ToLower() == Param.ToLower());

                    if (query.Count() > 0)
                    {
                        _data = query;
                        return _data.FirstOrDefault();
                    }
                }
                //*******************Sub Vendor Condition*****************************///





                if (!string.IsNullOrEmpty(IndexPage) && IndexPage.ToLower() != "index")
                {
                    var query =_data.Where(e => !string.IsNullOrEmpty(e.AppModule.ModuleIndex) && e.AppModule.ModuleIndex.ToLower() == IndexPage.ToLower());
                    if (query.Count() > 0)
                    {
                        _data = query;
                        return _data.FirstOrDefault();
                    }
                    else
                    {
                       
                        //do somethine here. if any issue raise
                    }
                }




               if (!string.IsNullOrEmpty(Param))
                {
                    _data = _data.Where(e => !string.IsNullOrEmpty(e.AppModule.ModuleParam) && e.AppModule.ModuleParam.ToLower() == Param.ToLower());
                    //_data = _data.Where(e => !string.IsNullOrEmpty(e.AppModule.ModuleIndex) && e.AppModule.ModuleIndex.ToLower() == IndexPage.ToLower());
                }

            }

			return _data.FirstOrDefault();
		}

		public IEnumerable<AppModule> getUserModules()
		{
			return _rep.getUserModules();
		}



		#region Module Methods

		public List<AppModule> GetAllModules()
		{
			return _rep.GetAllModules();
		}
		public AppModule GetModule(int Id)
		{
			return _rep.GetModule(Id);
		}

		public List<AppModuleConfig> getAppModuleConfig()
		{
			return _rep.GetAppModuleConfig();
		}

		public AppModuleConfig getAppModuleConfig(int Id)
		{
			return _rep.GetAppModuleConfig(Id);
		}

		public bool ConfigEdit(int Id, AppModuleConfig getDATA, out string error)
		{
			return _rep.ConfigEdit(Id, getDATA, out error);
		}



		public bool isModuleExist(string ModuleName, int ModuleID)
		{
			return (_rep.isModuleExist(ModuleName, ModuleID)) ? false : true;
		}
		public bool CreateModule(AppModule getDATA, out string error)
		{

			//getDATA.Custom1Label = (getDATA.HasCustom1) ? getDATA.Custom1Label : "";
			//getDATA.Custom2Label = (getDATA.HasCustom2) ? getDATA.Custom2Label : "";
			//getDATA.Custom3Label = (getDATA.HasCustom3) ? getDATA.Custom3Label : "";
			//getDATA.Custom4Label = (getDATA.HasCustom4) ? getDATA.Custom4Label : "";
			//getDATA.Custom5Label = (getDATA.HasCustom5) ? getDATA.Custom5Label : "";
			//getDATA.Custom6Label = (getDATA.HasCustom6) ? getDATA.Custom6Label : "";

			return _rep.CreateModule(getDATA, out error);
		}
		public bool EditModule(int Id, AppModule getDATA, out string error)
		{
			//getDATA.Custom1Label = (getDATA.HasCustom1) ? getDATA.Custom1Label : "";
			//getDATA.Custom2Label = (getDATA.HasCustom2) ? getDATA.Custom2Label : "";
			//getDATA.Custom3Label = (getDATA.HasCustom3) ? getDATA.Custom3Label : "";
			//getDATA.Custom4Label = (getDATA.HasCustom4) ? getDATA.Custom4Label : "";
			//getDATA.Custom5Label = (getDATA.HasCustom5) ? getDATA.Custom5Label : "";
			//getDATA.Custom6Label = (getDATA.HasCustom6) ? getDATA.Custom6Label : "";


			return _rep.EditModule(Id, getDATA, out error);
		}
		public bool DeleteModule(int Id, out string error)
		{
			return _rep.DeleteModule(Id, out error);
		}


		#endregion


		#region Group Methods


		public IEnumerable<AppRole> GetAllGroups()
		{
			return _rep.AppGroups();
		}
		public AppRole getAppGroup(int id)
		{
			return _rep.getAppGroup(id);
		}




		public bool CreateGroup(AppRole getDATA, out string error)
		{
			return _rep.CreateGroup(getDATA, out error);
		}
		public bool EditGroup(int Id, AppRole getDATA, out string error)
		{
			return _rep.EditGroup(Id, getDATA, out error);
		}
		public bool DeleteGroup(int Id, out string error)
		{
			return _rep.DeleteGroup(Id, out error);
		}



		public AppRole ProcessGroupData(AppRole appgroup, FormCollection fm, out List<AppModule> outPutAllmodules)
		{
			List<AppModule> getAllmodules = GetAllModules();
			List<AppModule> setAllmodules = new List<AppModule>();

			appgroup.Priority = 20;

			//ICollection<AppGroupModule> selModules = appgroup.AppGroupModules;
			List<AppRolesModule> selModules = new List<AppRolesModule>();
			appgroup.AppRolesModules.Clear();

			int i = 0;
			foreach (var item in getAllmodules)
			{
				List<ModuleRights> AllRights = new List<ModuleRights>();

				string permission = string.Empty;
				int j = 0;
				foreach (var subitem in getAllmodules[i].permissionlists)
				{
					string separator = !string.IsNullOrEmpty(permission) ? ", " : "";
					string RightsValue = (fm["SelectedModules[" + i + "].Rights[" + j + "]." + subitem.permissionname] != null) ? "true" : "false";
					permission += separator + subitem.permissionname + ":" + RightsValue;



				    ModuleRights Rights = new ModuleRights();
                    Rights.Name = subitem.permissionname;
                    
					Rights.isChecked = (RightsValue == "true") ? " checked=\"checked\"" : "";

					AllRights.Add(Rights);
					j++;
				}

				bool isAssignedtoGroup = (fm["SelectedModules[" + i + "].ModuleID"] != null) ? true : false;


				AppModule Module = new AppModule();
				Module = item;
				Module.isGroupModuleAsigned = isAssignedtoGroup;
				Module.RightsModified.AddRange(AllRights);
				setAllmodules.Add(Module);



				AppRolesModule groupModule = new AppRolesModule();
				groupModule.RolesId = appgroup.RoleId;
				groupModule.ModuleId = item.ModuleID;
				groupModule.IsAllowed = isAssignedtoGroup;
				groupModule.Permissions = permission;

				selModules.Add(groupModule);

				i++;
			}
			appgroup.AppRolesModules = selModules;


			outPutAllmodules = setAllmodules;
			return appgroup;
		}
		public List<AppModule> EditGroupData(AppRole getData)
		{
			List<AppModule> getAllmodules = GetAllModules();
			List<AppModule> setAllmodules = new List<AppModule>();


			int i = 0;
			foreach (var item in getAllmodules)
			{
				List<ModuleRights> AllRights = new List<ModuleRights>();

				var getPermission = getData.AppRolesModules.FirstOrDefault(e => e.ModuleId == item.ModuleID);

				var AccessRights = VerifyPermission(getPermission);



				foreach (var subitem in getAllmodules[i].permissionlists)
				{
		//			ModuleRights Rights = subitem;
                    ModuleRights Rights = new ModuleRights();
                    Rights.Name = subitem.permissionname;
					Rights.isChecked = AccessRights.Where(e => e.Item1 == subitem.permissionname && e.Item2 == "true").Any() ? " checked=\"checked\"" : "";
					AllRights.Add(Rights);
				}

				var GrpAllowd = getData.AppRolesModules.FirstOrDefault(e => e.ModuleId == item.ModuleID);
				AppModule Module = new AppModule();
				Module = item;
				Module.isGroupModuleAsigned = (GrpAllowd != null) ? GrpAllowd.IsAllowed : false;
				Module.RightsModified.AddRange(AllRights);
				setAllmodules.Add(Module);


				i++;
			}

			return setAllmodules;
		}


		public List<Tuple<string, string>> VerifyPermission(AppRolesModule getPermission, AppModule getModuleDetail = null)
		{

			var AccessRights = new List<Tuple<string, string>>();

			if (getPermission != null)
			{
				string permissionData = getPermission.Permissions;
				string[] permissionlist = permissionData.Split(", ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

				foreach (var t in permissionlist)
				{
					var name = t.Split(":".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
					if (name.Length > 1)
					{
						var tx = Tuple.Create(name[0], name[1]);
						AccessRights.Add(tx);
					}
				}
			}
			if (getModuleDetail != null)
			{
				var ModuleIndex = getModuleDetail.ModuleIndex != null ? getModuleDetail.ModuleIndex : "Index";
				var ModuleParam = getModuleDetail.ModuleParam != null ? getModuleDetail.ModuleParam : "";

				var tx1 = Tuple.Create(ModuleIndex + ModuleParam, "true");
				AccessRights.Add(tx1);
			}

			return AccessRights;
		}

		#endregion

	}
}