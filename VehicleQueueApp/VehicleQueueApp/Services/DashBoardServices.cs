﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VehicleQueueApp.Models;


namespace VehicleQueueApp
{
    public class DashBoardServices
    {
        DashBoardRepository dashBoardRepository = new DashBoardRepository();
        public decimal getTotaltokensGeneratedToday(int dayId)
        {
            return dashBoardRepository.getTotaltokensGeneratedToday(dayId);
        }
        public decimal getTodaysNonCommercialIncome(int dayId)
        {
            return dashBoardRepository.getTodaysNonCommercialIncome(dayId);
        }
        public List<TotalCollectionVM> categoryFee()
        {
            return dashBoardRepository.CategoryFee();
        }
        public List<TotalCollectionVM> CategoryFeeByOperation()
        {

            return dashBoardRepository.CategoryFeeByOperation();
        }
        public List<TotalCollectionVM> CategoryFeeCollection()
        {
            return dashBoardRepository.CategoryFeeCollection();
        }
        public List<Category> getCategories()
        {
            return dashBoardRepository.getCategories();
        }
        public decimal getTotaltodaysIncome(int dayId)
        {
            return dashBoardRepository.getTotaltodaysIncome(dayId);
        }
        public decimal getTodaysIndividualIncome(int dayId)
        {
            return dashBoardRepository.getTodaysIndividualIncome(dayId);
        }
        public decimal getTodaysTruckIncome(int dayId)
        {
            return dashBoardRepository.getTodaysTruckIncome(dayId);
        }
        public decimal getTodaysCarIncome(int dayId)
        {
            return dashBoardRepository.getTodaysCarIncome(dayId);
        }
        public decimal getTodaysMotorCycleIncome(int dayId)
        {
            return dashBoardRepository.getTodaysMotorCycleIncome(dayId);
        }
        public decimal getTokenCancelAmount(int dayId)
        {
            return dashBoardRepository.getTokenCancelAmount(dayId);
        }
    }
}