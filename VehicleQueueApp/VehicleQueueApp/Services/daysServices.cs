﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VehicleQueueApp.Models;

namespace VehicleQueueApp.Services
{
    public class daysServices
    {
        public DaysRepository daysRepository = new DaysRepository();
        public void deleteDay(int id)
        {
            daysRepository.deleteDay(id);

        }
        public Day getDaybyId(int id)
        {
            Day day = daysRepository.getDaybyId(id);
            return day;
        }
        public IEnumerable<Day> GetDays()
        {
            return daysRepository.GetDays();
        }
        public void insertDay(Day day)
        {
            daysRepository.insertDay(day);
        }
        public void updateDay(Day day)
        {
            daysRepository.updateDay(day);
        }
        public void StartDay(int userId)
        {
            daysRepository.StartDay(userId);
        }
        public void CLoseDay(int dayId, int userId,int shiftId)
        {
            daysRepository.CloseDay(dayId,userId,shiftId);
        }
        public int getActiveDay()
        {
           return daysRepository.GetActiveDay();
        }
        public int getActiveShift()
        {
            return daysRepository.GetActiveShift();
        }
    }
}