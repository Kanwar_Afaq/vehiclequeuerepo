﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VehicleQueueApp.Models;

namespace VehicleQueueApp.Services
{
    public class TokenServices
    {
        public TokenRepository tokenRepository = new TokenRepository();
        public void deleteToken(int id)
        {
            tokenRepository.deleteToken(id);

        }
        public bool AmountRefund(int tokenCancelreqID)
        {
            return tokenRepository.AmountRefund(tokenCancelreqID);
        }
        public List<TokenCancelRequest> gettokenCancelrequest(int userId)
        {
           return tokenRepository.gettokenCancelrequest(userId);
        }
        public bool tokenStatus(int cancelRequestId, string tokenstatus)
        {
           bool isCancel= tokenRepository.tokenStatus(cancelRequestId, tokenstatus);
            return isCancel;
        }
        public List<TokenCancelRequestVM> tokenCancelRequest(int tokenRequestId)
        {
            return tokenRepository.tokenCancelRequest(tokenRequestId);
        }

        public List<TokenCancelRequest> TokenCanceledForManager()
        {
            return tokenRepository.TokenCanceledForManager();
        }

        public bool CancelToken(string tokenNo, List<tokenDetails> tokendetailss)
        {
           var c= tokenRepository.CancelToken(tokenNo, tokendetailss);
            return c;
        }
        public string getIndividualNarration()
        {
            return tokenRepository.getIndividualNarration();
        }
        public List<Token> TokenGenerated(int tokenId)
        {
            return tokenRepository.generateToken(tokenId);
        }
        public List<TokenDetail> getTokenDetails(int tokenid)
        {
            List<TokenDetail>detail = tokenRepository.getTokenDetails(tokenid);
            return detail;

        }
        public Token getTokenbyId(int id)
        {
            Token Token = tokenRepository.GetTokenById(id);
            return Token;
        }
        public IEnumerable<Token> GetTokens()
        {
            return tokenRepository.GetAllTokens(); ;
        }
        public int AddToken(int operationId,int shiftid, int dayid, Token tokenobj,List<tokenDetails> tokendetailsobj)
        {
            int tokenId=tokenRepository.AddToken(operationId,shiftid, dayid, tokenobj,tokendetailsobj);
            return tokenId;
        }
        public List<Token> getTokenByShiftId(int shfitId)
        {
            List<Token> token = tokenRepository.getTokenByShiftId(shfitId);
            return token;
        }
        
        public IEnumerable<Category>GetAllCategories()
        {
            var category = tokenRepository.getAllCategory();
            return category;
        }

        public List<PlaceOfVisit> GetAllPlacesofVisit()
        {
            var placeofvisit = tokenRepository.GetAllPlacesofVisit();
            return placeofvisit;
        }
        public List<PurposeOfVisit> GetAllPurposeofVisit()
        {
            var purposeofvisit = tokenRepository.GetAllPurposeofVisit();
            return purposeofvisit;
        }
        public bool compareDateTime(string tokenNo)
        {
            return tokenRepository.compareDateTime(tokenNo);
        }
        public List<tokenCancelDetailsVM> tokenCancel(string tokenNo, int shiftId)
        {
            var tokendetails = tokenRepository.tokenCancel(tokenNo, shiftId);
            return tokendetails;
        }
        public int? getNoOfPassenger(string TokenNo)
        {
            return tokenRepository.getNoOfPassenger(TokenNo);
        }
        public int getTimeOutoftokenCancellation()
        {
            return tokenRepository.getTimeOutoftokenCancellation();
        }




        }
}