﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using VehicleQueueApp.Filters;
using VehicleQueueApp.Models;

using VehicleQueueApp.Services;

namespace VehicleQueueApp.Controllers
{
    [Restricted]
    public class CategoryController : Controller
    {
      
      
        public CategoryServices catServices = new CategoryServices();
        // GET: Category
        public ActionResult Index()
        {
            var category= catServices.GetCategorys().ToList();
            return View(category);
            
        }
        public ActionResult Edit(int id)
        {
            Category cat = catServices.getCategorybyId(id);
          
            return View(cat);
        }
        [HttpPost]
        public ActionResult Edit(Category cat)
        {

            catServices.updateCategory(cat);
           
            return RedirectToAction("Index");
        }
        public ActionResult EditCategoryFee(int id)
        {
            var categoryFees = catServices.getCategoryfeebyId(id);
            ViewBag.Departments = new SelectList(catServices.GetCategorys(), "CategoryId", "CategoryName",categoryFees.CategoryId);
            CategoryFee catFee = catServices.getCategoryfeebyId(id);
            return View(catFee);
        }
        [HttpPost]
        public ActionResult EditCategoryFee(int id,CategoryFee categoryFee)
        {
          
            if (ModelState.IsValid)
            {
                catServices.updateCategoryFee(categoryFee);
              
                    TempData["OutputMessage"] = "Record Successfully Update";

              
                    return RedirectToAction("CategoryFees");
            }
            var categoryFees = catServices.getCategoryfeebyId(id);
            ViewBag.Departments = new SelectList(catServices.GetCategorys(), "CategoryId", "CategoryName", categoryFees.CategoryId);

            return View();
        }
        public ActionResult Details(int id)
        {
            Category cat = catServices.getCategorybyId(id);
            return View("getCategoryDetails", cat);
        }
        public ActionResult Delete(int id)
        {
            Category cat = catServices.getCategorybyId(id);
            return View(cat);
        }
        [HttpPost,ActionName("Delete")]
        public ActionResult DeleteCategory(int id)
        {
            catServices.deleteCategory(id);
        
            return RedirectToAction("Index");
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Category cat)
        {
            catServices.insertCategory(cat);
          

            return RedirectToAction("Index");
        }
        public ActionResult CategoryFees()
        {
            var categoryFee = catServices.GetCategoryFee().ToList();
            return View(categoryFee);
         
        }
        [HttpGet]
        public ActionResult deleteCategoryFee(int id)
        {
          
            CategoryFee catFee = catServices.getCategoryfeebyId(id);
            return View(catFee);
        }
        [HttpPost,ActionName("deleteCategoryFee")]
        public ActionResult deleteCategoryFeeConfirmed(int id)
        {
            catServices.deleteCategoryFeeId(id);
            return RedirectToAction("CategoryFees");
        }
     
        public ActionResult CreateCategoryFees()
        {
            ViewBag.Departments = new SelectList(catServices.GetCategorys(), "CategoryId", "CategoryName");
            return View();
        }

        [HttpPost]
        public ActionResult CreateCategoryFees(CategoryFee categoryfees)
        {

            if (ModelState.IsValid)
            {
                catServices.insertCategoryFee(categoryfees);

                TempData["OutputMessage"] = "Record Successfully Created";


                return RedirectToAction("CategoryFees");
            }
          
            ViewBag.Departments = new SelectList(catServices.GetCategorys(), "CategoryId", "CategoryName");

            return View("CreateCategoryFees");
        }


    }
}