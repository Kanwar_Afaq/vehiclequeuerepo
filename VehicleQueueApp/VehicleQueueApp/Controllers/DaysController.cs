﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VehicleQueueApp.Filters;
using VehicleQueueApp.Helpers;
using VehicleQueueApp.Models;
using VehicleQueueApp.Services;

namespace VehicleQueueApp.Controllers
{
 
    [Restricted]  
    public class DaysController : Controller
    {
        public daysServices daysServices = new daysServices();
        // GET: Days
      
        public ActionResult Days()
        {
            ViewBag.UserName = Auth.UserName;
            ViewBag.issidebar = true;
            return View();
        }
        public ActionResult AllDays()
        {
              ViewBag.UserName = Auth.UserName;
            ViewBag.issidebar = true;
            var daysList = daysServices.GetDays().OrderByDescending(q=>q.DayId);
            return View(daysList);
        }
        public ActionResult Edit(int id)
        {
            Day day = daysServices.getDaybyId(id);

            return View(day);
        }
        [HttpPost]
        public ActionResult Edit(Day day)
        {

            daysServices.updateDay(day);

            return RedirectToAction("Index");
        }
        public ActionResult Details(int id)
        {
            Day day = daysServices.getDaybyId(id);
            return View(day);
        }
        public ActionResult Delete(int id)
        {
            Day day = daysServices.getDaybyId(id);
            return View(day);
        }
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteDay(int id)
        {
            daysServices.deleteDay(id);

            return RedirectToAction("Index");
        }
        public ActionResult Create()
        {



            return View();
        }
        [HttpPost]
        public ActionResult Create(Day day)
        {
            daysServices.insertDay(day);


            return RedirectToAction("Index");
        }
      
    }
}