﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VehicleQueueApp.Filters;
using VehicleQueueApp.Helpers;
using VehicleQueueApp.Models;
using VehicleQueueApp.Services;

namespace VehicleQueueApp.Controllers
{
    [Restricted]

    public class TokenController : Controller
    {
        TokenServices tokenServices = new TokenServices();
        ShiftServices ShiftServices = new ShiftServices();
        CategoryServices categoryServices = new CategoryServices();
        OperationServices operationServices = new OperationServices();
        DashBoardServices DashBoardServices = new DashBoardServices();
        UserService _serv = new UserService();
        // GET: Token
        public ActionResult Index()
        {            
            ViewBag.issidebar = true;
            ViewBag.UserName = Auth.UserName;
            ViewBag.UserId = Auth.UserID;
            ViewBag.PlaceVisit = new SelectList(tokenServices.GetAllPlacesofVisit(), "PlaceOfVisitId", "PlaceOfVisit1");
            ViewBag.Purposeofvisit = new SelectList(tokenServices.GetAllPurposeofVisit(), "PurposeOfVisitId", "PurposeOfVIsit1");
           
            int ActiveDay = ShiftServices.getActiveDay();
            int ActiveShift = ShiftServices.getActiveShift();
            int activeOperation = operationServices.GetActiveOperation();
           
            ViewBag.getAllCategory = tokenServices.GetAllCategories();
            ViewBag.livetokenAmount = operationServices.getLiveTokenAmountperUser(Auth.UserID, activeOperation);

            ViewBag.issidebar = false;
            if (activeOperation == 0)//redirect karnay k bad ye idhar wapis ayega :)
            {
                ViewBag.operationButonVisible = false;
                ViewBag.lowerDiv = false;
            }
            else
            {
                ViewBag.operationButonVisible = true;
                ViewBag.lowerDiv = true;
            }
            if (ActiveDay == 0)
            {

                ViewBag.Message = "No Active Day";

            }
            else if (ActiveShift == 0)
            {
                ViewBag.Message = "No Active Shift";
            }
            else
            {
                ViewBag.Category = new SelectList(categoryServices.GetCategorys(), "CategoryId", "CategoryName");
            }
            return View();
        }
        [HttpPost]
        public ActionResult Index(Token token , List<tokenDetails> tokenDetails)
        {

            int ActiveDay = ShiftServices.getActiveDay();
            int ActiveShift = ShiftServices.getActiveShift();
            int activeOperation = operationServices.GetActiveOperation();
            if (ModelState.IsValid)
            {
                int tokenid = tokenServices.AddToken(activeOperation, ActiveShift, ActiveDay, token, tokenDetails);
                return RedirectToAction("generateToken", new { Id = tokenid });
            }
            ViewBag.getAllCategory = tokenServices.GetAllCategories();
            ViewBag.PlaceVisit = new SelectList(tokenServices.GetAllPlacesofVisit(), "PlaceOfVisitId", "PlaceOfVisit1");
            ViewBag.Purposeofvisit = new SelectList(tokenServices.GetAllPurposeofVisit(), "PurposeOfVisitId", "PurposeOfVIsit1");
            return View();
        }
        //[HttpPost]
        //public JsonResult CNIC(string nic)
        //{
        //    string cnic="";
           
        //        if (nic != "" && cnic!=null)
        //        {
        //        string trimnic = nic.TrimStart('\r', '\n');
        //            cnic = trimnic.Substring(100, 50);
        //        }
        //    return Json(cnic, JsonRequestBehavior.AllowGet);
        //}
          

             
            
      

        public ActionResult MyOperations()
        {
            int userId = Auth.UserID;
            ViewBag.issidebar = false;
            ViewBag.UserName = Auth.UserName;
            var operation = operationServices.getOpeartionByUserId(userId);
            return View(operation.OrderByDescending(q => q.CreatedDate));
        }
        public ActionResult TokenAmount()
        {
            ViewBag.UserName = Auth.UserName;
            int activeShiftId = ShiftServices.getActiveShift();
            var finalAmount = operationServices.getLiveTokenAmountByOperation(Auth.UserID, activeShiftId);
            
            ViewBag.categoryFeeByOperation = DashBoardServices.CategoryFeeByOperation();
            ViewBag.FinalAmount = finalAmount;
            return View();
        }
        public ActionResult GetTokenDetail(int? tokenid)
        {
            ViewBag.issidebar = true;
            ViewBag.UserName = Auth.UserName;
            var tokens = tokenServices.getTokenDetails(Convert.ToInt32(tokenid));
            return View(tokens);
        }
        public ActionResult AllTokens(int? operationId)
        {
            ViewBag.issidebar = true;
            ViewBag.UserName = Auth.UserName;
            var alltokens = tokenServices.GetTokens();
            if (operationId > 0)
            {
                alltokens = alltokens.Where(token => token.OperationId == operationId);
            }

            return View(alltokens.OrderByDescending(t => t.TokenId));
        }



        //public JsonResult GetFees(int pass)
        //{
        //    var c = categoryServices.getCategoryfeebyCatId(pass);

        //    return Json(c.Fees, JsonRequestBehavior.AllowGet);
        //}
        public ActionResult generateToken(int Id)
        {
            ViewBag.UserName = Auth.UserName;
            ViewBag.IndividualNarration = tokenServices.getIndividualNarration();
            var token = tokenServices.TokenGenerated(Id);
            return View(token);

        }
        public JsonResult nic(string nic)
        {
            return Json(1, JsonRequestBehavior.AllowGet);
        }
        public ActionResult TokenCancel()
        {
            ViewBag.UserName = Auth.UserName;
            ViewBag.issidebar = false;
            int activeOperation = operationServices.GetActiveOperation();
            if (activeOperation== 0)
            {
                ViewBag.Message = "1";
                var operation = new List<TokenCancelRequest>();
                return View(operation);
            }
            else{
                var tokenrequest = tokenServices.gettokenCancelrequest(Auth.UserID);
                ViewBag.to = tokenServices.getTimeOutoftokenCancellation();
                return View(tokenrequest.OrderByDescending(q => q.TokenCancelRequestId));
            }
            
        }

        [HttpPost]
        public ActionResult TokenCancel(string TokenNo)
        {

            int shiftID = ShiftServices.getActiveShift();
            var tokenExpiry = tokenServices.compareDateTime(TokenNo);
            if (tokenExpiry == false)
            {
                return Json(1, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var cancelToken = tokenServices.tokenCancel(TokenNo, shiftID);
          

                return Json(cancelToken, JsonRequestBehavior.AllowGet);

            }
            //  ViewBag.tokenCount = tokenServices.tokenCancel(TokenNo).Count();



        }

     
        [HttpPost]
        public ActionResult Approval(string tokenNo, List<tokenDetails> tokenDetails)
        {
            var tokencancel = tokenServices.CancelToken(tokenNo, tokenDetails);
            return RedirectToAction("TokenCancel", "Token");

        }

        [HttpPost]
        public ActionResult StartOperation()
        {
            int userid = Auth.UserID;
            int shiftid = ShiftServices.getActiveShift();
            operationServices.StartOperation(userid, shiftid);
            return RedirectToAction("Index", "Token", new { isOperation = false });

        }
        [HttpPost]
        public ActionResult CloseOperation()
        {
           
            int userid = Auth.UserID;
            operationServices.CloseOperation(userid);
            return Json(1, JsonRequestBehavior.AllowGet);
            // return RedirectToAction("TokenAmount", "Token");

        }

        public ActionResult TokenCancellationManager()
        {
            ViewBag.UserName = Auth.UserName;
            ViewBag.issidebar = true;
            var tokencancelRequest = tokenServices.TokenCanceledForManager();
            return View(tokencancelRequest);
        }
       public JsonResult ApproveCancellationRequest(int tokenCancelrequestID, string tokenstatus)
        {
            var tokenrequestDetails = tokenServices.tokenCancelRequest(tokenCancelrequestID);
            return Json(tokenrequestDetails.OrderByDescending(q => q.tokenCancelrequestID), JsonRequestBehavior.AllowGet);
        }
        public ActionResult CancelledStatus(int tokenCancelrequestID, string tokenStatus)
        {
            bool tokenCancelRequest = tokenServices.tokenStatus(tokenCancelrequestID, tokenStatus);
            return RedirectToAction("TokenCancellationManager", "Token");
        }
        public ActionResult TokenRefundAmount(int tokenRequestId)
        {
            tokenServices.AmountRefund(tokenRequestId);
            return RedirectToAction("TokenCancel", "Token");
        }

    }
}