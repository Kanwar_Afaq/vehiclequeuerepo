﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VehicleQueueApp.Filters;
using VehicleQueueApp.Helpers;
using VehicleQueueApp.Models;
using VehicleQueueApp.Services;

namespace VehicleQueueApp.Controllers
{
    [Restricted]
    public class HomeController : Controller
    {
        public ShiftServices shiftServices = new ShiftServices();
        public daysServices daysServices = new daysServices();
        public OperationServices operationServices = new OperationServices();
        TokenServices tokenServices = new TokenServices();
        DashBoardServices DashBoardServices = new DashBoardServices();
        BankChallanServices BankChallanServices = new BankChallanServices();
        UserService _serv = new UserService();
        public ActionResult DayStatus()
        {
            int Activeday = shiftServices.getActiveDay();
            if (Activeday == 0)  // irrepsective first time arahah hai ya second time araha hai ya thirs araha
            {
                ViewBag.isDayOpen = true;
            }
            else
            {
                ViewBag.isDayOpen = false;
            }
            ViewBag.ShiftStatus = shiftServices.checkShiftButtonVisibility();
            return PartialView("DayStatus");
        }



     
        public ActionResult Restricted()
        {
            ViewBag.UserName = Auth.UserName;
            if (Auth.RoleId == 42)
            {
                ViewBag.issidebar = false;
            }
            else
            {
                ViewBag.issidebar = true;
            }
            return View();
        }
        public ActionResult Index()
        {    
            
   
            //if(Auth.RoleId==2)
            //{ 
                ViewBag.UserName = Auth.UserName;
                ViewBag.issidebar = true;
                ViewBag.ShiftStatus = shiftServices.checkShiftButtonVisibility();
                int Activeday = shiftServices.getActiveDay();
                if (Activeday == 0)  // irrepsective first time arahah hai ya second time araha hai ya thirs araha
                {
                    ViewBag.isDayOpen = true;
                  
                }
                else
                {
                   
                    ViewBag.TotalTokens = DashBoardServices.getTotaltokensGeneratedToday(Activeday);
                    ViewBag.TotalIncome = DashBoardServices.getTotaltodaysIncome(Activeday);
                    ViewBag.TotalIndividualIncome = DashBoardServices.getTodaysIndividualIncome(Activeday);
                    ViewBag.TotalTruckIncome = DashBoardServices.getTodaysTruckIncome(Activeday);
                    ViewBag.TotalCarIncome = DashBoardServices.getTodaysCarIncome(Activeday);
                    ViewBag.TotalMotorCycleIncome = DashBoardServices.getTodaysMotorCycleIncome(Activeday);
                    ViewBag.TotalTokenCancelAmount = DashBoardServices.getTokenCancelAmount(Activeday);
                    ViewBag.TotalNonCommercialAmount = DashBoardServices.getTodaysNonCommercialIncome(Activeday);
                    ViewBag.categoryFee = DashBoardServices.categoryFee();
                    //ViewBag.categories = DashBoardServices.getCategories();
                    ViewBag.isDayOpen = false;
                }
          //  }
            return View();
        }
        public JsonResult CategoryFee()
        {
            var categoryFee = DashBoardServices.categoryFee();
            return Json(categoryFee, JsonRequestBehavior.AllowGet);

        }
        public JsonResult CategoryFeeCollection()
        {
            var CollectionStats = DashBoardServices.CategoryFeeCollection();
            return Json(CollectionStats, JsonRequestBehavior.AllowGet);

        }

        public ActionResult Days()
        {
            return View();
        }
        public ActionResult StartShift()
        {
            int userid = Auth.UserID;

            int activeDay = shiftServices.getActiveDay();
            shiftServices.StartShift(userid, activeDay);
            return RedirectToAction("Index", "Home");

        }
        public ActionResult ShiftClose()
        {
            int userid = Auth.UserID;
            int dayId = shiftServices.getActiveDay();
            int shiftId = shiftServices.getActiveShift();

            shiftServices.CLoseShift(shiftId, dayId, userid);
            return RedirectToAction("TokenShiftAmount", "Home");
        }
        public ActionResult StartDay()
        {
            int userid = 1;
            daysServices.StartDay(userid);
            return RedirectToAction("Index", "Home", new { isdayClose = false });

        }
        public ActionResult CloseDay()
        {
            int userid = Auth.UserID;
            int dayId = daysServices.getActiveDay();
            int shiftId = daysServices.getActiveShift();

            daysServices.CLoseDay(dayId, userid, shiftId);
            bool isSuccess = BankChallanServices.AddChallan(2);
            return RedirectToAction("TokenDayAmount", "Home", new { isdayClose = true });
        }
        public ActionResult TokenDayAmount()
        {
            ViewBag.issidebar = true;
            var listOfShifts = operationServices.getLiveAmountRecordsByDay();
            ViewBag.dayAmount = operationServices.getLiveTokenAmountByDay();
 
        
            return View(listOfShifts);
        }
        public ActionResult TokenShiftAmount()
        {

            ViewBag.issidebar = true;
            ViewBag.shiftAmount = operationServices.getLiveTokenAmountByShift();


            return View();
        }
        public ActionResult bankChallan()
        {
            return View();
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

       
    
    }
}