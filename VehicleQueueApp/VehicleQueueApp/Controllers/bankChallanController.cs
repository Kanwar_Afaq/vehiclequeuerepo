﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VehicleQueueApp.Filters;
using VehicleQueueApp.Helpers;
using VehicleQueueApp.Models;
using VehicleQueueApp.Services;

namespace VehicleQueueApp.Controllers
{
    [Restricted]
    public class bankChallanController : Controller
    {
        BankChallanServices bankChallanServices = new BankChallanServices();
        ShiftServices ShiftServices = new ShiftServices();
        // GET: bankChallan
        public ActionResult Index()
        { 
            ViewBag.issidebar = true;
           
            bool isSuccess= bankChallanServices.AddChallan(2);
            return RedirectToAction("BankChallans", "BankChallan");
        }
    
        public ActionResult BankChallans()
        {
            ViewBag.UserName = Auth.UserName;
            ViewBag.issidebar = true;
            var bankChallans=bankChallanServices.GetAllBankChallans();
            ViewBag.MainVM = bankChallanServices.GetAllBankChallanByVM();
            return View(bankChallans.OrderByDescending(q=>q.BankChallanId));
        }
        public ActionResult BankChallanDetail(int? BankChallanId)
        {
            ViewBag.UserName = Auth.UserName;
            ViewBag.issidebar = true;
            var bankChallan = bankChallanServices.GetChallanDetailByID(Convert.ToInt32(BankChallanId));
           
            return View(bankChallan);
        }
        public ActionResult DepositToBank(int ChallanId, HttpPostedFileBase fileName)
        {
            //  var uploads = Path.Combine(_environment.WebRootPath, "uploads");
            //  var uploads = Path.Combine(_environment.WebRootPath, "uploads");
            var extension = Path.GetExtension(fileName.FileName);
            var filename = "bankchallan_" + Guid.NewGuid() + extension;
            var path = Path.Combine(Server.MapPath("~/Content/Upload/" + filename));


            if (fileName.ContentLength > 0)
            {
                long iFileSize = fileName.ContentLength;

                if (fileName.FileName.Contains("txt") || fileName.FileName.Contains("pdf") || fileName.FileName.Contains("doc"))
                {
                
                    string completename = filename;
                    bankChallanServices.SubmittedbankChallan(ChallanId, completename);
                    fileName.SaveAs(path);
                }


            }


            return RedirectToAction("BankChallans", "bankChallan");
        }
      
    
        public ActionResult FileUpload()
        {
         
            return View();
        }
    }
}