﻿using VehicleQueueApp.Filters;
using VehicleQueueApp.Helpers;
using VehicleQueueApp.Models;
using VehicleQueueApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Drawing.Imaging;

namespace VehicleQueueApp.Controllers
{

    public class UserController : Controller
    {
        private UserService _serv = new UserService();
        public SecurityService _servSec = new SecurityService();
        ShiftServices shiftServices = new ShiftServices();
        private string error = string.Empty;
        private bool isEmailSent = false;
        [ChildActionOnly]
        public ActionResult ThumbnailImage()
        {
            if (Auth.UserID > 0)
            {
                int userId = Auth.UserID;
                string thumbnail = _serv.GetAllEmployee(userId);
                var url = _serv.getUrls().FirstOrDefault(q => q.SystemConfigKey == "URL").SystemConfigValue;
                var directPath = _serv.getUrls().FirstOrDefault(q => q.SystemConfigKey == "ImageThumbNailPathForView").SystemConfigValue;
                var defaultImage = _serv.getUrls().FirstOrDefault(q => q.SystemConfigKey == "DefaultImage").SystemConfigValue;
                string UserProfilePic = url + directPath;
                if (thumbnail != null)
                {
                    ViewBag.Thumb = UserProfilePic + thumbnail;
                }
                else
                {
                    ViewBag.Thumb = url + defaultImage;
                }
            }
            return PartialView("ThumbnailImage");
        }

        [NotRestricted]
        public ActionResult Captcha(string prefix)
        {
            ImageCaptcha cap = new ImageCaptcha();
            var result = cap.GetCaptcha();
            Session.Add("Captcha" + prefix, result.Text);

            return new FileContentResult(result.Image, result.ContentType);
        }



        [NotRestricted]
        public ActionResult Login()
        {
            //Check if user sesssion is alive then send it to HomePage
            if (Auth.isLogin)
                return RedirectToAction("Index");

            return View(new LoginViewModel());
        }

        [NotRestricted]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel logindata, string url)
        {

            try
            {
                //Check if user sesssion is alive then send it to HomePage
                if (Auth.isLogin)
                    return RedirectToAction("Index");


                if (Session["Captcha"] != null)
                {
                    string GetCaptcha = Session["Captcha"].ToString();

                    if (string.IsNullOrEmpty(logindata.Captcha))
                        ModelState.AddModelError("_FORM", "Please verify word shown in the image.");
                    else if (!logindata.Captcha.Equals(GetCaptcha))
                        ModelState.AddModelError("_FORM", "Provided text does not match Captcha text.");

                    logindata.ShowCaptcha = true;
                    logindata.Captcha = "";
                }


                if (ModelState.IsValid)
                {
                    string error = "";
                    bool VerifyUser = _serv.CreateSession(logindata, out error);


                    if (VerifyUser)
                    {
                        Session.Remove("LoginCount");
                        Session.Remove("Captcha");

                        string RedirectURL = string.Empty;
                        if (!string.IsNullOrEmpty(url))
                            RedirectURL = EncryptDecryptHelper.Decrypt(url);

                        //if (!string.IsNullOrEmpty(RedirectURL) && RedirectURL.Contains("http://"))
                        //{
                        //    return new RedirectResult(RedirectURL);
                        //}
                        if (Auth.RoleId == 42)
                        {
                            shiftServices.AddShiftDetail();
                            return RedirectToAction("Index", "Token");
                        }
                        else if(Auth.RoleId==1)
                        {
                            return RedirectToAction("Index", "Module");
                        }
                        else
                            return RedirectToAction("Index", "Home");//Sucesfully Login
                    }
                    else
                        ModelState.AddModelError("_FORM", error);
                }

                logindata.Captcha = "";
                return View(logindata);

            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                ModelState.AddModelError("_FORM", "Something went wrong (" + ex + ")");

                return View(logindata);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("_FORM", "Error: " + ex);
                return View(logindata);

            }
            finally
            {
                if (!ModelState.IsValid)
                {
                    if (Session["LoginCount"] != null)
                    {
                        Session["LoginCount"] = HelperMethods.ConvertToInt(Session["LoginCount"]) + 1;
                        if (HelperMethods.ConvertToInt(Session["LoginCount"]) >= 3)
                            logindata.ShowCaptcha = true;//place captcha on page
                    }
                    else
                        Session["LoginCount"] = 1;
                }
            }

        }


        [HttpPost]
        public ActionResult Logout()
        {
            _serv.SignOut();

            Session["isLogin"] = "false";
            Session.Clear();
            Session.RemoveAll();
            Session.Abandon();

            HttpCookie LoginAuthKey = new HttpCookie("AuthKey", "-");
            LoginAuthKey.Expires = DateTime.Now.AddDays(-30d);
            Response.Cookies.Add(LoginAuthKey);
            //Response.Cookies.Remove("AuthKey");


            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();



            ModelState.AddModelError("_FORM", "You have been successfully logged out.");
            TempData["OutputMessage"] = "You have been successfully logged out.";
            TempData["AlertCSS"] = "alert success";

            //return new RedirectResult();
            return RedirectToAction("Login", "User");

            //return RedirectToAction("Login");
        }








        [NotRestricted]
        public ActionResult Forgot()
        {
            return View(new AppUserPwdRequest());
        }

        [NotRestricted]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Forgot(AppUserPwdRequest _data)
        {
            try
            {
                //Check if user sesssion is alive then send it to HomePage
                if (Auth.isLogin)
                    return RedirectToAction("Index");


                if (Session["Captcha"] != null)
                {
                    string GetCaptcha = Session["Captcha"].ToString();

                    if (string.IsNullOrEmpty(_data.Captcha))
                        ModelState.AddModelError("_FORM", "Please verify word shown in the image.");
                    else if (!_data.Captcha.Equals(GetCaptcha))
                        ModelState.AddModelError("_FORM", "Provided text does not match Captcha text.");

                    _data.ShowCaptcha = true;
                    _data.Captcha = "";
                }


                if (string.IsNullOrEmpty(_data.UserName) && string.IsNullOrEmpty(_data.Email))
                    ModelState.AddModelError("_FORM", "Please provide user name or email address.");

                if (string.IsNullOrEmpty(_data.UserName) && !HelperMethods.isEmail(_data.Email))
                    ModelState.AddModelError("Email", "Invalid email address.");


                if (ModelState.IsValid)
                {
                    bool isValid = _serv.Forgot(_data, out error);


                    if (isValid)
                    {
                        Session.Remove("LoginCount");
                        Session.Remove("Captcha");


                        TempData["OutputMessage"] = "An email is sent containing the link to reset your password.";
                        TempData["AlertCSS"] = "alert success";

                        return View(new AppUserPwdRequest());
                    }
                    else
                        ModelState.AddModelError("_FORM", error);
                }

                _data.Captcha = "";
                return View(_data);

            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                ModelState.AddModelError("_FORM", "Something went wrong (" + ex + ")");

                return View(_data);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("_FORM", "Error: " + ex);
                return View(_data);

            }
            finally
            {
                if (!ModelState.IsValid)
                {
                    if (Session["LoginCount"] != null)
                    {
                        Session["LoginCount"] = HelperMethods.ConvertToInt(Session["LoginCount"]) + 1;
                        if (HelperMethods.ConvertToInt(Session["LoginCount"]) >= 3)
                            _data.ShowCaptcha = true;//place captcha on page
                    }
                    else
                        Session["LoginCount"] = 1;
                }
            }
        }





        [NotRestricted]
        public ActionResult ResetPassword(AppUserPwdRequest _data, string r)
        {
            try
            {
                string key = EncryptDecryptHelper.Decrypt(r);
                if (key.Split('&').Length == 2)
                {
                    var randNo = key.Split('&')[0];
                    var RecID = key.Split('&')[1];


                    var RandomNo = randNo.Substring(randNo.IndexOf("=") + 1);
                    var ResetRecID = RecID.Substring(RecID.IndexOf("=") + 1);


                    if (Request.HttpMethod == "POST")
                    {
                        if (string.IsNullOrEmpty(_data.Password))
                            ModelState.AddModelError("Password", "Please enter a new password.");

                        if (string.IsNullOrEmpty(_data.Confirmpwd))
                            ModelState.AddModelError("Confirmpwd", "Please repeat the password for verification.");

                        if ((_data.Password != _data.Confirmpwd))
                            ModelState.AddModelError("Password", "Both Password Field does not match.");



                        if (ModelState.IsValid)
                        {

                            bool isValid = _serv.ResetPassword(ResetRecID.ConvertToInt(), _data.Password);

                            if (isValid)
                            {
                                TempData["OutputMessage"] = "Your Password has been reset sucessfully.";
                                TempData["AlertCSS"] = "alert success";
                            }
                            else
                            {
                                TempData["OutputMessage"] = "Password Reset fail.";
                                TempData["AlertCSS"] = "alert warning";
                            }


                            return RedirectToAction("login");
                        }
                    }

                }
                else
                {
                    TempData["OutputMessage"] = "Reference Key is not valid.";
                    TempData["AlertCSS"] = "alert error";
                }

                return View();
            }
            catch
            {
                ModelState.AddModelError("_FORM", "An error occured.");
                return View();
            }
        }






        public JsonResult isUserExist()
        {
            string Username = HelperMethods.CleanUserName(HelperMethods.ConvertToString(Request.QueryString["AppUser.UserName"]));
            int UserId = HelperMethods.ConvertToInt(Request.QueryString["UserId"]);

            UserId = HelperMethods.ConvertToInt(Request.QueryString["AppUser.UserId"]);

            return Json(_serv.isUserExist(Username, UserId), JsonRequestBehavior.AllowGet);
        }

        public JsonResult isUserEmailExist()
        {
            string Email = Request.QueryString["Email"].ConvertToString();
            int UserId = HelperMethods.ConvertToInt(Request.QueryString["UserId"]);

            return Json(_serv.isUserEmailExist(Email, UserId), JsonRequestBehavior.AllowGet);
        }



        [Restricted]
        public ActionResult Index(string q)
        {
            ViewBag.issidebar = true;
            ViewBag.UserName = Auth.UserName;
            if (!string.IsNullOrEmpty(q))
                return View(_serv.GetEmployees().Where(e => e.AppUser.UserName.Contains(q) || e.Email.Contains(q) || e.Name.Contains(q)));
            else
                return View(_serv.GetEmployees());
        }
        [Restricted]
        public ActionResult Create()
        {
            ViewBag.issidebar = true;
            ViewBag.UserName = Auth.UserName;
            ViewBag.RoleId = new SelectList(_servSec.GetAllGroups(), "RoleId", "RoleName");
            //ViewBag.PartyDepartments = new MultiSelectList(CommonService.GetPartyDepartments(), "PartyDepartmentId", "PartyDepartmentName");
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(AppEmployee employeeData, int RoleId, bool? isEmailSend)
        {
            //Clean Username with spaces
            employeeData.AppUser.UserName = HelperMethods.CleanUserName(employeeData.AppUser.UserName);

            if (!_serv.isUserExist(employeeData.AppUser.UserName, 0))
                ModelState.AddModelError("AppUser.UserName", "User Name already exist");

            //if (PartyDepartments == null)
            //	ModelState.AddModelError("PartyDepartments", "Must Specify Client Department Access");



            #region Image Upload


            int filesize = WebConstants.FileSize;
            string message = string.Empty;
            string filename = string.Empty;
            HttpPostedFileBase FileSourcefront;
            //HttpPostedFileBase FileSourceBack;
            string FileExtension = "jpg,jpeg,png,gif";
            string FilePath = _serv.getImagePath().FirstOrDefault(q=>q.SystemConfigKey== "ImagePathForSave").SystemConfigValue;
            string ImageThumbNailPathForSave = _serv.getImagePath().FirstOrDefault(q => q.SystemConfigKey == "ImageThumbNailPathForSave").SystemConfigValue;

            if (employeeData.front != null)
            {
                FileSourcefront = employeeData.front;
                if ((FileSourcefront.ContentLength / 1024) > filesize)
                {
                    ModelState.AddModelError("Size", "- The size of the file should not exceed " + filesize + " kb <br/>");
                }
                string fileExt = System.IO.Path.GetExtension(FileSourcefront.FileName).Substring(1).ToLower();
                if (FileExtension.Contains(fileExt))
                {

                    if (System.IO.Directory.Exists(Server.MapPath(FilePath)))
                    {
                        filename = "User" + Guid.NewGuid().ConvertToString() + FileSourcefront.FileName.ConvertToString();
                        string inputFileName =Path.Combine(Server.MapPath(FilePath) + filename);
                        string outputFileName =Path.Combine(Server.MapPath(ImageThumbNailPathForSave) + filename);
                        string fileinpath = FilePath + filename.Replace(" ", "");
                        FileSourcefront.SaveAs(Server.MapPath(fileinpath));
                        ImageFormat imageformat = _serv.ParseImageFormat(Path.GetExtension(FileSourcefront.FileName));
                        UserService.ResizeImage(inputFileName,outputFileName, 50.0, 50.0, imageformat);
                       
                        employeeData.ProfilePic= filename.Replace(" ", "");
                        string fileThumbnailpath = ImageThumbNailPathForSave + filename.Replace(" ", "");
                        employeeData.ProfilePicThumbNail = filename.Replace(" ", "");
                     
                       // FileSourcefront.SaveAs(Server.MapPath(fileThumbnailpath));



                    }
                   
                   
                     
                     
                     




                    
                    else
                    {
                        System.IO.Directory.CreateDirectory(FilePath);
                        filename = employeeData.AppUser.UserName.ConvertToString() + Guid.NewGuid().ConvertToString() + "User" + FileSourcefront.FileName.ConvertToString();
                        string fileinpath = FilePath + filename.Replace(" ", "");
                        employeeData.ProfilePic = filename.Replace(" ", "");
                        string fileThumbnailpath = ImageThumbNailPathForSave + filename.Replace(" ", "");
                        employeeData.ProfilePicThumbNail = filename.Replace(" ", "");
                        FileSourcefront.SaveAs(fileinpath);
                        FileSourcefront.SaveAs(fileThumbnailpath);
                    }
                }
                else
                    ModelState.AddModelError("Format", "- Invalid type. Only the following types (" + FileExtension + ") are supported.<br/>");
            }



            #endregion

            if (ModelState.IsValid)
            {
                bool isSuccess = _serv.CreateEmployee(employeeData, RoleId, isEmailSend.ConvertToBool(), out error, out isEmailSent);
                TempData["AlertCSS"] = isSuccess ? "alert success" : "alert error";

                if (!isSuccess)
                    ModelState.AddModelError("", error);
                else
                {
                    error = (isEmailSent && isEmailSend.ConvertToBool()) ? "" : " (Sending account information email failed!)";
                    TempData["OutputMessage"] = "Record Successfully Created" + error;
                }

                if (ModelState.IsValid)
                    return RedirectToAction("Index");
            }
            ViewBag.RoleId = new SelectList(_servSec.GetAllGroups(), "RoleId", "RoleName");
            return View(employeeData);
        }
        [Restricted]
        public ActionResult Edit(Guid id)
        {
            ViewBag.issidebar = true;
            ViewBag.UserName = Auth.UserName;
            if (!id.IsGUID())
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            AppEmployee employeeData = _serv.GetEmployee(id.ConvertToGUID());
            if (employeeData == null)
                return HttpNotFound();

            employeeData.AppUser.Password = "";
            ViewBag.RoleId = new SelectList(_servSec.GetAllGroups(), "RoleId", "RoleName", employeeData.AppUser.RoleId);
            //ViewBag.PartyDepartments = new MultiSelectList(CommonService.GetPartyDepartments(), "PartyDepartmentId", "PartyDepartmentName", employeeData.AppEmployeeDepartments.Select(e => e.PartyDepartmentId));

            return View(employeeData);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(string id, AppEmployee employeeData, int RoleId, bool? isEmailSend)
        {
            if (!id.IsGUID())
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            //if (PartyDepartments == null)
            //	ModelState.AddModelError("PartyDepartments", "Must Specify Client Department Access");



            #region Image Upload


            int filesize = WebConstants.FileSize;
            string message = string.Empty;
            string filename = string.Empty;
            HttpPostedFileBase FileSourcefront;
            HttpPostedFileBase FileSourceBack;
            string FileExtension = "jpg,jpeg,png,gif";
            string FilePath = WebConstants.PhysicalPath + WebConstants.FileFinalUploadFolder;

            if (employeeData.front != null)
            {
                FileSourcefront = employeeData.front;
                if ((FileSourcefront.ContentLength / 1024) > filesize)
                {
                    ModelState.AddModelError("Size", "- The size of the file should not exceed " + filesize + " kb <br/>");
                }
                string fileExt = System.IO.Path.GetExtension(FileSourcefront.FileName).Substring(1).ToLower();
                if (FileExtension.Contains(fileExt))
                {

                    if (System.IO.Directory.Exists(FilePath))
                    {
                        filename = employeeData.AppUser.UserName.ConvertToString() + Guid.NewGuid().ConvertToString() + "Front" + FileSourcefront.FileName.ConvertToString();
                        string fileinpath = FilePath + filename.Replace(" ", "");
                        employeeData.ProfilePic = filename.Replace(" ", "");
                        FileSourcefront.SaveAs(fileinpath);

                    }
                    else
                    {
                        System.IO.Directory.CreateDirectory(FilePath);
                        filename = employeeData.AppUser.UserName.ConvertToString() + Guid.NewGuid().ConvertToString() + "Front" + FileSourcefront.FileName.ConvertToString();
                        string fileinpath = FilePath + filename.Replace(" ", "");
                        employeeData.ProfilePic = filename.Replace(" ", "");
                        FileSourcefront.SaveAs(fileinpath);
                    }
                }
                else
                    ModelState.AddModelError("Format", "- Invalid type. Only the following types (" + FileExtension + ") are supported.<br/>");
            }
            if (employeeData.Back != null)
            {
                FileSourceBack = employeeData.Back;
                if ((FileSourceBack.ContentLength / 1024) > filesize)
                {
                    ModelState.AddModelError("Size", "- The size of the file should not exceed " + filesize + " kb <br/>");
                }
                string availableextensions = !string.IsNullOrEmpty(FileExtension) ? FileExtension : "jpg,jpeg,png,gif";
                string fileExt = System.IO.Path.GetExtension(FileSourceBack.FileName).Substring(1).ToLower();
                if (FileExtension.Contains(fileExt))
                {
                    if (availableextensions.Contains(fileExt))
                    {
                        if (System.IO.Directory.Exists(FilePath))
                        {
                            filename = employeeData.AppUser.UserName.ConvertToString() + Guid.NewGuid().ConvertToString() + "Back" + FileSourceBack.FileName.ConvertToString();
                            string fileinpath = FilePath + filename.Replace(" ", "");
                            employeeData.ProfilePic = filename.Replace(" ", "");
                            FileSourceBack.SaveAs(fileinpath);
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(FilePath);
                            filename = employeeData.AppUser.UserName.ConvertToString() + Guid.NewGuid().ConvertToString() + "Back" + FileSourceBack.FileName.ConvertToString();
                            string fileinpath = FilePath + filename.Replace(" ", "");
                            employeeData.ProfilePic = filename.Replace(" ", "");
                            FileSourceBack.SaveAs(fileinpath);
                        }
                    }
                    else
                        ModelState.AddModelError("Format", "- Invalid type. Only the following types (" + FileExtension + ") are supported.<br/>");
                }
            }



            #endregion

            if (ModelState.IsValid)
            {

                bool isSuccess = _serv.EditEmployee(id.ConvertToGUID(), employeeData, RoleId, isEmailSend.ConvertToBool(), out error, out isEmailSent);
                TempData["AlertCSS"] = isSuccess ? "alert success" : "alert error";

                if (!isSuccess)
                    ModelState.AddModelError("", error);
                else
                {
                    error = (isEmailSent && isEmailSend.ConvertToBool()) ? "" : " (Sending account information email failed!)";
                    TempData["OutputMessage"] = "Record Successfully Created" + error;
                }

                if (ModelState.IsValid)
                    return RedirectToAction("Index");
            }

            ViewBag.RoleId = new SelectList(_servSec.GetAllGroups(), "RoleId", "RoleName", RoleId);


            return View(employeeData);
        }
        [Restricted]
        public ActionResult ViewProfile(string id)
        {
            if (Auth.RoleId == 42)
            {
                ViewBag.issidebar = false;
            }
            else
            {
                ViewBag.issidebar = true;
            }
                ViewBag.UserName = Auth.UserName;
                int UserID = HelperMethods.IsNumeric(id) ? HelperMethods.ConvertToInt(id) : Auth.UserID;
                AppEmployee employeeData = _serv.GetUser(UserID);
                if (employeeData == null)
                {
                    TempData["AlertCSS"] = "alert error";
                    TempData["OutputMessage"] = "ID is not valid";
                    return RedirectToAction("Index");
                }
                employeeData.Notes = (employeeData.Notes != null) ? employeeData.Notes.Replace(System.Environment.NewLine, "<br/>") : employeeData.Notes;
                employeeData.Signature = (employeeData.Signature != null) ? employeeData.Signature.Replace(System.Environment.NewLine, "<br/>") : employeeData.Signature;
                //employeeData.CNICFrontJpeg=

                //var ThumbImg = employeeData.AppUser.AppUserMetas.Where(e => e.MetaKey == "Thumbnail-Original").Select(e => e.MetaValue).FirstOrDefault();

                var url = _serv.getUrls().FirstOrDefault(q => q.SystemConfigKey == "URL").SystemConfigValue;
                var directPath = _serv.getUrls().FirstOrDefault(q => q.SystemConfigKey == "ImagePathForView").SystemConfigValue;
                var defaultImage = _serv.getUrls().FirstOrDefault(q => q.SystemConfigKey == "DefaultImage").SystemConfigValue;
                string UserProfilePic = url + directPath;
                if (employeeData.ProfilePic != null)
                {
                    ViewBag.Thumb = UserProfilePic + employeeData.ProfilePic;
                }
                else
                {
                    ViewBag.Thumb = url + defaultImage;
                }

                return View(employeeData);
            
        }
        [Restricted]
        public ActionResult EditProfile()
        {
            ViewBag.issidebar = true;
            ViewBag.UserName = Auth.UserName;
            AppEmployee employeeData = _serv.GetUser(Auth.UserID);
            if (employeeData == null)
            {
                TempData["AlertCSS"] = "alert error";
                TempData["OutputMessage"] = "ID is not valid";
                return RedirectToAction("ViewProfile");
            }

            employeeData.AppUser.Password = "";
            return View(employeeData);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditProfile(AppEmployee employeeData)
        {

            if (ModelState.IsValid)
            {
                bool isSuccess = _serv.EditProfile(employeeData, out error);
                TempData["AlertCSS"] = isSuccess ? "alert success" : "alert error";

                if (!isSuccess)
                    ModelState.AddModelError("", error);
                else
                    TempData["OutputMessage"] = "Profile Successfully Updated";

                if (ModelState.IsValid)
                    return RedirectToAction("ViewProfile");
            }

            //employeeData.AppUser.Password = "";
            return View(employeeData);
        }


        [ValidateInput(false)]
        [HttpPost]
        public ActionResult UploadPicture(int userId, HttpPostedFileBase Profile)
        {


            if (Request.HttpMethod == "POST")
            {
           
                    //bool isSuccess = _serv.EditProfile(employeeData, out error);

                //string FileSavePath =_serv.getImagePath()+ fileName.FileName;
                string SaveFileName = Profile.FileName;
                string SuccessMsg = string.Empty;
                string ErrorMsg = string.Empty;
                var extension = Path.GetExtension(Profile.FileName);
                var filename = "User" + Guid.NewGuid() + extension;
                var imagePath = _serv.getUrls().FirstOrDefault(q => q.SystemConfigKey == "ImagePathForSave").SystemConfigValue;
                var path = Path.Combine(Server.MapPath(imagePath + filename));


                if (Profile.ContentLength > 0)
                {
                    long iFileSize = Profile.ContentLength;

                    if (Profile.FileName.Contains("jpg") || Profile.FileName.Contains("png") || Profile.FileName.Contains("bmp"))
                    {
                        string completename = filename;
                        bool isSuccess = _serv.EditProfilePicture(userId, completename);

                        Profile.SaveAs(path);
                     string ImageThumbNailPathForSave = _serv.getImagePath().FirstOrDefault(q => q.SystemConfigKey == "ImageThumbNailPathForSave").SystemConfigValue;
                   
                                    string inputFileName = path;
                                    string outputFileName = Path.Combine(Server.MapPath(ImageThumbNailPathForSave) + filename);
                            ImageFormat imageformat = _serv.ParseImageFormat(Path.GetExtension(Profile.FileName));
                                    UserService.ResizeImage(inputFileName, outputFileName, 50.0, 50.0, imageformat);

                                }
          
                }

                return RedirectToAction("ViewProfile");
            }

            return View();

        }



        [HttpPost]
        public ActionResult Delete(string id)
        {
            if (!id.IsNumeric())
            {
                TempData["OutputMessage"] = "Record ID Not Valid!";
                return RedirectToAction("Index");
            }


            bool isSuccess = _serv.DeletEmployee(id.ConvertToInt(), out error);
            TempData["AlertCSS"] = isSuccess ? "alert success" : "alert error";

            if (!isSuccess)
                TempData["OutputMessage"] = "Error Occured: " + error;
            else
                TempData["OutputMessage"] = "Record Successfully Deleted";

            return RedirectToAction("Index");
        }
    }
}
