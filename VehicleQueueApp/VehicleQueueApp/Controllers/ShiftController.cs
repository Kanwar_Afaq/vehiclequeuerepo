﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VehicleQueueApp.Filters;
using VehicleQueueApp.Helpers;
using VehicleQueueApp.Services;

namespace VehicleQueueApp.Controllers
{
    [Restricted]

    public class ShiftController : Controller
    {
        public ShiftServices shiftServices = new ShiftServices();
        // GET: Shift
        public ActionResult Index()
        {

            ViewBag.UserName = Auth.UserName;
            ViewBag.issidebar = true;
            ViewBag.ShiftStatus= shiftServices.checkShiftButtonVisibility();
    
            return View();
        }
        public ActionResult AllShifts(int? dayId)
        {
            ViewBag.UserName = Auth.UserName;
            ViewBag.issidebar = true;
            var allshifts = shiftServices.GetShifts();
            if (dayId>0)
            {
               allshifts = shiftServices.GetShifts().Where(q=>q.DayId==dayId).OrderByDescending(q=> q.ShiftId);             
            }
       
            return View(allshifts.OrderByDescending(q=>q.ShiftId));
        }
        public ActionResult GetShifts(int dayId)
        {
            ViewBag.UserName = Auth.UserName;
            ViewBag.issidebar = true;
            var shifts = shiftServices.GetShifts().Where(shift => shift.DayId == dayId).ToList();
            return View(shifts);
        }
        //public ActionResult StartShift()
        //{
        //    int userid = Auth.UserID;

        //    int activeDay = shiftServices.getActiveDay();
        //    shiftServices.StartShift(userid,activeDay);
        //    return RedirectToAction("Index","Home");

        //}
        //public ActionResult ShiftClose()
        //{
        //    int userid = Auth.UserID;
        //    int dayId = shiftServices.getActiveDay();
        //    int shiftId = shiftServices.getActiveShift();

        //    shiftServices.CLoseShift(shiftId, dayId, userid);
        //    return RedirectToAction("Index","Home");
        //}
    }
}