﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VehicleQueueApp.Helpers;
using VehicleQueueApp.Services;

namespace VehicleQueueApp.Controllers
{
    public class OperationController : Controller
    {
        public OperationServices operationServices = new OperationServices();
        // GET: Operation
        public ActionResult Index()
        {

            ViewBag.UserName = Auth.UserName;
            return View();
        }
        public ActionResult AllOperations(int? shiftId)
        {
            ViewBag.UserName = Auth.UserName;
            ViewBag.issidebar = true;
            var alloperations = operationServices.GetOperations();
           
            if (shiftId > 0)
            {
                alloperations = operationServices.GetOperations().Where(q => q.ShiftId == shiftId).OrderByDescending(q => q.OperationId);
            }

            return View(alloperations.OrderByDescending(q => q.ShiftId));
        }
    }
}