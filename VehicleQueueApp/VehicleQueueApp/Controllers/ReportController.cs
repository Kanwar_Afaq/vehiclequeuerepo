﻿using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.draw;
using iTextSharp.tool.xml;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using VehicleQueueApp.Filters;
using VehicleQueueApp.Models;
using VehicleQueueApp.Services;

namespace VehicleQueueApp.Controllers
{

    [Restricted]
    public class ReportController : Controller
    {
        BankChallanServices BankChallanServices = new BankChallanServices();
        string pdf;
        // GET: Report
        public ActionResult Index()
        {

            return View();
        }
        public PdfPCell getCellWithUnderLine(String text, int alignment,float padding)
        {
            Chunk chunk1 = new Chunk(text);
            chunk1.SetUnderline(1, -3);
            PdfPCell cell = new PdfPCell(new Phrase(chunk1));
            //cell.Padding=10;
            cell.PaddingTop = 5;
            cell.HorizontalAlignment = alignment;
            cell.Border = PdfPCell.NO_BORDER;
            cell.PaddingTop = padding;
            return cell;
        }
        public PdfPCell getCell(String text, int alignment)
        {
            PdfPCell cell = new PdfPCell(new Phrase(text));
            //cell.Padding=10;
            cell.PaddingTop = 10f;
            cell.HorizontalAlignment = alignment;
            cell.Border = PdfPCell.NO_BORDER;
          
            return cell;
        }
        public PdfPCell getCellWithPaddingTop(String text, int alignment, float padding)
        {
            PdfPCell cell = new PdfPCell(new Phrase(text));
            //cell.Padding=10;
            cell.PaddingTop = padding;
            cell.HorizontalAlignment = alignment;
            cell.Border = PdfPCell.NO_BORDER;

            return cell;
        }
        public PdfPCell getCellWithPaddingTopAndLeft(String text, int alignment, float toppadding,float leftpadding)
        {
            PdfPCell cell = new PdfPCell(new Phrase(text));
            //cell.Padding=10;
            cell.PaddingTop = toppadding;
            cell.PaddingLeft = leftpadding;
            cell.HorizontalAlignment = alignment;
            cell.Border = PdfPCell.NO_BORDER;

            return cell;
        }
        public PdfPCell getCellWithPaddingRight(String text, int alignment)
        {
            Font f = new Font();
            f.SetStyle(Font.BOLD);
            PdfPCell cell = new PdfPCell(new Phrase(text,f));
            //cell.Padding=10;
            cell.PaddingTop = 5;
            cell.HorizontalAlignment = alignment;
            cell.Border = PdfPCell.NO_BORDER;
            cell.PaddingRight = 20;
            

            return cell;
        }
        public PdfPCell getCellWithPaddingLeft(String text, int alignment)
        {
            Font f = new Font();
            f.SetStyle(Font.BOLD);
            PdfPCell cell = new PdfPCell(new Phrase(text, f));
         
            //cell.Padding=10;
            cell.PaddingTop = 5;
            cell.HorizontalAlignment = alignment;
            cell.Border = PdfPCell.NO_BORDER;
            cell.PaddingLeft = 14;

            return cell;
        }
        public PdfPCell getCell1(String text, int alignment)
        {
            Phrase phrase = new Phrase();
            phrase.Add(
                new Chunk("PORT QASIM AUTHORITY", new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD))
            );
            PdfPCell cell = new PdfPCell(phrase);
            //cell.Padding=10;

            cell.HorizontalAlignment = alignment;
            cell.Border = PdfPCell.NO_BORDER;

            return cell;
        }
       
        public PdfPCell getCell2(String text, int alignment, int padding)
        {


            PdfPCell cell = new PdfPCell(new Phrase(text, new Font(Font.FontFamily.TIMES_ROMAN, 14)));
            //cell.Padding=10;
            cell.PaddingBottom = padding;
            cell.HorizontalAlignment = alignment;
            cell.Border = PdfPCell.NO_BORDER;

            return cell;
        }
       
        [HttpPost]
        [ActionName("Index")]
        public ActionResult Index_Post(MainVM mainvm)
        {
            var reportResult = BankChallanServices.getPdfreportOfBankChallan(mainvm);
           
            Document pdfDoc = new Document(PageSize.A4, 25, 25, 25, 15);
            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();      


           
            foreach (var item in reportResult.GroupBy(q => q.BankChallanId))
            {
                DateTime? dt = item.FirstOrDefault().BankChallanDateTime;
                PdfPTable tableSearches = new PdfPTable(3);
               // tableSearches.DefaultCell.Border = Rectangle.ALIGN_LEFT | Rectangle.ALIGN_RIGHT;
                PdfPCell cellSearches = new PdfPCell();
                //cellSearches.Border = 0;
                cellSearches.BackgroundColor = BaseColor.LIGHT_GRAY;
                //Chunk chunk = new Chunk("Bank Challan", FontFactory.GetFont("Arial", 20, Font.BOLDITALIC, BaseColor.BLUE));
                PdfPTable tableTop = new PdfPTable(1);
                tableTop.WidthPercentage = 100;
                tableTop.AddCell(getCell1("PORT QASIM AUTHORITY", PdfPCell.ALIGN_CENTER));
                PdfPTable tableSub1Top = new PdfPTable(1);
                tableSub1Top.WidthPercentage = 100;
              
               
                tableSub1Top.AddCell(getCell2("BIN QASIM, KARACHI-75020", PdfPCell.ALIGN_CENTER, 0));
               
                pdfDoc.Add(tableTop);

                pdfDoc.Add(tableSub1Top);
              
                PdfPTable table = new PdfPTable(2);
                Chunk glue = new Chunk(new VerticalPositionMark());
                Paragraph para = new Paragraph();
                Phrase ph1 = new Phrase();
                ph1.Add(new Chunk(Environment.NewLine));
                string projectname1 = "CHALLAN No :";
                string projectname2 = item.FirstOrDefault().ChallanNo;
                string projectname = projectname1 + projectname2;
                string date = String.Format("{0:dd-MM-yy}", dt);
                string date2 = "Dated : "; 
                Paragraph main = new Paragraph();
                Chunk chunk1 = new Chunk(projectname1);
                Chunk chunk3 = new Chunk(date2);
                Chunk chunk4 = new Chunk(date);
                chunk4.SetUnderline(1, -3);
                Chunk chunk2 = new Chunk(projectname2);
                chunk2.SetUnderline(1, -3);
                ph1.Add(chunk1);
                ph1.Add(chunk2);
               // Here I add projectname as a chunk into Phrase.    
                ph1.Add(glue); // Here I add special chunk to the same phrase.    
                ph1.Add(chunk3);
                ph1.Add(chunk4);// Here I add date as a chunk into same phrase.    
                main.Add(ph1);
                para.Add(main);
                //table.WidthPercentage = 100;
                //table.AddCell(getCell("Challan No :", PdfPCell.ALIGN_LEFT));
                //table.AddCell(getCellWithUnderLine(item.FirstOrDefault().ChallanNo, PdfPCell.ALIGN_LEFT));

                //table.AddCell(getCell("Dated :" + String.Format("{0:dd-MM-yy}", dt), PdfPCell.ALIGN_RIGHT));
                pdfDoc.Add(para);
                //pdfDoc.Add(chunk);
                tableSearches.WidthPercentage = 100;
                tableSearches.PaddingTop = 50;
                tableSearches.SpacingBefore = 25f;//both are used to mention the space from heading
               // tableSearches.DefaultCell.HorizontalAlignment = Element.ALIGN_LEFT;
               

                cellSearches.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                cellSearches.Padding = 10;
               
                cellSearches.Phrase = new Phrase("Value");
               
                cellSearches.Border = PdfPCell.LEFT_BORDER|PdfPCell.RIGHT_BORDER;
                tableSearches.AddCell(cellSearches);
                cellSearches.Phrase = new Phrase("Total No of reciept issued");
                cellSearches.Border = PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER;
                tableSearches.AddCell(cellSearches);
                cellSearches.Phrase = new Phrase("Amount");
                cellSearches.Border = PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER;
                tableSearches.AddCell(cellSearches);
              

                int i = 0;
                decimal? amount = 0;
                foreach (var m in item)
                {
                    i++;
                    cellSearches.BackgroundColor = BaseColor.WHITE;
                    amount = amount + m.TotalAmount;
                    if (i <= item.Count())
                    {
                        cellSearches.Phrase = new Phrase(m.CategoryName + "\n" + m.CategoryFees);
                        cellSearches.Border = PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER;
                        tableSearches.AddCell(cellSearches);

                        cellSearches.Phrase = new Phrase(m.TotalNoOftoken.ToString());
                        cellSearches.Border = PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER;
                        tableSearches.AddCell(cellSearches);
                        cellSearches.Phrase = new Phrase(m.TotalAmount.ToString());

                        cellSearches.Border = PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER;
                        tableSearches.AddCell(cellSearches);
                      if(i==item.Count())
                        {
                            Font f = new Font();
                            f.SetStyle(Font.BOLD);
                            cellSearches.Phrase = new Phrase("TOTAL", f);
                            cellSearches.Border = PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.TOP_BORDER;
                            tableSearches.AddCell(cellSearches);

                            cellSearches.Phrase = new Phrase("");
                            cellSearches.Border = PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.TOP_BORDER;
                            tableSearches.AddCell(cellSearches);
                            cellSearches.Phrase = new Phrase(amount.ToString(), f);

                            cellSearches.Border = PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.TOP_BORDER;
                            tableSearches.AddCell(cellSearches);
                        }
                    }

                



                }
             
                
                tableSearches.AddCell(cellSearches);
                pdfDoc.Add(tableSearches);

                

                PdfPTable bottomTable = new PdfPTable(2);
                bottomTable.SpacingBefore = 20f;
                bottomTable.WidthPercentage = 100f;
              
                string number = "";
                number = Convert.ToDecimal(amount).ToString();
                string isNegative="";
                if (number.Contains("-"))
                {
                    isNegative = "Minus ";
                    number = number.Substring(1, number.Length - 1);
                }
                if (number == "0")
                {
                    bottomTable.AddCell(getCell("Total Amount In Words Rupees", PdfPCell.ALIGN_LEFT));
                  
                    bottomTable.AddCell(getCellWithUnderLine("0 RS Only", PdfPCell.ALIGN_LEFT,20f));
                    bottomTable.AddCell(getCell("Countersignature of the head of department", PdfPCell.ALIGN_LEFT));
                    bottomTable.AddCell(getCellWithUnderLine("..........", PdfPCell.ALIGN_LEFT,20f));
                    bottomTable.AddCell(getCell("Recieve Payment (In Words) Rupees", PdfPCell.ALIGN_LEFT));
                    bottomTable.AddCell(getCellWithUnderLine("0 Rupees Only", PdfPCell.ALIGN_LEFT,20f));
                }
                else
                {
                    bottomTable.AddCell(getCellWithPaddingTopAndLeft("Amount In Words Rupees", PdfPCell.ALIGN_LEFT,50f,50f));

                    bottomTable.AddCell(getCellWithUnderLine(isNegative + BankChallanRepository.ConvertToWords(number), PdfPCell.ALIGN_MIDDLE,50f));
                    bottomTable.AddCell(getCellWithPaddingTopAndLeft("Countersignature of the head of department", PdfPCell.ALIGN_LEFT,50f,50f));
                    bottomTable.AddCell(getCellWithPaddingTop("---------------------------------------------------------------", PdfPCell.ALIGN_MIDDLE,55f));
                    bottomTable.AddCell(getCellWithPaddingTopAndLeft("Recieve Payment (In Words) Rupees", PdfPCell.ALIGN_LEFT,50f,50f));
                    bottomTable.AddCell(getCellWithUnderLine(isNegative + BankChallanRepository.ConvertToWords(number), PdfPCell.ALIGN_MIDDLE,50f));
                    
                }
                float[] tblwidth = new float[] { 100f, 130f, 100f,40f,130f,180f };
                PdfPTable lastTable = new PdfPTable(tblwidth);
                lastTable.WidthPercentage = 100f;
              
                lastTable.SpacingBefore = 50f;
                lastTable.AddCell(getCellWithPaddingTopAndLeft("Date", PdfPCell.ALIGN_LEFT,10f,50f));
                lastTable.AddCell(getCellWithPaddingTop("------------------------", PdfPCell.ALIGN_LEFT,15f));
                lastTable.AddCell(getCell("Bank Officer", PdfPCell.ALIGN_LEFT));
                lastTable.AddCell(getCell("Date", PdfPCell.ALIGN_MIDDLE));
                lastTable.AddCell(getCellWithPaddingTop("------------------------", PdfPCell.ALIGN_MIDDLE,15f));
                lastTable.AddCell(getCell("Cashier, \nPort Qasim Authority", PdfPCell.ALIGN_MIDDLE));
                pdfDoc.Add(bottomTable);
              
                pdfDoc.Add(lastTable);
               
                pdfWriter.PageEvent = new Header();
                pdfWriter.PageEvent = new Footer();

               
                pdfDoc.NewPage();
            }
          
            pdfWriter.CloseStream = false;

            Response.Buffer = true;
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=Bank Challan Report.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Write(pdfDoc);
            pdfDoc.Close();
            Response.End();

            return View();
        }
        public ActionResult ReportByChallanId(int BankChallanId)
        {
            var reportResult = BankChallanServices.getPdfreportByBankChallanId(BankChallanId);
            Document pdfDoc = new Document(PageSize.A4, 25, 25, 25, 15);
            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();



            foreach (var item in reportResult.GroupBy(q => q.BankChallanId))
            {
                DateTime? dt = item.FirstOrDefault().BankChallanDateTime;
                PdfPTable tableSearches = new PdfPTable(3);
                // tableSearches.DefaultCell.Border = Rectangle.ALIGN_LEFT | Rectangle.ALIGN_RIGHT;
                PdfPCell cellSearches = new PdfPCell();
                //cellSearches.Border = 0;
                cellSearches.BackgroundColor = BaseColor.LIGHT_GRAY;
                //Chunk chunk = new Chunk("Bank Challan", FontFactory.GetFont("Arial", 20, Font.BOLDITALIC, BaseColor.BLUE));
                PdfPTable tableTop = new PdfPTable(1);
                tableTop.WidthPercentage = 100;
                tableTop.AddCell(getCell1("PORT QASIM AUTHORITY", PdfPCell.ALIGN_CENTER));
                PdfPTable tableSub1Top = new PdfPTable(1);
                tableSub1Top.WidthPercentage = 100;


                tableSub1Top.AddCell(getCell2("BIN QASIM, KARACHI-75020", PdfPCell.ALIGN_CENTER, 0));

                pdfDoc.Add(tableTop);

                pdfDoc.Add(tableSub1Top);

                PdfPTable table = new PdfPTable(2);
                Chunk glue = new Chunk(new VerticalPositionMark());
                Paragraph para = new Paragraph();
                Phrase ph1 = new Phrase();
                ph1.Add(new Chunk(Environment.NewLine));
                string projectname1 = "CHALLAN No : ";
                string projectname2 = item.FirstOrDefault().ChallanNo;
                string projectname = projectname1 + projectname2;
                string date = String.Format("{0:dd-MM-yy}", dt);
                string date2 = "Dated : ";
                Paragraph main = new Paragraph();
                Chunk chunk1 = new Chunk(projectname1);
                Chunk chunk3 = new Chunk(date2);
                Chunk chunk4 = new Chunk(date);
                chunk4.SetUnderline(1, -3);
                Chunk chunk2 = new Chunk(projectname2);
                chunk2.SetUnderline(1, -3);
                ph1.Add(chunk1);
                ph1.Add(chunk2);
                // Here I add projectname as a chunk into Phrase.    
                ph1.Add(glue); // Here I add special chunk to the same phrase.    
                ph1.Add(chunk3);
                ph1.Add(chunk4);// Here I add date as a chunk into same phrase.    
                main.Add(ph1);
                para.Add(main);
                //table.WidthPercentage = 100;
                //table.AddCell(getCell("Challan No :", PdfPCell.ALIGN_LEFT));
                //table.AddCell(getCellWithUnderLine(item.FirstOrDefault().ChallanNo, PdfPCell.ALIGN_LEFT));

                //table.AddCell(getCell("Dated :" + String.Format("{0:dd-MM-yy}", dt), PdfPCell.ALIGN_RIGHT));
                pdfDoc.Add(para);
                //pdfDoc.Add(chunk);
                tableSearches.WidthPercentage = 100;
                tableSearches.PaddingTop = 50;
                tableSearches.SpacingBefore = 25f;//both are used to mention the space from heading
                                                  // tableSearches.DefaultCell.HorizontalAlignment = Element.ALIGN_LEFT;


                cellSearches.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                cellSearches.Padding = 10;

                cellSearches.Phrase = new Phrase("Value");

                cellSearches.Border = PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER;
                tableSearches.AddCell(cellSearches);
                cellSearches.Phrase = new Phrase("Total No of reciept issued");
                cellSearches.Border = PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER;
                tableSearches.AddCell(cellSearches);
                cellSearches.Phrase = new Phrase("Amount");
                cellSearches.Border = PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER;
                tableSearches.AddCell(cellSearches);


                int i = 0;
                decimal? amount = 0;
                foreach (var m in item)
                {
                    i++;
                    cellSearches.BackgroundColor = BaseColor.WHITE;
                    amount = amount + m.TotalAmount;
                    if (i <= item.Count())
                    {
                        cellSearches.Phrase = new Phrase(m.CategoryName + "\n" + m.CategoryFees);
                        cellSearches.Border = PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER;
                        tableSearches.AddCell(cellSearches);

                        cellSearches.Phrase = new Phrase(m.TotalNoOftoken.ToString());
                        cellSearches.Border = PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER;
                        tableSearches.AddCell(cellSearches);
                        cellSearches.Phrase = new Phrase(m.TotalAmount.ToString());

                        cellSearches.Border = PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER;
                        tableSearches.AddCell(cellSearches);
                        if (i == item.Count())
                        {
                            Font f = new Font();
                            f.SetStyle(Font.BOLD);
                            cellSearches.Phrase = new Phrase("TOTAL", f);
                            cellSearches.Border = PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.TOP_BORDER;
                            tableSearches.AddCell(cellSearches);

                            cellSearches.Phrase = new Phrase("");
                            cellSearches.Border = PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.TOP_BORDER;
                            tableSearches.AddCell(cellSearches);
                            cellSearches.Phrase = new Phrase(amount.ToString(), f);

                            cellSearches.Border = PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.TOP_BORDER;
                            tableSearches.AddCell(cellSearches);
                        }
                    }





                }


                tableSearches.AddCell(cellSearches);
                pdfDoc.Add(tableSearches);



                PdfPTable bottomTable = new PdfPTable(2);
                bottomTable.SpacingBefore = 20f;
                bottomTable.WidthPercentage = 100f;

                string number = "";
                number = Convert.ToDecimal(amount).ToString();
                string isNegative = "";
                if (number.Contains("-"))
                {
                    isNegative = "Minus ";
                    number = number.Substring(1, number.Length - 1);
                }
                if (number == "0")
                {
                    bottomTable.AddCell(getCell("Total Amount In Words Rupees", PdfPCell.ALIGN_LEFT));

                    bottomTable.AddCell(getCellWithUnderLine("0 RS Only", PdfPCell.ALIGN_LEFT, 20f));
                    bottomTable.AddCell(getCell("Countersignature of the head of department", PdfPCell.ALIGN_LEFT));
                    bottomTable.AddCell(getCellWithUnderLine("..........", PdfPCell.ALIGN_LEFT, 20f));
                    bottomTable.AddCell(getCell("Recieve Payment (In Words) Rupees", PdfPCell.ALIGN_LEFT));
                    bottomTable.AddCell(getCellWithUnderLine("0 Rupees Only", PdfPCell.ALIGN_LEFT, 20f));
                }
                else
                {
                    bottomTable.AddCell(getCellWithPaddingTopAndLeft("Amount In Words Rupees", PdfPCell.ALIGN_LEFT, 50f, 50f));

                    bottomTable.AddCell(getCellWithUnderLine(isNegative + BankChallanRepository.ConvertToWords(number), PdfPCell.ALIGN_MIDDLE, 50f));
                    bottomTable.AddCell(getCellWithPaddingTopAndLeft("Countersignature of the head of department", PdfPCell.ALIGN_LEFT, 50f, 50f));
                    bottomTable.AddCell(getCellWithPaddingTop("---------------------------------------------------------------", PdfPCell.ALIGN_MIDDLE, 55f));
                    bottomTable.AddCell(getCellWithPaddingTopAndLeft("Recieve Payment (In Words) Rupees", PdfPCell.ALIGN_LEFT, 50f, 50f));
                    bottomTable.AddCell(getCellWithUnderLine(isNegative + BankChallanRepository.ConvertToWords(number), PdfPCell.ALIGN_MIDDLE, 50f));

                }
                float[] tblwidth = new float[] { 100f, 130f, 100f, 40f, 130f, 180f };
                PdfPTable lastTable = new PdfPTable(tblwidth);
                lastTable.WidthPercentage = 100f;

                lastTable.SpacingBefore = 50f;
                lastTable.AddCell(getCellWithPaddingTopAndLeft("Date", PdfPCell.ALIGN_LEFT, 10f, 50f));
                lastTable.AddCell(getCellWithPaddingTop("------------------------", PdfPCell.ALIGN_LEFT, 15f));
                lastTable.AddCell(getCell("Bank Officer", PdfPCell.ALIGN_LEFT));
                lastTable.AddCell(getCell("Date", PdfPCell.ALIGN_MIDDLE));
                lastTable.AddCell(getCellWithPaddingTop("------------------------", PdfPCell.ALIGN_MIDDLE, 15f));
                lastTable.AddCell(getCell("Cashier, \nPort Qasim Authority", PdfPCell.ALIGN_MIDDLE));
                pdfDoc.Add(bottomTable);

                pdfDoc.Add(lastTable);

                pdfWriter.PageEvent = new Header();
                pdfWriter.PageEvent = new Footer();


                pdfDoc.NewPage();
            }

            pdfWriter.CloseStream = false;

            Response.Buffer = true;
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=Bank Challan Report.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Write(pdfDoc);
            pdfDoc.Close();
            Response.End();

            return View();
        }
      
        //[HttpPost]
    
        //public ActionResult ReportByChallanIdPost(int BankChallanId)
        //{
          
        //    return View();
        //}

    }
    public partial class Footer : PdfPageEventHelper
    {
        public override void OnEndPage(PdfWriter writer, Document doc)
        {

            Paragraph footer = new Paragraph("Design By Sepia Soulutions", FontFactory.GetFont(FontFactory.TIMES, 12, iTextSharp.text.Font.BOLDITALIC));
            footer.Alignment = Element.ALIGN_CENTER;
            PdfPTable footerTbl = new PdfPTable(1);
            footerTbl.TotalWidth = 300;
            footerTbl.HorizontalAlignment = Element.ALIGN_CENTER;
            PdfPCell cell = new PdfPCell(footer);
            cell.Border = 0;
            cell.PaddingLeft = 10;
            footerTbl.AddCell(cell);
            footerTbl.WriteSelectedRows(0, -1, 415, 30, writer.DirectContent);
        }
    }
    public partial class Header : PdfPageEventHelper
    {
        public override void OnEndPage(PdfWriter writer, Document doc)
        {

            //string imageURL = Server.MapPath("~/Content/Vehicle/images") + "/pqa-logo.jpg";
          //  iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(@"D:\AfaqWorkspace\VehicleQueueApp\VehicleQueueApp\Content\Vehicle\images\pqa-logo.jpg");
            //Resize image depend upon your need
          //  jpg.ScaleToFit(140f, 120f);
            //Give space before image
            //jpg.SpacingBefore = 10f;
            //Give some space after the image
           // jpg.SpacingAfter = 1f;
           // jpg.Alignment = Element.ALIGN_LEFT;
          
            PdfPTable headerTbl = new PdfPTable(1);
            headerTbl.TotalWidth = 300;
            headerTbl.HorizontalAlignment = Element.ALIGN_LEFT;
           // PdfPCell cell = new PdfPCell(jpg);
          //  cell.Border = 0;
           // cell.PaddingLeft = 10;
           // headerTbl.AddCell(cell);
            headerTbl.WriteSelectedRows(0, -1, 0, (doc.PageSize.Height - 10), writer.DirectContent); 
        }
    }

}