﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using VehicleQueueApp.Helpers;
using VehicleQueueApp.Services;
using System.Collections.Specialized;


namespace VehicleQueueApp.Filters
{
	public class NotRestrictedAttribute : FilterAttribute
	{
		// Does nothing, just used for decoration
	}

    //[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    //public class PreventDuplicateRequestAttribute : ActionFilterAttribute
    //{
    //    public override void OnActionExecuting(ActionExecutingContext filterContext)
    //    {
    //        if (HttpContext.Current.Request["__RequestVerificationToken"] == null)
    //            return;

    //        var currentToken = HttpContext.Current.Request["__RequestVerificationToken"].ToString();

    //        if (HttpContext.Current.Session["LastProcessedToken"] == null)
    //        {
    //            HttpContext.Current.Session["LastProcessedToken"] = currentToken;
    //            return;
    //        }

    //        lock (HttpContext.Current.Session["LastProcessedToken"])
    //        {
    //            var lastToken = HttpContext.Current.Session["LastProcessedToken"].ToString();

    //            if (lastToken == currentToken)
    //            {
    //                filterContext.Controller.ViewData.ModelState.AddModelError("", "Looks like you accidentally tried to double post.");
    //                return;
    //            }

    //            HttpContext.Current.Session["LastProcessedToken"] = currentToken;
    //        }
    //    }
    //}

	public class RestrictedAttribute : ActionFilterAttribute, IActionFilter
	{
		[ValidateInput(false)]
		void IActionFilter.OnActionExecuting(ActionExecutingContext filterContext)
		{

			string RefreshSysMeta = WebConfig.GetSysConfig().RefreshCache();


			//Add Cookie value to use in User Login and other places
			WebConfig.setWebsiteCookie();



			// Check if this action has NotRestricted....this will skip specific method for restricting
			object[] attributes = filterContext.ActionDescriptor.GetCustomAttributes(true);
			if (attributes.Any(a => a is NotRestrictedAttribute)) return;



			//Verify User Login Cookkie Value is valid & Re-activate user session
			UserService _UserService = new UserService();
			_UserService.CreateSessionByCookie();


			if (!Auth.isLogin)
			{
				var values = new RouteValueDictionary(new { action = "Login", controller = "User" });
				values.Add("url", WebConfig.EncryptedAbsoluteURL);
				filterContext.Result = new RedirectToRouteResult(values);
				base.OnActionExecuting(filterContext);
			}
			else
			{
				bool isSkipAuth = false;
				bool isAllow = false;
				string param = string.Empty;

				var req = filterContext.HttpContext.Request;

				var descriptor = filterContext.ActionDescriptor;
				var ActionParameter = filterContext.ActionParameters;
				var actionName = descriptor.ActionName;
				var controllerName = descriptor.ControllerDescriptor.ControllerName;

				var type = filterContext.Controller.GetType();
				//var method = type.GetMethod(actionName);
				//var returnType = method.ReturnType;




				if (req.AcceptTypes.Contains("text/html")) { }
				else if (req.AcceptTypes.Contains("application/json"))
					isSkipAuth = true;
				else if (req.AcceptTypes.Contains("application/xml") || req.AcceptTypes.Contains("text/xml"))
					isSkipAuth = true;


				if (req.IsAjaxRequest())
					isSkipAuth = true;


				if (actionName == "Restricted" && controllerName == "Home")
					isSkipAuth = true;

				//Allow all Partial Request to pass through Authentication
				if (actionName.StartsWith("_"))
					isSkipAuth = true;


				//Enable Full Access to SuperAdmin Group
				//if (Auth.RoleId == 1)
				//	isSkipAuth = true;





				if (isSkipAuth)
					return;
				else
				{

					///Get List of All QueryString to Append in the URL
					var queryStringParams = new NameValueCollection(req.QueryString);

					foreach (string x in queryStringParams)
					{
                        if (controllerName.Equals("Company") && queryStringParams.AllKeys.Length > 1)
                        {
                            if (x.Equals("typ"))
                            {
                                if (!string.IsNullOrEmpty(param))
                                    param = "&" + param;
                                param = x + "=" + queryStringParams[x];

                                break;
                            }
                        }

                        if (!string.IsNullOrEmpty(param) && !param.Equals("q"))
                        {
                            if (!string.IsNullOrEmpty(param))
                                param = "&" + param;
                            param = x + "=" + queryStringParams[x];
                        }

                        if (!string.IsNullOrEmpty(x) && x.Equals("typ") && controllerName.Equals("Company"))
                        {
                            if (!string.IsNullOrEmpty(param))
                                param = "&" + param;
                            param = x + "=" + queryStringParams[x];

                            break;
                        }
					}
					if (!string.IsNullOrEmpty(param))
						param = "?" + param.ToLower();


					SecurityService _serv = new SecurityService();
                    
                    //15 Jan 2015 Comment code
                    string pgIndex = (actionName.ToLower() == "index" ? actionName : "");
                    string pgParameter = !string.IsNullOrEmpty(pgIndex) ? param : "";
                    var getAccessDetail = _serv.VerifyUserAccess(controllerName, pgIndex, pgParameter);

                    //var getAccessDetail = _serv.VerifyUserAccess(controllerName, actionName, param);
					var getModuleDetail = getAccessDetail != null ? getAccessDetail.AppModule : null;
					var AccessRights = _serv.VerifyPermission(getAccessDetail, getModuleDetail);
					bool isFullAccess = AccessRights.Where(e => e.Item1.ToLower() == "fullaccess" && e.Item2.ToLower() == "true").Any() ? true : false;
					isAllow = AccessRights.Where(e => e.Item1.ToLower() == actionName.ToLower() && e.Item2.ToLower() == "true").Any() ? true : false;

                    bool isAllowParam = false, VerifyParam = false;  
                    if (!string.IsNullOrEmpty(param))
                    {		//Verify if param is also need to validate then change isAllow accordingly			
                        if (AccessRights.Where(e => (e.Item1.ToLower() == actionName.ToLower() + param || e.Item1.ToLower() == param)).Count() > 0)
                        {
                            VerifyParam = true;
                            isAllowParam = AccessRights.Where(e => (e.Item1.ToLower() == actionName.ToLower() + param || e.Item1.ToLower() == param)).First().Item2.ConvertToBool();
                        }

                        if (VerifyParam)
                            isAllow = isAllowParam;
                    }
					filterContext.Controller.TempData["AccessRights"] = AccessRights;

					if (isFullAccess)
						isAllow = true;

					if (!isAllow)
					{
                        if (req.IsAjaxRequest())
                        {
                            filterContext.Controller.TempData["AlertCSS"] = "alert error";
                            filterContext.Controller.TempData["OutputMessage"] = "You are not authorized to perform this action.";
                        }

						var values = new RouteValueDictionary(new { action = "Restricted", controller = "Home" });
						values.Add("url", controllerName + "/" + actionName + param);
						values.Add("ref", HttpUtility.HtmlEncode(req.Url.AbsolutePath));

						filterContext.Result = new RedirectToRouteResult(values);
						base.OnActionExecuting(filterContext);
					}
				}
			}

		}

	}

    public class NoCacheAttribute : ActionFilterAttribute
    {
        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            //filterContext.HttpContext.Response.Cache.SetExpires(DateTime.UtcNow.AddDays(-1));
            //filterContext.HttpContext.Response.Cache.SetValidUntilExpires(false);
            //filterContext.HttpContext.Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
            //filterContext.HttpContext.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //filterContext.HttpContext.Response.Cache.SetNoStore();


            HttpCachePolicyBase cache = filterContext.HttpContext.Response.Cache;
            cache.SetCacheability(HttpCacheability.NoCache);
            cache.SetMaxAge(TimeSpan.Zero);
            cache.AppendCacheExtension("must-revalidate, no-store"); 

            base.OnResultExecuting(filterContext);
        }
    }

}