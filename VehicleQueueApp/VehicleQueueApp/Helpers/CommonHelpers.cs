﻿using System;
using System.Data.Entity.Validation;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using VehicleQueueApp.Helpers;
using VehicleQueueApp.Models;
using VehicleQueueApp.Services;



namespace VehicleQueueApp.Helpers
{
    public struct DateTimeSpan
    {
        private readonly int years;
        private readonly int months;
        private readonly int days;
        private readonly int hours;
        private readonly int minutes;
        private readonly int seconds;
        private readonly int milliseconds;

        public DateTimeSpan(int years, int months, int days, int hours, int minutes, int seconds, int milliseconds)
        {
            this.years = years;
            this.months = months;
            this.days = days;
            this.hours = hours;
            this.minutes = minutes;
            this.seconds = seconds;
            this.milliseconds = milliseconds;
        }

        public int Years { get { return years; } }
        public int Months { get { return months; } }
        public int Days { get { return days; } }
        public int Hours { get { return hours; } }
        public int Minutes { get { return minutes; } }
        public int Seconds { get { return seconds; } }
        public int Milliseconds { get { return milliseconds; } }

        enum Phase { Years, Months, Days, Done }

        public static DateTimeSpan CompareDates(DateTime date1, DateTime date2)
        {
            if (date2 < date1)
            {
                var sub = date1;
                date1 = date2;
                date2 = sub;
            }

            DateTime current = date1;
            int years = 0;
            int months = 0;
            int days = 0;

            Phase phase = Phase.Years;
            DateTimeSpan span = new DateTimeSpan();

            while (phase != Phase.Done)
            {
                switch (phase)
                {
                    case Phase.Years:
                        if (current.AddYears(years + 1) > date2)
                        {
                            phase = Phase.Months;
                            current = current.AddYears(years);
                        }
                        else
                        {
                            years++;
                        }
                        break;
                    case Phase.Months:
                        if (current.AddMonths(months + 1) > date2)
                        {
                            phase = Phase.Days;
                            current = current.AddMonths(months);
                        }
                        else
                        {
                            months++;
                        }
                        break;
                    case Phase.Days:
                        if (current.AddDays(days + 1) > date2)
                        {
                            current = current.AddDays(days);
                            var timespan = date2 - current;
                            span = new DateTimeSpan(years, months, days, timespan.Hours, timespan.Minutes, timespan.Seconds, timespan.Milliseconds);
                            phase = Phase.Done;
                        }
                        else
                        {
                            days++;
                        }
                        break;
                }
            }

            return span;
        }
    }
	public static class CommonHelpers
	{

		public static string GetRedirectionLink(string LinkName)
		{
			string key = EncryptDecryptHelper.Encrypt(LinkName);
			StringWriter writer = new StringWriter();
			HttpContext.Current.Server.UrlEncode(key, writer);
			return string.Format(WebConfig.AppURL + "user/resetpassword?r={0}", writer.ToString());
		}




		public static string AssignBy(IEnumerable<AppEmployee> AssignBy)
		{
			string AssignByName = string.Empty;
			var Name = AssignBy.FirstOrDefault();
			if (Name != null)
				AssignByName = Name.Name;

			return AssignByName;
		}

	






		//public static string RenderPartialToString(string controlName, object viewData) {
		//	ViewPage viewPage = new ViewPage() { ViewContext = new ViewContext() };

		//	viewPage.ViewData = new ViewDataDictionary(viewData);
		//	viewPage.Controls.Add(viewPage.LoadControl(controlName));

		//	StringBuilder sb = new StringBuilder();
		//	using (StringWriter sw = new StringWriter(sb)) {
		//		using (HtmlTextWriter tw = new HtmlTextWriter(sw)) {
		//			viewPage.RenderControl(tw);
		//		}
		//	}

		//	return sb.ToString();
		//}


		public static string RenderPartialToString(string controlName, object viewData)
		{
			ViewRenderer render = new ViewRenderer();
			return render.RenderPartialView(controlName, viewData);
		}


		public static string dbErrorDetail(DbEntityValidationException e)
		{
			string error = string.Empty;
			foreach (var eve in e.EntityValidationErrors)
			{
				string x = string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State);
				error += x;
				foreach (var ve in eve.ValidationErrors)
				{
					string abc = string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
					error += abc;
				}
			}

			return error;
		}



	}


	/// <summary>
	/// Class that renders MVC views to a string using the
	/// standard MVC View Engine to render the view. 
	/// </summary>
	public class ViewRenderer
	{
		/// <summary>
		/// Required Controller Context
		/// </summary>
		protected ControllerContext Context { get; set; }

		/// <summary>
		/// Initializes the ViewRenderer with a Context.
		/// </summary>
		/// <param name="controllerContext">
		/// If you are running within the context of an ASP.NET MVC request pass in
		/// the controller's context. 
		/// Only leave out the context if no context is otherwise available.
		/// </param>
		public ViewRenderer(ControllerContext controllerContext = null)
		{
			// Create a known controller from HttpContext if no context is passed
			if (controllerContext == null)
			{
				if (HttpContext.Current != null)
					controllerContext = CreateController<Controllers.HomeController>().ControllerContext;
				else
					throw new InvalidOperationException(
						"ViewRenderer must run in the context of an ASP.NET " +
						"Application and requires HttpContext.Current to be present.");
			}
			Context = controllerContext;
		}

		/// <summary>
		/// Renders a full MVC view to a string. Will render with the full MVC
		/// View engine including running _ViewStart and merging into _Layout        
		/// </summary>
		/// <param name="viewPath">
		/// The path to the view to render. Either in same controller, shared by 
		/// name or as fully qualified ~/ path including extension
		/// </param>
		/// <param name="model">The model to render the view with</param>
		/// <returns>String of the rendered view or null on error</returns>
		public string RenderView(string viewPath, object model)
		{
			return RenderViewToStringInternal(viewPath, model, false);
		}


		/// <summary>
		/// Renders a partial MVC view to string. Use this method to render
		/// a partial view that doesn't merge with _Layout and doesn't fire
		/// _ViewStart.
		/// </summary>
		/// <param name="viewPath">
		/// The path to the view to render. Either in same controller, shared by 
		/// name or as fully qualified ~/ path including extension
		/// </param>
		/// <param name="model">The model to pass to the viewRenderer</param>
		/// <returns>String of the rendered view or null on error</returns>
		public string RenderPartialView(string viewPath, object model)
		{
			return RenderViewToStringInternal(viewPath, model, true);
		}

		/// <summary>
		/// Renders a partial MVC view to string. Use this method to render
		/// a partial view that doesn't merge with _Layout and doesn't fire
		/// _ViewStart.
		/// </summary>
		/// <param name="viewPath">
		/// The path to the view to render. Either in same controller, shared by 
		/// name or as fully qualified ~/ path including extension
		/// </param>
		/// <param name="model">The model to pass to the viewRenderer</param>
		/// <param name="controllerContext">Active Controller context</param>
		/// <returns>String of the rendered view or null on error</returns>
		public static string RenderView(string viewPath, object model,
										ControllerContext controllerContext)
		{
			ViewRenderer renderer = new ViewRenderer(controllerContext);
			return renderer.RenderView(viewPath, model);
		}

		/// <summary>
		/// Renders a partial MVC view to string. Use this method to render
		/// a partial view that doesn't merge with _Layout and doesn't fire
		/// _ViewStart.
		/// </summary>
		/// <param name="viewPath">
		/// The path to the view to render. Either in same controller, shared by 
		/// name or as fully qualified ~/ path including extension
		/// </param>
		/// <param name="model">The model to pass to the viewRenderer</param>
		/// <param name="controllerContext">Active Controller context</param>
		/// <param name="errorMessage">optional out parameter that captures an error message instead of throwing</param>
		/// <returns>String of the rendered view or null on error</returns>
		public static string RenderView(string viewPath, object model,
										ControllerContext controllerContext,
										out string errorMessage)
		{
			errorMessage = null;
			try
			{
				ViewRenderer renderer = new ViewRenderer(controllerContext);
				return renderer.RenderView(viewPath, model);
			}
			catch (Exception ex)
			{
				errorMessage = ex.GetBaseException().Message;
			}
			return null;
		}

		/// <summary>
		/// Renders a partial MVC view to string. Use this method to render
		/// a partial view that doesn't merge with _Layout and doesn't fire
		/// _ViewStart.
		/// </summary>
		/// <param name="viewPath">
		/// The path to the view to render. Either in same controller, shared by 
		/// name or as fully qualified ~/ path including extension
		/// </param>
		/// <param name="model">The model to pass to the viewRenderer</param>
		/// <param name="controllerContext">Active controller context</param>
		/// <returns>String of the rendered view or null on error</returns>
		public static string RenderPartialView(string viewPath, object model,
												ControllerContext controllerContext)
		{
			ViewRenderer renderer = new ViewRenderer(controllerContext);
			return renderer.RenderPartialView(viewPath, model);
		}

		/// <summary>
		/// Renders a partial MVC view to string. Use this method to render
		/// a partial view that doesn't merge with _Layout and doesn't fire
		/// _ViewStart.
		/// </summary>
		/// <param name="viewPath">
		/// The path to the view to render. Either in same controller, shared by 
		/// name or as fully qualified ~/ path including extension
		/// </param>
		/// <param name="model">The model to pass to the viewRenderer</param>
		/// <param name="controllerContext">Active controller context</param>
		/// <param name="errorMessage">optional output parameter to receive an error message on failure</param>
		/// <returns>String of the rendered view or null on error</returns>
		public static string RenderPartialView(string viewPath, object model,
												ControllerContext controllerContext,
												out string errorMessage)
		{
			errorMessage = null;
			try
			{
				ViewRenderer renderer = new ViewRenderer(controllerContext);
				return renderer.RenderPartialView(viewPath, model);
			}
			catch (Exception ex)
			{
				errorMessage = ex.GetBaseException().Message;
			}
			return null;
		}

		/// <summary>
		/// Internal method that handles rendering of either partial or 
		/// or full views.
		/// </summary>
		/// <param name="viewPath">
		/// The path to the view to render. Either in same controller, shared by 
		/// name or as fully qualified ~/ path including extension
		/// </param>
		/// <param name="model">Model to render the view with</param>
		/// <param name="partial">Determines whether to render a full or partial view</param>
		/// <returns>String of the rendered view</returns>
		protected string RenderViewToStringInternal(string viewPath, object model, bool partial = false)
		{
			// first find the ViewEngine for this view
			ViewEngineResult viewEngineResult = null;
			if (partial)
				viewEngineResult = ViewEngines.Engines.FindPartialView(Context, viewPath);
			else
				viewEngineResult = ViewEngines.Engines.FindView(Context, viewPath, null);


			//if (viewEngineResult == null)
			//	throw new FileNotFoundException(Resources.ViewCouldNotBeFound);



			// get the view and attach the model to view data
			var view = viewEngineResult.View;
			Context.Controller.ViewData.Model = model;

			string result = null;

			using (var sw = new StringWriter())
			{
				var ctx = new ViewContext(Context, view,
											Context.Controller.ViewData,
											Context.Controller.TempData,
											sw);
				view.Render(ctx, sw);
				result = sw.ToString();
			}

			return result;
		}


		/// <summary>
		/// Creates an instance of an MVC controller from scratch 
		/// when no existing ControllerContext is present       
		/// </summary>
		/// <typeparam name="T">Type of the controller to create</typeparam>
		/// <returns></returns>
		public static T CreateController<T>(RouteData routeData = null)
					where T : Controller, new()
		{
			T controller = new T();

			// Create an MVC Controller Context
			HttpContextBase wrapper = null;
			if (HttpContext.Current != null)
				wrapper = new HttpContextWrapper(System.Web.HttpContext.Current);
			//else
			//    wrapper = CreateHttpContextBase(writer);


			if (routeData == null)
				routeData = new RouteData();

			if (!routeData.Values.ContainsKey("controller") && !routeData.Values.ContainsKey("Controller"))
				routeData.Values.Add("controller", controller.GetType().Name
															.ToLower()
															.Replace("controller", ""));

			controller.ControllerContext = new ControllerContext(wrapper, routeData, controller);
			return controller;
		}
	}

}