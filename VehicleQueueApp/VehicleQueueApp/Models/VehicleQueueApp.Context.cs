﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace VehicleQueueApp.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class VehicleQueueAppEntities : DbContext
    {
        public VehicleQueueAppEntities()
            : base("name=VehicleQueueAppEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<AppEmployeeAgency> AppEmployeeAgencies { get; set; }
        public virtual DbSet<AppEmployeeDepartment> AppEmployeeDepartments { get; set; }
        public virtual DbSet<AppEmployee> AppEmployees { get; set; }
        public virtual DbSet<AppEmployeeService> AppEmployeeServices { get; set; }
        public virtual DbSet<AppModuleConfig> AppModuleConfigs { get; set; }
        public virtual DbSet<AppModulePermission> AppModulePermissions { get; set; }
        public virtual DbSet<AppModule> AppModules { get; set; }
        public virtual DbSet<AppRole> AppRoles { get; set; }
        public virtual DbSet<AppRolesModule> AppRolesModules { get; set; }
        public virtual DbSet<AppUserLog> AppUserLogs { get; set; }
        public virtual DbSet<AppUserMeta> AppUserMetas { get; set; }
        public virtual DbSet<AppUserPwdRequest> AppUserPwdRequests { get; set; }
        public virtual DbSet<AppUser> AppUsers { get; set; }
        public virtual DbSet<Bank> Banks { get; set; }
        public virtual DbSet<BankChallan> BankChallans { get; set; }
        public virtual DbSet<BankChallanDetail> BankChallanDetails { get; set; }
        public virtual DbSet<CancelStatu> CancelStatus { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<CategoryFee> CategoryFees { get; set; }
        public virtual DbSet<Day> Days { get; set; }
        public virtual DbSet<Operation> Operations { get; set; }
        public virtual DbSet<PlaceOfVisit> PlaceOfVisits { get; set; }
        public virtual DbSet<PurposeOfVisit> PurposeOfVisits { get; set; }
        public virtual DbSet<Shift> Shifts { get; set; }
        public virtual DbSet<ShiftDetail> ShiftDetails { get; set; }
        public virtual DbSet<SystemConfig> SystemConfigs { get; set; }
        public virtual DbSet<Token> Tokens { get; set; }
        public virtual DbSet<TokenCancelledRequestDetail> TokenCancelledRequestDetails { get; set; }
        public virtual DbSet<TokenCancelRequest> TokenCancelRequests { get; set; }
        public virtual DbSet<TokenDetail> TokenDetails { get; set; }
        public virtual DbSet<TokenTransaction> TokenTransactions { get; set; }
    }
}
