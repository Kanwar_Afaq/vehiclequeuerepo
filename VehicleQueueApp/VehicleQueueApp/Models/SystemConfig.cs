//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace VehicleQueueApp.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class SystemConfig
    {
        public int SystemConfigId { get; set; }
        public string SystemConfigKey { get; set; }
        public string SystemConfigValue { get; set; }
    }
}
