﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VehicleQueueApp.Models
{
    [MetadataType(typeof(TokenMetaData))]
    public partial class Token
    {
      
        public int CategoryId { get; set; }
       
        public string IndividualNarration { get; set; }
    }
    public class TokenMetaData
    {
     
    }

    [MetadataType(typeof(TokenDetailMetaData))]
    public partial class TokenDetail
    {

    }
    public class TokenDetailMetaData
    {
        public string Name { get; set; }
        [Required]
        [StringLength(15, MinimumLength = 15, ErrorMessage = "field must be atleast 13 characters")]
        public string NIC { get; set; }
    
        public string Prepaid { get; set; }
    }

    public class tokenDetails
    {
        public int TokenId { get; set; }
        public string TokenNo { get; set; }
        public string Name { get; set; }
        public string NIC { get; set; }
        public string Prepaid { get; set; }
        public bool isApproved { get; set; }

    }

}