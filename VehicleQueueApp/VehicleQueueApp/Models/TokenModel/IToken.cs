﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VehicleQueueApp.Models.TokenModel
{
    interface IToken:IDisposable
    {
        List<Token> GetAllTokens();
     

        int AddToken(int operationId,int shiftid, int dayid, Token tokenobj,List<tokenDetails> tokendetails);
        void deleteToken(int id);
        //bool RequestTokenCancellation(int tokendetailid);
      //  bool CancelToken(int tokendetailid, TokenCancelled tokencancelled);
        Token GetTokenById(int id);
        List<PlaceOfVisit> GetAllPlacesofVisit();

        List<PurposeOfVisit> GetAllPurposeofVisit();
       
    }
  
}
