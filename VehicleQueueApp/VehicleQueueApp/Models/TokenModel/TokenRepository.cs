﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VehicleQueueApp.Helpers;
using VehicleQueueApp.Models.OperationModel;
using VehicleQueueApp.Models.TokenModel;

namespace VehicleQueueApp.Models
{
    public class TokenRepository : IToken
    {
        public VehicleQueueAppEntities _context = new VehicleQueueAppEntities();
        public OperationRepository operationRepository = new OperationRepository();
        public void deleteToken(int id)
        {

            Token Token = _context.Tokens.Find(id);
            _context.Tokens.Remove(Token);
            _context.SaveChanges();
        }
        public int getTimeOutoftokenCancellation()
        {
            var val = _context.SystemConfigs.FirstOrDefault(q=>q.SystemConfigKey== "tokenCancellationTimeOut");
            if (val != null)
            {
                return val.SystemConfigValue.ConvertToInt();
            }
            else
            {
                return 0;
            }
        }
        public bool tokenStatus(int cancelRequestId, string tokenstatus)
        {
            var tokenStatus = _context.TokenCancelRequests.FirstOrDefault(q => q.TokenCancelRequestId == cancelRequestId && q.CancelStatusId == 1);

            if (tokenStatus != null)
            {
                switch (tokenstatus)
                {
                    case "Approved":
                        tokenStatus.CancelStatusId = 2;

                        _context.SaveChanges();

                        return true;
                        break;
                    case "Reject":
                        tokenStatus.CancelStatusId = 3;
                        _context.SaveChanges();
                        return true;

                }



            }
            return false;
        }
        public List<TokenDetail> getTokenDetails(int tokenId)
        {
            var tokendetails = _context.TokenDetails.Where(t => t.TokenId == tokenId);
            if (tokendetails != null)
            {
                return tokendetails.ToList();
            }
            else
            {
                return null;
            }


        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public List<PlaceOfVisit> GetAllPlacesofVisit()
        {
            var qry = (from G in _context.PlaceOfVisits
                       orderby G.PlaceOfVisitId descending
                       select G).ToList();
            return qry;
        }
        public List<PurposeOfVisit> GetAllPurposeofVisit()
        {
            var qry = (from G in _context.PurposeOfVisits
                       select G).ToList();
            return qry;
        }
        public List<Token> GetAllTokens()
        {
            var qry = (from G in _context.Tokens
                       orderby G.TokenId descending
                       select G).ToList();
            return qry;
        }
        public Token GetTokenById(int id)
        {
            var qry = (from G in _context.Tokens
                       where G.TokenId == id
                       orderby G.TokenId descending
                       select G);
            if (qry != null)
            {
                return qry.FirstOrDefault();
            }
            else
            {
                return null;
            }
        }
        public List<Token> getTokenByShiftId(int Operationid)
        {
            var token = _context.Tokens.Where(t => t.OperationId == Operationid);
            return token.ToList();
        }

        public List<TokenCancelRequest> gettokenCancelrequest(int userId)
        {
            var tokenrequest = _context.TokenCancelRequests.Where(q => q.CreatedBy == userId);
            return tokenrequest.ToList();
        }


        public int AddToken(int operationId, int shiftid, int dayid, Token tokenobj, List<tokenDetails> tokendetailsobj)
        {
            if(Auth.UserID>0)
            {
                if (operationId > 0 && tokenobj.CategoryId>0)
                {
                    DateTime today = System.DateTime.Now;
                    Token token = new Token();
                    token.OperationId = operationId;
                    token.PlaceOfVisitId = tokenobj.PlaceOfVisitId;
                    token.PurposeOfVisit = tokenobj.PurposeOfVisit;
                    token.Noofpassengers = tokenobj.Noofpassengers;
                    token.IssueDateTime = System.DateTime.Now;
                    token.TokenValidity = today.AddHours(6);
                    token.VehiclePlateNo = tokenobj.VehiclePlateNo;
                    token.CategoryId = tokenobj.CategoryId;
                    string tokenno = today.Day.ToString() + today.Second + today.Millisecond;
                    token.TokenNo = tokenno + "-0";

                    var categoryFees = _context.CategoryFees.ToList();
                    if (categoryFees != null)
                    {
                        var c = categoryFees.Where(q => q.CategoryId == tokenobj.CategoryId).OrderByDescending(q => q.EffectiveDate);
                        token.CategoryFeesId = c.FirstOrDefault().CategoryFeeId;
                        token.IsClosed = false;
                        _context.Tokens.Add(token);
                        if (token.Noofpassengers == 1)
                        {

                            TokenDetail tokendetail = new TokenDetail();
                            tokendetail.Prepaid = tokendetailsobj[0].Prepaid;
                            tokendetail.NIC = tokendetailsobj[0].NIC;
                            tokendetail.TokenNo = tokenno + "-0";
                            tokendetail.Name = tokendetailsobj[0].Name;
                            tokendetail.IsDriver = true;
                            tokendetail.Fees = c.FirstOrDefault().Fees;
                            tokendetail.TokenId = token.TokenId;
                         

                            TokenTransaction tr = new TokenTransaction();
                            tr.Amount = Convert.ToDecimal(c.FirstOrDefault().Fees);
                            tr.isPayIn = true;
                            tr.TokendetailId = tokendetail.TokenDetailsId;
                            tr.OperationId = operationId;
                            tr.CreatedBy = Auth.UserID;
                            tr.CreatedDate = System.DateTime.Now;
                            _context.TokenDetails.Add(tokendetail);
                            _context.TokenTransactions.Add(tr);


                        }
                        else if (token.Noofpassengers > 1)
                        {
                            for (int i = 0; i < token.Noofpassengers; i++)
                            {
                                TokenDetail tokendetail = new TokenDetail();
                                tokendetail.Prepaid = tokendetailsobj[0].Prepaid;
                                tokendetail.NIC = tokendetailsobj[i].NIC;
                                tokendetail.TokenNo = tokenno + "-" + i;
                                tokendetail.Name = tokendetailsobj[i].Name;
                                decimal feess = 0;
                                if (i == 0)//Driver
                                {
                                    tokendetail.IsDriver = true;
                                    tokendetail.Fees = c.FirstOrDefault().Fees;
                                    feess = Convert.ToDecimal(tokendetail.Fees);
                                }
                                else//Individual
                                {
                                    tokendetail.IsDriver = false;
                                    var individualfees = categoryFees.Where(q => q.CategoryId == 2).OrderByDescending(q => q.EffectiveDate);
                                    tokendetail.Fees = individualfees.FirstOrDefault().Fees;
                                    feess = Convert.ToDecimal(tokendetail.Fees);
                                }
                                tokendetail.TokenId = token.TokenId;
                                _context.TokenDetails.Add(tokendetail);
                                TokenTransaction tr = new TokenTransaction();
                                tr.Amount = feess;
                                tr.isPayIn = true;
                                tr.TokenDetail = tokendetail;
                                tr.CreatedBy = Auth.UserID;
                                tr.OperationId = operationId;
                                tr.CreatedDate = System.DateTime.Now;
                               
                                _context.TokenTransactions.Add(tr);
                            }
                        }
                        _context.SaveChanges();

                        int tokenId = token.TokenId;
                        return tokenId;
                    }
                }
            }
            return 0;
        }
        public IEnumerable<Category> getAllCategory()
        {
            return _context.Categories.ToList();

        }
        public bool CancelToken(string tokenNo, List<tokenDetails> tokendetails) // tokenid kai under token detail id hai
        {
            if (tokendetails.Count() > 0)
            {
                TokenCancelRequest tokencancelrequest = new TokenCancelRequest();
                int tokendetailsid = tokendetails.FirstOrDefault().TokenId;  // it is token detail id
                var c = _context.TokenDetails.FirstOrDefault(q => q.TokenDetailsId == tokendetailsid);
                tokencancelrequest.TokenId = c.TokenId;
                tokencancelrequest.NoofTickets = tokendetails.Count();
                tokencancelrequest.CategoryId = c.Token.CategoryFee.CategoryId;
                var str = tokendetails.Select(q => q.TokenId);
                var refundedamount = _context.TokenDetails.Where(q => str.Contains(q.TokenDetailsId));
                if (refundedamount != null)
                {
                    tokencancelrequest.RefundedAmount = refundedamount.Sum(q => q.Fees);
                }
                tokencancelrequest.CancelStatusId = 1;
                tokencancelrequest.CreatedBy = Auth.UserID;
                tokencancelrequest.CreatedDate = System.DateTime.Now;
                _context.TokenCancelRequests.Add(tokencancelrequest);

                foreach (var subitem in tokendetails)
                {
                    TokenCancelledRequestDetail tokencancelrequestdetail = new TokenCancelledRequestDetail();
                    tokencancelrequestdetail.TokenDetailId = subitem.TokenId;
                    tokencancelrequest.TokenCancelledRequestDetails.Add(tokencancelrequestdetail);
                }
                _context.SaveChanges();
            }

            return true;

        }
        public bool AmountRefund(int tokenCancelreqID)
        {

            int operationId = operationRepository.GetActiveOperation();
          
            if (operationId > 0)
            {
                var torefundtokendetailidssss = _context.TokenCancelledRequestDetails.Where(t => t.TokenCancelRequestId == tokenCancelreqID).Select(a => new { a.TokenDetailId, a.TokenDetail.Fees });

                if (torefundtokendetailidssss != null)
                {
                    var tokencancelStatus = _context.TokenCancelRequests.FirstOrDefault(q => q.TokenCancelRequestId == tokenCancelreqID);
                    tokencancelStatus.CancelStatusId = 4;
                    foreach (var item in torefundtokendetailidssss)
                    {
                        var tokenCancel = _context.TokenDetails.FirstOrDefault(q => q.TokenDetailsId == item.TokenDetailId);
                        tokenCancel.isCancel = true;
                        TokenTransaction tr = new TokenTransaction();
                        tr.Amount = Convert.ToDecimal(item.Fees);
                        tr.isPayIn = false;
                        tr.CreatedBy = Auth.UserID;
                        tr.OperationId = operationId;
                        tr.CreatedDate = System.DateTime.Now;
                        tr.TokendetailId = item.TokenDetailId;

                        _context.TokenTransactions.Add(tr);

                    }
                    _context.SaveChanges();
                    return true;
                }
            }
            return false;
        }


        public string getIndividualNarration()
        {
            var narration = _context.Categories.FirstOrDefault(q => q.CategoryId == 2);
            if (narration != null)
            {
                return narration.Narration;
            }
            else
            {
                return null;
            }
        }
        public int? getNoOfPassenger(string TokenNo)
        {
            var noOfPassenger = _context.TokenDetails.FirstOrDefault(q => q.TokenNo == TokenNo);
            return noOfPassenger.Token.Noofpassengers;
        }
        public List<TokenCancelRequestVM> tokenCancelRequest(int tokenRequestId)
        {
            var tokencancelrequest = _context.TokenCancelRequests.FirstOrDefault(t => t.TokenCancelRequestId == tokenRequestId && t.CancelStatusId == 1);

            List<TokenCancelRequestVM> tc = new List<TokenCancelRequestVM>();
            if (tokencancelrequest != null)
            {
                var str = tokencancelrequest.TokenCancelledRequestDetails.Select(q => q.TokenDetailId);
                var query = (from g in _context.TokenDetails
                             where str.Contains(g.TokenDetailsId)
                             select new TokenCancelRequestVM
                             {
                                 tokenCancelrequestID = tokenRequestId,
                                 TokenNo = g.TokenNo,
                                 Name = g.Name,
                                 NIC = g.NIC,
                                 Fees = g.Fees
                             }).ToList();
                return query;
            }
            return null;
        }
        public List<tokenCancelDetailsVM> tokenCancel(string tokenNo, int shiftId)
        {
            var token = _context.TokenDetails.FirstOrDefault(q => q.TokenNo == tokenNo && q.isCancel == false);
            List<tokenCancelDetailsVM> cc = new List<tokenCancelDetailsVM>();
            if (token != null)
            {
                if (token.IsDriver == true && token.Token.CategoryFee.CategoryId == 2)
                {
                    var query = (from g in _context.TokenDetails
                                 where g.TokenId == token.TokenId
                                 select new tokenCancelDetailsVM
                                 {
                                     tokenId = g.TokenDetailsId,
                                     Name = g.Name,
                                     TokenNo = g.TokenNo,
                                     NIC = g.NIC,
                                     Fees = g.Fees

                                 }).ToList();
                    return query;

                }
                else if (token.IsDriver == false && token.Token.CategoryFee.CategoryId != 2)
                {
                    tokenCancelDetailsVM tokencancel = new tokenCancelDetailsVM();
                    tokencancel.tokenId = token.TokenDetailsId;
                    tokencancel.Fees = token.Fees;
                    tokencancel.NIC = token.NIC;
                    tokencancel.Name = token.Name;
                    tokencancel.TokenNo = token.TokenNo;
                    cc.Add(tokencancel);
                    return cc;

                }
                else if (token.IsDriver == true && token.Token.CategoryFee.CategoryId != 2)
                {
                    var query = (from g in _context.TokenDetails
                                 where g.TokenId == token.TokenId
                                 select new tokenCancelDetailsVM
                                 {
                                     tokenId = g.TokenDetailsId,
                                     Name = g.Name,
                                     TokenNo = g.TokenNo,
                                     NIC = g.NIC,
                                     Fees = g.Fees
                                 }).ToList();
                    return query;
                }
            }
            return null;
        }
        public bool compareDateTime(string tokenNo)
        {
            var tokendetails = _context.TokenDetails.FirstOrDefault(q => q.TokenNo == tokenNo);
          
            if(tokendetails != null)
            {
                DateTime currentTime = System.DateTime.Now;
                DateTime tokenIssueDate =Convert.ToDateTime(tokendetails.Token.IssueDateTime);
                var isTokenvalid = currentTime.Subtract(tokenIssueDate).TotalMinutes;
                if(isTokenvalid<= getTimeOutoftokenCancellation())
                {
                    return true;
                }
                
            }
            return false;
        }
        public List<Token> generateToken(int tokenId)
        {
            var token = _context.Tokens.Where(q => q.TokenId == tokenId).ToList();
            if (token != null)
            {
                return token;
            }
            else
            {
                return null;
            }


        }




        public List<TokenCancelRequest> TokenCanceledForManager()
        {
            var tokenCancelrequest = _context.TokenCancelRequests.OrderByDescending(q => q.TokenCancelRequestId).ToList();
            return tokenCancelrequest;
        }
    }

}
