﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VehicleQueueApp.Models.DaysModel;

namespace VehicleQueueApp.Models
{
    public class DaysRepository : IDays
    {
        public VehicleQueueAppEntities _context = new VehicleQueueAppEntities();

        public void deleteDay(int id)
        {
            Day day = _context.Days.Find(id);
            _context.Days.Remove(day);
            _context.SaveChanges();
        }

        public void Dispose()
        {

        }

        public Day getDaybyId(int id)
        {
            Day day = _context.Days.Find(id);
            return day;
        }

        public IEnumerable<Day> GetDays()
        {
            return _context.Days.ToList().OrderByDescending(q=>q.DayId);
        }

        public void insertDay(Day day)
        {
            _context.Days.Add(day);
            _context.SaveChanges();
        }

        public void updateDay(Day day)
        {
            _context.Entry(day).State = System.Data.Entity.EntityState.Modified;
            _context.SaveChanges();
        }
        public void StartDay(int userId)
        {
            if (userId > 0)
            {
                DateTime startDateTime = System.DateTime.Now;
                Day day = new Day();
                day.StartedBy = userId;
                day.StartDateTime = startDateTime;
                day.FinalAmount = 0;
                day.CreatedBy = userId;
                day.CreatedDate = System.DateTime.Now;
                _context.Days.Add(day);

                Shift shift = new Shift();
                shift.ShiftStartedBy = userId;
                shift.ShiftStartingDatetime = startDateTime;
                shift.CreatedBy = userId;
                shift.CreatedDate = System.DateTime.Now;
                day.Shifts.Add(shift);

                _context.SaveChanges();
            }

        }
        public void CloseDay(int dayId, int userId, int shiftId)
        {
            var dayclosed = _context.Days.FirstOrDefault(q => q.DayId == dayId);
            if (dayclosed != null)  //completed
            {
                dayclosed.IsDayClosed = true;
                dayclosed.EndingDateTime = System.DateTime.Now;
                decimal? finaldayAmountcalculated = 0;
                var add = _context.TokenTransactions.Where(q => q.Operation.Shift.DayId == dayId && q.isPayIn == true);
                var sub = _context.TokenTransactions.Where(q => q.Operation.Shift.DayId == dayId && q.isPayIn == false);
                finaldayAmountcalculated = (add.Count() > 0 ? add.Sum(q => q.Amount) : 0) - (sub.Count() > 0 ? sub.Sum(q => q.Amount) : 0);

                dayclosed.FinalAmount = Convert.ToDecimal(finaldayAmountcalculated);

                if (shiftId > 0) // completed
                {
                    var shiftcolsed = _context.Shifts.FirstOrDefault(q => q.ShiftId == shiftId);
                    if (shiftcolsed != null)
                    {
                        shiftcolsed.ShiftClosed = true;
                        shiftcolsed.ShiftEndingDatetime = System.DateTime.Now;
                        decimal? finalshiftAmountcalculated = 0;
                        var addshift = _context.TokenTransactions.Where(q => q.Operation.ShiftId == shiftId && q.isPayIn == true);
                        var subshift = _context.TokenTransactions.Where(q => q.Operation.ShiftId == shiftId && q.isPayIn == false);
                        finalshiftAmountcalculated = (addshift.Count() > 0 ? addshift.Sum(q => q.Amount) : 0) - (subshift.Count() > 0 ? subshift.Sum(q => q.Amount) : 0);
                        shiftcolsed.AmountToBeCollected = Convert.ToDecimal(finalshiftAmountcalculated);
                    }
                    var operationclosed = _context.Operations.Where(q => q.ShiftId == shiftId && q.OpeartionClosed == false).ToList();
                    if (operationclosed != null)
                    {
                        foreach (var item in operationclosed)
                        {
                            item.OpeartionClosed = true;
                            item.OperationEndingDateTime = System.DateTime.Now;
                            decimal? finaloperationAmountcalculated = 0;
                            var addoperation = _context.TokenTransactions.Where(q => q.OperationId == item.OperationId && q.isPayIn == true);
                            var suboperation = _context.TokenTransactions.Where(q => q.OperationId == item.OperationId && q.isPayIn == false);
                            finaloperationAmountcalculated = (addoperation.Count() > 0 ? addoperation.Sum(q => q.Amount) : 0) - (suboperation.Count() > 0 ? suboperation.Sum(q => q.Amount) : 0);
                            item.TotalAmountinOperation = Convert.ToDecimal(finaloperationAmountcalculated);
                        }
                    }
                    var token = _context.Tokens.Where(q => q.Operation.ShiftId == shiftId).ToList();
                    if (token.Any())
                    {
                        token.ForEach(q => q.IsClosed = true);
                    }
                }// shiftclosed
            } // dayclsoed
            _context.SaveChanges();
        }
        public int GetActiveDay()
        {
            var activeDay = _context.Days.FirstOrDefault(x => x.IsDayClosed == false);
            if (activeDay != null)
            {
                return activeDay.DayId;
            }
            return 0;
        }
        public int GetActiveShift()
        {
            var activeShift = _context.Shifts.FirstOrDefault(x => x.ShiftClosed == false);
            if (activeShift != null)
            {
                return activeShift.ShiftId;
            }
            return 0;
        }


    }
}