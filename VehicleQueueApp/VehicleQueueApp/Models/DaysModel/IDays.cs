﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VehicleQueueApp.Models;
namespace VehicleQueueApp.Models.DaysModel
{
    interface IDays:IDisposable
    {
        IEnumerable<Day> GetDays();
        void updateDay(Day day);
        
        void insertDay(Day day);
        void deleteDay(int id);
      
        Day getDaybyId(int id);
 
    }
}
