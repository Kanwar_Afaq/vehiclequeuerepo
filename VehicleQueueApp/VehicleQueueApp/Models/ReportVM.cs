﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VehicleQueueApp.Models
{
    public class ReportVM
    {

        public int? BankChallanId { get; set; }
        public int CategoryId { get; set; }
        public int TotalNoOftoken { get; set; }
        public decimal? TotalAmount { get; set; }
        public decimal? CategoryFees { get; set; }
        public string CategoryName { get; set; }
        public string ChallanNo { get; set; }

        public DateTime? BankChallanDateTime { get; set; }

    }

    public class CategoryViewModel
    {
        public int CategoryId { get; set; }
        public string categoryName { get; set; }
        public int TicketIssued { get; set; }
    }
    public class TotalCollectionVM
    {
        public int CategoryId { get; set; }
        public string categoryName { get; set; }
        public string Narration { get; set; }

        public int TicketIssued { get; set; }


        public int TicketFeesint { get; set; }


        public decimal? TicketFeesdecimal { get; set; }

        public decimal? Amountpercategorydecimal { get; set; }

        public int Amountpercategoryint { get; set; }

        public string ClassName { get; set; }
    }
}