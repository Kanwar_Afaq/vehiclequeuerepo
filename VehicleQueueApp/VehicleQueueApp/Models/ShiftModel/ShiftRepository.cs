﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VehicleQueueApp.Helpers;
using VehicleQueueApp.Models.ShiftModel;

namespace VehicleQueueApp.Models
{
    public class ShiftRepository : IShift
    {
        public VehicleQueueAppEntities _context = new VehicleQueueAppEntities();

        public void deleteShift(int id)
        {
            Shift shift = _context.Shifts.Find(id);
            _context.Shifts.Remove(shift);
            _context.SaveChanges();
        }

        public void Dispose()
        {
          
        }

        public Shift getShiftbyId(int id)
        {
            Shift shift = _context.Shifts.Find(id);
            return shift;
        }

        public IEnumerable<Shift> GetShifts()
        {
            return _context.Shifts.ToList();
        }

        public void insertShift(Shift shift)
        {
            _context.Shifts.Add(shift);
            _context.SaveChanges();
        }

        public void updateShift(Shift shift)
        {
            _context.Entry(shift).State = System.Data.Entity.EntityState.Modified;
            _context.SaveChanges();
        }
        public void StartShift(int userId,int dayid)
        {
            if (userId > 0)
            {
                DateTime ShiftStartDateTime = System.DateTime.Now;

                Shift shift = new Shift();
                shift.ShiftStartedBy = userId;
                shift.ShiftStartingDatetime = ShiftStartDateTime;
                shift.DayId = dayid;
                shift.CreatedBy = userId;
                shift.CreatedDate = System.DateTime.Now;
                _context.Shifts.Add(shift);

                _context.SaveChanges();
            }

        }
        //public string checkShiftButtonVisibility()
        //{
        //    int activeDay= GetActiveDay();
        //    if(activeDay>0)
        //    {
        //        int activeShift = GetActiveShift();
        //        if(activeShift>0)
        //        {
        //            return "0";//if any shif has open then visible close button on view
        //        }
        //        else
        //        {
        //            return "1";
        //        }
        //    }
        //    return "2";
        //}
        public void CloseShift(int shiftId, int dayId, int userId)
        {

            var shiftcolsed = _context.Shifts.FirstOrDefault(q => q.ShiftId == shiftId);
            if (shiftcolsed!=null)
            {
                shiftcolsed.ShiftClosed = true;
                decimal? finalshiftAmountcalculated = 0;
                var addshift = _context.TokenTransactions.Where(q => q.Operation.ShiftId == shiftId && q.isPayIn == true);
                var subshift = _context.TokenTransactions.Where(q => q.Operation.ShiftId == shiftId && q.isPayIn == false);
                finalshiftAmountcalculated = (addshift.Count() >0 ?  addshift.Sum(q => q.Amount) :0) - (subshift.Count() >0 ? subshift.Sum(q => q.Amount) : 0);
                shiftcolsed.AmountToBeCollected = Convert.ToDecimal(finalshiftAmountcalculated);

                var operationclosed = _context.Operations.Where(q => q.ShiftId == shiftId && q.OpeartionClosed == false).ToList();
                if (operationclosed != null)
                {
                    decimal? finaloperationAmountcalculated = 0;
                    foreach (var item in operationclosed)
                    {
                        item.OpeartionClosed = true;
                        item.OperationEndingDateTime = System.DateTime.Now;
                        var addoperation = _context.TokenTransactions.Where(q => q.OperationId == item.OperationId && q.isPayIn == true);
                        var suboperation = _context.TokenTransactions.Where(q => q.OperationId == item.OperationId && q.isPayIn == false);
                        finaloperationAmountcalculated =(addoperation.Count() >0 ? addoperation.Sum(q => q.Amount) :0) - (suboperation.Count() > 0 ?  suboperation.Sum(q => q.Amount) : 0);
                        item.TotalAmountinOperation = Convert.ToDecimal(finaloperationAmountcalculated);
                    }
                }
                var token = _context.Tokens.Where(q => q.Operation.ShiftId == shiftId).ToList();
                if (token.Any())
                {
                    token.ForEach(q => q.IsClosed = true);
                  
                }
                _context.SaveChanges();
            }
            
        }
        public int GetActiveDay()
        {
           
            var activeDay = _context.Days.FirstOrDefault(x => x.IsDayClosed == false);
            if (activeDay != null)
            {
                return activeDay.DayId;
            }
            else
            {
                return 0;

            }

            }
        


    
        
        public int GetCLoseDay()
        {
            var CLoseDay = _context.Days.FirstOrDefault(x => x.IsDayClosed == true);
            if (CLoseDay != null)
            {
                return CLoseDay.DayId;
            }
            else
            {
                return 0;
            }

        }
        public int GetActiveShift()
        {
          
                var activeShift = _context.Shifts.FirstOrDefault(x => x.ShiftClosed == false);
                if (activeShift != null)
                {
                    return activeShift.ShiftId;
                }
                else
                {
                    return 0;
                }
          
       

        }

        public void AddShiftdetail()
        {
            int shiftId = GetActiveShift();
            if (shiftId > 0)
            {
                ShiftDetail shiftDetail = new ShiftDetail();

                shiftDetail.ShiftId =shiftId;
                shiftDetail.UserLoggedIn = Auth.UserID;
                shiftDetail.UserLoggedInDateTime = System.DateTime.Now;
                // shiftDetail.UserLoggedOut = Auth.UserID;//logout
                // shiftDetail.UserLoggedOutDateTime = System.DateTime.Now;
                shiftDetail.CreatedDate = System.DateTime.Now;
                shiftDetail.CreatedBy = Auth.UserID;
                _context.ShiftDetails.Add(shiftDetail);
                _context.SaveChanges();
            }
           
          
        }
    }
}