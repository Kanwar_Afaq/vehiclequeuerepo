﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VehicleQueueApp.Models.ShiftModel
{
    interface IShift:IDisposable
    {
        IEnumerable<Shift> GetShifts();
        void updateShift(Shift shift);

        void insertShift(Shift shift);
        void deleteShift(int id);

        Shift getShiftbyId(int id);
        void AddShiftdetail();

    }
}
