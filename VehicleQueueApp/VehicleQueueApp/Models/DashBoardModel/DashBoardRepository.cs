﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VehicleQueueApp.Helpers;
using VehicleQueueApp.Models;
using VehicleQueueApp.Models.OperationModel;

namespace VehicleQueueApp
{
    public class DashBoardRepository
    {
        public VehicleQueueAppEntities _context = new VehicleQueueAppEntities();
        public DaysRepository _dayrepository = new DaysRepository();
        public OperationRepository _operationRepository = new OperationRepository();
        public ShiftRepository _shiftRepository = new ShiftRepository();
        public decimal getTotaltokensGeneratedToday(int dayId)
        {
            decimal totalToken = _context.TokenDetails.Where(q => q.Token.Operation.Shift.Day.DayId == dayId).Count();
            if (totalToken > 0)
            {
                return Convert.ToDecimal(totalToken);
            }
            else
            {
                return 0;
            }

        }
        public List<Category> getCategories()
        {
            var Categories = _context.Categories.ToList();
            return Categories;
        }
        public List<TotalCollectionVM> CategoryFee()
        {

            var dt = System.DateTime.Now;
            int activeday = _dayrepository.GetActiveDay();
            var categoryFee = (from c in _context.Categories
                               join d in _context.CategoryFees
                               on c.CategoryId equals d.CategoryId
                               join t in _context.Tokens
                               on d.CategoryFeeId equals t.CategoryFeesId
                               join td in _context.TokenDetails
                               on t.TokenId equals td.TokenId

                               group d by c.CategoryId into g
                               select new TotalCollectionVM
                               {

                                   CategoryId = g.Key,
                                   TicketFeesdecimal = g.FirstOrDefault().Fees,
                                   TicketIssued = _context.TokenDetails.Where(q => q.Token.CategoryFee.CategoryId == g.Key && q.Token.Operation.Shift.Day.DayId == activeday).Count(),
                                   Amountpercategorydecimal = _context.TokenDetails.Where(q => q.Token.CategoryFee.CategoryId == g.Key && q.Token.Operation.Shift.Day.DayId == activeday).Sum(q => q.Fees),
                                   ClassName = g.FirstOrDefault().ClassName,
                                   categoryName = g.FirstOrDefault().Category.CategoryName,
                                   Narration = g.FirstOrDefault().Category.Narration

                                 





                               }).ToList().Select(query => new TotalCollectionVM()
                                 {


                                   CategoryId = query.CategoryId,
                                   TicketFeesint = Convert.ToInt32(query.TicketFeesdecimal),
                                   TicketIssued = query.TicketIssued,
                                   Amountpercategoryint = Convert.ToInt32(query.Amountpercategorydecimal),
                                   ClassName = query.ClassName,
                                   categoryName = query.categoryName,
                                   Narration = query.Narration



                               });

            if (categoryFee!=null)
            {
                return categoryFee.ToList();
            }
            return null;
        }
        public List<TotalCollectionVM> CategoryFeeCollection()
        {

            var dt = System.DateTime.Now;
            int activeday = _dayrepository.GetActiveDay();
            var categoryFee = (from c in _context.Categories
                               join d in _context.CategoryFees
                               on c.CategoryId equals d.CategoryId
                               join t in _context.Tokens
                               on d.CategoryFeeId equals t.CategoryFeesId
                               join td in _context.TokenDetails
                               on t.TokenId equals td.TokenId

                               group d by c.CategoryId into g
                               select new TotalCollectionVM
                               {

                                   CategoryId = g.Key,
                                   TicketFeesdecimal = g.FirstOrDefault().Fees,
                                   TicketIssued = _context.TokenDetails.Where(q => q.Token.CategoryFee.CategoryId == g.Key && q.Token.Operation.Shift.Day.DayId == activeday).Count(),
                                   Amountpercategorydecimal = _context.TokenDetails.Where(q => q.Token.CategoryFee.CategoryId == g.Key && q.Token.Operation.Shift.Day.DayId == activeday).Sum(q => q.Fees),
                                   ClassName = g.FirstOrDefault().ClassName,
                                   categoryName = g.FirstOrDefault().Category.CategoryName,
                                   Narration = g.FirstOrDefault().Category.Narration







                               }).ToList().Select(query => new TotalCollectionVM()
                               {


                                   CategoryId = query.CategoryId,
                                   TicketFeesint = Convert.ToInt32(query.TicketFeesdecimal),
                                   TicketIssued = query.TicketIssued,
                                   Amountpercategoryint = Convert.ToInt32(query.Amountpercategorydecimal),
                                   ClassName = query.ClassName,
                                   categoryName = query.categoryName,
                                   Narration = query.Narration



                               });

            if (categoryFee != null)
            {
                return categoryFee.ToList();
            }
            return null;
        }
        public List<TotalCollectionVM> CategoryFeeByOperation()
        {

            var dt = System.DateTime.Now;
            int userId = Auth.UserID;
            int activeShift = _shiftRepository.GetActiveShift();
            int activeOpeartion = _operationRepository.getLiveOpeartionId(userId,activeShift);
            var categoryFee = (from c in _context.Categories
                               join d in _context.CategoryFees
                               on c.CategoryId equals d.CategoryId
                               join t in _context.Tokens
                               on d.CategoryFeeId equals t.CategoryFeesId
                               join td in _context.TokenDetails
                               on t.TokenId equals td.TokenId

                               group d by c.CategoryId into g
                               select new TotalCollectionVM
                               {
                                   CategoryId = g.Key,
                                   TicketFeesdecimal = g.FirstOrDefault().Fees,
                                   TicketIssued = _context.TokenDetails.Where(q => q.Token.CategoryFee.CategoryId == g.Key && q.Token.Operation.OperationId == activeOpeartion).Count(),
                                   Amountpercategorydecimal = _context.TokenDetails.Where(q => q.Token.CategoryFee.CategoryId == g.Key && q.Token.Operation.OperationId == activeOpeartion).Sum(q => q.Fees),
                                   ClassName = g.FirstOrDefault().ClassName,
                                   categoryName = g.FirstOrDefault().Category.CategoryName,
                                   Narration = g.FirstOrDefault().Category.Narration




                               }).ToList().Select(query => new TotalCollectionVM()
                               {
                                   CategoryId = query.CategoryId,
                                   TicketFeesint = Convert.ToInt32(query.TicketFeesdecimal),
                                   TicketIssued = query.TicketIssued,
                                   Amountpercategoryint = Convert.ToInt32(query.Amountpercategorydecimal),
                                   ClassName = query.ClassName,
                                   categoryName = query.categoryName,
                                   Narration = query.Narration

                               });

            if (categoryFee != null)
            {
                return categoryFee.ToList();
            }
            return null;
        }
        public decimal getTotaltodaysIncome(int dayId)
        {
            var totalIndividual = _context.TokenTransactions.Where(q=>q.Operation.Shift.Day.DayId == dayId && q.isPayIn == true).ToList();
            var totalIndividual2 = _context.TokenTransactions.Where(q =>q.Operation.Shift.Day.DayId == dayId && q.isPayIn == false).ToList();
            if (totalIndividual!=null && totalIndividual2!=null )
            {
                decimal? finaltotalDayAmountcalculated = 0;

                finaltotalDayAmountcalculated = (totalIndividual.Count() > 0 ? totalIndividual.Sum(q => q.Amount) : 0) - (totalIndividual2.Count() > 0 ? totalIndividual2.Sum(q => q.Amount) : 0);

                return Convert.ToDecimal(finaltotalDayAmountcalculated);
            }
            else
            {
                return 0;
            }
        }
        public decimal getTodaysIndividualIncome(int dayId)
        {
           var totalIndividual = _context.TokenTransactions.Where(q => q.TokenDetail.Token.CategoryFee.Category.CategoryId==2 && q.Operation.Shift.Day.DayId == dayId && q.isPayIn==true).ToList();
            var totalIndividual2 = _context.TokenTransactions.Where(q => q.TokenDetail.Token.CategoryFee.Category.CategoryId == 2 && q.Operation.Shift.Day.DayId == dayId && q.isPayIn == false).ToList();
          
            if (totalIndividual!=null && totalIndividual2!=null)
            {
                decimal? finaloperationAmountcalculated = 0;

                finaloperationAmountcalculated = (totalIndividual.Count() > 0 ? totalIndividual.Sum(q => q.Amount) : 0) - (totalIndividual2.Count() > 0 ? totalIndividual2.Sum(q => q.Amount) : 0);

                return Convert.ToDecimal(finaloperationAmountcalculated);
            }
            else
            {
                return 0;
            }
        }
        public decimal getTodaysTruckIncome(int dayId)
        {
            var totalIndividual = _context.TokenTransactions.Where(q => q.TokenDetail.Token.CategoryFee.Category.CategoryId == 10 && q.Operation.Shift.Day.DayId == dayId && q.isPayIn == true).ToList();
            var totalIndividual2 = _context.TokenTransactions.Where(q => q.TokenDetail.Token.CategoryFee.Category.CategoryId == 10 && q.Operation.Shift.Day.DayId == dayId && q.isPayIn == false).ToList();

            if (totalIndividual != null && totalIndividual2 != null)
            {
                decimal? finalTruckAmountcalculated = 0;

                finalTruckAmountcalculated = (totalIndividual.Count() > 0 ? totalIndividual.Sum(q => q.Amount) : 0) - (totalIndividual2.Count() > 0 ? totalIndividual2.Sum(q => q.Amount) : 0);

                return Convert.ToDecimal(finalTruckAmountcalculated);
            }
            else
            {
                return 0;
            }
        }
        public decimal getTodaysCarIncome(int dayId)
        {
            var totalIndividual = _context.TokenTransactions.Where(q => q.TokenDetail.Token.CategoryFee.Category.CategoryId == 8 && q.Operation.Shift.Day.DayId == dayId && q.isPayIn == true).ToList();
            var totalIndividual2 = _context.TokenTransactions.Where(q => q.TokenDetail.Token.CategoryFee.Category.CategoryId == 8 && q.Operation.Shift.Day.DayId == dayId && q.isPayIn == false).ToList();

            if (totalIndividual != null && totalIndividual2 != null)
            {
                decimal? finalCarAmountcalculated = 0;

                finalCarAmountcalculated = (totalIndividual.Count() > 0 ? totalIndividual.Sum(q => q.Amount) : 0) - (totalIndividual2.Count() > 0 ? totalIndividual2.Sum(q => q.Amount) : 0);

                return Convert.ToDecimal(finalCarAmountcalculated);
            }
            else
            {
                return 0;
            }
        }
        public decimal getTodaysMotorCycleIncome(int dayId)
        {
            var totalIndividual = _context.TokenTransactions.Where(q => q.TokenDetail.Token.CategoryFee.Category.CategoryId == 3 && q.Operation.Shift.Day.DayId == dayId && q.isPayIn == true).ToList();
            var totalIndividual2 = _context.TokenTransactions.Where(q => q.TokenDetail.Token.CategoryFee.Category.CategoryId == 3 && q.Operation.Shift.Day.DayId == dayId && q.isPayIn == false).ToList();

            if (totalIndividual != null && totalIndividual2 != null)
            {
                decimal? finalMotorCycleAmountcalculated = 0;

                finalMotorCycleAmountcalculated = (totalIndividual.Count() > 0 ? totalIndividual.Sum(q => q.Amount) : 0) - (totalIndividual2.Count() > 0 ? totalIndividual2.Sum(q => q.Amount) : 0);

                return Convert.ToDecimal(finalMotorCycleAmountcalculated);
            }
            else
            {
                return 0;
            }
        }
        public decimal getTodaysNonCommercialIncome(int dayId)
        {
            var totalIndividual = _context.TokenTransactions.Where(q => q.TokenDetail.Token.CategoryFee.Category.CategoryId == 4 && q.Operation.Shift.Day.DayId == dayId && q.isPayIn == true).ToList();
            var totalIndividual2 = _context.TokenTransactions.Where(q => q.TokenDetail.Token.CategoryFee.Category.CategoryId == 4 && q.Operation.Shift.Day.DayId == dayId && q.isPayIn == false).ToList();

            if (totalIndividual != null && totalIndividual2 != null)
            {
                decimal? finalMotorCycleAmountcalculated = 0;

                finalMotorCycleAmountcalculated = (totalIndividual.Count() > 0 ? totalIndividual.Sum(q => q.Amount) : 0) - (totalIndividual2.Count() > 0 ? totalIndividual2.Sum(q => q.Amount) : 0);

                return Convert.ToDecimal(finalMotorCycleAmountcalculated);
            }
            else
            {
                return 0;
            }
        }

        public decimal getTokenCancelAmount(int dayId)
        {
            var totalIndividual2 = _context.TokenTransactions.Where(q => q.TokenDetail.isCancel == true && q.Operation.Shift.Day.DayId == dayId && q.isPayIn == false).ToList();
    
             if(totalIndividual2!=null)
            {
                return totalIndividual2.Sum(q => q.Amount);
            }
            else
            {
                return 0;
            }
            
        }
    }
}