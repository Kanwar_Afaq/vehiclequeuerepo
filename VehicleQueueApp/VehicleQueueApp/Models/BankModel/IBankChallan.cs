﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VehicleQueueApp.Models
{
    interface IBankChallan
    {
        List<BankChallan> GetAllBankChallans();


        bool AddChallan(int bankId);
        void deleteBankChallan(int id);

        BankChallan GetChallanByID(int id);
    }
}

