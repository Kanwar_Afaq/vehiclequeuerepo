﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using VehicleQueueApp.Models;

namespace VehicleQueueApp
{
    public class BankChallanRepository : IBankChallan
    {
        public VehicleQueueAppEntities _context = new VehicleQueueAppEntities();


        public bool SubmittedbankChallan(int challanId, string fileName)
        {
            var bankChallan = _context.BankChallans.FirstOrDefault(q => q.BankChallanId == challanId);
            bankChallan.isChallanSubmitedtoBank = true;
            bankChallan.DepositSlip = fileName;
            _context.SaveChanges();
            return true;
        }
        public bool AddChallan(int bankId)
        {
            var days = _context.Days.Where(q => q.IsBankChallanGenerated == false).ToList();

            if (days != null)
            {
                foreach (var item in days)
                {
                    DateTime today = System.DateTime.Now;
                    BankChallan bankChallan = new BankChallan();
                    bankChallan.DayId = item.DayId;
                    bankChallan.CreatedDatetime = today;
                    bankChallan.ChallanDatetime = today;
                    string challanNo = today.Day.ToString() + item.DayId + "0";
                    bankChallan.ChallanNo = challanNo;
                    bankChallan.BankId = bankId;
                    bankChallan.isChallanSubmitedtoBank = false;
                    bankChallan.TotalAmount = item.FinalAmount;
                    item.IsBankChallanGenerated = true;
                    _context.BankChallans.Add(bankChallan);
                    var token = _context.TokenDetails.Where(q => q.Token.Operation.Shift.DayId == item.DayId).ToList();

                    foreach (var subItem in token)
                    {
                        BankChallanDetail bankchallandetail = new BankChallanDetail();
                        bankchallandetail.BankChallanId = bankChallan.BankChallanId;
                        bankchallandetail.TokenDetailId = subItem.TokenDetailsId;

                        bankchallandetail.Fees = subItem.Fees;
                        _context.BankChallanDetails.Add(bankchallandetail);
                    }
                    _context.SaveChanges();

                }






            }

            return true;
        }
        public MainVM GetAllBankChallanByVM()
        {
            var result = _context.BankChallans.ToList();

            var r = (from c in _context.BankChallans
                     orderby c.BankChallanId descending
                     select new BankChallanDetailcheckboxVM
                     {
                         BankChallanId = c.BankChallanId,
                         IsPrint = false
                     }).ToList();

            MainVM v = new MainVM();
            v.bankchallandetailvm = r;
            return v;
        }



        public void deleteBankChallan(int id)
        {
            BankChallan bankChallan = _context.BankChallans.Find(id);
            _context.BankChallans.Remove(bankChallan);
        }

        public List<BankChallan> GetAllBankChallans()
        {
            return _context.BankChallans.ToList();
        }

        public BankChallan GetChallanByID(int id)
        {
            BankChallan bankChallan = _context.BankChallans.Find(id);
            return bankChallan;
        }
        public List<BankChallanDetail> GetChallanDetailByID(int bankchallanid)
        {
            var bankChallanDetail = _context.BankChallanDetails.Where(q => q.BankChallanId == bankchallanid).ToList();
            return bankChallanDetail;
        }
        public static String ones(String Number)
        {
            int _Number = Convert.ToInt32(Number);
            String name = "";
            switch (_Number)
            {

                case 1:
                    name = "One";
                    break;
                case 2:
                    name = "Two";
                    break;
                case 3:
                    name = "Three";
                    break;
                case 4:
                    name = "Four";
                    break;
                case 5:
                    name = "Five";
                    break;
                case 6:
                    name = "Six";
                    break;
                case 7:
                    name = "Seven";
                    break;
                case 8:
                    name = "Eight";
                    break;
                case 9:
                    name = "Nine";
                    break;
            }
            return name;
        }
        public static String tens(String Number)
        {
            int _Number = Convert.ToInt32(Number);
            String name = null;
            switch (_Number)
            {
                case 10:
                    name = "Ten";
                    break;
                case 11:
                    name = "Eleven";
                    break;
                case 12:
                    name = "Twelve";
                    break;
                case 13:
                    name = "Thirteen";
                    break;
                case 14:
                    name = "Fourteen";
                    break;
                case 15:
                    name = "Fifteen";
                    break;
                case 16:
                    name = "Sixteen";
                    break;
                case 17:
                    name = "Seventeen";
                    break;
                case 18:
                    name = "Eighteen";
                    break;
                case 19:
                    name = "Nineteen";
                    break;
                case 20:
                    name = "Twenty";
                    break;
                case 30:
                    name = "Thirty";
                    break;
                case 40:
                    name = "Fourty";
                    break;
                case 50:
                    name = "Fifty";
                    break;
                case 60:
                    name = "Sixty";
                    break;
                case 70:
                    name = "Seventy";
                    break;
                case 80:
                    name = "Eighty";
                    break;
                case 90:
                    name = "Ninety";
                    break;
                default:
                    if (_Number > 0)
                    {
                        name = tens(Number.Substring(0, 1) + "0") + " " + ones(Number.Substring(1));
                    }
                    break;
            }
            return name;
        }
        public static String ConvertToWords(String numb)
        {
            String val = "", wholeNo = numb, points = "", andStr = "", pointStr = "";
            String endStr = "Only";
            try
            {
                int decimalPlace = numb.IndexOf(".");
                if (decimalPlace > 0)
                {
                    wholeNo = numb.Substring(0, decimalPlace);
                    points = numb.Substring(decimalPlace + 1);
                    if (Convert.ToInt32(points) > 0)
                    {
                        andStr = "and";// just to separate whole numbers from points/cents  
                        endStr = "Paisa " + endStr;//Cents  
                        pointStr = ConvertDecimals(points);
                    }
                }
                val = String.Format("{0} {1}{2} {3} ", ConvertWholeNumber(wholeNo).Trim() + " RS ", andStr, pointStr, endStr);
            }
            catch { }
            return val;
        }
        public static String ConvertWholeNumber(String Number)
        {
            string word = "";
            try
            {
                bool beginsZero = false;//tests for 0XX  
                bool isDone = false;//test if already translated  
                double dblAmt = (Convert.ToDouble(Number));
                //if ((dblAmt > 0) && number.StartsWith("0"))  
                if (dblAmt > 0)
                {//test for zero or digit zero in a nuemric  
                    beginsZero = Number.StartsWith("0");

                    int numDigits = Number.Length;
                    int pos = 0;//store digit grouping  
                    String place = "";//digit grouping name:hundres,thousand,etc...  
                    switch (numDigits)
                    {
                        case 1://ones' range  

                            word = ones(Number);
                            isDone = true;
                            break;
                        case 2://tens' range  
                            word = tens(Number);
                            isDone = true;
                            break;
                        case 3://hundreds' range  
                            pos = (numDigits % 3) + 1;
                            place = " Hundred ";
                            break;
                        case 4://thousands' range  
                        case 5:
                        case 6:
                            pos = (numDigits % 4) + 1;
                            place = " Thousand ";
                            break;
                        case 7://millions' range  
                        case 8:
                        case 9:
                            pos = (numDigits % 7) + 1;
                            place = " Million ";
                            break;
                        case 10://Billions's range  
                        case 11:
                        case 12:

                            pos = (numDigits % 10) + 1;
                            place = " Billion ";
                            break;
                        //add extra case options for anything above Billion...  
                        default:
                            isDone = true;
                            break;
                    }
                    if (!isDone)
                    {//if transalation is not done, continue...(Recursion comes in now!!)  
                        if (Number.Substring(0, pos) != "0" && Number.Substring(pos) != "0")
                        {
                            try
                            {
                                word = ConvertWholeNumber(Number.Substring(0, pos)) + place + ConvertWholeNumber(Number.Substring(pos));
                            }
                            catch { }
                        }
                        else
                        {
                            word = ConvertWholeNumber(Number.Substring(0, pos)) + ConvertWholeNumber(Number.Substring(pos));
                        }

                        //check for trailing zeros  
                        //if (beginsZero) word = " and " + word.Trim();  
                    }
                    //ignore digit grouping names  
                    if (word.Trim().Equals(place.Trim())) word = "";
                }
            }
            catch { }
            return word.Trim();
        }
        public static String ConvertDecimals(String number)
        {
            String cd = "", digit = "", engOne = "";
            for (int i = 0; i < number.Length; i++)
            {
                digit = number[i].ToString();
                if (digit.Equals("0"))
                {
                    engOne = "Zero";
                }
                else
                {
                    engOne = ones(digit);
                }
                cd += " " + engOne;
            }
            return cd;
        }
        public List<ReportVM> getPdfreportOfBankChallan(MainVM mainvm)
        {
            var c = mainvm.bankchallandetailvm.Where(q=>q.IsPrint==true).Select(q=>q.BankChallanId);
            var result = (from q in _context.BankChallanDetails
                          where c.Contains(q.BankChallanId)
                          group q by  new { q.BankChallanId, q.TokenDetail.Token.CategoryFee.CategoryId }  into g
                          select new ReportVM
                          {
                              BankChallanId=g.Key.BankChallanId,
                              CategoryId = g.Key.CategoryId,
                              TotalAmount = g.Sum(q => q.Fees),
                              CategoryFees = g.FirstOrDefault().TokenDetail.Token.CategoryFee.Fees,
                              CategoryName = g.FirstOrDefault().TokenDetail.Token.CategoryFee.Category.CategoryName,
                              ChallanNo=g.FirstOrDefault().BankChallan.ChallanNo,
                              BankChallanDateTime= g.FirstOrDefault().BankChallan.ChallanDatetime,
                              TotalNoOftoken = g.Count()
                          }).ToList();
            if (result != null)
            {
                return result;
            }
            return null;
        }
        public List<ReportVM> getPdfreportByBankChallanId(int BankchallanId)
        {
            var query = _context.BankChallanDetails.FirstOrDefault(q => q.BankChallanId == BankchallanId);
            var result = (from q in _context.BankChallanDetails
                          where q.BankChallanId==BankchallanId
                          group q by new { q.BankChallanId, q.TokenDetail.Token.CategoryFee.Category.CategoryId } into g
                          select new ReportVM
                          {
                              BankChallanId = g.Key.BankChallanId,
                              CategoryId = g.Key.CategoryId,
                              TotalAmount = g.Sum(q => q.Fees),
                              CategoryFees = g.FirstOrDefault().TokenDetail.Token.CategoryFee.Fees,
                              CategoryName = g.FirstOrDefault().TokenDetail.Token.CategoryFee.Category.CategoryName,
                              ChallanNo = g.FirstOrDefault().BankChallan.ChallanNo,
                              BankChallanDateTime = g.FirstOrDefault().BankChallan.ChallanDatetime,
                              TotalNoOftoken = g.Count()
                          }).ToList();
            if (result != null)
            {
                return result;
            }
            return null;
        }
        public bool ChallanDepositToBank(int? id, bool value)
        {
            if (id == null)
            {
                return false;
            }
            BankChallan bankChallan = _context.BankChallans.Find(id);
            if (bankChallan == null)
            {
                return false;
            }
            else
            {
                var challan = _context.BankChallans.Where(q => q.isChallanSubmitedtoBank == false).ToList();

                if (challan != null)
                {
                    foreach (var item in challan)
                    {
                        DateTime today = System.DateTime.Now;

                        bankChallan.DayId = item.DayId;
                        //bankChallan.CreatedDatetime = today;
                        //bankChallan.ChallanDatetime = today;
                        string challanNo = today.Day.ToString() + item.DayId + "0";
                        bankChallan.ChallanNo = challanNo;
                        // bankChallan.BankId = bankId;
                        bankChallan.isChallanSubmitedtoBank = value;
                        bankChallan.TotalAmount = item.TotalAmount;

                        _context.BankChallans.Add(bankChallan);
                        //var token = _context.TokenDetails.Where(q => q.Token.Shift.Day.DayId == item.DayId).ToList();

                        //foreach (var subItem in token)
                        //{
                        //    BankChallanDetail bankchallandetail = new BankChallanDetail();
                        //    bankchallandetail.BankChallanId = bankChallan.BankChallanId;
                        //    bankchallandetail.TokenDetailId = subItem.TokenDetailsId;

                        //    bankchallandetail.Fees = subItem.Token.CategoryFee.Fees;
                        //    _context.BankChallanDetails.Add(bankchallandetail);
                        //}

                        _context.SaveChanges();
                    }
                    return true;

                }






            }

            return true;
        }
    }

}