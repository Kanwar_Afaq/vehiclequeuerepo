﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using VehicleQueueApp.Controllers;
using Quartz.Impl;

namespace VehicleQueueApp.Models
{
    public class DayStartJob : IJob
    {
        HomeController home = new HomeController();
        public Task Execute(IJobExecutionContext context)
        {
            return Task.Run(() => { home.StartDay(); });
        }
        public class DayStartJobScheduler
        {
            public async static void Start()
            {
                IScheduler scheduler = await StdSchedulerFactory.GetDefaultScheduler();
                await scheduler.Start();

                IJobDetail job = JobBuilder.Create<DayStartJob>().Build();
                ITrigger trigger = TriggerBuilder.Create()
                .WithCronSchedule("0 05 8 ? * MON-FRI *") //This expression to schedule your job Mon-Fri 8.05 AM
                .Build();

                await scheduler.ScheduleJob(job, trigger);
            }
        }
    }
}