﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using VehicleQueueApp.Helpers;
using VehicleQueueApp.Models;
using VehicleQueueApp.Services;

namespace VehicleQueueApp.Models
{
    public class SecurityRepository
    {
		public VehicleQueueAppEntities _context = new VehicleQueueAppEntities();

		public IEnumerable<AppRolesModule> VerifyUserAccess(string controllerName)
		{
			var qry = (from G in _context.AppRolesModules
					   join GM in _context.AppModules on G.ModuleId equals GM.ModuleID
					   where G.RolesId == Auth.RoleId
					   && GM.ModuleLink.ToLower() == controllerName.ToLower()
					   && G.IsAllowed == true
					   select G);
			return qry;
		}


		public IEnumerable<AppModule> getUserModules()
		{
			var qry = (from G in _context.AppRolesModules
					   join GM in _context.AppModules on G.ModuleId equals GM.ModuleID
					   where G.RolesId == Auth.RoleId
					   && G.IsAllowed == true
					   select GM);

			return qry;
		}

		#region Module Methods

		public List<AppModule> GetAllModules()
        {
            var Features = (from M in _context.AppModules
                            select new 
                            {
                                ModuleID = M.ModuleID,
                                ModuleName = M.ModuleName,
                                permissionlists= (from c in M.AppModulePermissions
                                                    where c.ModuleID == M.ModuleID
                                                    select new PermissionsList
                                                    {
                                                        permissionname = c.PermissionName,
                                                       
                                                        
                                                    }).ToList()
                            }).ToList().Select(s =>
                              new AppModule
                              {
                                  ModuleID=s.ModuleID,
                                  ModuleName=s.ModuleName,
                                  permissionlists=s.permissionlists,
                                  Rolesassigned = String.Join(",", s.permissionlists.Select(q=>q.permissionname))
                              }).ToList();

           
            return Features;


		}
		public AppModule GetModule(int Id) {
			var Features = (from AM in _context.AppModules
							where AM.ModuleID == Id
							select AM).FirstOrDefault();
            List<string> Access = new List<string>() { "FullAccess", "Create", "Edit", "Delete", "Detail","View" };

            Features.permissionlists= (from AM in _context.AppModulePermissions
                                       where AM.ModuleID == Id && !Access.Contains(AM.PermissionName)
                                       select new PermissionsList
                                       {
                                        //   permissionid=AM.PermissionID,
                                           permissionname=AM.PermissionName
                                       }).ToList();
            return Features;
		}



		public List<AppModuleConfig> GetAppModuleConfig()
		{
			var qry = (from M in _context.AppModuleConfigs
							select M).ToList();

			return qry;
		}

		public AppModuleConfig GetAppModuleConfig(int Id)
		{
			var qry = (from M in _context.AppModuleConfigs
					   where M.ConfigID == Id
					   select M).FirstOrDefault();

			return qry;
		}

		public bool ConfigEdit(int Id, AppModuleConfig getDATA, out string error)
		{
			error = string.Empty;
			try
			{
				var setData = _context.AppModuleConfigs.FirstOrDefault(m => m.ConfigID == Id);

				if (setData != null)
				{
					//setData.ModuleID = getDATA.ModuleID;
					setData.ConfigKey = getDATA.ConfigKey;
					setData.ConfigValue = getDATA.ConfigValue;

					setData.ModifiedBy = Auth.UserID;
					setData.ModifiedDate = DateTime.Now;



					_context.Entry(setData).State = System.Data.Entity.EntityState.Modified;
					_context.SaveChanges();
					return true;
				}
				else
				{
					error = "ID Not Valid";
					return false;
				}

			}
			catch (DbEntityValidationException e)
			{
				error = CommonHelpers.dbErrorDetail(e);
				return false;
			}
			catch (Exception ex)
			{
				error = ex.Message;
				return false;
			}
		}



		public bool isModuleExist(string ModuleName, int ModuleID) {
			bool isExist = false;
			try {
				if (ModuleID == 0)
					isExist = _context.AppModules.Any(m => m.ModuleName == ModuleName);
				else
					isExist = _context.AppModules.Any(m => m.ModuleName == ModuleName && m.ModuleID != ModuleID);

			} catch { }

			return isExist;
		}
		public bool CreateModule(AppModule getDATA, out string error) {
			error = string.Empty;
			try {
				getDATA.CreatedBy = Auth.UserID;
				getDATA.CreatedDate = DateTime.Now;
				_context.AppModules.Add(getDATA);
                if (getDATA.permissionlists != null)
                {
                    foreach (var item in getDATA.permissionlists)
                    {
                        AppModulePermission per = new AppModulePermission();
                     //   per.PermissionID = item.permissionid;
                        per.ModuleID = getDATA.ModuleID;
                        per.PermissionName = item.permissionname;
                        per.isAvailable = true;
                        getDATA.AppModulePermissions.Add(per);
                    }
                }
             
                if (getDATA.HasFullAccess == true)
                {
                    AppModulePermission per = new AppModulePermission();
                  //  per.PermissionID = Guid.NewGuid();
                    per.ModuleID = getDATA.ModuleID;
                    per.PermissionName = "FullAccess";
                    per.isAvailable = true;
                    getDATA.AppModulePermissions.Add(per);
                }
                if (getDATA.HasCreate == true)
                {
                    AppModulePermission per = new AppModulePermission();
                //    per.PermissionID = Guid.NewGuid();
                    per.ModuleID = getDATA.ModuleID;
                    per.PermissionName = "Create";
                    per.isAvailable = true;
                    getDATA.AppModulePermissions.Add(per);
                }
                if (getDATA.HasEdit == true)
                {
                    AppModulePermission per = new AppModulePermission();
                 //   per.PermissionID = Guid.NewGuid();
                    per.ModuleID = getDATA.ModuleID;
                    per.PermissionName = "Edit";
                    per.isAvailable = true;
                    getDATA.AppModulePermissions.Add(per);
                }
                if (getDATA.HasDelete == true)
                {
                    AppModulePermission per = new AppModulePermission();
                //    per.PermissionID = Guid.NewGuid();
                    per.ModuleID = getDATA.ModuleID;
                    per.PermissionName = "Delete";
                    per.isAvailable = true;
                    getDATA.AppModulePermissions.Add(per);
                }
                if (getDATA.HasDetail == true)
                {
                    AppModulePermission per = new AppModulePermission();
               //     per.PermissionID = Guid.NewGuid();
                    per.ModuleID = getDATA.ModuleID;
                    per.PermissionName = "Detail";
                    per.isAvailable = true;
                    getDATA.AppModulePermissions.Add(per);
                }
                if (getDATA.HasView == true)
                {
                    AppModulePermission per = new AppModulePermission();
               //     per.PermissionID = Guid.NewGuid();
                    per.ModuleID = getDATA.ModuleID;
                    per.PermissionName = "Index";
                    per.isAvailable = true;
                    getDATA.AppModulePermissions.Add(per);
                }

                _context.SaveChanges();

				return true;

			} catch (DbEntityValidationException e) {
				error = CommonHelpers.dbErrorDetail(e);
				return false;
			} catch (Exception ex) {
				error = ex.Message;
				return false;
			}
		}		 																			   
		public bool EditModule(int Id, AppModule getDATA, out string error) {
			error = string.Empty;
			try {
				var setData = _context.AppModules.FirstOrDefault(m => m.ModuleID == Id);

				if (setData != null)
                {
					setData.ModuleID = getDATA.ModuleID;
					setData.ModuleName = getDATA.ModuleName;
					setData.ModuleLink = getDATA.ModuleLink;
					setData.ModuleIndex = getDATA.ModuleIndex;
					setData.ModuleParam = getDATA.ModuleParam;
					setData.ShowInMenu = getDATA.ShowInMenu;
					setData.isPrimary = getDATA.isPrimary;

					setData.Sort = getDATA.Sort;
					setData.CssClass = getDATA.CssClass;

                    setData.HasFullAccess = getDATA.HasFullAccess;
                    setData.HasCreate = getDATA.HasCreate;
                    setData.HasEdit = getDATA.HasEdit;
                    setData.HasDelete = getDATA.HasDelete;
                    setData.HasView = getDATA.HasView;
                    setData.HasDetail = getDATA.HasDetail;
             

                    var del = _context.AppModulePermissions.Where(q => q.ModuleID == Id);
                    _context.AppModulePermissions.RemoveRange(del);

                    if (getDATA.permissionlists!=null)
                    {
                        foreach (var item in getDATA.permissionlists)
                        {
                            AppModulePermission per = new AppModulePermission();
                        //    per.PermissionID = item.permissionid;
                            per.ModuleID = Id;
                            per.PermissionName = item.permissionname;
                            per.isAvailable = true;
                            setData.AppModulePermissions.Add(per);
                        }
                    }

                    if (getDATA.HasFullAccess)
                    {
                            AppModulePermission per = new AppModulePermission();
                        //    per.PermissionID = Guid.NewGuid();
                            per.ModuleID = getDATA.ModuleID;
                            per.PermissionName = "FullAccess";
                            per.isAvailable = true;
                            setData.AppModulePermissions.Add(per);
                    }

                    if (getDATA.HasCreate == true)
                    {
                            AppModulePermission per = new AppModulePermission();
                        //    per.PermissionID = Guid.NewGuid();
                            per.ModuleID = getDATA.ModuleID;
                            per.PermissionName = "Create";
                            per.isAvailable = true;
                            setData.AppModulePermissions.Add(per);
                    }

                    if (getDATA.HasEdit == true)
                    {
                            AppModulePermission per = new AppModulePermission();
                         //   per.PermissionID = Guid.NewGuid();
                            per.ModuleID = getDATA.ModuleID;
                            per.PermissionName = "Edit";
                            per.isAvailable = true;
                            setData.AppModulePermissions.Add(per);
                    }

                    if (getDATA.HasDelete == true)
                    {
                            AppModulePermission per = new AppModulePermission();
                       //     per.PermissionID = Guid.NewGuid();
                            per.ModuleID = getDATA.ModuleID;
                            per.PermissionName = "Delete";
                            per.isAvailable = true;
                            setData.AppModulePermissions.Add(per);
                    }

                    if (getDATA.HasDetail == true)
                    {
                            AppModulePermission per = new AppModulePermission();
                       //     per.PermissionID = Guid.NewGuid();
                            per.ModuleID = getDATA.ModuleID;
                            per.PermissionName = "Detail";
                            per.isAvailable = true;
                            setData.AppModulePermissions.Add(per);
                    }

                    if (getDATA.HasView == true)
                    {
                            AppModulePermission per = new AppModulePermission();
                      //      per.PermissionID = Guid.NewGuid();
                            per.ModuleID = getDATA.ModuleID;
                            per.PermissionName = "Index";
                            per.isAvailable = true;
                            setData.AppModulePermissions.Add(per);
                    }

                    setData.ModifiedBy = Auth.UserID;
					setData.ModifiedDate = DateTime.Now;
					_context.Entry(setData).State = System.Data.Entity.EntityState.Modified;
					_context.SaveChanges();
					return true;
				} else {
					error = "ID Not Valid";
					return false;
				}

			} catch (DbEntityValidationException e) {
				error = CommonHelpers.dbErrorDetail(e);
				return false;
			} catch (Exception ex) {
				error = ex.Message;
				return false;
			}
		}
		public bool DeleteModule(int Id, out string error) {
			error = string.Empty;
			try {
				AppModule delData = _context.AppModules.Where(m => m.ModuleID == Id).FirstOrDefault();
				if (delData != null) {
					_context.AppModules.Remove(delData);
					_context.SaveChanges();

					return true;

				} else {
					error = "ID Not Valid";
					return false;
				}

			} catch (DbEntityValidationException e) {
				error = CommonHelpers.dbErrorDetail(e);
				return false;
			} catch (Exception ex) {
				error = ex.Message;
				return false;
			}
		}
		#endregion
		#region Group Methods

		public IEnumerable<AppRole> AppGroups() 
        {
            var data = _context.AppRoles;
            foreach (var item in data)
            {
                if (item.RoleId == Auth.RoleId && item.Priority == 20)
                {
                    return data.Where(x => x.RoleId == Auth.RoleId);
                } 
            }
			return _context.AppRoles;
		}
		public AppRole getAppGroup(int Id) {
			return _context.AppRoles.FirstOrDefault(e => e.RoleId == Id);
		} 



		public bool CreateGroup(AppRole getDATA, out string error) {
			error = string.Empty;
			try {
				getDATA.CreatedBy = Auth.UserID;
				getDATA.CreatedDate = DateTime.Now;
				_context.AppRoles.Add(getDATA);
				_context.SaveChanges();

				return true;

			} catch (DbEntityValidationException e) {
				error = CommonHelpers.dbErrorDetail(e);
				return false;
			} catch (Exception ex) {
				error = ex.Message;
				return false;
			}
		}			 
		public bool EditGroup(int Id, AppRole getDATA, out string error) {
			error = string.Empty;
			try {
				var setData = _context.AppRoles.Where(m => m.RoleId == Id).FirstOrDefault();
				
				
				if (setData != null) {
					_context.AppRolesModules.RemoveRange(setData.AppRolesModules);

					setData.RoleName = getDATA.RoleName;
					setData.isDisable = getDATA.isDisable;
					setData.AppRolesModules = getDATA.AppRolesModules;

					setData.ModifiedBy = Auth.UserID;
					setData.Modified = DateTime.Now;

					_context.Entry(setData).State = System.Data.Entity.EntityState.Modified;
					_context.SaveChanges();
					return true;


				} else {
					error = "ID Not Valid";
					return false;
				}

			} catch (DbEntityValidationException e) {
				error = CommonHelpers.dbErrorDetail(e);
				return false;
			} catch (Exception ex) {
				error = ex.Message;
				return false;
			}
		}
		public bool DeleteGroup(int Id, out string error) {
			error = string.Empty;
			try {
				AppRole delData = _context.AppRoles.Where(m => m.RoleId == Id).FirstOrDefault();
				_context.AppRolesModules.RemoveRange(delData.AppRolesModules);
				_context.AppRoles.Remove(delData);
				_context.SaveChanges();

				return true;

			} catch (DbEntityValidationException e) {
				error = CommonHelpers.dbErrorDetail(e);
				return false;
			} catch (Exception ex) {
				error = ex.Message;
				return false;
			}
		}

		#endregion
		public void Dispose(bool disposing) {
			if (disposing) {
				_context.Dispose();
			}

		}
	}
}