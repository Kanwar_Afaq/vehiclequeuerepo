﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VehicleQueueApp.Models.OperationModel
{
    interface IOpeartioncs:IDisposable
    {
        IEnumerable<Operation> GetOpeartions();
        void updateOperation(Operation operation);

        void insertOpeartion(Operation operation);
        void deleteOpeartion(int id);

        Operation getOpeartion(int id);
        void AddOpeartiondetail();
        Operation getOpeartionByShiftId(int shiftid);
    }
}
