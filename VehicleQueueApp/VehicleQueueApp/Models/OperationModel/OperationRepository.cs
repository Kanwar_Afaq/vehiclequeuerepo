﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VehicleQueueApp.Helpers;

namespace VehicleQueueApp.Models.OperationModel
{
    public class OperationRepository : IOpeartioncs
    {
        public VehicleQueueAppEntities _context = new VehicleQueueAppEntities();
        public void AddOpeartiondetail()
        {
            throw new NotImplementedException();
        }

        public void deleteOpeartion(int id)
        {
            Operation operation = _context.Operations.Find(id);
            _context.Operations.Remove(operation);
            _context.SaveChanges();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
        public Operation getOpeartionByShiftId(int shiftid)
        {
            Operation operation = _context.Operations.FirstOrDefault(q => q.ShiftId == shiftid);
            return operation;
        }
        public Operation getOpeartion(int id)
        {
            Operation operation = _context.Operations.Find(id);
            return operation;
        }
        public IEnumerable<Operation> getOpeartionByUserId(int userId)
        {
            var operation = _context.Operations.Where(q => q.OperationStartedBy == userId).ToList();
            return operation;
        }
        public IEnumerable<Operation> GetOpeartions()
        {
            return _context.Operations.ToList();
        }

        public void insertOpeartion(Operation operation)
        {
            _context.Operations.Add(operation);
            _context.SaveChanges();
        }

        public void updateOperation(Operation operation)
        {
            _context.Entry(operation).State = System.Data.Entity.EntityState.Modified;
            _context.SaveChanges();
        }
        public void StartOpeartion(int userId, int shiftId)
        {
            if (userId > 0)
            {
                DateTime OperationStartDateTime = System.DateTime.Now;

                Operation operation = new Operation();
                operation.OperationStartedBy = userId;
                operation.OperationStartingDateTime = OperationStartDateTime;
                operation.ShiftId = shiftId;
                operation.CreatedBy = userId;
                operation.CreatedDate = System.DateTime.Now;
                _context.Operations.Add(operation);

                _context.SaveChanges();
            }

        }
        public int GetActiveOperation()
        {
            int UserId = Auth.UserID;
            var activeoperations = _context.Operations.FirstOrDefault(x => x.OpeartionClosed == false && x.OperationStartedBy == UserId);
            if (activeoperations != null)
            {
                return activeoperations.OperationId;
            }
            else
            {
                return 0;
            }

        }
        public decimal? getLiveTokenAmountperUser(int userId, int activeOperationID)
        {
            var liveTokenAmountadd = _context.TokenTransactions.Where(q => q.OperationId == activeOperationID && q.isPayIn == true);
            var liveTokenAmountsub = _context.TokenTransactions.Where(q => q.OperationId == activeOperationID && q.isPayIn==false);

            return (liveTokenAmountadd.Count() > 0 ? liveTokenAmountadd.Sum(q => q.Amount) : 0) - (liveTokenAmountsub.Count() > 0 ?  liveTokenAmountsub.Sum(q=>q.Amount) : 0) ;
         
        
        }
        public decimal? getLiveTokenAmountByOperation(int userId, int shiftId)
        {
            var liveTokenAmount = _context.Operations.Where(q => q.OperationStartedBy == userId && q.ShiftId == shiftId).OrderByDescending(q => q.OperationId);
            if (liveTokenAmount.Any())
            {
                return liveTokenAmount.FirstOrDefault().TotalAmountinOperation;
            }
            return 0;
        }
        public int getLiveOpeartionId(int userId, int shiftId)
        {
            var liveOperationID = _context.Operations.Where(q => q.OperationStartedBy == userId && q.ShiftId == shiftId).OrderByDescending(q => q.OperationId);
            if (liveOperationID.Any())
            {
                return liveOperationID.FirstOrDefault().OperationId;
            }
            return 0;
        }
        public decimal? getLiveTokenAmountByDay()
        {
            var liveTokenAmount = _context.Days.OrderByDescending(q => q.DayId);
            if (liveTokenAmount.Any())
            {
                return liveTokenAmount.FirstOrDefault().FinalAmount;
            }
            return 0;
        }

        public decimal? getLiveTokenAmountByShift()
        {
            var liveTokenAmount = _context.Shifts.OrderByDescending(q => q.ShiftId);
            if (liveTokenAmount.Any())
            {
                return liveTokenAmount.FirstOrDefault().AmountToBeCollected;
            }
            return 0;
        }

        public List<Shift> getLiveAmountRecordsByDay()
        {
            var Days = _context.Days.OrderByDescending(q => q.DayId);
            if (Days.Any())
            {
                var dayId = Days.FirstOrDefault().DayId;
                var shifts = _context.Shifts.Where(q => q.DayId == dayId).ToList();
                return shifts.OrderByDescending(q=>q.ShiftId).ToList();
            }
            return null;
        }

        public Shift getLiveAmountRecordsByShift()
        {
            var shifts = _context.Shifts.OrderByDescending(q => q.ShiftId);
            if (shifts.Any())
            {
                var shiftId = shifts.FirstOrDefault().ShiftId;
                var shiftss = _context.Shifts.FirstOrDefault(q => q.ShiftId == shiftId);
                return shiftss;
            }
            return null;
        }
        public void CloseOperation(int userId)
        {
            int ActiveOperationId = GetActiveOperation();
            if (ActiveOperationId > 0)
            {
                decimal? finaloperationAmountcalculated = 0;
                var operationclosed = _context.Operations.FirstOrDefault(q => q.OperationStartedBy == userId && q.OpeartionClosed == false);
                if (operationclosed != null)
                {
                    operationclosed.OpeartionClosed = true;
                    operationclosed.OperationEndingDateTime = System.DateTime.Now;
                    var addoperation = _context.TokenTransactions.Where(q => q.OperationId == ActiveOperationId && q.isPayIn == true);
                    var suboperation = _context.TokenTransactions.Where(q => q.OperationId == ActiveOperationId && q.isPayIn == false);
                    finaloperationAmountcalculated = (addoperation.Count() >0 ? addoperation.Sum(q => q.Amount) : 0) - (suboperation.Count() > 0 ?  suboperation.Sum(q => q.Amount) : 0);
                    operationclosed.TotalAmountinOperation = Convert.ToDecimal(finaloperationAmountcalculated);                  
                }
                var token = _context.Tokens.Where(q => q.OperationId == operationclosed.OperationId).ToList();
                if (token.Any())
                {
                    token.ForEach(q => q.IsClosed = true);

                }
                _context.SaveChanges();
            }

        }



    }

}

