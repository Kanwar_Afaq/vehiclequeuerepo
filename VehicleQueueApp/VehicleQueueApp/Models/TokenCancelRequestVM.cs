﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VehicleQueueApp.Models
{
    public class TokenCancelRequestVM
    {
        public int tokenCancelrequestID { get; set; }
     
        public string Name { get; set; }
        public string NIC { get; set; }
        public decimal? Fees { get; set; }
        public string TokenNo { get; set; }
   
    }
}