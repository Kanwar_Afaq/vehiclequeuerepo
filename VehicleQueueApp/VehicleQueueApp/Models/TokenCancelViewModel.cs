﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VehicleQueueApp.Models
{

    public class tokenCancelDetailsVM
    {
        public int tokenId { get; set; }
        public string Name { get; set; }
        public string NIC { get; set; }
        public decimal? Fees { get; set; }
        public string TokenNo { get; set; }
        public bool isDriver { get; set; }
        public bool isApproved { get; set; }
    }

}