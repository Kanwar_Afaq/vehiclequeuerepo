﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VehicleQueueApp.Models
{
    interface ICategory:IDisposable
    {
        IEnumerable<Category> GetCategorys();
        void updateCategory(Category Category);
        void updateCategoryFee(CategoryFee CategoryFee);
        void insertCategory(Category Category);
        void deleteCategory(int id);
        void deleteCategoryFee(int id);
        Category getCategorybyId(int id);
        CategoryFee getCategoryFeebyId(int id);
        void insertCategoryFee(CategoryFee CategoryFee);


    }
}
