﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity.Infrastructure;
namespace VehicleQueueApp.Models
{
    public class CategoriesRepository : ICategory
    {
        VehicleQueueAppEntities _context =new VehicleQueueAppEntities();
      
        public void deleteCategory(int id)
        {
            Category category = _context.Categories.Find(id);
            _context.Categories.Remove(category);
            _context.SaveChanges();
        }
     

        public void Dispose()
        {
        
        }

        public Category getCategorybyId(int id)
        {
            Category cat= _context.Categories.Find(id);
            return cat;
        }
        public CategoryFee getCategoryFeebyId(int id)
        {
            CategoryFee catFee = _context.CategoryFees.Find(id);
            return catFee;
        }

        public CategoryFee getCategoryfeebyCatId(int id)
        {
            CategoryFee catFee = _context.CategoryFees.FirstOrDefault(q => q.CategoryId == id);
            return catFee;
        }
        public IEnumerable<Category> GetCategorys()
        {
            return _context.Categories.ToList();
        }

        public void insertCategory(Category Category)
        {
            _context.Categories.Add(Category);
            _context.SaveChanges();
        }

      

        public void updateCategory(Category Category)
        {
            _context.Entry(Category).State = System.Data.Entity.EntityState.Modified;
            _context.SaveChanges();
        }
        public void updateCategoryFee(CategoryFee categoryFee)
        {
            //categoryFee=_context.CategoryFees.Include(i)
            _context.Entry(categoryFee).State = System.Data.Entity.EntityState.Modified;
            _context.SaveChanges();
        }
        public IEnumerable<CategoryFee> GetCategoryFee()
        {
            return _context.CategoryFees.Include("Category").ToList();
        }

        public void deleteCategoryFee(int id)
        {
            CategoryFee categoryFee = _context.CategoryFees.Find(id);
            _context.CategoryFees.Remove(categoryFee);
            _context.SaveChanges();
        }

        public void insertCategoryFee(CategoryFee categoryFee)
        {
            _context.CategoryFees.Add(categoryFee);
            _context.SaveChanges();
        }
    }
}