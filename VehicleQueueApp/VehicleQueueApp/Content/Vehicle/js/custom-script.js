$(function () {
    $('.mask').mask('00000-0000000-0');
    $('.mask').focus();

    //$('html').bind('keypress', function (event) {
    //    if (event.which == '13') {
    //        event.preventDefault();
    //        return false;
    //    }
    //});

    //    $(document).on('focusout', 'input.mask', function () {
    //        var id = $(this).data('id');
    //        var result = $('#NIC-' + id).val();

    //         console.log(id);
    //            $.ajax({
    //                url: '/Token/CNIC/',
    //                type: "POST",
    //                dataType: 'json',
    //                data:{nic:result},
    //                success: function (data) {

    //                    $('#NIC-'+id).val(data);

    //                }

    //        });
    //    });
    //});


});

$(function () {

    var dataPointsTemp = [];
    var dataPointsHumidity = [];
    $.ajax({
        url: '/Home/CategoryFee/',
        type: "GET",
        dataType: 'json',
        success: function (data) {

            //console.log(data);
            var jsonObject = JSON.stringify(data);


            for (var i = 0; i < data.length; i++) {
                dataPointsTemp.push({ y: data[i].TicketIssued, label: data[i].categoryName });
                dataPointsHumidity.push({ y: data[i].TicketIssued, label: data[i].categoryName });

            }


            var chart = new CanvasJS.Chart("ChartContainer", {
                animationEnabled: true,
                title: {
                    horizontalAlign: "left"
                },
                data: [
                {
                    type: "doughnut",
                    startAngle: 10,
                    innerRadius: 40,
                    indexLabelFontSize: 10,
                    indexLabel: " ",
                    dataPoints: dataPointsHumidity
                }],


                //    data: [
                //{
                //    type: "doughnut",
                //    startAngle: 10,
                //    innerRadius: 35,
                //    indexLabelFontSize: 10,
                //    indexLabel: " ",
                //    dataPoints:dataPointsTemp
                //},
                //{
                //    type: "doughnut",
                //    startAngle: 10,
                //    innerRadius: 35,
                //    indexLabelFontSize: 10,
                //    indexLabel: " ",
                //    dataPoints:dataPointsHumidity
                //}
                //    ]
            });
            chart.render();
        }
    });
});


window.onload = function () {

    var dataPointsTemp = [];
    var dataPointsHumidity = [];
    $.ajax({
        url: '/Home/CategoryFeeCollection/',
        type: "GET",
        dataType: 'json',
        success: function (data) {


            var jsonObject = JSON.stringify(data);
            //console.log(data);

            for (var i = 0; i < data.length; i++) {
                dataPointsTemp.push({ y: data[i].Amountpercategoryint, label: data[i].TicketFeesint });
                dataPointsHumidity.push({ y: data[i].Amountpercategoryint, label: data[i].TicketFeesint });

            }


            var chart = new CanvasJS.Chart("chartContainer1", {
                animationEnabled: false,
                title: {
                    horizontalAlign: "left"
                },
                data: [
                {
                    type: "doughnut",
                    startAngle: 10,
                    innerRadius: 40,
                    indexLabelFontSize: 10,
                    indexLabel: " ",
                    dataPoints: dataPointsHumidity
                }],


                //    data: [
                //{
                //    type: "doughnut",
                //    startAngle: 10,
                //    innerRadius: 35,
                //    indexLabelFontSize: 10,
                //    indexLabel: " ",
                //    dataPoints:dataPointsTemp
                //},
                //{
                //    type: "doughnut",
                //    startAngle: 10,
                //    innerRadius: 35,
                //    indexLabelFontSize: 10,
                //    indexLabel: " ",
                //    dataPoints:dataPointsHumidity
                //}
                //    ]
            });
            chart.render();
        }
    });
}





//------------------------------------------------------------------
// Phone Menu
//------------------------------------------------------------------

$('.toggle-menu').click(function () {
    $(this).parents('.menu-cont').toggleClass('open-menu');
})

// add Submenu button.
if ($('#phone-nav').length) {
    $('#phone-nav .sub-menu').each(function (i, v) {
        $(this).prev('a').addClass('sub-menu-trigger');
        $('<span class="trigger"> &raquo; </span>').insertAfter($(this).prev('a'));
    })
    $('.trigger').on('click', function () {
        $(this).next().slideToggle();
    })
}

$('.toggle-search').click(function () {
    $(this).parents('.menu-cont').toggleClass('open-search')
})




function printtickets() {
    window.print();
}


//------------------------------------------------------------------
// Toggle Button
//------------------------------------------------------------------
$('.btn-toggle').click(function () {
    $('.top_nav').removeClass('bd-bottom-red').toggleClass("bd-bottom-green");
});


