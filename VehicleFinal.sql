USE [master]
GO
/****** Object:  Database [VehicleDB]    Script Date: 1/2/2018 7:03:32 PM ******/
CREATE DATABASE [VehicleDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'VehicleDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\VehicleDB.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'VehicleDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\VehicleDB_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [VehicleDB] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [VehicleDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [VehicleDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [VehicleDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [VehicleDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [VehicleDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [VehicleDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [VehicleDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [VehicleDB] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [VehicleDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [VehicleDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [VehicleDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [VehicleDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [VehicleDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [VehicleDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [VehicleDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [VehicleDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [VehicleDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [VehicleDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [VehicleDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [VehicleDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [VehicleDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [VehicleDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [VehicleDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [VehicleDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [VehicleDB] SET RECOVERY FULL 
GO
ALTER DATABASE [VehicleDB] SET  MULTI_USER 
GO
ALTER DATABASE [VehicleDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [VehicleDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [VehicleDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [VehicleDB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [VehicleDB]
GO
/****** Object:  Table [dbo].[AppEmployeeAgencies]    Script Date: 1/2/2018 7:03:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AppEmployeeAgencies](
	[EmployeeAgencyId] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeID] [int] NOT NULL,
	[AgencyId] [int] NOT NULL,
 CONSTRAINT [PK_AppEmployeeAgencies] PRIMARY KEY CLUSTERED 
(
	[EmployeeAgencyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AppEmployeeDepartments]    Script Date: 1/2/2018 7:03:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AppEmployeeDepartments](
	[EmployeeDeptID] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeID] [int] NOT NULL,
	[DepartmentId] [int] NOT NULL,
 CONSTRAINT [PK_UserDepartments] PRIMARY KEY CLUSTERED 
(
	[EmployeeDeptID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AppEmployees]    Script Date: 1/2/2018 7:03:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AppEmployees](
	[EmployeeID] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeGUID] [uniqueidentifier] NOT NULL,
	[UserId] [int] NOT NULL,
	[Name] [varchar](100) NULL,
	[Email] [varchar](100) NOT NULL,
	[PersonalEmail] [varchar](100) NULL,
	[MobileNo] [varchar](20) NULL,
	[TelNo] [varchar](20) NULL,
	[TelNoHome] [varchar](20) NULL,
	[FaxNo] [varchar](20) NULL,
	[FaxNoHome] [varchar](20) NULL,
	[Designation] [varchar](50) NULL,
	[DepartmentId] [int] NULL,
	[Salary] [decimal](18, 2) NULL,
	[CNICNumber] [varchar](30) NULL,
	[ProfilePic] [nvarchar](200) NULL,
	[ProfilePicThumbNail] [nvarchar](200) NULL,
	[YahooID] [varchar](50) NULL,
	[SkypeID] [varchar](50) NULL,
	[BBMCode] [varchar](50) NULL,
	[Signature] [text] NULL,
	[Notes] [text] NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_UserProfiles_1] PRIMARY KEY CLUSTERED 
(
	[EmployeeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AppEmployeeServices]    Script Date: 1/2/2018 7:03:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AppEmployeeServices](
	[EmployeeServiceId] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeID] [int] NOT NULL,
	[ServiceId] [int] NOT NULL,
 CONSTRAINT [PK_AppEmployeeServices] PRIMARY KEY CLUSTERED 
(
	[EmployeeServiceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AppModuleConfigs]    Script Date: 1/2/2018 7:03:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AppModuleConfigs](
	[ConfigID] [int] IDENTITY(1,1) NOT NULL,
	[ModuleID] [int] NULL,
	[isLock] [bit] NOT NULL,
	[ConfigKey] [varchar](200) NULL,
	[ConfigValue] [varchar](200) NULL,
	[Description] [varchar](200) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
 CONSTRAINT [PK_ModuleConfig] PRIMARY KEY CLUSTERED 
(
	[ConfigID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AppModulePermissions]    Script Date: 1/2/2018 7:03:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AppModulePermissions](
	[PermissionID] [int] IDENTITY(1,1) NOT NULL,
	[ModuleID] [int] NOT NULL,
	[PermissionName] [varchar](100) NULL,
	[isAvailable] [bit] NOT NULL,
 CONSTRAINT [PK_AppModulePermissions] PRIMARY KEY CLUSTERED 
(
	[PermissionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AppModules]    Script Date: 1/2/2018 7:03:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AppModules](
	[ModuleID] [int] IDENTITY(1,1) NOT NULL,
	[ModuleName] [varchar](100) NULL,
	[ModuleLink] [varchar](50) NULL,
	[ModuleIndex] [varchar](50) NULL,
	[ModuleParam] [varchar](50) NULL,
	[ShowInMenu] [bit] NOT NULL,
	[isPrimary] [bit] NOT NULL,
	[Sort] [int] NOT NULL,
	[CssClass] [varchar](50) NULL,
	[isDisable] [bit] NOT NULL,
	[HasFullAccess] [bit] NOT NULL,
	[HasCreate] [bit] NOT NULL,
	[HasEdit] [bit] NOT NULL,
	[HasDelete] [bit] NOT NULL,
	[HasView] [bit] NOT NULL,
	[HasDetail] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_Features_1] PRIMARY KEY CLUSTERED 
(
	[ModuleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AppRoles]    Script Date: 1/2/2018 7:03:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AppRoles](
	[RoleId] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [varchar](100) NULL,
	[Priority] [int] NOT NULL,
	[isEditable] [bit] NOT NULL,
	[isDisable] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[Modified] [datetime] NULL,
 CONSTRAINT [PK_Group] PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AppRolesModules]    Script Date: 1/2/2018 7:03:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AppRolesModules](
	[RolesModulesID] [int] IDENTITY(1,1) NOT NULL,
	[RolesId] [int] NOT NULL,
	[ModuleId] [int] NOT NULL,
	[IsAllowed] [bit] NOT NULL,
	[Permissions] [text] NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_GroupFeature] PRIMARY KEY CLUSTERED 
(
	[RolesModulesID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AppUserLogs]    Script Date: 1/2/2018 7:03:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AppUserLogs](
	[UserLogID] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[IPAddress] [varchar](50) NULL,
	[Browser] [varchar](50) NULL,
	[SessionID] [varchar](50) NULL,
	[LoginDateTime] [datetime] NULL,
	[LogOutDateTime] [datetime] NULL,
 CONSTRAINT [PK_AppUserLogs] PRIMARY KEY CLUSTERED 
(
	[UserLogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AppUserMeta]    Script Date: 1/2/2018 7:03:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AppUserMeta](
	[UserMetaID] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[MetaKey] [varchar](255) NULL,
	[MetaValue] [varchar](255) NULL,
	[isInternal] [bit] NOT NULL,
 CONSTRAINT [PK_AppUserMeta] PRIMARY KEY CLUSTERED 
(
	[UserMetaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AppUserPwdRequest]    Script Date: 1/2/2018 7:03:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AppUserPwdRequest](
	[RequestId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[RandomNo] [varchar](50) NULL,
	[IPAddress] [varchar](50) NULL,
	[Browser] [varchar](50) NULL,
	[isGenerated] [bit] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_AppUserPwdRequest] PRIMARY KEY CLUSTERED 
(
	[RequestId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AppUsers]    Script Date: 1/2/2018 7:03:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AppUsers](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[UserGUID] [uniqueidentifier] NOT NULL,
	[isEmployee] [bit] NOT NULL,
	[RoleId] [int] NOT NULL,
	[UserName] [varchar](50) NOT NULL,
	[Password] [varchar](255) NOT NULL,
	[PassKey] [varchar](10) NULL,
	[ExpiryDate] [datetime] NULL,
	[LastLogn] [datetime] NULL,
	[LoginCount] [bigint] NOT NULL,
	[isActive] [bit] NOT NULL,
	[isDisable] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_Users_1] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Bank]    Script Date: 1/2/2018 7:03:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Bank](
	[BankId] [int] IDENTITY(1,1) NOT NULL,
	[BankName] [nvarchar](100) NULL,
	[Slip] [varbinary](max) NULL,
 CONSTRAINT [PK_Bank] PRIMARY KEY CLUSTERED 
(
	[BankId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BankChallan]    Script Date: 1/2/2018 7:03:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BankChallan](
	[BankChallanId] [int] IDENTITY(1,1) NOT NULL,
	[DayId] [int] NULL,
	[BankId] [int] NULL,
	[ChallanNo] [nvarchar](50) NULL,
	[ChallanDatetime] [datetime] NULL,
	[isChallanSubmitedtoBank] [bit] NOT NULL,
	[DepositSlip] [nvarchar](500) NULL,
	[TotalAmount] [decimal](18, 2) NULL,
	[TotalAmountinword] [nvarchar](50) NULL,
	[CreatedBy] [int] NULL,
	[CreatedDatetime] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDatetime] [nchar](10) NULL,
 CONSTRAINT [PK_BankChallanDetails] PRIMARY KEY CLUSTERED 
(
	[BankChallanId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BankChallanDetail]    Script Date: 1/2/2018 7:03:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BankChallanDetail](
	[BankChallenDetailsId] [int] IDENTITY(1,1) NOT NULL,
	[BankChallanId] [int] NULL,
	[TokenDetailId] [int] NULL,
	[Fees] [decimal](18, 2) NULL,
 CONSTRAINT [PK_BankChallanDetail] PRIMARY KEY CLUSTERED 
(
	[BankChallenDetailsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CancelStatus]    Script Date: 1/2/2018 7:03:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CancelStatus](
	[CancelStatusId] [int] IDENTITY(1,1) NOT NULL,
	[CancelStatus] [nvarchar](50) NULL,
 CONSTRAINT [PK_CancelStatus] PRIMARY KEY CLUSTERED 
(
	[CancelStatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Category]    Script Date: 1/2/2018 7:03:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[CategoryId] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [nvarchar](50) NULL,
	[Narration] [nvarchar](50) NULL,
 CONSTRAINT [PK_VehicleCategory] PRIMARY KEY CLUSTERED 
(
	[CategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CategoryFees]    Script Date: 1/2/2018 7:03:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CategoryFees](
	[CategoryFeeId] [int] IDENTITY(1,1) NOT NULL,
	[CategoryId] [int] NOT NULL,
	[Fees] [decimal](18, 2) NULL,
	[BoxClass] [nvarchar](50) NULL,
	[ClassName] [nvarchar](50) NULL,
	[EffectiveDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [date] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_VehicleCategoryFees] PRIMARY KEY CLUSTERED 
(
	[CategoryFeeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Day]    Script Date: 1/2/2018 7:03:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Day](
	[DayId] [int] IDENTITY(1,1) NOT NULL,
	[StartedBy] [int] NOT NULL,
	[StartDateTime] [datetime] NOT NULL,
	[EndingDateTime] [datetime] NULL,
	[IsDayClosed] [bit] NOT NULL,
	[FinalAmount] [decimal](18, 2) NOT NULL,
	[IsBankChallanGenerated] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_Day] PRIMARY KEY CLUSTERED 
(
	[DayId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Operation]    Script Date: 1/2/2018 7:03:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Operation](
	[OperationId] [int] IDENTITY(1,1) NOT NULL,
	[ShiftId] [int] NULL,
	[OperationStartedBy] [int] NULL,
	[OperationStartingDateTime] [datetime] NULL,
	[OperationEndingDateTime] [datetime] NULL,
	[TotalAmountinOperation] [decimal](18, 2) NULL,
	[OpeartionClosed] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_Operation] PRIMARY KEY CLUSTERED 
(
	[OperationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PlaceOfVisit]    Script Date: 1/2/2018 7:03:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PlaceOfVisit](
	[PlaceOfVisitId] [int] IDENTITY(1,1) NOT NULL,
	[PlaceOfVisit] [nvarchar](500) NULL,
 CONSTRAINT [PK_PlaceOfVisit] PRIMARY KEY CLUSTERED 
(
	[PlaceOfVisitId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PurposeOfVisit]    Script Date: 1/2/2018 7:03:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PurposeOfVisit](
	[PurposeOfVisitId] [int] IDENTITY(1,1) NOT NULL,
	[PurposeOfVIsit] [nvarchar](500) NULL,
 CONSTRAINT [PK_PurposeOfVisit] PRIMARY KEY CLUSTERED 
(
	[PurposeOfVisitId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Shift]    Script Date: 1/2/2018 7:03:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Shift](
	[ShiftId] [int] IDENTITY(1,1) NOT NULL,
	[DayId] [int] NOT NULL,
	[ShiftStartedBy] [int] NOT NULL,
	[ShiftStartingDatetime] [datetime] NULL,
	[ShiftEndingDatetime] [datetime] NULL,
	[AmountToBeCollected] [decimal](18, 2) NULL,
	[ShiftClosed] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_Shift] PRIMARY KEY CLUSTERED 
(
	[ShiftId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ShiftDetails]    Script Date: 1/2/2018 7:03:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ShiftDetails](
	[ShiftDetailId] [int] IDENTITY(1,1) NOT NULL,
	[ShiftId] [int] NULL,
	[UserLoggedIn] [int] NULL,
	[UserLoggedInDateTime] [datetime] NULL,
	[UserLoggedOut] [int] NULL,
	[UserLoggedOutDateTime] [datetime] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[ModifiedDate] [nchar](10) NULL,
	[ModifiedBy] [int] NULL,
 CONSTRAINT [PK_ShiftDetails] PRIMARY KEY CLUSTERED 
(
	[ShiftDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SystemConfig]    Script Date: 1/2/2018 7:03:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SystemConfig](
	[SystemConfigId] [int] IDENTITY(1,1) NOT NULL,
	[SystemConfigKey] [nvarchar](100) NULL,
	[SystemConfigValue] [nvarchar](100) NULL,
 CONSTRAINT [PK_SystemConfig] PRIMARY KEY CLUSTERED 
(
	[SystemConfigId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Token]    Script Date: 1/2/2018 7:03:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Token](
	[TokenId] [int] IDENTITY(1,1) NOT NULL,
	[OperationId] [int] NOT NULL,
	[IssueDateTime] [datetime] NULL,
	[TokenValidity] [datetime] NULL,
	[VehiclePlateNo] [nvarchar](100) NULL,
	[CategoryFeesId] [int] NULL,
	[TokenNo] [nvarchar](100) NOT NULL,
	[PurposeOfVisit] [int] NULL,
	[PlaceOfVisitId] [int] NULL,
	[Noofpassengers] [int] NULL,
	[IsNicAvailable] [bit] NOT NULL,
	[IsClosed] [bit] NOT NULL,
	[closedDatetime] [datetime] NULL,
	[GoAheadReasonWithoutNIC] [text] NULL,
	[DatetimeCanceled] [datetime] NULL,
	[Reason] [text] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifedBy] [int] NULL,
 CONSTRAINT [PK_Token] PRIMARY KEY CLUSTERED 
(
	[TokenId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TokenCancelledRequestDetails]    Script Date: 1/2/2018 7:03:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TokenCancelledRequestDetails](
	[TokenCancelledRequestDetail] [int] IDENTITY(1,1) NOT NULL,
	[TokenDetailId] [int] NOT NULL,
	[TokenCancelRequestId] [int] NOT NULL,
 CONSTRAINT [PK_TokenCancelledRequestDetails] PRIMARY KEY CLUSTERED 
(
	[TokenCancelledRequestDetail] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TokenCancelRequest]    Script Date: 1/2/2018 7:03:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TokenCancelRequest](
	[TokenCancelRequestId] [int] IDENTITY(1,1) NOT NULL,
	[TokenId] [int] NOT NULL,
	[NoofTickets] [int] NULL,
	[CategoryId] [int] NOT NULL,
	[RefundedAmount] [decimal](18, 2) NULL,
	[DateTimeCancelled] [datetime] NULL,
	[Reason] [nvarchar](max) NULL,
	[CancelStatusId] [int] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_TokenCancelRequest] PRIMARY KEY CLUSTERED 
(
	[TokenCancelRequestId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TokenDetails]    Script Date: 1/2/2018 7:03:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TokenDetails](
	[TokenDetailsId] [int] IDENTITY(1,1) NOT NULL,
	[TokenId] [int] NOT NULL,
	[TokenNo] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[NIC] [nvarchar](15) NOT NULL,
	[Prepaid] [nvarchar](50) NULL,
	[IsDriver] [bit] NOT NULL,
	[isCancel] [bit] NOT NULL,
	[Fees] [decimal](18, 2) NULL,
 CONSTRAINT [PK_TokenDetails] PRIMARY KEY CLUSTERED 
(
	[TokenDetailsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TokenTransaction]    Script Date: 1/2/2018 7:03:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TokenTransaction](
	[TokenTransactionID] [int] IDENTITY(1,1) NOT NULL,
	[TokendetailId] [int] NOT NULL,
	[OperationId] [int] NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[isPayIn] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_TokenTransaction] PRIMARY KEY CLUSTERED 
(
	[TokenTransactionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[AppEmployees] ON 

INSERT [dbo].[AppEmployees] ([EmployeeID], [EmployeeGUID], [UserId], [Name], [Email], [PersonalEmail], [MobileNo], [TelNo], [TelNoHome], [FaxNo], [FaxNoHome], [Designation], [DepartmentId], [Salary], [CNICNumber], [ProfilePic], [ProfilePicThumbNail], [YahooID], [SkypeID], [BBMCode], [Signature], [Notes], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (5, N'eb114868-9141-44f9-beef-550427cfc742', 5, N'shift manager', N's@c.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'User22393e11-49a0-426e-a195-5635d0fdfc62.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 0, CAST(0x0000A85101166789 AS DateTime), 5, CAST(0x0000A85600D5C8AD AS DateTime))
INSERT [dbo].[AppEmployees] ([EmployeeID], [EmployeeGUID], [UserId], [Name], [Email], [PersonalEmail], [MobileNo], [TelNo], [TelNoHome], [FaxNo], [FaxNoHome], [Designation], [DepartmentId], [Salary], [CNICNumber], [ProfilePic], [ProfilePicThumbNail], [YahooID], [SkypeID], [BBMCode], [Signature], [Notes], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (6, N'bea8dd51-69b7-4c53-a907-5a9c91295a65', 6, N'operator1', N'operator1@hotmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Userc7b5201c-8084-4bfe-a488-c6d41b7032aa.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 0, CAST(0x0000A8510116AC5D AS DateTime), NULL, NULL)
INSERT [dbo].[AppEmployees] ([EmployeeID], [EmployeeGUID], [UserId], [Name], [Email], [PersonalEmail], [MobileNo], [TelNo], [TelNoHome], [FaxNo], [FaxNoHome], [Designation], [DepartmentId], [Salary], [CNICNumber], [ProfilePic], [ProfilePicThumbNail], [YahooID], [SkypeID], [BBMCode], [Signature], [Notes], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (7, N'31dd15b5-87bc-433c-9447-8d9720848ef2', 7, N'admin', N'mamaztt@sepiasolutions.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'User9b6cf9ad-836f-4a9c-bf38-5fb72f2e6708.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 0, CAST(0x0000A85101343C07 AS DateTime), NULL, NULL)
INSERT [dbo].[AppEmployees] ([EmployeeID], [EmployeeGUID], [UserId], [Name], [Email], [PersonalEmail], [MobileNo], [TelNo], [TelNoHome], [FaxNo], [FaxNoHome], [Designation], [DepartmentId], [Salary], [CNICNumber], [ProfilePic], [ProfilePicThumbNail], [YahooID], [SkypeID], [BBMCode], [Signature], [Notes], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (15, N'ffe77501-4540-4c4a-a8ee-6da08e60b03b', 15, N'operator3', N'operator3@hotmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7, CAST(0x0000A857011FCA0A AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[AppEmployees] OFF
SET IDENTITY_INSERT [dbo].[AppModulePermissions] ON 

INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (194, 26, N'FullAccess', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (195, 26, N'Create', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (196, 26, N'Edit', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (197, 26, N'Delete', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (198, 26, N'Detail', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (199, 26, N'Index', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (200, 27, N'FullAccess', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (201, 27, N'Create', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (202, 27, N'Edit', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (203, 27, N'Delete', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (204, 27, N'Detail', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (205, 27, N'Index', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (206, 28, N'FullAccess', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (207, 28, N'Create', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (208, 28, N'Edit', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (209, 28, N'Delete', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (210, 28, N'Detail', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (211, 28, N'Index', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (212, 29, N'FullAccess', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (213, 29, N'Create', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (214, 29, N'Edit', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (215, 29, N'Delete', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (216, 29, N'Detail', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (217, 29, N'Index', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (218, 30, N'FullAccess', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (219, 30, N'Create', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (220, 30, N'Edit', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (221, 30, N'Delete', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (222, 30, N'Detail', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (223, 30, N'Index', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (224, 31, N'FullAccess', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (225, 31, N'Create', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (226, 31, N'Edit', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (227, 31, N'Delete', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (228, 31, N'Detail', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (229, 31, N'Index', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (230, 32, N'FullAccess', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (231, 32, N'Create', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (232, 32, N'Edit', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (233, 32, N'Delete', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (234, 32, N'Detail', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (235, 32, N'Index', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (236, 33, N'FullAccess', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (237, 33, N'Create', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (238, 33, N'Edit', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (239, 33, N'Delete', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (240, 33, N'Detail', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (241, 33, N'Index', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (242, 34, N'FullAccess', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (243, 34, N'Create', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (244, 34, N'Edit', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (245, 34, N'Delete', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (246, 34, N'Detail', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (247, 34, N'Index', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (275, 36, N'ViewProfile', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (276, 36, N'FullAccess', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (277, 36, N'Create', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (278, 36, N'Edit', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (279, 36, N'Delete', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (280, 36, N'Detail', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (281, 36, N'Index', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (407, 25, N'Index', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (408, 25, N'AllTokens', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (409, 25, N'Index', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (410, 25, N'GetTokenDetail', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (411, 25, N'Index', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (412, 25, N'MyOperations', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (413, 25, N'Index', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (414, 25, N'TokenCancel', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (415, 25, N'TokenCancellationManager', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (416, 25, N'Index', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (417, 25, N'generateToken', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (418, 25, N'Index', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (419, 25, N'TokenRefundAmount', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (420, 25, N'Index', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (421, 25, N'Approval', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (422, 25, N'Index', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (423, 25, N'CancelledStatus', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (424, 25, N'Index', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (425, 25, N'TokenAmount', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (426, 25, N'FullAccess', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (427, 25, N'Create', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (428, 25, N'Edit', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (429, 25, N'Delete', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (430, 25, N'Detail', 1)
INSERT [dbo].[AppModulePermissions] ([PermissionID], [ModuleID], [PermissionName], [isAvailable]) VALUES (431, 25, N'Index', 1)
SET IDENTITY_INSERT [dbo].[AppModulePermissions] OFF
SET IDENTITY_INSERT [dbo].[AppModules] ON 

INSERT [dbo].[AppModules] ([ModuleID], [ModuleName], [ModuleLink], [ModuleIndex], [ModuleParam], [ShowInMenu], [isPrimary], [Sort], [CssClass], [isDisable], [HasFullAccess], [HasCreate], [HasEdit], [HasDelete], [HasView], [HasDetail], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (25, N'Token', N'Token', N'1', NULL, 0, 0, 0, NULL, 0, 1, 1, 1, 1, 1, 1, 5, CAST(0x0000A851012D74B6 AS DateTime), 7, CAST(0x0000A85800D53B90 AS DateTime))
INSERT [dbo].[AppModules] ([ModuleID], [ModuleName], [ModuleLink], [ModuleIndex], [ModuleParam], [ShowInMenu], [isPrimary], [Sort], [CssClass], [isDisable], [HasFullAccess], [HasCreate], [HasEdit], [HasDelete], [HasView], [HasDetail], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (26, N'Shift', N'Shift', N'1', NULL, 0, 0, 0, NULL, 0, 1, 1, 1, 1, 1, 1, 5, CAST(0x0000A851012D9F34 AS DateTime), NULL, NULL)
INSERT [dbo].[AppModules] ([ModuleID], [ModuleName], [ModuleLink], [ModuleIndex], [ModuleParam], [ShowInMenu], [isPrimary], [Sort], [CssClass], [isDisable], [HasFullAccess], [HasCreate], [HasEdit], [HasDelete], [HasView], [HasDetail], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (27, N'Operation', N'Operation', N'1', NULL, 0, 0, 0, NULL, 0, 1, 1, 1, 1, 1, 1, 5, CAST(0x0000A851012DC741 AS DateTime), NULL, NULL)
INSERT [dbo].[AppModules] ([ModuleID], [ModuleName], [ModuleLink], [ModuleIndex], [ModuleParam], [ShowInMenu], [isPrimary], [Sort], [CssClass], [isDisable], [HasFullAccess], [HasCreate], [HasEdit], [HasDelete], [HasView], [HasDetail], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (28, N'Home', N'Home', N'1', NULL, 0, 0, 0, NULL, 0, 1, 1, 1, 1, 1, 1, 5, CAST(0x0000A851012DF364 AS DateTime), NULL, NULL)
INSERT [dbo].[AppModules] ([ModuleID], [ModuleName], [ModuleLink], [ModuleIndex], [ModuleParam], [ShowInMenu], [isPrimary], [Sort], [CssClass], [isDisable], [HasFullAccess], [HasCreate], [HasEdit], [HasDelete], [HasView], [HasDetail], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (29, N'Report', N'Report', N'1', NULL, 0, 0, 0, NULL, 0, 1, 1, 1, 1, 1, 1, 5, CAST(0x0000A851012E211E AS DateTime), NULL, NULL)
INSERT [dbo].[AppModules] ([ModuleID], [ModuleName], [ModuleLink], [ModuleIndex], [ModuleParam], [ShowInMenu], [isPrimary], [Sort], [CssClass], [isDisable], [HasFullAccess], [HasCreate], [HasEdit], [HasDelete], [HasView], [HasDetail], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (30, N'Bank Challan', N'bankChallan', N'1', NULL, 0, 0, 0, NULL, 0, 1, 1, 1, 1, 1, 1, 5, CAST(0x0000A851012E4F7D AS DateTime), NULL, NULL)
INSERT [dbo].[AppModules] ([ModuleID], [ModuleName], [ModuleLink], [ModuleIndex], [ModuleParam], [ShowInMenu], [isPrimary], [Sort], [CssClass], [isDisable], [HasFullAccess], [HasCreate], [HasEdit], [HasDelete], [HasView], [HasDetail], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (31, N'Categories', N'Category', N'1', NULL, 0, 0, 0, NULL, 0, 1, 1, 1, 1, 1, 1, 5, CAST(0x0000A851012E805B AS DateTime), NULL, NULL)
INSERT [dbo].[AppModules] ([ModuleID], [ModuleName], [ModuleLink], [ModuleIndex], [ModuleParam], [ShowInMenu], [isPrimary], [Sort], [CssClass], [isDisable], [HasFullAccess], [HasCreate], [HasEdit], [HasDelete], [HasView], [HasDetail], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (32, N'Days', N'Days', N'1', NULL, 0, 0, 0, NULL, 0, 1, 1, 1, 1, 1, 1, 5, CAST(0x0000A851012ED0EA AS DateTime), NULL, NULL)
INSERT [dbo].[AppModules] ([ModuleID], [ModuleName], [ModuleLink], [ModuleIndex], [ModuleParam], [ShowInMenu], [isPrimary], [Sort], [CssClass], [isDisable], [HasFullAccess], [HasCreate], [HasEdit], [HasDelete], [HasView], [HasDetail], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (33, N'Group', N'Group', N'1', NULL, 0, 0, 0, NULL, 0, 1, 1, 1, 1, 1, 1, 5, CAST(0x0000A851012F19AF AS DateTime), NULL, NULL)
INSERT [dbo].[AppModules] ([ModuleID], [ModuleName], [ModuleLink], [ModuleIndex], [ModuleParam], [ShowInMenu], [isPrimary], [Sort], [CssClass], [isDisable], [HasFullAccess], [HasCreate], [HasEdit], [HasDelete], [HasView], [HasDetail], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (34, N'Module', N'Module', N'1', NULL, 0, 0, 0, NULL, 0, 1, 1, 1, 1, 1, 1, 5, CAST(0x0000A851012F5F2B AS DateTime), NULL, NULL)
INSERT [dbo].[AppModules] ([ModuleID], [ModuleName], [ModuleLink], [ModuleIndex], [ModuleParam], [ShowInMenu], [isPrimary], [Sort], [CssClass], [isDisable], [HasFullAccess], [HasCreate], [HasEdit], [HasDelete], [HasView], [HasDetail], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (36, N'Users', N'User', N'1', NULL, 1, 0, 0, NULL, 0, 1, 1, 1, 1, 1, 1, 7, CAST(0x0000A852013F93D8 AS DateTime), 7, CAST(0x0000A85700F2D87B AS DateTime))
SET IDENTITY_INSERT [dbo].[AppModules] OFF
SET IDENTITY_INSERT [dbo].[AppRoles] ON 

INSERT [dbo].[AppRoles] ([RoleId], [RoleName], [Priority], [isEditable], [isDisable], [CreatedBy], [CreatedDate], [ModifiedBy], [Modified]) VALUES (1, N'Admin', 50, 0, 0, 0, CAST(0x0000A81500C71D18 AS DateTime), 7, CAST(0x0000A85700F9AE2A AS DateTime))
INSERT [dbo].[AppRoles] ([RoleId], [RoleName], [Priority], [isEditable], [isDisable], [CreatedBy], [CreatedDate], [ModifiedBy], [Modified]) VALUES (2, N'Manager', 10, 0, 0, 0, CAST(0x0000A81500C73279 AS DateTime), 7, CAST(0x0000A857010CCA36 AS DateTime))
INSERT [dbo].[AppRoles] ([RoleId], [RoleName], [Priority], [isEditable], [isDisable], [CreatedBy], [CreatedDate], [ModifiedBy], [Modified]) VALUES (42, N'Operator', 20, 1, 0, NULL, NULL, 7, CAST(0x0000A85800D56585 AS DateTime))
SET IDENTITY_INSERT [dbo].[AppRoles] OFF
SET IDENTITY_INSERT [dbo].[AppRolesModules] ON 

INSERT [dbo].[AppRolesModules] ([RolesModulesID], [RolesId], [ModuleId], [IsAllowed], [Permissions], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (626, 1, 25, 0, N'FullAccess:false, Create:false, Edit:false, Delete:false, Detail:false, Index:false', NULL, NULL, NULL, NULL)
INSERT [dbo].[AppRolesModules] ([RolesModulesID], [RolesId], [ModuleId], [IsAllowed], [Permissions], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (627, 1, 26, 0, N'FullAccess:false, Create:false, Edit:false, Delete:false, Detail:false, Index:false', NULL, NULL, NULL, NULL)
INSERT [dbo].[AppRolesModules] ([RolesModulesID], [RolesId], [ModuleId], [IsAllowed], [Permissions], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (628, 1, 27, 0, N'FullAccess:false, Create:false, Edit:false, Delete:false, Detail:false, Index:false', NULL, NULL, NULL, NULL)
INSERT [dbo].[AppRolesModules] ([RolesModulesID], [RolesId], [ModuleId], [IsAllowed], [Permissions], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (629, 1, 28, 0, N'FullAccess:false, Create:false, Edit:false, Delete:false, Detail:false, Index:false', NULL, NULL, NULL, NULL)
INSERT [dbo].[AppRolesModules] ([RolesModulesID], [RolesId], [ModuleId], [IsAllowed], [Permissions], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (630, 1, 29, 0, N'FullAccess:false, Create:false, Edit:false, Delete:false, Detail:false, Index:false', NULL, NULL, NULL, NULL)
INSERT [dbo].[AppRolesModules] ([RolesModulesID], [RolesId], [ModuleId], [IsAllowed], [Permissions], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (631, 1, 30, 0, N'FullAccess:false, Create:false, Edit:false, Delete:false, Detail:false, Index:false', NULL, NULL, NULL, NULL)
INSERT [dbo].[AppRolesModules] ([RolesModulesID], [RolesId], [ModuleId], [IsAllowed], [Permissions], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (632, 1, 31, 0, N'FullAccess:false, Create:false, Edit:false, Delete:false, Detail:false, Index:false', NULL, NULL, NULL, NULL)
INSERT [dbo].[AppRolesModules] ([RolesModulesID], [RolesId], [ModuleId], [IsAllowed], [Permissions], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (633, 1, 32, 0, N'FullAccess:false, Create:false, Edit:false, Delete:false, Detail:false, Index:false', NULL, NULL, NULL, NULL)
INSERT [dbo].[AppRolesModules] ([RolesModulesID], [RolesId], [ModuleId], [IsAllowed], [Permissions], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (634, 1, 33, 1, N'FullAccess:true, Create:true, Edit:true, Delete:true, Detail:true, Index:true', NULL, NULL, NULL, NULL)
INSERT [dbo].[AppRolesModules] ([RolesModulesID], [RolesId], [ModuleId], [IsAllowed], [Permissions], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (635, 1, 34, 1, N'FullAccess:true, Create:true, Edit:true, Delete:true, Detail:true, Index:true', NULL, NULL, NULL, NULL)
INSERT [dbo].[AppRolesModules] ([RolesModulesID], [RolesId], [ModuleId], [IsAllowed], [Permissions], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (637, 1, 36, 1, N'ViewProfile:true, FullAccess:true, Create:true, Edit:true, Delete:true, Detail:true, Index:true', NULL, NULL, NULL, NULL)
INSERT [dbo].[AppRolesModules] ([RolesModulesID], [RolesId], [ModuleId], [IsAllowed], [Permissions], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (770, 2, 25, 1, N'Index:true, AllTokens:true, Index:true, GetTokenDetail:true, Index:true, MyOperations:false, Index:true, TokenCancel:false, TokenCancellationManager:true, Index:true, generateToken:false, Index:true, TokenRefundAmount:false, Index:true, Approval:false, Index:true, CancelledStatus:true, FullAccess:false, Create:false, Edit:false, Delete:false, Detail:false, Index:true', NULL, NULL, NULL, NULL)
INSERT [dbo].[AppRolesModules] ([RolesModulesID], [RolesId], [ModuleId], [IsAllowed], [Permissions], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (771, 2, 26, 1, N'FullAccess:true, Create:true, Edit:true, Delete:true, Detail:true, Index:true', NULL, NULL, NULL, NULL)
INSERT [dbo].[AppRolesModules] ([RolesModulesID], [RolesId], [ModuleId], [IsAllowed], [Permissions], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (772, 2, 27, 1, N'FullAccess:true, Create:true, Edit:true, Delete:true, Detail:true, Index:true', NULL, NULL, NULL, NULL)
INSERT [dbo].[AppRolesModules] ([RolesModulesID], [RolesId], [ModuleId], [IsAllowed], [Permissions], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (773, 2, 28, 1, N'FullAccess:true, Create:true, Edit:true, Delete:true, Detail:true, Index:true', NULL, NULL, NULL, NULL)
INSERT [dbo].[AppRolesModules] ([RolesModulesID], [RolesId], [ModuleId], [IsAllowed], [Permissions], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (774, 2, 29, 1, N'FullAccess:true, Create:true, Edit:true, Delete:true, Detail:true, Index:true', NULL, NULL, NULL, NULL)
INSERT [dbo].[AppRolesModules] ([RolesModulesID], [RolesId], [ModuleId], [IsAllowed], [Permissions], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (775, 2, 30, 1, N'FullAccess:true, Create:true, Edit:false, Delete:true, Detail:true, Index:true', NULL, NULL, NULL, NULL)
INSERT [dbo].[AppRolesModules] ([RolesModulesID], [RolesId], [ModuleId], [IsAllowed], [Permissions], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (776, 2, 31, 1, N'FullAccess:true, Create:true, Edit:true, Delete:true, Detail:true, Index:true', NULL, NULL, NULL, NULL)
INSERT [dbo].[AppRolesModules] ([RolesModulesID], [RolesId], [ModuleId], [IsAllowed], [Permissions], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (777, 2, 32, 1, N'FullAccess:true, Create:true, Edit:true, Delete:true, Detail:true, Index:true', NULL, NULL, NULL, NULL)
INSERT [dbo].[AppRolesModules] ([RolesModulesID], [RolesId], [ModuleId], [IsAllowed], [Permissions], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (778, 2, 33, 0, N'FullAccess:false, Create:false, Edit:false, Delete:false, Detail:false, Index:false', NULL, NULL, NULL, NULL)
INSERT [dbo].[AppRolesModules] ([RolesModulesID], [RolesId], [ModuleId], [IsAllowed], [Permissions], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (779, 2, 34, 0, N'FullAccess:false, Create:false, Edit:false, Delete:false, Detail:false, Index:false', NULL, NULL, NULL, NULL)
INSERT [dbo].[AppRolesModules] ([RolesModulesID], [RolesId], [ModuleId], [IsAllowed], [Permissions], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (781, 2, 36, 1, N'ViewProfile:true, FullAccess:false, Create:false, Edit:false, Delete:false, Detail:false, Index:false', NULL, NULL, NULL, NULL)
INSERT [dbo].[AppRolesModules] ([RolesModulesID], [RolesId], [ModuleId], [IsAllowed], [Permissions], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (782, 42, 25, 1, N'Index:true, AllTokens:false, Index:true, GetTokenDetail:true, Index:true, MyOperations:true, Index:true, TokenCancel:true, TokenCancellationManager:false, Index:true, generateToken:true, Index:true, TokenRefundAmount:true, Index:true, Approval:true, Index:true, CancelledStatus:false, Index:true, TokenAmount:true, FullAccess:false, Create:false, Edit:false, Delete:false, Detail:false, Index:true', NULL, NULL, NULL, NULL)
INSERT [dbo].[AppRolesModules] ([RolesModulesID], [RolesId], [ModuleId], [IsAllowed], [Permissions], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (783, 42, 26, 0, N'FullAccess:false, Create:false, Edit:false, Delete:false, Detail:false, Index:false', NULL, NULL, NULL, NULL)
INSERT [dbo].[AppRolesModules] ([RolesModulesID], [RolesId], [ModuleId], [IsAllowed], [Permissions], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (784, 42, 27, 1, N'FullAccess:true, Create:true, Edit:true, Delete:true, Detail:true, Index:true', NULL, NULL, NULL, NULL)
INSERT [dbo].[AppRolesModules] ([RolesModulesID], [RolesId], [ModuleId], [IsAllowed], [Permissions], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (785, 42, 28, 1, N'FullAccess:false, Create:false, Edit:false, Delete:false, Detail:false, Index:true', NULL, NULL, NULL, NULL)
INSERT [dbo].[AppRolesModules] ([RolesModulesID], [RolesId], [ModuleId], [IsAllowed], [Permissions], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (786, 42, 29, 0, N'FullAccess:false, Create:false, Edit:false, Delete:false, Detail:false, Index:false', NULL, NULL, NULL, NULL)
INSERT [dbo].[AppRolesModules] ([RolesModulesID], [RolesId], [ModuleId], [IsAllowed], [Permissions], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (787, 42, 30, 0, N'FullAccess:false, Create:false, Edit:false, Delete:false, Detail:false, Index:false', NULL, NULL, NULL, NULL)
INSERT [dbo].[AppRolesModules] ([RolesModulesID], [RolesId], [ModuleId], [IsAllowed], [Permissions], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (788, 42, 31, 0, N'FullAccess:false, Create:false, Edit:false, Delete:false, Detail:false, Index:false', NULL, NULL, NULL, NULL)
INSERT [dbo].[AppRolesModules] ([RolesModulesID], [RolesId], [ModuleId], [IsAllowed], [Permissions], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (789, 42, 32, 0, N'FullAccess:false, Create:false, Edit:false, Delete:false, Detail:false, Index:false', NULL, NULL, NULL, NULL)
INSERT [dbo].[AppRolesModules] ([RolesModulesID], [RolesId], [ModuleId], [IsAllowed], [Permissions], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (790, 42, 33, 0, N'FullAccess:false, Create:false, Edit:false, Delete:false, Detail:false, Index:false', NULL, NULL, NULL, NULL)
INSERT [dbo].[AppRolesModules] ([RolesModulesID], [RolesId], [ModuleId], [IsAllowed], [Permissions], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (791, 42, 34, 0, N'FullAccess:false, Create:false, Edit:false, Delete:false, Detail:false, Index:false', NULL, NULL, NULL, NULL)
INSERT [dbo].[AppRolesModules] ([RolesModulesID], [RolesId], [ModuleId], [IsAllowed], [Permissions], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (792, 42, 36, 1, N'ViewProfile:true, FullAccess:false, Create:false, Edit:false, Delete:false, Detail:false, Index:false', NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[AppRolesModules] OFF
SET IDENTITY_INSERT [dbo].[AppUserLogs] ON 

INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (1188, 6, N'::1', N'Chrome 63.0', NULL, CAST(0x0000A857011F5F71 AS DateTime), CAST(0x0000A857011F6849 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (1189, 7, N'::1', N'Chrome 63.0', NULL, CAST(0x0000A857011F72A5 AS DateTime), CAST(0x0000A857011F87ED AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (1190, 5, N'::1', N'Chrome 63.0', NULL, CAST(0x0000A857011F8CA8 AS DateTime), CAST(0x0000A857011F9214 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (1191, 7, N'::1', N'Chrome 63.0', NULL, CAST(0x0000A857011F98D6 AS DateTime), CAST(0x0000A8570120247F AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (1192, 15, N'::1', N'Chrome 63.0', NULL, CAST(0x0000A85701202CE0 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (1193, 15, N'::1', N'Chrome 63.0', NULL, CAST(0x0000A8570121BC2F AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (1194, 7, N'::1', N'Chrome 63.0', NULL, CAST(0x0000A8570125278C AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (1195, 7, N'::1', N'Chrome 63.0', NULL, CAST(0x0000A857012C86DA AS DateTime), CAST(0x0000A857012C9787 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (1196, 5, N'::1', N'Chrome 63.0', NULL, CAST(0x0000A857012C9E46 AS DateTime), CAST(0x0000A857012CB1DC AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (1197, 6, N'::1', N'Chrome 63.0', NULL, CAST(0x0000A85800CE9521 AS DateTime), CAST(0x0000A85800D13E55 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (1198, 6, N'::1', N'Chrome 63.0', NULL, CAST(0x0000A85800D15851 AS DateTime), CAST(0x0000A85800D15BF6 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (1199, 6, N'::1', N'Chrome 63.0', NULL, CAST(0x0000A85800D19ADC AS DateTime), CAST(0x0000A85800D25055 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (1200, 5, N'::1', N'Chrome 63.0', NULL, CAST(0x0000A85800D25640 AS DateTime), CAST(0x0000A85800D2C698 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (1201, 6, N'::1', N'Chrome 63.0', NULL, CAST(0x0000A85800D2CA85 AS DateTime), CAST(0x0000A85800D51046 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (1202, 7, N'::1', N'Chrome 63.0', NULL, CAST(0x0000A85800D51404 AS DateTime), CAST(0x0000A85800D5696B AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (1203, 6, N'::1', N'Chrome 63.0', NULL, CAST(0x0000A85800D56E50 AS DateTime), CAST(0x0000A85800D88E7D AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (1204, 5, N'::1', N'Chrome 63.0', NULL, CAST(0x0000A85800D8949A AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (1205, 6, N'::1', N'Chrome 63.0', NULL, CAST(0x0000A85800E2EE1F AS DateTime), CAST(0x0000A85800E2F5FB AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (1206, 7, N'::1', N'Chrome 63.0', NULL, CAST(0x0000A85800E2FD4B AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (1207, 5, N'::1', N'Chrome 63.0', NULL, CAST(0x0000A85800E652DC AS DateTime), CAST(0x0000A85800E666D5 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (1208, 6, N'::1', N'Chrome 63.0', NULL, CAST(0x0000A85800E66A40 AS DateTime), CAST(0x0000A85800E75799 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (1209, 5, N'::1', N'Chrome 63.0', NULL, CAST(0x0000A85800E75DE4 AS DateTime), CAST(0x0000A85800E83E24 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (1210, 6, N'::1', N'Chrome 63.0', NULL, CAST(0x0000A85800E841B5 AS DateTime), CAST(0x0000A85800E8448F AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (1211, 6, N'::1', N'Chrome 63.0', NULL, CAST(0x0000A85800E84614 AS DateTime), CAST(0x0000A85800E86D71 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (1212, 7, N'::1', N'Chrome 63.0', NULL, CAST(0x0000A85800E8714A AS DateTime), CAST(0x0000A85800E89F46 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (1213, 6, N'::1', N'Chrome 63.0', NULL, CAST(0x0000A85800E8A121 AS DateTime), CAST(0x0000A85800E8B08C AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (1214, 7, N'::1', N'Chrome 63.0', NULL, CAST(0x0000A85800E8B446 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (1215, 6, N'::1', N'Chrome 63.0', NULL, CAST(0x0000A85800EBB75F AS DateTime), CAST(0x0000A85800EED543 AS DateTime))
SET IDENTITY_INSERT [dbo].[AppUserLogs] OFF
SET IDENTITY_INSERT [dbo].[AppUserMeta] ON 

INSERT [dbo].[AppUserMeta] ([UserMetaID], [UserId], [MetaKey], [MetaValue], [isInternal]) VALUES (1, 5, N'Thumbnail-Original', NULL, 0)
SET IDENTITY_INSERT [dbo].[AppUserMeta] OFF
SET IDENTITY_INSERT [dbo].[AppUsers] ON 

INSERT [dbo].[AppUsers] ([UserId], [UserGUID], [isEmployee], [RoleId], [UserName], [Password], [PassKey], [ExpiryDate], [LastLogn], [LoginCount], [isActive], [isDisable], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (5, N'9625febf-b952-4e6e-9701-3c6af3cba18b', 1, 2, N'shiftmanager', N'VV/lS2TZp+A=', NULL, NULL, CAST(0x0000A85800E75DE4 AS DateTime), 88, 1, 0, 0, CAST(0x0000A85101166789 AS DateTime), NULL, NULL)
INSERT [dbo].[AppUsers] ([UserId], [UserGUID], [isEmployee], [RoleId], [UserName], [Password], [PassKey], [ExpiryDate], [LastLogn], [LoginCount], [isActive], [isDisable], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (6, N'62cde3b6-61c1-43f1-a401-ccd0e7444619', 1, 42, N'operator1', N'VV/lS2TZp+A=', NULL, NULL, CAST(0x0000A85800EBB75F AS DateTime), 126, 1, 0, 0, CAST(0x0000A8510116AC5D AS DateTime), NULL, NULL)
INSERT [dbo].[AppUsers] ([UserId], [UserGUID], [isEmployee], [RoleId], [UserName], [Password], [PassKey], [ExpiryDate], [LastLogn], [LoginCount], [isActive], [isDisable], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (7, N'd065d651-0acd-4b1c-9165-6ac50fa42cf8', 1, 1, N'admin', N'VV/lS2TZp+A=', NULL, NULL, CAST(0x0000A85800E8B446 AS DateTime), 70, 1, 0, 0, CAST(0x0000A85101343C07 AS DateTime), NULL, NULL)
INSERT [dbo].[AppUsers] ([UserId], [UserGUID], [isEmployee], [RoleId], [UserName], [Password], [PassKey], [ExpiryDate], [LastLogn], [LoginCount], [isActive], [isDisable], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (15, N'71a4b8c9-0add-440d-8784-31a85e40b1d0', 1, 1, N'operator2', N'VV/lS2TZp+A=', NULL, NULL, CAST(0x0000A8570121BC2F AS DateTime), 2, 1, 0, 7, CAST(0x0000A857011FCA0A AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[AppUsers] OFF
SET IDENTITY_INSERT [dbo].[Bank] ON 

INSERT [dbo].[Bank] ([BankId], [BankName], [Slip]) VALUES (2, N'Islamic Bank', NULL)
SET IDENTITY_INSERT [dbo].[Bank] OFF
SET IDENTITY_INSERT [dbo].[BankChallan] ON 

INSERT [dbo].[BankChallan] ([BankChallanId], [DayId], [BankId], [ChallanNo], [ChallanDatetime], [isChallanSubmitedtoBank], [DepositSlip], [TotalAmount], [TotalAmountinword], [CreatedBy], [CreatedDatetime], [ModifiedBy], [ModifiedDatetime]) VALUES (108, 167, 2, N'291670', CAST(0x0000A85800D90543 AS DateTime), 0, NULL, CAST(170.00 AS Decimal(18, 2)), NULL, NULL, CAST(0x0000A85800D90543 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[BankChallan] OFF
SET IDENTITY_INSERT [dbo].[BankChallanDetail] ON 

INSERT [dbo].[BankChallanDetail] ([BankChallenDetailsId], [BankChallanId], [TokenDetailId], [Fees]) VALUES (478, 108, 210, CAST(50.00 AS Decimal(18, 2)))
INSERT [dbo].[BankChallanDetail] ([BankChallenDetailsId], [BankChallanId], [TokenDetailId], [Fees]) VALUES (479, 108, 211, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[BankChallanDetail] ([BankChallenDetailsId], [BankChallanId], [TokenDetailId], [Fees]) VALUES (480, 108, 212, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[BankChallanDetail] ([BankChallenDetailsId], [BankChallanId], [TokenDetailId], [Fees]) VALUES (481, 108, 213, CAST(0.00 AS Decimal(18, 2)))
INSERT [dbo].[BankChallanDetail] ([BankChallenDetailsId], [BankChallanId], [TokenDetailId], [Fees]) VALUES (482, 108, 214, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[BankChallanDetail] ([BankChallenDetailsId], [BankChallanId], [TokenDetailId], [Fees]) VALUES (483, 108, 215, CAST(0.00 AS Decimal(18, 2)))
INSERT [dbo].[BankChallanDetail] ([BankChallenDetailsId], [BankChallanId], [TokenDetailId], [Fees]) VALUES (484, 108, 216, CAST(70.00 AS Decimal(18, 2)))
INSERT [dbo].[BankChallanDetail] ([BankChallenDetailsId], [BankChallanId], [TokenDetailId], [Fees]) VALUES (485, 108, 217, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[BankChallanDetail] ([BankChallenDetailsId], [BankChallanId], [TokenDetailId], [Fees]) VALUES (486, 108, 218, CAST(10.00 AS Decimal(18, 2)))
SET IDENTITY_INSERT [dbo].[BankChallanDetail] OFF
SET IDENTITY_INSERT [dbo].[CancelStatus] ON 

INSERT [dbo].[CancelStatus] ([CancelStatusId], [CancelStatus]) VALUES (1, N'Pending')
INSERT [dbo].[CancelStatus] ([CancelStatusId], [CancelStatus]) VALUES (2, N'Approved')
INSERT [dbo].[CancelStatus] ([CancelStatusId], [CancelStatus]) VALUES (3, N'Reject')
INSERT [dbo].[CancelStatus] ([CancelStatusId], [CancelStatus]) VALUES (4, N'Refund')
SET IDENTITY_INSERT [dbo].[CancelStatus] OFF
SET IDENTITY_INSERT [dbo].[Category] ON 

INSERT [dbo].[Category] ([CategoryId], [CategoryName], [Narration]) VALUES (2, N'Individual', N'Pass for Individual')
INSERT [dbo].[Category] ([CategoryId], [CategoryName], [Narration]) VALUES (3, N'MotorCycle', N'Commercial Pass')
INSERT [dbo].[Category] ([CategoryId], [CategoryName], [Narration]) VALUES (4, N'non-commercial', N'Vehicle Pass')
INSERT [dbo].[Category] ([CategoryId], [CategoryName], [Narration]) VALUES (8, N'Commecial Vehicle', N'Pass')
INSERT [dbo].[Category] ([CategoryId], [CategoryName], [Narration]) VALUES (10, N'Commercial Vehicle above', N'10 wheelers')
INSERT [dbo].[Category] ([CategoryId], [CategoryName], [Narration]) VALUES (11, N'PrePaid', N'Free Pass')
SET IDENTITY_INSERT [dbo].[Category] OFF
SET IDENTITY_INSERT [dbo].[CategoryFees] ON 

INSERT [dbo].[CategoryFees] ([CategoryFeeId], [CategoryId], [Fees], [BoxClass], [ClassName], [EffectiveDate], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, 2, CAST(10.00 AS Decimal(18, 2)), N'green-box', N'green', CAST(0x0000A84100000000 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[CategoryFees] ([CategoryFeeId], [CategoryId], [Fees], [BoxClass], [ClassName], [EffectiveDate], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, 3, CAST(40.00 AS Decimal(18, 2)), N'orange-box', N'orange', CAST(0x0000A84100000000 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[CategoryFees] ([CategoryFeeId], [CategoryId], [Fees], [BoxClass], [ClassName], [EffectiveDate], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, 4, CAST(50.00 AS Decimal(18, 2)), N'yellow-box', N'yellow', CAST(0x0000A84100000000 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[CategoryFees] ([CategoryFeeId], [CategoryId], [Fees], [BoxClass], [ClassName], [EffectiveDate], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (10, 8, CAST(70.00 AS Decimal(18, 2)), N'pink-box', N'pink', CAST(0x0000A84100000000 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[CategoryFees] ([CategoryFeeId], [CategoryId], [Fees], [BoxClass], [ClassName], [EffectiveDate], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (11, 10, CAST(100.00 AS Decimal(18, 2)), N'blue-box', N'blue', CAST(0x0000A84100000000 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[CategoryFees] ([CategoryFeeId], [CategoryId], [Fees], [BoxClass], [ClassName], [EffectiveDate], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (12, 11, CAST(0.00 AS Decimal(18, 2)), N'sea-green-box', N'sea-green', CAST(0x0000A84100000000 AS DateTime), NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[CategoryFees] OFF
SET IDENTITY_INSERT [dbo].[Day] ON 

INSERT [dbo].[Day] ([DayId], [StartedBy], [StartDateTime], [EndingDateTime], [IsDayClosed], [FinalAmount], [IsBankChallanGenerated], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (167, 5, CAST(0x0000A85800D25A08 AS DateTime), CAST(0x0000A85800D90539 AS DateTime), 1, CAST(170.00 AS Decimal(18, 2)), 1, 5, CAST(0x0000A85800D25A08 AS DateTime), NULL, NULL)
INSERT [dbo].[Day] ([DayId], [StartedBy], [StartDateTime], [EndingDateTime], [IsDayClosed], [FinalAmount], [IsBankChallanGenerated], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (168, 5, CAST(0x0000A85800E65F37 AS DateTime), NULL, 0, CAST(0.00 AS Decimal(18, 2)), 0, 5, CAST(0x0000A85800E65F37 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[Day] OFF
SET IDENTITY_INSERT [dbo].[Operation] ON 

INSERT [dbo].[Operation] ([OperationId], [ShiftId], [OperationStartedBy], [OperationStartingDateTime], [OperationEndingDateTime], [TotalAmountinOperation], [OpeartionClosed], [CreatedBy], [CreatedDate], [ModifyBy], [ModifiedDate]) VALUES (187, 198, 6, CAST(0x0000A85800D2D2FB AS DateTime), CAST(0x0000A85800D44CF7 AS DateTime), CAST(70.00 AS Decimal(18, 2)), 1, 6, CAST(0x0000A85800D2D2FB AS DateTime), NULL, NULL)
INSERT [dbo].[Operation] ([OperationId], [ShiftId], [OperationStartedBy], [OperationStartingDateTime], [OperationEndingDateTime], [TotalAmountinOperation], [OpeartionClosed], [CreatedBy], [CreatedDate], [ModifyBy], [ModifiedDate]) VALUES (188, 198, 6, CAST(0x0000A85800D5724A AS DateTime), CAST(0x0000A85800D6235A AS DateTime), CAST(100.00 AS Decimal(18, 2)), 1, 6, CAST(0x0000A85800D5724A AS DateTime), NULL, NULL)
INSERT [dbo].[Operation] ([OperationId], [ShiftId], [OperationStartedBy], [OperationStartingDateTime], [OperationEndingDateTime], [TotalAmountinOperation], [OpeartionClosed], [CreatedBy], [CreatedDate], [ModifyBy], [ModifiedDate]) VALUES (189, 199, 6, CAST(0x0000A85800E6729A AS DateTime), CAST(0x0000A85800E7518C AS DateTime), CAST(370.00 AS Decimal(18, 2)), 1, 6, CAST(0x0000A85800E6729A AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[Operation] OFF
SET IDENTITY_INSERT [dbo].[PlaceOfVisit] ON 

INSERT [dbo].[PlaceOfVisit] ([PlaceOfVisitId], [PlaceOfVisit]) VALUES (1, N'Abc department')
INSERT [dbo].[PlaceOfVisit] ([PlaceOfVisitId], [PlaceOfVisit]) VALUES (2, N'department 2')
INSERT [dbo].[PlaceOfVisit] ([PlaceOfVisitId], [PlaceOfVisit]) VALUES (3, N'department 3')
INSERT [dbo].[PlaceOfVisit] ([PlaceOfVisitId], [PlaceOfVisit]) VALUES (4, N'department 4')
INSERT [dbo].[PlaceOfVisit] ([PlaceOfVisitId], [PlaceOfVisit]) VALUES (5, N'department 5')
SET IDENTITY_INSERT [dbo].[PlaceOfVisit] OFF
SET IDENTITY_INSERT [dbo].[PurposeOfVisit] ON 

INSERT [dbo].[PurposeOfVisit] ([PurposeOfVisitId], [PurposeOfVIsit]) VALUES (1, N'meeting')
INSERT [dbo].[PurposeOfVisit] ([PurposeOfVisitId], [PurposeOfVIsit]) VALUES (2, N'Personal')
INSERT [dbo].[PurposeOfVisit] ([PurposeOfVisitId], [PurposeOfVIsit]) VALUES (3, N'Travelling')
SET IDENTITY_INSERT [dbo].[PurposeOfVisit] OFF
SET IDENTITY_INSERT [dbo].[Shift] ON 

INSERT [dbo].[Shift] ([ShiftId], [DayId], [ShiftStartedBy], [ShiftStartingDatetime], [ShiftEndingDatetime], [AmountToBeCollected], [ShiftClosed], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (198, 167, 5, CAST(0x0000A85800D25A08 AS DateTime), NULL, CAST(170.00 AS Decimal(18, 2)), 1, 5, CAST(0x0000A85800D25A09 AS DateTime), NULL, NULL)
INSERT [dbo].[Shift] ([ShiftId], [DayId], [ShiftStartedBy], [ShiftStartingDatetime], [ShiftEndingDatetime], [AmountToBeCollected], [ShiftClosed], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (199, 168, 5, CAST(0x0000A85800E65F37 AS DateTime), NULL, NULL, 0, 5, CAST(0x0000A85800E65F38 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[Shift] OFF
SET IDENTITY_INSERT [dbo].[ShiftDetails] ON 

INSERT [dbo].[ShiftDetails] ([ShiftDetailId], [ShiftId], [UserLoggedIn], [UserLoggedInDateTime], [UserLoggedOut], [UserLoggedOutDateTime], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (489, 198, 6, CAST(0x0000A85800D2CA89 AS DateTime), NULL, NULL, CAST(0x0000A85800D2CA89 AS DateTime), 6, NULL, NULL)
INSERT [dbo].[ShiftDetails] ([ShiftDetailId], [ShiftId], [UserLoggedIn], [UserLoggedInDateTime], [UserLoggedOut], [UserLoggedOutDateTime], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (490, 198, 6, CAST(0x0000A85800D56E51 AS DateTime), NULL, NULL, CAST(0x0000A85800D56E51 AS DateTime), 6, NULL, NULL)
INSERT [dbo].[ShiftDetails] ([ShiftDetailId], [ShiftId], [UserLoggedIn], [UserLoggedInDateTime], [UserLoggedOut], [UserLoggedOutDateTime], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (491, 199, 6, CAST(0x0000A85800E66A4D AS DateTime), NULL, NULL, CAST(0x0000A85800E66A4D AS DateTime), 6, NULL, NULL)
INSERT [dbo].[ShiftDetails] ([ShiftDetailId], [ShiftId], [UserLoggedIn], [UserLoggedInDateTime], [UserLoggedOut], [UserLoggedOutDateTime], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (492, 199, 6, CAST(0x0000A85800E841C7 AS DateTime), NULL, NULL, CAST(0x0000A85800E841C7 AS DateTime), 6, NULL, NULL)
INSERT [dbo].[ShiftDetails] ([ShiftDetailId], [ShiftId], [UserLoggedIn], [UserLoggedInDateTime], [UserLoggedOut], [UserLoggedOutDateTime], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (493, 199, 6, CAST(0x0000A85800E84619 AS DateTime), NULL, NULL, CAST(0x0000A85800E84619 AS DateTime), 6, NULL, NULL)
INSERT [dbo].[ShiftDetails] ([ShiftDetailId], [ShiftId], [UserLoggedIn], [UserLoggedInDateTime], [UserLoggedOut], [UserLoggedOutDateTime], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (494, 199, 6, CAST(0x0000A85800E8A131 AS DateTime), NULL, NULL, CAST(0x0000A85800E8A131 AS DateTime), 6, NULL, NULL)
INSERT [dbo].[ShiftDetails] ([ShiftDetailId], [ShiftId], [UserLoggedIn], [UserLoggedInDateTime], [UserLoggedOut], [UserLoggedOutDateTime], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (495, 199, 6, CAST(0x0000A85800EBB771 AS DateTime), NULL, NULL, CAST(0x0000A85800EBB771 AS DateTime), 6, NULL, NULL)
SET IDENTITY_INSERT [dbo].[ShiftDetails] OFF
SET IDENTITY_INSERT [dbo].[SystemConfig] ON 

INSERT [dbo].[SystemConfig] ([SystemConfigId], [SystemConfigKey], [SystemConfigValue]) VALUES (1, N'tokenCancellationTimeOut', N'60')
INSERT [dbo].[SystemConfig] ([SystemConfigId], [SystemConfigKey], [SystemConfigValue]) VALUES (2, N'ImagePathForSave', N'~/Content/Upload/FinalUploadFolder/')
INSERT [dbo].[SystemConfig] ([SystemConfigId], [SystemConfigKey], [SystemConfigValue]) VALUES (3, N'ImagePathForView', N'Content/Upload/FinalUploadFolder/')
INSERT [dbo].[SystemConfig] ([SystemConfigId], [SystemConfigKey], [SystemConfigValue]) VALUES (4, N'URL', N'http://localhost:49599/')
INSERT [dbo].[SystemConfig] ([SystemConfigId], [SystemConfigKey], [SystemConfigValue]) VALUES (5, N'DefaultImage', N'Content/Upload/DefaultImage/Default.png')
INSERT [dbo].[SystemConfig] ([SystemConfigId], [SystemConfigKey], [SystemConfigValue]) VALUES (9, N'ImageThumbNailPathForSave', N'~/Content/Upload/FinalUploadFolderThumbNail/')
INSERT [dbo].[SystemConfig] ([SystemConfigId], [SystemConfigKey], [SystemConfigValue]) VALUES (10, N'ImageThumbNailPathForView', N'Content/Upload/FinalUploadFolderThumbNail/')
SET IDENTITY_INSERT [dbo].[SystemConfig] OFF
SET IDENTITY_INSERT [dbo].[Token] ON 

INSERT [dbo].[Token] ([TokenId], [OperationId], [IssueDateTime], [TokenValidity], [VehiclePlateNo], [CategoryFeesId], [TokenNo], [PurposeOfVisit], [PlaceOfVisitId], [Noofpassengers], [IsNicAvailable], [IsClosed], [closedDatetime], [GoAheadReasonWithoutNIC], [DatetimeCanceled], [Reason], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifedBy]) VALUES (391, 187, CAST(0x0000A85800D42E14 AS DateTime), CAST(0x0000A85801370E94 AS DateTime), NULL, 3, N'2931424-0', NULL, NULL, 3, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Token] ([TokenId], [OperationId], [IssueDateTime], [TokenValidity], [VehiclePlateNo], [CategoryFeesId], [TokenNo], [PurposeOfVisit], [PlaceOfVisitId], [Noofpassengers], [IsNicAvailable], [IsClosed], [closedDatetime], [GoAheadReasonWithoutNIC], [DatetimeCanceled], [Reason], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifedBy]) VALUES (392, 188, CAST(0x0000A85800D5DFFA AS DateTime), CAST(0x0000A8580138C07A AS DateTime), NULL, 12, N'2941685-0', 3, 3, 1, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Token] ([TokenId], [OperationId], [IssueDateTime], [TokenValidity], [VehiclePlateNo], [CategoryFeesId], [TokenNo], [PurposeOfVisit], [PlaceOfVisitId], [Noofpassengers], [IsNicAvailable], [IsClosed], [closedDatetime], [GoAheadReasonWithoutNIC], [DatetimeCanceled], [Reason], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifedBy]) VALUES (393, 188, CAST(0x0000A85800D5EF08 AS DateTime), CAST(0x0000A8580138CF88 AS DateTime), NULL, 1, N'2954533-0', NULL, NULL, 1, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Token] ([TokenId], [OperationId], [IssueDateTime], [TokenValidity], [VehiclePlateNo], [CategoryFeesId], [TokenNo], [PurposeOfVisit], [PlaceOfVisitId], [Noofpassengers], [IsNicAvailable], [IsClosed], [closedDatetime], [GoAheadReasonWithoutNIC], [DatetimeCanceled], [Reason], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifedBy]) VALUES (394, 188, CAST(0x0000A85800D5F996 AS DateTime), CAST(0x0000A8580138DA16 AS DateTime), NULL, 12, N'293538-0', NULL, NULL, 1, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Token] ([TokenId], [OperationId], [IssueDateTime], [TokenValidity], [VehiclePlateNo], [CategoryFeesId], [TokenNo], [PurposeOfVisit], [PlaceOfVisitId], [Noofpassengers], [IsNicAvailable], [IsClosed], [closedDatetime], [GoAheadReasonWithoutNIC], [DatetimeCanceled], [Reason], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifedBy]) VALUES (395, 188, CAST(0x0000A85800D60DBD AS DateTime), CAST(0x0000A8580138EE3D AS DateTime), NULL, 10, N'2920735-0', 1, 5, 3, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Token] ([TokenId], [OperationId], [IssueDateTime], [TokenValidity], [VehiclePlateNo], [CategoryFeesId], [TokenNo], [PurposeOfVisit], [PlaceOfVisitId], [Noofpassengers], [IsNicAvailable], [IsClosed], [closedDatetime], [GoAheadReasonWithoutNIC], [DatetimeCanceled], [Reason], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifedBy]) VALUES (396, 189, CAST(0x0000A85800E67DE9 AS DateTime), CAST(0x0000A85801495E69 AS DateTime), NULL, 1, N'2911709-0', 1, 5, 1, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Token] ([TokenId], [OperationId], [IssueDateTime], [TokenValidity], [VehiclePlateNo], [CategoryFeesId], [TokenNo], [PurposeOfVisit], [PlaceOfVisitId], [Noofpassengers], [IsNicAvailable], [IsClosed], [closedDatetime], [GoAheadReasonWithoutNIC], [DatetimeCanceled], [Reason], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifedBy]) VALUES (397, 189, CAST(0x0000A85800E6C32C AS DateTime), CAST(0x0000A8580149A3AC AS DateTime), N'212esd', 2, N'2910813-0', NULL, 4, 3, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Token] ([TokenId], [OperationId], [IssueDateTime], [TokenValidity], [VehiclePlateNo], [CategoryFeesId], [TokenNo], [PurposeOfVisit], [PlaceOfVisitId], [Noofpassengers], [IsNicAvailable], [IsClosed], [closedDatetime], [GoAheadReasonWithoutNIC], [DatetimeCanceled], [Reason], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifedBy]) VALUES (398, 189, CAST(0x0000A85800E6FDA7 AS DateTime), CAST(0x0000A8580149DE27 AS DateTime), N'2412551', 3, N'290717-0', 1, 5, 3, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Token] ([TokenId], [OperationId], [IssueDateTime], [TokenValidity], [VehiclePlateNo], [CategoryFeesId], [TokenNo], [PurposeOfVisit], [PlaceOfVisitId], [Noofpassengers], [IsNicAvailable], [IsClosed], [closedDatetime], [GoAheadReasonWithoutNIC], [DatetimeCanceled], [Reason], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifedBy]) VALUES (399, 189, CAST(0x0000A85800E71ACF AS DateTime), CAST(0x0000A8580149FB4F AS DateTime), NULL, 10, N'2925595-0', 3, 1, 3, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Token] ([TokenId], [OperationId], [IssueDateTime], [TokenValidity], [VehiclePlateNo], [CategoryFeesId], [TokenNo], [PurposeOfVisit], [PlaceOfVisitId], [Noofpassengers], [IsNicAvailable], [IsClosed], [closedDatetime], [GoAheadReasonWithoutNIC], [DatetimeCanceled], [Reason], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifedBy]) VALUES (400, 189, CAST(0x0000A85800E74306 AS DateTime), CAST(0x0000A858014A2386 AS DateTime), N'r3dd', 11, N'2959912-0', 2, 5, 5, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Token] ([TokenId], [OperationId], [IssueDateTime], [TokenValidity], [VehiclePlateNo], [CategoryFeesId], [TokenNo], [PurposeOfVisit], [PlaceOfVisitId], [Noofpassengers], [IsNicAvailable], [IsClosed], [closedDatetime], [GoAheadReasonWithoutNIC], [DatetimeCanceled], [Reason], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifedBy]) VALUES (401, 189, CAST(0x0000A85800E74D2D AS DateTime), CAST(0x0000A858014A2DAD AS DateTime), NULL, 12, N'298577-0', NULL, NULL, 1, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Token] OFF
SET IDENTITY_INSERT [dbo].[TokenDetails] ON 

INSERT [dbo].[TokenDetails] ([TokenDetailsId], [TokenId], [TokenNo], [Name], [NIC], [Prepaid], [IsDriver], [isCancel], [Fees]) VALUES (210, 391, N'2931424-0', N'TEAG', N'41532-5235135-3', NULL, 1, 0, CAST(50.00 AS Decimal(18, 2)))
INSERT [dbo].[TokenDetails] ([TokenDetailsId], [TokenId], [TokenNo], [Name], [NIC], [Prepaid], [IsDriver], [isCancel], [Fees]) VALUES (211, 391, N'2931424-1', N'AWRAFF', N'35325-2362464-2', NULL, 0, 0, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[TokenDetails] ([TokenDetailsId], [TokenId], [TokenNo], [Name], [NIC], [Prepaid], [IsDriver], [isCancel], [Fees]) VALUES (212, 391, N'2931424-2', N'sadfA', N'95462-3515326-3', NULL, 0, 0, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[TokenDetails] ([TokenDetailsId], [TokenId], [TokenNo], [Name], [NIC], [Prepaid], [IsDriver], [isCancel], [Fees]) VALUES (213, 392, N'2941685-0', N'sfasFAF', N'35325-2464262-3', N'242352356326', 1, 0, CAST(0.00 AS Decimal(18, 2)))
INSERT [dbo].[TokenDetails] ([TokenDetailsId], [TokenId], [TokenNo], [Name], [NIC], [Prepaid], [IsDriver], [isCancel], [Fees]) VALUES (214, 393, N'2954533-0', N'asfasFF', N'24123-5235626-2', NULL, 1, 0, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[TokenDetails] ([TokenDetailsId], [TokenId], [TokenNo], [Name], [NIC], [Prepaid], [IsDriver], [isCancel], [Fees]) VALUES (215, 394, N'293538-0', N'2352TEGF', N'32562-3462452-5', N'4325235236', 1, 0, CAST(0.00 AS Decimal(18, 2)))
INSERT [dbo].[TokenDetails] ([TokenDetailsId], [TokenId], [TokenNo], [Name], [NIC], [Prepaid], [IsDriver], [isCancel], [Fees]) VALUES (216, 395, N'2920735-0', N'SACFASF', N'34325-2353252-3', NULL, 1, 0, CAST(70.00 AS Decimal(18, 2)))
INSERT [dbo].[TokenDetails] ([TokenDetailsId], [TokenId], [TokenNo], [Name], [NIC], [Prepaid], [IsDriver], [isCancel], [Fees]) VALUES (217, 395, N'2920735-1', N'GFEGFEAF', N'35235-2365235-3', NULL, 0, 0, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[TokenDetails] ([TokenDetailsId], [TokenId], [TokenNo], [Name], [NIC], [Prepaid], [IsDriver], [isCancel], [Fees]) VALUES (218, 395, N'2920735-2', N'EFEGF', N'23652-3575478-5', NULL, 0, 0, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[TokenDetails] ([TokenDetailsId], [TokenId], [TokenNo], [Name], [NIC], [Prepaid], [IsDriver], [isCancel], [Fees]) VALUES (219, 396, N'2911709-0', N'ddAd', N'21523-5236263-2', NULL, 1, 0, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[TokenDetails] ([TokenDetailsId], [TokenId], [TokenNo], [Name], [NIC], [Prepaid], [IsDriver], [isCancel], [Fees]) VALUES (220, 397, N'2910813-0', N'sfsafs', N'21435-2352362-4', NULL, 1, 0, CAST(40.00 AS Decimal(18, 2)))
INSERT [dbo].[TokenDetails] ([TokenDetailsId], [TokenId], [TokenNo], [Name], [NIC], [Prepaid], [IsDriver], [isCancel], [Fees]) VALUES (221, 397, N'2910813-1', N'frwr3', N'51241-2523648-4', NULL, 0, 0, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[TokenDetails] ([TokenDetailsId], [TokenId], [TokenNo], [Name], [NIC], [Prepaid], [IsDriver], [isCancel], [Fees]) VALUES (222, 397, N'2910813-2', N'fawrf', N'83462-3513684-6', NULL, 0, 0, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[TokenDetails] ([TokenDetailsId], [TokenId], [TokenNo], [Name], [NIC], [Prepaid], [IsDriver], [isCancel], [Fees]) VALUES (223, 398, N'290717-0', N'asdw', N'24325-3463473-2', NULL, 1, 0, CAST(50.00 AS Decimal(18, 2)))
INSERT [dbo].[TokenDetails] ([TokenDetailsId], [TokenId], [TokenNo], [Name], [NIC], [Prepaid], [IsDriver], [isCancel], [Fees]) VALUES (224, 398, N'290717-1', N'dwwfd', N'31523-6384675-3', NULL, 0, 0, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[TokenDetails] ([TokenDetailsId], [TokenId], [TokenNo], [Name], [NIC], [Prepaid], [IsDriver], [isCancel], [Fees]) VALUES (225, 398, N'290717-2', N'23dfwf', N'15236-4848956-4', NULL, 0, 0, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[TokenDetails] ([TokenDetailsId], [TokenId], [TokenNo], [Name], [NIC], [Prepaid], [IsDriver], [isCancel], [Fees]) VALUES (226, 399, N'2925595-0', N'sfwf', N'12423-5325235-2', NULL, 1, 0, CAST(70.00 AS Decimal(18, 2)))
INSERT [dbo].[TokenDetails] ([TokenDetailsId], [TokenId], [TokenNo], [Name], [NIC], [Prepaid], [IsDriver], [isCancel], [Fees]) VALUES (227, 399, N'2925595-1', N'aawfwfwf', N'63246-3476325-3', NULL, 0, 0, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[TokenDetails] ([TokenDetailsId], [TokenId], [TokenNo], [Name], [NIC], [Prepaid], [IsDriver], [isCancel], [Fees]) VALUES (228, 399, N'2925595-2', N'fwfwc', N'35624-7624562-4', NULL, 0, 0, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[TokenDetails] ([TokenDetailsId], [TokenId], [TokenNo], [Name], [NIC], [Prepaid], [IsDriver], [isCancel], [Fees]) VALUES (229, 400, N'2959912-0', N'sdasd', N'41253-2513553-2', NULL, 1, 0, CAST(100.00 AS Decimal(18, 2)))
INSERT [dbo].[TokenDetails] ([TokenDetailsId], [TokenId], [TokenNo], [Name], [NIC], [Prepaid], [IsDriver], [isCancel], [Fees]) VALUES (230, 400, N'2959912-1', N'swscfw', N'52352-3462352-3', NULL, 0, 0, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[TokenDetails] ([TokenDetailsId], [TokenId], [TokenNo], [Name], [NIC], [Prepaid], [IsDriver], [isCancel], [Fees]) VALUES (231, 400, N'2959912-2', N'wsfc', N'35263-7896784-5', NULL, 0, 0, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[TokenDetails] ([TokenDetailsId], [TokenId], [TokenNo], [Name], [NIC], [Prepaid], [IsDriver], [isCancel], [Fees]) VALUES (232, 400, N'2959912-3', N'wscfwffc', N'23526-2472316-3', NULL, 0, 0, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[TokenDetails] ([TokenDetailsId], [TokenId], [TokenNo], [Name], [NIC], [Prepaid], [IsDriver], [isCancel], [Fees]) VALUES (233, 400, N'2959912-4', N'wff', N'21442-3657485-4', NULL, 0, 0, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[TokenDetails] ([TokenDetailsId], [TokenId], [TokenNo], [Name], [NIC], [Prepaid], [IsDriver], [isCancel], [Fees]) VALUES (234, 401, N'298577-0', N'sdfvasfef', N'51353-2613532-6', N'2412535', 1, 0, CAST(0.00 AS Decimal(18, 2)))
SET IDENTITY_INSERT [dbo].[TokenDetails] OFF
SET IDENTITY_INSERT [dbo].[TokenTransaction] ON 

INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (565, 210, 187, CAST(50.00 AS Decimal(18, 2)), 1, 6, CAST(0x0000A85800D42E17 AS DateTime))
INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (566, 211, 187, CAST(10.00 AS Decimal(18, 2)), 1, 6, CAST(0x0000A85800D42E18 AS DateTime))
INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (567, 212, 187, CAST(10.00 AS Decimal(18, 2)), 1, 6, CAST(0x0000A85800D42E18 AS DateTime))
INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (568, 213, 188, CAST(0.00 AS Decimal(18, 2)), 1, 6, CAST(0x0000A85800D5DFFA AS DateTime))
INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (569, 214, 188, CAST(10.00 AS Decimal(18, 2)), 1, 6, CAST(0x0000A85800D5EF09 AS DateTime))
INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (570, 215, 188, CAST(0.00 AS Decimal(18, 2)), 1, 6, CAST(0x0000A85800D5F996 AS DateTime))
INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (571, 216, 188, CAST(70.00 AS Decimal(18, 2)), 1, 6, CAST(0x0000A85800D60DBD AS DateTime))
INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (572, 217, 188, CAST(10.00 AS Decimal(18, 2)), 1, 6, CAST(0x0000A85800D60DBD AS DateTime))
INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (573, 218, 188, CAST(10.00 AS Decimal(18, 2)), 1, 6, CAST(0x0000A85800D60DBD AS DateTime))
INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (574, 219, 189, CAST(10.00 AS Decimal(18, 2)), 1, 6, CAST(0x0000A85800E67DEC AS DateTime))
INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (575, 220, 189, CAST(40.00 AS Decimal(18, 2)), 1, 6, CAST(0x0000A85800E6C32D AS DateTime))
INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (576, 221, 189, CAST(10.00 AS Decimal(18, 2)), 1, 6, CAST(0x0000A85800E6C32D AS DateTime))
INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (577, 222, 189, CAST(10.00 AS Decimal(18, 2)), 1, 6, CAST(0x0000A85800E6C32D AS DateTime))
INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (578, 223, 189, CAST(50.00 AS Decimal(18, 2)), 1, 6, CAST(0x0000A85800E6FDA8 AS DateTime))
INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (579, 224, 189, CAST(10.00 AS Decimal(18, 2)), 1, 6, CAST(0x0000A85800E6FDA8 AS DateTime))
INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (580, 225, 189, CAST(10.00 AS Decimal(18, 2)), 1, 6, CAST(0x0000A85800E6FDA8 AS DateTime))
INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (581, 226, 189, CAST(70.00 AS Decimal(18, 2)), 1, 6, CAST(0x0000A85800E71ACF AS DateTime))
INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (582, 227, 189, CAST(10.00 AS Decimal(18, 2)), 1, 6, CAST(0x0000A85800E71ACF AS DateTime))
INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (583, 228, 189, CAST(10.00 AS Decimal(18, 2)), 1, 6, CAST(0x0000A85800E71ACF AS DateTime))
INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (584, 229, 189, CAST(100.00 AS Decimal(18, 2)), 1, 6, CAST(0x0000A85800E74306 AS DateTime))
INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (585, 230, 189, CAST(10.00 AS Decimal(18, 2)), 1, 6, CAST(0x0000A85800E74307 AS DateTime))
INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (586, 231, 189, CAST(10.00 AS Decimal(18, 2)), 1, 6, CAST(0x0000A85800E74307 AS DateTime))
INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (587, 232, 189, CAST(10.00 AS Decimal(18, 2)), 1, 6, CAST(0x0000A85800E74307 AS DateTime))
INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (588, 233, 189, CAST(10.00 AS Decimal(18, 2)), 1, 6, CAST(0x0000A85800E74307 AS DateTime))
INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (589, 234, 189, CAST(0.00 AS Decimal(18, 2)), 1, 6, CAST(0x0000A85800E74D2E AS DateTime))
SET IDENTITY_INSERT [dbo].[TokenTransaction] OFF
ALTER TABLE [dbo].[AppEmployees] ADD  CONSTRAINT [DF_UserProfiles_UserProfileID]  DEFAULT (newid()) FOR [EmployeeGUID]
GO
ALTER TABLE [dbo].[AppModuleConfigs] ADD  CONSTRAINT [DF_ModuleConfigs_ConfigModuleID]  DEFAULT ((0)) FOR [ModuleID]
GO
ALTER TABLE [dbo].[AppModuleConfigs] ADD  CONSTRAINT [DF_AppModuleConfigs_isLock]  DEFAULT ((0)) FOR [isLock]
GO
ALTER TABLE [dbo].[AppModulePermissions] ADD  CONSTRAINT [DF_AppModuleMeta_isAvailable]  DEFAULT ((0)) FOR [isAvailable]
GO
ALTER TABLE [dbo].[AppModules] ADD  CONSTRAINT [DF_Modules_ShowInMenu]  DEFAULT ((0)) FOR [ShowInMenu]
GO
ALTER TABLE [dbo].[AppModules] ADD  CONSTRAINT [DF_AppModules_isPrimary]  DEFAULT ((0)) FOR [isPrimary]
GO
ALTER TABLE [dbo].[AppModules] ADD  CONSTRAINT [DF_Modules_Sort]  DEFAULT ((0)) FOR [Sort]
GO
ALTER TABLE [dbo].[AppModules] ADD  CONSTRAINT [DF_Modules_IsDisable]  DEFAULT ((0)) FOR [isDisable]
GO
ALTER TABLE [dbo].[AppModules] ADD  CONSTRAINT [DF_AppModules_HasFullAccess]  DEFAULT ((0)) FOR [HasFullAccess]
GO
ALTER TABLE [dbo].[AppModules] ADD  CONSTRAINT [DF_AppModules_HasCreate]  DEFAULT ((0)) FOR [HasCreate]
GO
ALTER TABLE [dbo].[AppModules] ADD  CONSTRAINT [DF_AppModules_HasEdit]  DEFAULT ((0)) FOR [HasEdit]
GO
ALTER TABLE [dbo].[AppModules] ADD  CONSTRAINT [DF_AppModules_HasView]  DEFAULT ((0)) FOR [HasView]
GO
ALTER TABLE [dbo].[AppModules] ADD  CONSTRAINT [DF_AppModules_HasDetail]  DEFAULT ((0)) FOR [HasDetail]
GO
ALTER TABLE [dbo].[AppRoles] ADD  CONSTRAINT [DF_Group_Priority]  DEFAULT ((1)) FOR [Priority]
GO
ALTER TABLE [dbo].[AppRoles] ADD  CONSTRAINT [DF_AppGroups_isLock]  DEFAULT ((1)) FOR [isEditable]
GO
ALTER TABLE [dbo].[AppRoles] ADD  CONSTRAINT [DF_Group_isDisable]  DEFAULT ((0)) FOR [isDisable]
GO
ALTER TABLE [dbo].[AppRolesModules] ADD  CONSTRAINT [DF_GroupModules_IsAllowed]  DEFAULT ((0)) FOR [IsAllowed]
GO
ALTER TABLE [dbo].[AppUserLogs] ADD  CONSTRAINT [DF_AppUserLogs_LoginDateTime]  DEFAULT (getdate()) FOR [LoginDateTime]
GO
ALTER TABLE [dbo].[AppUserMeta] ADD  CONSTRAINT [DF_AppUserMeta_isInternal]  DEFAULT ((0)) FOR [isInternal]
GO
ALTER TABLE [dbo].[AppUserPwdRequest] ADD  CONSTRAINT [DF_AppUserPwdRequest_isGenerated]  DEFAULT ((0)) FOR [isGenerated]
GO
ALTER TABLE [dbo].[AppUsers] ADD  CONSTRAINT [DF_Users_Userkey]  DEFAULT (newid()) FOR [UserGUID]
GO
ALTER TABLE [dbo].[AppUsers] ADD  CONSTRAINT [DF_Users_isEmployee]  DEFAULT ((1)) FOR [isEmployee]
GO
ALTER TABLE [dbo].[AppUsers] ADD  CONSTRAINT [DF_Users_LoginCount]  DEFAULT ((0)) FOR [LoginCount]
GO
ALTER TABLE [dbo].[AppUsers] ADD  CONSTRAINT [DF_AppUsers_isDisable1]  DEFAULT ((0)) FOR [isActive]
GO
ALTER TABLE [dbo].[AppUsers] ADD  CONSTRAINT [DF_Users_isActive]  DEFAULT ((0)) FOR [isDisable]
GO
ALTER TABLE [dbo].[BankChallan] ADD  CONSTRAINT [DF_BankChallan_isChallanSubmitedtoBank]  DEFAULT ((0)) FOR [isChallanSubmitedtoBank]
GO
ALTER TABLE [dbo].[Day] ADD  CONSTRAINT [DF_Day_DayClosed]  DEFAULT ((0)) FOR [IsDayClosed]
GO
ALTER TABLE [dbo].[Day] ADD  CONSTRAINT [DF_Day_IsBankChallanGenerated]  DEFAULT ((0)) FOR [IsBankChallanGenerated]
GO
ALTER TABLE [dbo].[Operation] ADD  CONSTRAINT [DF_Operation_OpeartionClosed]  DEFAULT ((0)) FOR [OpeartionClosed]
GO
ALTER TABLE [dbo].[Shift] ADD  CONSTRAINT [DF_Shift_ShiftClosed]  DEFAULT ((0)) FOR [ShiftClosed]
GO
ALTER TABLE [dbo].[Token] ADD  CONSTRAINT [DF_Token_IsNicAvailable]  DEFAULT ((0)) FOR [IsNicAvailable]
GO
ALTER TABLE [dbo].[TokenCancelRequest] ADD  CONSTRAINT [DF_TokenCancelRequest_IsApproved]  DEFAULT ((0)) FOR [CancelStatusId]
GO
ALTER TABLE [dbo].[TokenDetails] ADD  CONSTRAINT [DF_TokenDetails_isCancel]  DEFAULT ((0)) FOR [isCancel]
GO
ALTER TABLE [dbo].[TokenTransaction] ADD  CONSTRAINT [DF_TokenTransaction_isPayIn]  DEFAULT ((0)) FOR [isPayIn]
GO
ALTER TABLE [dbo].[AppEmployeeAgencies]  WITH CHECK ADD  CONSTRAINT [FK_AppEmployeeAgencies_AppEmployees] FOREIGN KEY([EmployeeID])
REFERENCES [dbo].[AppEmployees] ([EmployeeID])
GO
ALTER TABLE [dbo].[AppEmployeeAgencies] CHECK CONSTRAINT [FK_AppEmployeeAgencies_AppEmployees]
GO
ALTER TABLE [dbo].[AppEmployeeDepartments]  WITH CHECK ADD  CONSTRAINT [FK_AppEmployeeDepartments_AppEmployees] FOREIGN KEY([EmployeeID])
REFERENCES [dbo].[AppEmployees] ([EmployeeID])
GO
ALTER TABLE [dbo].[AppEmployeeDepartments] CHECK CONSTRAINT [FK_AppEmployeeDepartments_AppEmployees]
GO
ALTER TABLE [dbo].[AppEmployees]  WITH CHECK ADD  CONSTRAINT [FK_Employees_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[AppUsers] ([UserId])
GO
ALTER TABLE [dbo].[AppEmployees] CHECK CONSTRAINT [FK_Employees_Users]
GO
ALTER TABLE [dbo].[AppEmployeeServices]  WITH CHECK ADD  CONSTRAINT [FK_AppEmployeeServices_AppEmployees] FOREIGN KEY([EmployeeID])
REFERENCES [dbo].[AppEmployees] ([EmployeeID])
GO
ALTER TABLE [dbo].[AppEmployeeServices] CHECK CONSTRAINT [FK_AppEmployeeServices_AppEmployees]
GO
ALTER TABLE [dbo].[AppModuleConfigs]  WITH CHECK ADD  CONSTRAINT [FK_ModuleConfigs_Modules] FOREIGN KEY([ModuleID])
REFERENCES [dbo].[AppModules] ([ModuleID])
GO
ALTER TABLE [dbo].[AppModuleConfigs] CHECK CONSTRAINT [FK_ModuleConfigs_Modules]
GO
ALTER TABLE [dbo].[AppModulePermissions]  WITH CHECK ADD  CONSTRAINT [FK_AppModuleMeta_AppModules] FOREIGN KEY([ModuleID])
REFERENCES [dbo].[AppModules] ([ModuleID])
GO
ALTER TABLE [dbo].[AppModulePermissions] CHECK CONSTRAINT [FK_AppModuleMeta_AppModules]
GO
ALTER TABLE [dbo].[AppRolesModules]  WITH CHECK ADD  CONSTRAINT [FK_GroupFeature_Features] FOREIGN KEY([ModuleId])
REFERENCES [dbo].[AppModules] ([ModuleID])
GO
ALTER TABLE [dbo].[AppRolesModules] CHECK CONSTRAINT [FK_GroupFeature_Features]
GO
ALTER TABLE [dbo].[AppRolesModules]  WITH CHECK ADD  CONSTRAINT [FK_GroupFeature_Group] FOREIGN KEY([RolesId])
REFERENCES [dbo].[AppRoles] ([RoleId])
GO
ALTER TABLE [dbo].[AppRolesModules] CHECK CONSTRAINT [FK_GroupFeature_Group]
GO
ALTER TABLE [dbo].[AppUserLogs]  WITH CHECK ADD  CONSTRAINT [FK_AppUserLogs_AppUsers] FOREIGN KEY([UserId])
REFERENCES [dbo].[AppUsers] ([UserId])
GO
ALTER TABLE [dbo].[AppUserLogs] CHECK CONSTRAINT [FK_AppUserLogs_AppUsers]
GO
ALTER TABLE [dbo].[AppUserMeta]  WITH CHECK ADD  CONSTRAINT [FK_AppUserMeta_AppUsers] FOREIGN KEY([UserId])
REFERENCES [dbo].[AppUsers] ([UserId])
GO
ALTER TABLE [dbo].[AppUserMeta] CHECK CONSTRAINT [FK_AppUserMeta_AppUsers]
GO
ALTER TABLE [dbo].[AppUserPwdRequest]  WITH CHECK ADD  CONSTRAINT [FK_AppUserPwdRequest_AppUsers] FOREIGN KEY([UserId])
REFERENCES [dbo].[AppUsers] ([UserId])
GO
ALTER TABLE [dbo].[AppUserPwdRequest] CHECK CONSTRAINT [FK_AppUserPwdRequest_AppUsers]
GO
ALTER TABLE [dbo].[AppUsers]  WITH CHECK ADD  CONSTRAINT [FK_Users_Groups] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AppRoles] ([RoleId])
GO
ALTER TABLE [dbo].[AppUsers] CHECK CONSTRAINT [FK_Users_Groups]
GO
ALTER TABLE [dbo].[BankChallan]  WITH CHECK ADD  CONSTRAINT [FK_BankChallan_Day] FOREIGN KEY([DayId])
REFERENCES [dbo].[Day] ([DayId])
GO
ALTER TABLE [dbo].[BankChallan] CHECK CONSTRAINT [FK_BankChallan_Day]
GO
ALTER TABLE [dbo].[BankChallan]  WITH CHECK ADD  CONSTRAINT [FK_BankChallanDetails_Bank] FOREIGN KEY([BankId])
REFERENCES [dbo].[Bank] ([BankId])
GO
ALTER TABLE [dbo].[BankChallan] CHECK CONSTRAINT [FK_BankChallanDetails_Bank]
GO
ALTER TABLE [dbo].[BankChallanDetail]  WITH CHECK ADD  CONSTRAINT [FK_BankChallanDetail_BankId] FOREIGN KEY([BankChallanId])
REFERENCES [dbo].[BankChallan] ([BankChallanId])
GO
ALTER TABLE [dbo].[BankChallanDetail] CHECK CONSTRAINT [FK_BankChallanDetail_BankId]
GO
ALTER TABLE [dbo].[BankChallanDetail]  WITH CHECK ADD  CONSTRAINT [FK_BankChallanDetail_TokenDetails] FOREIGN KEY([TokenDetailId])
REFERENCES [dbo].[TokenDetails] ([TokenDetailsId])
GO
ALTER TABLE [dbo].[BankChallanDetail] CHECK CONSTRAINT [FK_BankChallanDetail_TokenDetails]
GO
ALTER TABLE [dbo].[CategoryFees]  WITH CHECK ADD  CONSTRAINT [FK_CategoryFees_Category] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Category] ([CategoryId])
GO
ALTER TABLE [dbo].[CategoryFees] CHECK CONSTRAINT [FK_CategoryFees_Category]
GO
ALTER TABLE [dbo].[Day]  WITH CHECK ADD  CONSTRAINT [FK_Day_AppUsers] FOREIGN KEY([StartedBy])
REFERENCES [dbo].[AppUsers] ([UserId])
GO
ALTER TABLE [dbo].[Day] CHECK CONSTRAINT [FK_Day_AppUsers]
GO
ALTER TABLE [dbo].[Operation]  WITH CHECK ADD  CONSTRAINT [FK_Operation_AppUsers] FOREIGN KEY([OperationStartedBy])
REFERENCES [dbo].[AppUsers] ([UserId])
GO
ALTER TABLE [dbo].[Operation] CHECK CONSTRAINT [FK_Operation_AppUsers]
GO
ALTER TABLE [dbo].[Operation]  WITH CHECK ADD  CONSTRAINT [FK_Shift_Operation] FOREIGN KEY([ShiftId])
REFERENCES [dbo].[Shift] ([ShiftId])
GO
ALTER TABLE [dbo].[Operation] CHECK CONSTRAINT [FK_Shift_Operation]
GO
ALTER TABLE [dbo].[Shift]  WITH CHECK ADD  CONSTRAINT [FK_Shift_AppUsers] FOREIGN KEY([ShiftStartedBy])
REFERENCES [dbo].[AppUsers] ([UserId])
GO
ALTER TABLE [dbo].[Shift] CHECK CONSTRAINT [FK_Shift_AppUsers]
GO
ALTER TABLE [dbo].[Shift]  WITH CHECK ADD  CONSTRAINT [FK_Shift_Day] FOREIGN KEY([DayId])
REFERENCES [dbo].[Day] ([DayId])
GO
ALTER TABLE [dbo].[Shift] CHECK CONSTRAINT [FK_Shift_Day]
GO
ALTER TABLE [dbo].[ShiftDetails]  WITH CHECK ADD  CONSTRAINT [FK_ShiftDetails_AppUsers] FOREIGN KEY([UserLoggedIn])
REFERENCES [dbo].[AppUsers] ([UserId])
GO
ALTER TABLE [dbo].[ShiftDetails] CHECK CONSTRAINT [FK_ShiftDetails_AppUsers]
GO
ALTER TABLE [dbo].[ShiftDetails]  WITH CHECK ADD  CONSTRAINT [FK_ShiftDetails_AppUsers1] FOREIGN KEY([UserLoggedOut])
REFERENCES [dbo].[AppUsers] ([UserId])
GO
ALTER TABLE [dbo].[ShiftDetails] CHECK CONSTRAINT [FK_ShiftDetails_AppUsers1]
GO
ALTER TABLE [dbo].[ShiftDetails]  WITH CHECK ADD  CONSTRAINT [FK_ShiftDetails_Shift] FOREIGN KEY([ShiftId])
REFERENCES [dbo].[Shift] ([ShiftId])
GO
ALTER TABLE [dbo].[ShiftDetails] CHECK CONSTRAINT [FK_ShiftDetails_Shift]
GO
ALTER TABLE [dbo].[Token]  WITH CHECK ADD  CONSTRAINT [FK_Token_CategoryFees] FOREIGN KEY([CategoryFeesId])
REFERENCES [dbo].[CategoryFees] ([CategoryFeeId])
GO
ALTER TABLE [dbo].[Token] CHECK CONSTRAINT [FK_Token_CategoryFees]
GO
ALTER TABLE [dbo].[Token]  WITH CHECK ADD  CONSTRAINT [FK_Token_Operation] FOREIGN KEY([OperationId])
REFERENCES [dbo].[Operation] ([OperationId])
GO
ALTER TABLE [dbo].[Token] CHECK CONSTRAINT [FK_Token_Operation]
GO
ALTER TABLE [dbo].[Token]  WITH CHECK ADD  CONSTRAINT [fkToken_PlaceOfVisit] FOREIGN KEY([PlaceOfVisitId])
REFERENCES [dbo].[PlaceOfVisit] ([PlaceOfVisitId])
GO
ALTER TABLE [dbo].[Token] CHECK CONSTRAINT [fkToken_PlaceOfVisit]
GO
ALTER TABLE [dbo].[Token]  WITH CHECK ADD  CONSTRAINT [fkToken_PurposeOfVisit] FOREIGN KEY([PurposeOfVisit])
REFERENCES [dbo].[PurposeOfVisit] ([PurposeOfVisitId])
GO
ALTER TABLE [dbo].[Token] CHECK CONSTRAINT [fkToken_PurposeOfVisit]
GO
ALTER TABLE [dbo].[TokenCancelledRequestDetails]  WITH CHECK ADD  CONSTRAINT [FK_TokenCancelledRequestDetails_TokenCancelRequest] FOREIGN KEY([TokenCancelRequestId])
REFERENCES [dbo].[TokenCancelRequest] ([TokenCancelRequestId])
GO
ALTER TABLE [dbo].[TokenCancelledRequestDetails] CHECK CONSTRAINT [FK_TokenCancelledRequestDetails_TokenCancelRequest]
GO
ALTER TABLE [dbo].[TokenCancelledRequestDetails]  WITH CHECK ADD  CONSTRAINT [FK_TokenCancelledRequestDetails_TokenDetails] FOREIGN KEY([TokenDetailId])
REFERENCES [dbo].[TokenDetails] ([TokenDetailsId])
GO
ALTER TABLE [dbo].[TokenCancelledRequestDetails] CHECK CONSTRAINT [FK_TokenCancelledRequestDetails_TokenDetails]
GO
ALTER TABLE [dbo].[TokenCancelRequest]  WITH CHECK ADD  CONSTRAINT [FK_TokenCancelRequest_AppUsers] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[AppUsers] ([UserId])
GO
ALTER TABLE [dbo].[TokenCancelRequest] CHECK CONSTRAINT [FK_TokenCancelRequest_AppUsers]
GO
ALTER TABLE [dbo].[TokenCancelRequest]  WITH CHECK ADD  CONSTRAINT [FK_TokenCancelRequest_CancelStatus] FOREIGN KEY([CancelStatusId])
REFERENCES [dbo].[CancelStatus] ([CancelStatusId])
GO
ALTER TABLE [dbo].[TokenCancelRequest] CHECK CONSTRAINT [FK_TokenCancelRequest_CancelStatus]
GO
ALTER TABLE [dbo].[TokenCancelRequest]  WITH CHECK ADD  CONSTRAINT [FK_TokenCancelRequest_Category] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Category] ([CategoryId])
GO
ALTER TABLE [dbo].[TokenCancelRequest] CHECK CONSTRAINT [FK_TokenCancelRequest_Category]
GO
ALTER TABLE [dbo].[TokenCancelRequest]  WITH CHECK ADD  CONSTRAINT [FK_TokenCancelRequest_TokenDetails] FOREIGN KEY([TokenId])
REFERENCES [dbo].[Token] ([TokenId])
GO
ALTER TABLE [dbo].[TokenCancelRequest] CHECK CONSTRAINT [FK_TokenCancelRequest_TokenDetails]
GO
ALTER TABLE [dbo].[TokenDetails]  WITH CHECK ADD  CONSTRAINT [FK_TokenDetails_Token] FOREIGN KEY([TokenId])
REFERENCES [dbo].[Token] ([TokenId])
GO
ALTER TABLE [dbo].[TokenDetails] CHECK CONSTRAINT [FK_TokenDetails_Token]
GO
ALTER TABLE [dbo].[TokenTransaction]  WITH CHECK ADD  CONSTRAINT [FK_TokenTransaction_Operation] FOREIGN KEY([OperationId])
REFERENCES [dbo].[Operation] ([OperationId])
GO
ALTER TABLE [dbo].[TokenTransaction] CHECK CONSTRAINT [FK_TokenTransaction_Operation]
GO
ALTER TABLE [dbo].[TokenTransaction]  WITH CHECK ADD  CONSTRAINT [FK_TokenTransaction_Token] FOREIGN KEY([TokendetailId])
REFERENCES [dbo].[TokenDetails] ([TokenDetailsId])
GO
ALTER TABLE [dbo].[TokenTransaction] CHECK CONSTRAINT [FK_TokenTransaction_Token]
GO
USE [master]
GO
ALTER DATABASE [VehicleDB] SET  READ_WRITE 
GO
