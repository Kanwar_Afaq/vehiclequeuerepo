USE [master]
GO
/****** Object:  Database [VehicleDB]    Script Date: 11/20/2017 4:03:37 PM ******/
CREATE DATABASE [VehicleDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'VehicleDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\VehicleDB.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'VehicleDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\VehicleDB_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [VehicleDB] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [VehicleDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [VehicleDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [VehicleDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [VehicleDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [VehicleDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [VehicleDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [VehicleDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [VehicleDB] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [VehicleDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [VehicleDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [VehicleDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [VehicleDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [VehicleDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [VehicleDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [VehicleDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [VehicleDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [VehicleDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [VehicleDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [VehicleDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [VehicleDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [VehicleDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [VehicleDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [VehicleDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [VehicleDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [VehicleDB] SET RECOVERY FULL 
GO
ALTER DATABASE [VehicleDB] SET  MULTI_USER 
GO
ALTER DATABASE [VehicleDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [VehicleDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [VehicleDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [VehicleDB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [VehicleDB]
GO
/****** Object:  Table [dbo].[AppEmployeeAgencies]    Script Date: 11/20/2017 4:03:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AppEmployeeAgencies](
	[EmployeeAgencyId] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeID] [int] NOT NULL,
	[AgencyId] [int] NOT NULL,
 CONSTRAINT [PK_AppEmployeeAgencies] PRIMARY KEY CLUSTERED 
(
	[EmployeeAgencyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AppEmployeeDepartments]    Script Date: 11/20/2017 4:03:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AppEmployeeDepartments](
	[EmployeeDeptID] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeID] [int] NOT NULL,
	[DepartmentId] [int] NOT NULL,
 CONSTRAINT [PK_UserDepartments] PRIMARY KEY CLUSTERED 
(
	[EmployeeDeptID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AppEmployees]    Script Date: 11/20/2017 4:03:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AppEmployees](
	[EmployeeID] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeGUID] [uniqueidentifier] NOT NULL,
	[UserId] [int] NOT NULL,
	[Name] [varchar](100) NULL,
	[Email] [varchar](100) NOT NULL,
	[PersonalEmail] [varchar](100) NULL,
	[MobileNo] [varchar](20) NULL,
	[TelNo] [varchar](20) NULL,
	[TelNoHome] [varchar](20) NULL,
	[FaxNo] [varchar](20) NULL,
	[FaxNoHome] [varchar](20) NULL,
	[Designation] [varchar](50) NULL,
	[DepartmentId] [int] NULL,
	[Salary] [decimal](18, 2) NULL,
	[CNICNumber] [varchar](30) NULL,
	[CNICFrontJpeg] [varchar](100) NULL,
	[CNICBackJpeg] [varchar](100) NULL,
	[YahooID] [varchar](50) NULL,
	[SkypeID] [varchar](50) NULL,
	[BBMCode] [varchar](50) NULL,
	[Signature] [text] NULL,
	[Notes] [text] NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_UserProfiles_1] PRIMARY KEY CLUSTERED 
(
	[EmployeeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AppEmployeeServices]    Script Date: 11/20/2017 4:03:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AppEmployeeServices](
	[EmployeeServiceId] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeID] [int] NOT NULL,
	[ServiceId] [int] NOT NULL,
 CONSTRAINT [PK_AppEmployeeServices] PRIMARY KEY CLUSTERED 
(
	[EmployeeServiceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AppModuleConfigs]    Script Date: 11/20/2017 4:03:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AppModuleConfigs](
	[ConfigID] [int] IDENTITY(1,1) NOT NULL,
	[ModuleID] [int] NULL,
	[isLock] [bit] NOT NULL,
	[ConfigKey] [varchar](200) NULL,
	[ConfigValue] [varchar](200) NULL,
	[Description] [varchar](200) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
 CONSTRAINT [PK_ModuleConfig] PRIMARY KEY CLUSTERED 
(
	[ConfigID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AppModulePermissions]    Script Date: 11/20/2017 4:03:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AppModulePermissions](
	[PermissionID] [int] IDENTITY(1,1) NOT NULL,
	[ModuleID] [int] NOT NULL,
	[PermissionName] [varchar](100) NULL,
	[isAvailable] [bit] NOT NULL,
 CONSTRAINT [PK_AppModulePermissions] PRIMARY KEY CLUSTERED 
(
	[PermissionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AppModules]    Script Date: 11/20/2017 4:03:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AppModules](
	[ModuleID] [int] IDENTITY(1,1) NOT NULL,
	[ModuleName] [varchar](100) NULL,
	[ModuleLink] [varchar](50) NULL,
	[ModuleIndex] [varchar](50) NULL,
	[ModuleParam] [varchar](50) NULL,
	[ShowInMenu] [bit] NOT NULL,
	[isPrimary] [bit] NOT NULL,
	[Sort] [int] NOT NULL,
	[CssClass] [varchar](50) NULL,
	[isDisable] [bit] NOT NULL,
	[HasFullAccess] [bit] NOT NULL,
	[HasCreate] [bit] NOT NULL,
	[HasEdit] [bit] NOT NULL,
	[HasDelete] [bit] NOT NULL,
	[HasView] [bit] NOT NULL,
	[HasDetail] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_Features_1] PRIMARY KEY CLUSTERED 
(
	[ModuleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AppRoles]    Script Date: 11/20/2017 4:03:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AppRoles](
	[RoleId] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [varchar](100) NULL,
	[Priority] [int] NOT NULL,
	[isEditable] [bit] NOT NULL,
	[isDisable] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[Modified] [datetime] NULL,
 CONSTRAINT [PK_Group] PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AppRolesModules]    Script Date: 11/20/2017 4:03:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AppRolesModules](
	[RolesModulesID] [int] IDENTITY(1,1) NOT NULL,
	[RolesId] [int] NOT NULL,
	[ModuleId] [int] NOT NULL,
	[IsAllowed] [bit] NOT NULL,
	[Permissions] [text] NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_GroupFeature] PRIMARY KEY CLUSTERED 
(
	[RolesModulesID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AppUserLogs]    Script Date: 11/20/2017 4:03:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AppUserLogs](
	[UserLogID] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[IPAddress] [varchar](50) NULL,
	[Browser] [varchar](50) NULL,
	[SessionID] [varchar](50) NULL,
	[LoginDateTime] [datetime] NULL,
	[LogOutDateTime] [datetime] NULL,
 CONSTRAINT [PK_AppUserLogs] PRIMARY KEY CLUSTERED 
(
	[UserLogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AppUserMeta]    Script Date: 11/20/2017 4:03:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AppUserMeta](
	[UserMetaID] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[MetaKey] [varchar](255) NULL,
	[MetaValue] [varchar](255) NULL,
	[isInternal] [bit] NOT NULL,
 CONSTRAINT [PK_AppUserMeta] PRIMARY KEY CLUSTERED 
(
	[UserMetaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AppUserPwdRequest]    Script Date: 11/20/2017 4:03:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AppUserPwdRequest](
	[RequestId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[RandomNo] [varchar](50) NULL,
	[IPAddress] [varchar](50) NULL,
	[Browser] [varchar](50) NULL,
	[isGenerated] [bit] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_AppUserPwdRequest] PRIMARY KEY CLUSTERED 
(
	[RequestId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AppUsers]    Script Date: 11/20/2017 4:03:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AppUsers](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[UserGUID] [uniqueidentifier] NOT NULL,
	[isEmployee] [bit] NOT NULL,
	[RoleId] [int] NOT NULL,
	[UserName] [varchar](50) NOT NULL,
	[Password] [varchar](255) NOT NULL,
	[PassKey] [varchar](10) NULL,
	[ExpiryDate] [datetime] NULL,
	[LastLogn] [datetime] NULL,
	[LoginCount] [bigint] NOT NULL,
	[isActive] [bit] NOT NULL,
	[isDisable] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_Users_1] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Bank]    Script Date: 11/20/2017 4:03:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Bank](
	[BankId] [int] IDENTITY(1,1) NOT NULL,
	[BankName] [nvarchar](100) NULL,
	[Slip] [varbinary](max) NULL,
 CONSTRAINT [PK_Bank] PRIMARY KEY CLUSTERED 
(
	[BankId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BankChallan]    Script Date: 11/20/2017 4:03:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BankChallan](
	[BankChallanId] [int] IDENTITY(1,1) NOT NULL,
	[DayId] [int] NULL,
	[BankId] [int] NULL,
	[ChallanNo] [nvarchar](50) NULL,
	[ChallanDatetime] [datetime] NULL,
	[isChallanSubmitedtoBank] [bit] NOT NULL,
	[DepositSlip] [nvarchar](500) NULL,
	[TotalAmount] [decimal](18, 2) NULL,
	[TotalAmountinword] [nvarchar](50) NULL,
	[CreatedBy] [int] NULL,
	[CreatedDatetime] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDatetime] [nchar](10) NULL,
 CONSTRAINT [PK_BankChallanDetails] PRIMARY KEY CLUSTERED 
(
	[BankChallanId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BankChallanDetail]    Script Date: 11/20/2017 4:03:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BankChallanDetail](
	[BankChallenDetailsId] [int] IDENTITY(1,1) NOT NULL,
	[BankChallanId] [int] NULL,
	[TokenDetailId] [int] NULL,
	[Fees] [decimal](18, 2) NULL,
 CONSTRAINT [PK_BankChallanDetail] PRIMARY KEY CLUSTERED 
(
	[BankChallenDetailsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CancelStatus]    Script Date: 11/20/2017 4:03:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CancelStatus](
	[CancelStatusId] [int] IDENTITY(1,1) NOT NULL,
	[CancelStatus] [nvarchar](50) NULL,
 CONSTRAINT [PK_CancelStatus] PRIMARY KEY CLUSTERED 
(
	[CancelStatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Category]    Script Date: 11/20/2017 4:03:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[CategoryId] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [nvarchar](50) NULL,
	[Narration] [nvarchar](50) NULL,
 CONSTRAINT [PK_VehicleCategory] PRIMARY KEY CLUSTERED 
(
	[CategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CategoryFees]    Script Date: 11/20/2017 4:03:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CategoryFees](
	[CategoryFeeId] [int] IDENTITY(1,1) NOT NULL,
	[CategoryId] [int] NOT NULL,
	[Fees] [decimal](18, 2) NULL,
	[EffectiveDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [date] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_VehicleCategoryFees] PRIMARY KEY CLUSTERED 
(
	[CategoryFeeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Day]    Script Date: 11/20/2017 4:03:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Day](
	[DayId] [int] IDENTITY(1,1) NOT NULL,
	[StartedBy] [int] NOT NULL,
	[StartDateTime] [datetime] NOT NULL,
	[EndingDateTime] [datetime] NULL,
	[IsDayClosed] [bit] NOT NULL,
	[FinalAmount] [decimal](18, 2) NOT NULL,
	[IsBankChallanGenerated] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_Day] PRIMARY KEY CLUSTERED 
(
	[DayId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Operation]    Script Date: 11/20/2017 4:03:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Operation](
	[OperationId] [int] IDENTITY(1,1) NOT NULL,
	[ShiftId] [int] NULL,
	[OperationStartedBy] [int] NULL,
	[OperationStartingDateTime] [datetime] NULL,
	[OperationEndingDateTime] [datetime] NULL,
	[TotalAmountinOperation] [decimal](18, 2) NULL,
	[OpeartionClosed] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_Operation] PRIMARY KEY CLUSTERED 
(
	[OperationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PlaceOfVisit]    Script Date: 11/20/2017 4:03:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PlaceOfVisit](
	[PlaceOfVisitId] [int] IDENTITY(1,1) NOT NULL,
	[PlaceOfVisit] [nvarchar](500) NULL,
 CONSTRAINT [PK_PlaceOfVisit] PRIMARY KEY CLUSTERED 
(
	[PlaceOfVisitId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PurposeOfVisit]    Script Date: 11/20/2017 4:03:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PurposeOfVisit](
	[PurposeOfVisitId] [int] IDENTITY(1,1) NOT NULL,
	[PurposeOfVIsit] [nvarchar](500) NULL,
 CONSTRAINT [PK_PurposeOfVisit] PRIMARY KEY CLUSTERED 
(
	[PurposeOfVisitId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Shift]    Script Date: 11/20/2017 4:03:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Shift](
	[ShiftId] [int] IDENTITY(1,1) NOT NULL,
	[DayId] [int] NOT NULL,
	[ShiftStartedBy] [int] NOT NULL,
	[ShiftStartingDatetime] [datetime] NULL,
	[ShiftEndingDatetime] [datetime] NULL,
	[AmountToBeCollected] [decimal](18, 2) NULL,
	[ShiftClosed] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_Shift] PRIMARY KEY CLUSTERED 
(
	[ShiftId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ShiftDetails]    Script Date: 11/20/2017 4:03:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ShiftDetails](
	[ShiftDetailId] [int] IDENTITY(1,1) NOT NULL,
	[ShiftId] [int] NULL,
	[UserLoggedIn] [int] NULL,
	[UserLoggedInDateTime] [datetime] NULL,
	[UserLoggedOut] [int] NULL,
	[UserLoggedOutDateTime] [datetime] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[ModifiedDate] [nchar](10) NULL,
	[ModifiedBy] [int] NULL,
 CONSTRAINT [PK_ShiftDetails] PRIMARY KEY CLUSTERED 
(
	[ShiftDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SystemConfig]    Script Date: 11/20/2017 4:03:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SystemConfig](
	[SystemConfigId] [int] IDENTITY(1,1) NOT NULL,
	[SystemConfigKey] [nvarchar](100) NULL,
	[SystemConfigValue] [nvarchar](100) NULL,
 CONSTRAINT [PK_SystemConfig] PRIMARY KEY CLUSTERED 
(
	[SystemConfigId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Token]    Script Date: 11/20/2017 4:03:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Token](
	[TokenId] [int] IDENTITY(1,1) NOT NULL,
	[OperationId] [int] NOT NULL,
	[IssueDateTime] [datetime] NULL,
	[VehiclePlateNo] [nvarchar](100) NULL,
	[CategoryFeesId] [int] NULL,
	[TokenNo] [nvarchar](100) NOT NULL,
	[PurposeOfVisit] [int] NULL,
	[PlaceOfVisitId] [int] NULL,
	[Noofpassengers] [int] NULL,
	[IsNicAvailable] [bit] NOT NULL,
	[IsClosed] [bit] NOT NULL,
	[closedDatetime] [datetime] NULL,
	[GoAheadReasonWithoutNIC] [text] NULL,
	[DatetimeCanceled] [datetime] NULL,
	[Reason] [text] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifedBy] [int] NULL,
 CONSTRAINT [PK_Token] PRIMARY KEY CLUSTERED 
(
	[TokenId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TokenCancelledRequestDetails]    Script Date: 11/20/2017 4:03:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TokenCancelledRequestDetails](
	[TokenCancelledRequestDetail] [int] IDENTITY(1,1) NOT NULL,
	[TokenDetailId] [int] NOT NULL,
	[TokenCancelRequestId] [int] NOT NULL,
 CONSTRAINT [PK_TokenCancelledRequestDetails] PRIMARY KEY CLUSTERED 
(
	[TokenCancelledRequestDetail] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TokenCancelRequest]    Script Date: 11/20/2017 4:03:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TokenCancelRequest](
	[TokenCancelRequestId] [int] IDENTITY(1,1) NOT NULL,
	[TokenId] [int] NOT NULL,
	[NoofTickets] [int] NULL,
	[CategoryId] [int] NOT NULL,
	[RefundedAmount] [decimal](18, 2) NULL,
	[DateTimeCancelled] [datetime] NULL,
	[Reason] [nvarchar](max) NULL,
	[CancelStatusId] [int] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_TokenCancelRequest] PRIMARY KEY CLUSTERED 
(
	[TokenCancelRequestId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TokenDetails]    Script Date: 11/20/2017 4:03:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TokenDetails](
	[TokenDetailsId] [int] IDENTITY(1,1) NOT NULL,
	[TokenId] [int] NOT NULL,
	[TokenNo] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[NIC] [nvarchar](30) NULL,
	[IsDriver] [bit] NOT NULL,
	[isCancel] [bit] NOT NULL,
	[Fees] [decimal](18, 2) NULL,
 CONSTRAINT [PK_TokenDetails] PRIMARY KEY CLUSTERED 
(
	[TokenDetailsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TokenTransaction]    Script Date: 11/20/2017 4:03:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TokenTransaction](
	[TokenTransactionID] [int] IDENTITY(1,1) NOT NULL,
	[TokendetailId] [int] NOT NULL,
	[OperationId] [int] NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[isPayIn] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_TokenTransaction] PRIMARY KEY CLUSTERED 
(
	[TokenTransactionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[AppEmployees] ON 

INSERT [dbo].[AppEmployees] ([EmployeeID], [EmployeeGUID], [UserId], [Name], [Email], [PersonalEmail], [MobileNo], [TelNo], [TelNoHome], [FaxNo], [FaxNoHome], [Designation], [DepartmentId], [Salary], [CNICNumber], [CNICFrontJpeg], [CNICBackJpeg], [YahooID], [SkypeID], [BBMCode], [Signature], [Notes], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, N'7fdc3992-7be9-4b0b-b383-c005dbb28a40', 1, N'mazhar', N'maz3tt1@hotmail.com', N'maz3tt@hotmail.com', N'asdasdasd', N'1', N'2', N'3', N'5', N'4', NULL, CAST(9.00 AS Decimal(18, 2)), N'10', N'mazhard9db43b8-1722-4c13-a57e-c7f04bc65d73FrontFooter.jpg', NULL, N'7', N'6', N'8', N'ASDASD', N'ASDASD', 0, CAST(0x0000A81500D37435 AS DateTime), 0, CAST(0x0000A81E00C2A214 AS DateTime))
INSERT [dbo].[AppEmployees] ([EmployeeID], [EmployeeGUID], [UserId], [Name], [Email], [PersonalEmail], [MobileNo], [TelNo], [TelNoHome], [FaxNo], [FaxNoHome], [Designation], [DepartmentId], [Salary], [CNICNumber], [CNICFrontJpeg], [CNICBackJpeg], [YahooID], [SkypeID], [BBMCode], [Signature], [Notes], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, N'bd4581c3-1ea1-46fa-8ed9-6e3607c68c39', 2, N'adminASD', N'admin@sepiasolutions.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, CAST(0x0000A81500D8F505 AS DateTime), 1, CAST(0x0000A81E00C2FBD5 AS DateTime))
INSERT [dbo].[AppEmployees] ([EmployeeID], [EmployeeGUID], [UserId], [Name], [Email], [PersonalEmail], [MobileNo], [TelNo], [TelNoHome], [FaxNo], [FaxNoHome], [Designation], [DepartmentId], [Salary], [CNICNumber], [CNICFrontJpeg], [CNICBackJpeg], [YahooID], [SkypeID], [BBMCode], [Signature], [Notes], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, N'52a905ca-1bb7-44b8-be21-d5e053589f78', 3, N'operator1', N'operator1@hotmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, CAST(0x0000A81E00C428C6 AS DateTime), NULL, NULL)
INSERT [dbo].[AppEmployees] ([EmployeeID], [EmployeeGUID], [UserId], [Name], [Email], [PersonalEmail], [MobileNo], [TelNo], [TelNoHome], [FaxNo], [FaxNoHome], [Designation], [DepartmentId], [Salary], [CNICNumber], [CNICFrontJpeg], [CNICBackJpeg], [YahooID], [SkypeID], [BBMCode], [Signature], [Notes], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (4, N'77b66a1f-9dd7-4157-b529-227210d2da5a', 4, N'operator2', N'operator2@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, CAST(0x0000A81E00C48D55 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[AppEmployees] OFF
SET IDENTITY_INSERT [dbo].[AppModules] ON 

INSERT [dbo].[AppModules] ([ModuleID], [ModuleName], [ModuleLink], [ModuleIndex], [ModuleParam], [ShowInMenu], [isPrimary], [Sort], [CssClass], [isDisable], [HasFullAccess], [HasCreate], [HasEdit], [HasDelete], [HasView], [HasDetail], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, N'qweqwe', N'qweqwe', N'qweqwe', N'qweqwe', 1, 1, 0, N'qweqwe', 0, 0, 1, 1, 1, 1, 1, 0, CAST(0x0000A817011D891B AS DateTime), 0, CAST(0x0000A81901213F2A AS DateTime))
INSERT [dbo].[AppModules] ([ModuleID], [ModuleName], [ModuleLink], [ModuleIndex], [ModuleParam], [ShowInMenu], [isPrimary], [Sort], [CssClass], [isDisable], [HasFullAccess], [HasCreate], [HasEdit], [HasDelete], [HasView], [HasDetail], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, N'asdasdasdasdasd', N'asdasd', N'asdasd', NULL, 0, 0, 0, NULL, 0, 1, 1, 1, 1, 1, 1, 0, CAST(0x0000A81701372B4B AS DateTime), 0, CAST(0x0000A819011E7F58 AS DateTime))
INSERT [dbo].[AppModules] ([ModuleID], [ModuleName], [ModuleLink], [ModuleIndex], [ModuleParam], [ShowInMenu], [isPrimary], [Sort], [CssClass], [isDisable], [HasFullAccess], [HasCreate], [HasEdit], [HasDelete], [HasView], [HasDetail], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (4, N'dddd', N'dddd', N'ddddd', NULL, 0, 0, 0, NULL, 1, 1, 1, 0, 0, 0, 0, 0, CAST(0x0000A818011C75BD AS DateTime), 0, CAST(0x0000A818011CABEF AS DateTime))
INSERT [dbo].[AppModules] ([ModuleID], [ModuleName], [ModuleLink], [ModuleIndex], [ModuleParam], [ShowInMenu], [isPrimary], [Sort], [CssClass], [isDisable], [HasFullAccess], [HasCreate], [HasEdit], [HasDelete], [HasView], [HasDetail], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (5, N'12e', N'weqwe', N'qweqwe', NULL, 0, 0, 0, NULL, 1, 1, 1, 0, 0, 0, 0, 0, CAST(0x0000A818011CD833 AS DateTime), NULL, NULL)
INSERT [dbo].[AppModules] ([ModuleID], [ModuleName], [ModuleLink], [ModuleIndex], [ModuleParam], [ShowInMenu], [isPrimary], [Sort], [CssClass], [isDisable], [HasFullAccess], [HasCreate], [HasEdit], [HasDelete], [HasView], [HasDetail], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (6, N'ddddee', N'asdasdasd', NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, 1, 1, 1, 1, 0, CAST(0x0000A818011CF2ED AS DateTime), NULL, NULL)
INSERT [dbo].[AppModules] ([ModuleID], [ModuleName], [ModuleLink], [ModuleIndex], [ModuleParam], [ShowInMenu], [isPrimary], [Sort], [CssClass], [isDisable], [HasFullAccess], [HasCreate], [HasEdit], [HasDelete], [HasView], [HasDetail], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (7, N'tttttt12', N'ttttt123', N'ttttt12', N'tttttt', 1, 1, 0, N'tttt', 0, 1, 1, 1, 1, 1, 0, 0, CAST(0x0000A819011A1E59 AS DateTime), 0, CAST(0x0000A819011E5E89 AS DateTime))
INSERT [dbo].[AppModules] ([ModuleID], [ModuleName], [ModuleLink], [ModuleIndex], [ModuleParam], [ShowInMenu], [isPrimary], [Sort], [CssClass], [isDisable], [HasFullAccess], [HasCreate], [HasEdit], [HasDelete], [HasView], [HasDetail], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (8, N'asdasd', N'asdasd', N'asda', N'asda', 1, 1, 0, NULL, 0, 1, 1, 1, 1, 1, 1, 0, CAST(0x0000A819011B454F AS DateTime), NULL, NULL)
INSERT [dbo].[AppModules] ([ModuleID], [ModuleName], [ModuleLink], [ModuleIndex], [ModuleParam], [ShowInMenu], [isPrimary], [Sort], [CssClass], [isDisable], [HasFullAccess], [HasCreate], [HasEdit], [HasDelete], [HasView], [HasDetail], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (9, N'adfsdfsdf', N'sdfsdfsdf', N'sdfsdfsdf', NULL, 1, 1, 0, NULL, 0, 1, 1, 1, 1, 1, 1, 0, CAST(0x0000A819011B5BAF AS DateTime), 0, CAST(0x0000A819011B6B2D AS DateTime))
INSERT [dbo].[AppModules] ([ModuleID], [ModuleName], [ModuleLink], [ModuleIndex], [ModuleParam], [ShowInMenu], [isPrimary], [Sort], [CssClass], [isDisable], [HasFullAccess], [HasCreate], [HasEdit], [HasDelete], [HasView], [HasDetail], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (10, N'asdasdfsdf', N'werwerwerwer', N'werwer', NULL, 0, 0, 0, NULL, 1, 1, 1, 1, 0, 0, 0, 0, CAST(0x0000A819011B9E21 AS DateTime), NULL, NULL)
INSERT [dbo].[AppModules] ([ModuleID], [ModuleName], [ModuleLink], [ModuleIndex], [ModuleParam], [ShowInMenu], [isPrimary], [Sort], [CssClass], [isDisable], [HasFullAccess], [HasCreate], [HasEdit], [HasDelete], [HasView], [HasDetail], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (11, N'dafsdfs', N'dfsdfsdf', N'sdfsdf', NULL, 0, 0, 0, NULL, 0, 1, 1, 0, 0, 0, 0, 0, CAST(0x0000A819011D79F6 AS DateTime), 0, CAST(0x0000A819011E530C AS DateTime))
INSERT [dbo].[AppModules] ([ModuleID], [ModuleName], [ModuleLink], [ModuleIndex], [ModuleParam], [ShowInMenu], [isPrimary], [Sort], [CssClass], [isDisable], [HasFullAccess], [HasCreate], [HasEdit], [HasDelete], [HasView], [HasDetail], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (12, N'asdasDFFF', N'asdASD', NULL, NULL, 0, 0, 0, NULL, 0, 1, 1, 1, 0, 0, 0, 0, CAST(0x0000A819011F1E98 AS DateTime), NULL, NULL)
INSERT [dbo].[AppModules] ([ModuleID], [ModuleName], [ModuleLink], [ModuleIndex], [ModuleParam], [ShowInMenu], [isPrimary], [Sort], [CssClass], [isDisable], [HasFullAccess], [HasCreate], [HasEdit], [HasDelete], [HasView], [HasDetail], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (13, N'asdasdasdasdasdqwe', N'asdasd', N'asdasd', N'qweqwe', 0, 0, 0, N'qweqwe', 0, 1, 1, 1, 1, 1, 0, 0, CAST(0x0000A81A00CADC21 AS DateTime), NULL, NULL)
INSERT [dbo].[AppModules] ([ModuleID], [ModuleName], [ModuleLink], [ModuleIndex], [ModuleParam], [ShowInMenu], [isPrimary], [Sort], [CssClass], [isDisable], [HasFullAccess], [HasCreate], [HasEdit], [HasDelete], [HasView], [HasDetail], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (14, N'qweqw', N'eqwe', N'asdasd', N'asdasf', 0, 0, 0, N'asdasd', 0, 1, 1, 1, 1, 0, 0, 0, CAST(0x0000A81A00CAEAF9 AS DateTime), 0, CAST(0x0000A81A00CB66C7 AS DateTime))
SET IDENTITY_INSERT [dbo].[AppModules] OFF
SET IDENTITY_INSERT [dbo].[AppRoles] ON 

INSERT [dbo].[AppRoles] ([RoleId], [RoleName], [Priority], [isEditable], [isDisable], [CreatedBy], [CreatedDate], [ModifiedBy], [Modified]) VALUES (1, N'Admin', 0, 0, 1, 0, CAST(0x0000A81500C71D18 AS DateTime), 0, CAST(0x0000A819011FCBC8 AS DateTime))
INSERT [dbo].[AppRoles] ([RoleId], [RoleName], [Priority], [isEditable], [isDisable], [CreatedBy], [CreatedDate], [ModifiedBy], [Modified]) VALUES (2, N'Manager', 10, 0, 1, 0, CAST(0x0000A81500C73279 AS DateTime), 0, CAST(0x0000A81500D4D4CC AS DateTime))
INSERT [dbo].[AppRoles] ([RoleId], [RoleName], [Priority], [isEditable], [isDisable], [CreatedBy], [CreatedDate], [ModifiedBy], [Modified]) VALUES (42, N'Operator', 20, 1, 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[AppRoles] OFF
SET IDENTITY_INSERT [dbo].[AppUserLogs] ON 

INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (19, 1, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A81E00C2CD2B AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (20, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A81E00C38561 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (21, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A81E00D5A8BA AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (22, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A81E00D8C5AD AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (23, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A81E0109E274 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (24, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A81E0111BAA5 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (25, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A81E012130A3 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (26, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A81E0138133D AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (27, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A81E0139F9CD AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (28, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A81E013AB337 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (29, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A81E013C1369 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (30, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A81E013E7805 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (31, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A81E013F3BBC AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (32, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A81E01402ED7 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (33, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A81E0141A40B AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (34, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A81E01424E2C AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (35, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A81E0145B279 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (36, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A81E0146473C AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (37, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A81F00C2FE18 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (38, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A81F00CA506E AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (39, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A81F00CD4C51 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (40, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A81F00CE77A2 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (41, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A81F00CFC7C6 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (42, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A81F00D04851 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (43, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A81F00D2B70B AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (44, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A81F00D3AEE7 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (45, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A81F00D5C1AC AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (46, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A81F00E36B19 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (47, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A81F00E4C3A6 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (48, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A81F00FAA6B1 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (49, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A81F010B4865 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (50, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A81F013EF97D AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (51, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82000C6510F AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (52, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82000D5D191 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (53, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82000D827E3 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (54, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82000E6DA27 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (55, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82000E9593B AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (56, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82000EA14E1 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (57, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82000ED617B AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (58, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82000EF40B9 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (59, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82000EFB302 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (60, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82000F63E16 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (61, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82000F72F57 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (62, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82000F7B4DA AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (63, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82000F94740 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (64, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82000FA4CF5 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (65, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A8200100B9BA AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (66, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A8200105A02E AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (67, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A8200106A550 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (68, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82001324CF4 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (69, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A8200136DD4D AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (70, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A820013FB787 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (71, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82001423027 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (72, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A8200142EFB4 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (73, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A8200143CBF2 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (74, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82001457A9C AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (75, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82100EDE3F0 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (76, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82100FDDBC2 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (77, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A821011AC711 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (78, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A821011B7152 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (79, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A821011BAAE7 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (80, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A821011C6A0B AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (81, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A821011CCFE1 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (82, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A821011D6D14 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (83, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82101238C5C AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (84, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82101240395 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (85, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A821012672F9 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (86, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A821012672F1 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (87, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82300D5AFFF AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (88, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82300D76DF8 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (89, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82300D81332 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (90, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82300D89BD8 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (91, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82300D8BD5D AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (92, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82300D8E94B AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (93, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82300DA2022 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (94, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82300DE6269 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (95, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82300E0CFED AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (96, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82300E1234B AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (97, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82300E78A42 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (98, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82300EAA3BB AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (99, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82300EB01B3 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (100, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82300ED5962 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (101, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82300F0E99C AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (102, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82300F134CC AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (103, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82300FBC1F3 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (104, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82300FC780D AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (105, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82300FCFE47 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (106, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82300FDEE55 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (107, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82300FE3E5B AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (108, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A8230102563B AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (109, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A8230105C28A AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (110, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82301063B3C AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (111, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A8230108D02A AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (112, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A823010A0B13 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (113, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A823010DB588 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (114, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A823011066C5 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (115, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A8230111966B AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (116, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A823011B2C9D AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (117, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A823011F0862 AS DateTime), NULL)
GO
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (118, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A8230123B82B AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (119, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A8230124291F AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (120, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A823012567B9 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (121, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A823012C9213 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (122, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A8230130C2EA AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (123, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82400FA7AE1 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (124, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82400FD61DA AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (125, 2, N'::1', N'Firefox 56.0', NULL, CAST(0x0000A82400FDC905 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (126, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A8240103F8C0 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (127, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A8240106163C AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (128, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A8240106807F AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (129, 2, N'::1', N'Firefox 56.0', NULL, CAST(0x0000A824010A9F70 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (130, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A824011C1986 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (131, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A824011D078C AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (132, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A824011FEC2D AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (133, 3, N'::1', N'Firefox 56.0', NULL, CAST(0x0000A824011FF818 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (134, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A8240121A269 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (135, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A824012497CD AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (136, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A824012F3B12 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (137, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A824012FA3D6 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (138, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A824013194F1 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (139, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82401326147 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (140, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A8240134EB55 AS DateTime), CAST(0x0000A8240134F741 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (141, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A8240135E318 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (142, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A8240136492B AS DateTime), CAST(0x0000A82401365AD6 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (143, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82401366104 AS DateTime), CAST(0x0000A82401366875 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (144, 2, N'::1', N'Firefox 56.0', NULL, CAST(0x0000A82401378C6E AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (145, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A824013802C0 AS DateTime), CAST(0x0000A82401389D9C AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (146, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82401395530 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (147, 2, N'::1', N'Firefox 56.0', NULL, CAST(0x0000A82401397CDD AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (148, 2, N'::1', N'Firefox 56.0', NULL, CAST(0x0000A824013A33CE AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (149, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A824013AA301 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (150, 2, N'::1', N'Firefox 56.0', NULL, CAST(0x0000A824013B1DAF AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (151, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A824013BE3D2 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (152, 3, N'::1', N'Firefox 56.0', NULL, CAST(0x0000A824013C0EED AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (153, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A824013D02D9 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (154, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82500CB17DE AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (155, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82500D03E58 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (156, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82500D131AE AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (157, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82500D25663 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (158, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82500D31AEC AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (159, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82500D4EAF1 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (160, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82500D7C511 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (161, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82500E6232E AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (162, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82500EDC80B AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (163, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82500FADD79 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (164, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A8250110A349 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (165, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A8250119F900 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (166, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82501207394 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (167, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A8250131A2A2 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (168, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82501333903 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (169, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A8250136FFB0 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (170, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82501380E00 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (171, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A825013982C7 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (172, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A825013A40E9 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (173, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A8250140C23D AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (174, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82600D02D30 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (175, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82600E8118A AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (176, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82600F46103 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (177, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82600F5E080 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (178, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82600F77B2B AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (179, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82600F85A95 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (180, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A826012E7EE3 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (181, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A8260135D2FB AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (182, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82601368557 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (183, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82800F215A5 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (184, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82801306D72 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (185, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A8280137E5CE AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (186, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A828013C9AC0 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (187, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82A00E1F641 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (188, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82A0122FB06 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (189, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82A0130A5CF AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (190, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82A01401F74 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (191, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82A0141F125 AS DateTime), CAST(0x0000A82A0141F60C AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (192, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82A0141FE0F AS DateTime), CAST(0x0000A82A014212D5 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (193, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82A014218FD AS DateTime), CAST(0x0000A82A01421FC1 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (194, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82A014225DB AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (195, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82A01470863 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (196, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82B00CE8CB8 AS DateTime), CAST(0x0000A82B00CE9670 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (197, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82B00CE9D44 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (198, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82B0139079E AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (199, 3, N'::1', N'Firefox 56.0', NULL, CAST(0x0000A82B01393A96 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (200, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82B013AC096 AS DateTime), CAST(0x0000A82B013C12C1 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (201, 3, N'::1', N'Firefox 56.0', NULL, CAST(0x0000A82B013ACCAF AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (202, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82B013C193A AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (203, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82B0144D7C7 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (204, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82B014BB920 AS DateTime), CAST(0x0000A82B014BBC76 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (205, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82B014BC263 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (206, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82B014CE6BF AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (207, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82C00E3D7FA AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (208, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82C00E9CE51 AS DateTime), CAST(0x0000A82C00E9D432 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (209, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82C00E9DA17 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (210, 3, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82C00EC7815 AS DateTime), CAST(0x0000A82C00EDCCA2 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (211, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82C00EDD3CD AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (212, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82C00EE6EFD AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (213, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82C00F1AB1D AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (214, 2, N'::1', N'Chrome 61.0', NULL, CAST(0x0000A82C00F3E9C9 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (215, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82C00F521B2 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (216, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82C00F593D5 AS DateTime), CAST(0x0000A82C00F5BF85 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (217, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82C00F5C4B9 AS DateTime), CAST(0x0000A82C00F5EAE1 AS DateTime))
GO
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (218, 4, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82C00F5F6E5 AS DateTime), CAST(0x0000A82C00F62D78 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (219, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82C00F63480 AS DateTime), CAST(0x0000A82C00F6519F AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (220, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82C00F656E5 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (221, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82C00F78BEF AS DateTime), CAST(0x0000A82C00F796BB AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (222, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82C00F79C3A AS DateTime), CAST(0x0000A82C00F7A1DE AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (223, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82C00F7A7D9 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (224, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82C00F8AF3F AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (225, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82C00F912C6 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (226, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82C00F9E161 AS DateTime), CAST(0x0000A82C00FA5E7E AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (227, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82C0100469F AS DateTime), CAST(0x0000A82C01005EB5 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (228, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82C01006988 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (229, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82C0100D88F AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (230, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82C01018786 AS DateTime), CAST(0x0000A82C01019270 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (231, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82C01019854 AS DateTime), CAST(0x0000A82C0101CE3E AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (232, 4, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82C0101D535 AS DateTime), CAST(0x0000A82C0101E841 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (233, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82C0101EEE3 AS DateTime), CAST(0x0000A82C010205D6 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (234, 4, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82C01021262 AS DateTime), CAST(0x0000A82C01022585 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (235, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82C01022C98 AS DateTime), CAST(0x0000A82C010240B5 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (236, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82C010248FF AS DateTime), CAST(0x0000A82C01027B5B AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (237, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82C010283DF AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (238, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82C01033961 AS DateTime), CAST(0x0000A82C01036296 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (239, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82C0103783F AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (240, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82C01042D2B AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (241, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82C010B675F AS DateTime), CAST(0x0000A82C010B6B8B AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (242, 4, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82C010B719F AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (243, 4, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82C0121F37F AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (244, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82C0123DF78 AS DateTime), CAST(0x0000A82C0123F3A7 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (245, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82C0123F9EE AS DateTime), CAST(0x0000A82C012410D3 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (246, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82C01241ABF AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (247, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82C01368707 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (248, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82C01370873 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (249, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82C01395397 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (250, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82C0139F40F AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (251, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82D00D9BB00 AS DateTime), CAST(0x0000A82D00DACFC0 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (252, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82D00DADFA5 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (253, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82D00EF9AB4 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (254, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82D00FAEC42 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (255, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82D010644D7 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (256, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82D0109A85B AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (257, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82D010CCB82 AS DateTime), CAST(0x0000A82D010D453B AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (258, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82D010D4C21 AS DateTime), CAST(0x0000A82D010F7676 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (259, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82D010F7F32 AS DateTime), CAST(0x0000A82D01101044 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (260, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82D011015BF AS DateTime), CAST(0x0000A82D0110754A AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (261, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82D011403CA AS DateTime), CAST(0x0000A82D011486D2 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (262, 4, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82D01148E61 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (263, 4, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82D01174849 AS DateTime), CAST(0x0000A82D01181B75 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (264, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82D01183024 AS DateTime), CAST(0x0000A82D01222E09 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (265, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82D01223425 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (266, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82D01295B73 AS DateTime), CAST(0x0000A82D01296FB2 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (267, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82D0129F8DF AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (268, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82D012CB28E AS DateTime), CAST(0x0000A82D012D1869 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (269, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82D012D24DD AS DateTime), CAST(0x0000A82D012D61C9 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (270, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82D012D671A AS DateTime), CAST(0x0000A82D012D7B74 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (271, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82D012D854E AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (272, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82D01473A94 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (273, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82D01493576 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (274, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82D014B0A1E AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (275, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82E010134B6 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (276, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82E01040498 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (277, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82E0119A256 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (278, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82E0124DE07 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (279, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82E012580C1 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (280, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82E012C3AE2 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (281, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82E013A5577 AS DateTime), CAST(0x0000A82E013AE0BB AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (282, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82E013AE795 AS DateTime), CAST(0x0000A82E013B61B6 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (283, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82E013B6A7B AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (284, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82E013D65C7 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (285, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F00E02967 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (286, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F00E2ED6A AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (287, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F00E92B44 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (288, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F00EA87B9 AS DateTime), CAST(0x0000A82F00EAE216 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (289, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F00EAE89C AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (290, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F00EB61BA AS DateTime), CAST(0x0000A82F00EB7C34 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (291, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F00EB821A AS DateTime), CAST(0x0000A82F00EB8558 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (292, 4, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F00EB91E6 AS DateTime), CAST(0x0000A82F00EBA0C4 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (293, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F00EBA6FE AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (294, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F00FC1457 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (295, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F00FC5FCB AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (296, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F00FDD942 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (297, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F0100978E AS DateTime), CAST(0x0000A82F01009C46 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (298, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F0100A328 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (299, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F0106B450 AS DateTime), CAST(0x0000A82F0106CCD6 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (300, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F0106D48B AS DateTime), CAST(0x0000A82F0106F4FB AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (301, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F0106FE00 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (302, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F010744BE AS DateTime), CAST(0x0000A82F01074CFC AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (303, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F01075289 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (304, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F010AD624 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (305, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F010B7943 AS DateTime), CAST(0x0000A82F010BB988 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (306, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F010BBF9B AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (307, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F010C0DBD AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (308, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F0113E228 AS DateTime), CAST(0x0000A82F0113E788 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (309, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F0113ECCB AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (310, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F01156A2D AS DateTime), CAST(0x0000A82F0116D0D7 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (311, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F0116D61A AS DateTime), CAST(0x0000A82F0116DF14 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (312, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F0116E6FF AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (313, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F0124701C AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (314, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F012528A2 AS DateTime), CAST(0x0000A82F012567ED AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (315, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F01256D75 AS DateTime), CAST(0x0000A82F0125D4FA AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (316, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F0125DE13 AS DateTime), CAST(0x0000A82F0125E570 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (317, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F0125EAF3 AS DateTime), CAST(0x0000A82F012620A6 AS DateTime))
GO
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (318, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F0126259F AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (319, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F012A4BA8 AS DateTime), CAST(0x0000A82F012A709E AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (320, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F012A7D8F AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (321, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F012BDDAD AS DateTime), CAST(0x0000A82F012C353B AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (322, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F012C48E4 AS DateTime), CAST(0x0000A82F012C6872 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (323, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F012C709B AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (324, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F012DD7FB AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (325, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F012E49AB AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (326, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F0130C2FA AS DateTime), CAST(0x0000A82F0130E00B AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (327, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F0130E677 AS DateTime), CAST(0x0000A82F01312F6E AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (328, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F01313612 AS DateTime), CAST(0x0000A82F01314C0D AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (329, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F01315401 AS DateTime), CAST(0x0000A82F013170C8 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (330, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F01317707 AS DateTime), CAST(0x0000A82F0131BA50 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (331, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F0131C393 AS DateTime), CAST(0x0000A82F0131D0B9 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (332, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F0131E4A5 AS DateTime), CAST(0x0000A82F0131E87B AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (333, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F0131EDF2 AS DateTime), CAST(0x0000A82F013222D0 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (334, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F01322BFB AS DateTime), CAST(0x0000A82F0132351C AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (335, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F01323C71 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (336, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F01390155 AS DateTime), CAST(0x0000A82F013912B5 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (337, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F0139197D AS DateTime), CAST(0x0000A82F01395CDB AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (338, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F01396596 AS DateTime), CAST(0x0000A82F0139A76C AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (339, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F0139AFA3 AS DateTime), CAST(0x0000A82F0139BB8C AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (340, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F0139C542 AS DateTime), CAST(0x0000A82F0139C8E9 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (341, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F0139D8F4 AS DateTime), CAST(0x0000A82F013A0C96 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (342, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F013A12BB AS DateTime), CAST(0x0000A82F013A1FC7 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (343, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F013A26F6 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (344, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F013E8128 AS DateTime), CAST(0x0000A82F013E92E8 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (345, 4, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F013E9B01 AS DateTime), CAST(0x0000A82F013EB5F3 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (346, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F013EBDB2 AS DateTime), CAST(0x0000A82F013ED433 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (347, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F013ED8E2 AS DateTime), CAST(0x0000A82F013F1865 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (348, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F013F2030 AS DateTime), CAST(0x0000A82F013F2932 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (349, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F013F2F02 AS DateTime), CAST(0x0000A82F013F3D57 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (350, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F013F45DA AS DateTime), CAST(0x0000A82F013F7341 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (351, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F013F796D AS DateTime), CAST(0x0000A82F013F8274 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (352, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F013F8898 AS DateTime), CAST(0x0000A82F013FBB4D AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (353, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F013FC256 AS DateTime), CAST(0x0000A82F013FD11C AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (354, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F013FD7D3 AS DateTime), CAST(0x0000A82F013FF1AB AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (355, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F013FF914 AS DateTime), CAST(0x0000A82F01401631 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (356, 4, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F014020BE AS DateTime), CAST(0x0000A82F014028CD AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (357, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F01402F00 AS DateTime), CAST(0x0000A82F014034A7 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (358, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F01403AF4 AS DateTime), CAST(0x0000A82F014040FB AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (359, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F01404B8A AS DateTime), CAST(0x0000A82F01404FDD AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (360, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F01405806 AS DateTime), CAST(0x0000A82F01407C1A AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (361, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F0140838B AS DateTime), CAST(0x0000A82F014099F0 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (362, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F0140A130 AS DateTime), CAST(0x0000A82F0140D440 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (363, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F0140E0CB AS DateTime), CAST(0x0000A82F0140F03C AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (364, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F0140FC3F AS DateTime), CAST(0x0000A82F01412596 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (365, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F01412BDF AS DateTime), CAST(0x0000A82F01417BC2 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (366, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F0141885B AS DateTime), CAST(0x0000A82F01420CF3 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (367, 4, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F01421572 AS DateTime), CAST(0x0000A82F01423134 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (368, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F01423A0E AS DateTime), CAST(0x0000A82F01424A7D AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (369, 4, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F01425215 AS DateTime), CAST(0x0000A82F01426E10 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (370, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F01427407 AS DateTime), CAST(0x0000A82F0142C5AA AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (371, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F0142CD0D AS DateTime), CAST(0x0000A82F01439EA5 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (372, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F0143A4E4 AS DateTime), CAST(0x0000A82F0143B208 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (373, 4, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F0143D028 AS DateTime), CAST(0x0000A82F0143FBA7 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (374, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F0144020F AS DateTime), CAST(0x0000A82F01440D8D AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (375, 4, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F014415D6 AS DateTime), CAST(0x0000A82F014428CB AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (376, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F0144315C AS DateTime), CAST(0x0000A82F01454AD6 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (377, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F01455289 AS DateTime), CAST(0x0000A82F0146618B AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (378, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F01466B6D AS DateTime), CAST(0x0000A82F0146747F AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (379, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F01467ADD AS DateTime), CAST(0x0000A82F014690B2 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (380, 4, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F0146BB15 AS DateTime), CAST(0x0000A82F01473B48 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (381, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F01474301 AS DateTime), CAST(0x0000A82F014759F8 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (382, 4, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F01475FB5 AS DateTime), CAST(0x0000A82F014800FA AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (383, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F01480AD6 AS DateTime), CAST(0x0000A82F01481024 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (384, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F014815BD AS DateTime), CAST(0x0000A82F0149004F AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (385, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F01490E96 AS DateTime), CAST(0x0000A82F01491262 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (386, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F0149184D AS DateTime), CAST(0x0000A82F01491C3C AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (387, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F014921D2 AS DateTime), CAST(0x0000A82F0149445E AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (388, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F01494984 AS DateTime), CAST(0x0000A82F01495562 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (389, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A82F01495F70 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (390, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A83100CBBFEF AS DateTime), CAST(0x0000A83100CD79BA AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (391, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A83100CD7F66 AS DateTime), CAST(0x0000A83100CD8BAC AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (392, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A83100CD92D1 AS DateTime), CAST(0x0000A83100CDEAE9 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (393, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A83100CDF13C AS DateTime), CAST(0x0000A83100CDF98B AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (394, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A83100CDFF0D AS DateTime), CAST(0x0000A83100CE245C AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (395, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A83100CE35DD AS DateTime), CAST(0x0000A83100CE9273 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (396, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A83100CE9A5F AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (397, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A83100E5C3C7 AS DateTime), CAST(0x0000A83100E5C89B AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (398, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A83100E5CEF1 AS DateTime), CAST(0x0000A83100E5FFE9 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (399, 4, N'::1', N'Firefox 57.0', NULL, CAST(0x0000A83100E5F5D1 AS DateTime), CAST(0x0000A83100E6FFCA AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (400, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A83100E60901 AS DateTime), CAST(0x0000A83100E74FB8 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (401, 2, N'::1', N'Firefox 57.0', NULL, CAST(0x0000A83100E705BD AS DateTime), CAST(0x0000A83100E8449D AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (402, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A83100E75629 AS DateTime), CAST(0x0000A83100E75A8B AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (403, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A83100E75FBB AS DateTime), CAST(0x0000A83100E81A0E AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (404, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A83100E81E79 AS DateTime), CAST(0x0000A83100E82579 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (405, 4, N'::1', N'Firefox 57.0', NULL, CAST(0x0000A83100E84B05 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (406, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A83100E85AFD AS DateTime), CAST(0x0000A83100E8D7AA AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (407, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A83100E8DD6C AS DateTime), CAST(0x0000A83100E900C3 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (408, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A83100E906C6 AS DateTime), CAST(0x0000A83100E95B4A AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (409, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A83100E96197 AS DateTime), CAST(0x0000A83100E9DBAF AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (410, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A83100E9E765 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (411, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A83100EEBAAF AS DateTime), CAST(0x0000A83100EEC8BF AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (412, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A83100EECFB6 AS DateTime), CAST(0x0000A83100EFE66A AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (413, 2, N'::1', N'Firefox 57.0', NULL, CAST(0x0000A83100EEFCFF AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (414, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A83100EFEC26 AS DateTime), CAST(0x0000A83100EFEFA6 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (415, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A83100EFF636 AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (416, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A83100FED1A9 AS DateTime), CAST(0x0000A83101003AEB AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (417, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A83101004269 AS DateTime), CAST(0x0000A83101005151 AS DateTime))
GO
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (418, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A8310100595E AS DateTime), NULL)
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (419, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A8310101DD3F AS DateTime), CAST(0x0000A8310101E7D2 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (420, 3, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A8310101EDF0 AS DateTime), CAST(0x0000A83101021ECE AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (421, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A83101022473 AS DateTime), CAST(0x0000A83101032135 AS DateTime))
INSERT [dbo].[AppUserLogs] ([UserLogID], [UserId], [IPAddress], [Browser], [SessionID], [LoginDateTime], [LogOutDateTime]) VALUES (422, 2, N'::1', N'Chrome 62.0', NULL, CAST(0x0000A83101033444 AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[AppUserLogs] OFF
SET IDENTITY_INSERT [dbo].[AppUsers] ON 

INSERT [dbo].[AppUsers] ([UserId], [UserGUID], [isEmployee], [RoleId], [UserName], [Password], [PassKey], [ExpiryDate], [LastLogn], [LoginCount], [isActive], [isDisable], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, N'a7bf1fa6-3fd9-4b8f-8a50-bffc5c8a6616', 1, 1, N'admin', N'VV/lS2TZp+A=', NULL, NULL, CAST(0x0000A81E00C2CD2B AS DateTime), 17, 1, 0, 0, CAST(0x0000A81500D373FA AS DateTime), NULL, NULL)
INSERT [dbo].[AppUsers] ([UserId], [UserGUID], [isEmployee], [RoleId], [UserName], [Password], [PassKey], [ExpiryDate], [LastLogn], [LoginCount], [isActive], [isDisable], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, N'256301a6-81df-417f-9c8f-ccc81d425018', 1, 2, N'shiftmanager', N'VV/lS2TZp+A=', NULL, NULL, CAST(0x0000A83101033444 AS DateTime), 204, 1, 0, 0, CAST(0x0000A81500D8F505 AS DateTime), NULL, NULL)
INSERT [dbo].[AppUsers] ([UserId], [UserGUID], [isEmployee], [RoleId], [UserName], [Password], [PassKey], [ExpiryDate], [LastLogn], [LoginCount], [isActive], [isDisable], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, N'2cb28a7e-9fb0-4482-ab96-34c84f09e695', 1, 42, N'operator1', N'VV/lS2TZp+A=', NULL, NULL, CAST(0x0000A8310101EDF0 AS DateTime), 182, 1, 0, 2, CAST(0x0000A81E00C428C6 AS DateTime), NULL, NULL)
INSERT [dbo].[AppUsers] ([UserId], [UserGUID], [isEmployee], [RoleId], [UserName], [Password], [PassKey], [ExpiryDate], [LastLogn], [LoginCount], [isActive], [isDisable], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (4, N'2a2b3d55-74bd-40f7-a71e-976678e93c2e', 1, 42, N'operator2', N'VV/lS2TZp+A=', NULL, NULL, CAST(0x0000A83100E84B05 AS DateTime), 18, 1, 0, 2, CAST(0x0000A81E00C48D55 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[AppUsers] OFF
SET IDENTITY_INSERT [dbo].[Bank] ON 

INSERT [dbo].[Bank] ([BankId], [BankName], [Slip]) VALUES (2, N'www', NULL)
SET IDENTITY_INSERT [dbo].[Bank] OFF
SET IDENTITY_INSERT [dbo].[BankChallan] ON 

INSERT [dbo].[BankChallan] ([BankChallanId], [DayId], [BankId], [ChallanNo], [ChallanDatetime], [isChallanSubmitedtoBank], [DepositSlip], [TotalAmount], [TotalAmountinword], [CreatedBy], [CreatedDatetime], [ModifiedBy], [ModifiedDatetime]) VALUES (26, 81, 2, N'20810', CAST(0x0000A83100FEE3B7 AS DateTime), 1, N'bankchallan_010801c9-3a97-4285-a7b5-29f83ddff100.txt', CAST(210.00 AS Decimal(18, 2)), NULL, NULL, CAST(0x0000A83100FEE3B7 AS DateTime), NULL, NULL)
INSERT [dbo].[BankChallan] ([BankChallanId], [DayId], [BankId], [ChallanNo], [ChallanDatetime], [isChallanSubmitedtoBank], [DepositSlip], [TotalAmount], [TotalAmountinword], [CreatedBy], [CreatedDatetime], [ModifiedBy], [ModifiedDatetime]) VALUES (27, 82, 2, N'20820', CAST(0x0000A83100FEE3E3 AS DateTime), 0, NULL, CAST(70.00 AS Decimal(18, 2)), NULL, NULL, CAST(0x0000A83100FEE3E3 AS DateTime), NULL, NULL)
INSERT [dbo].[BankChallan] ([BankChallanId], [DayId], [BankId], [ChallanNo], [ChallanDatetime], [isChallanSubmitedtoBank], [DepositSlip], [TotalAmount], [TotalAmountinword], [CreatedBy], [CreatedDatetime], [ModifiedBy], [ModifiedDatetime]) VALUES (28, 83, 2, N'20830', CAST(0x0000A83100FEE3E8 AS DateTime), 0, NULL, CAST(0.00 AS Decimal(18, 2)), NULL, NULL, CAST(0x0000A83100FEE3E8 AS DateTime), NULL, NULL)
INSERT [dbo].[BankChallan] ([BankChallanId], [DayId], [BankId], [ChallanNo], [ChallanDatetime], [isChallanSubmitedtoBank], [DepositSlip], [TotalAmount], [TotalAmountinword], [CreatedBy], [CreatedDatetime], [ModifiedBy], [ModifiedDatetime]) VALUES (29, 84, 2, N'20840', CAST(0x0000A83100FEE3E9 AS DateTime), 0, NULL, CAST(0.00 AS Decimal(18, 2)), NULL, NULL, CAST(0x0000A83100FEE3E9 AS DateTime), NULL, NULL)
INSERT [dbo].[BankChallan] ([BankChallanId], [DayId], [BankId], [ChallanNo], [ChallanDatetime], [isChallanSubmitedtoBank], [DepositSlip], [TotalAmount], [TotalAmountinword], [CreatedBy], [CreatedDatetime], [ModifiedBy], [ModifiedDatetime]) VALUES (30, 85, 2, N'20850', CAST(0x0000A83100FEE3EB AS DateTime), 0, NULL, CAST(0.00 AS Decimal(18, 2)), NULL, NULL, CAST(0x0000A83100FEE3EB AS DateTime), NULL, NULL)
INSERT [dbo].[BankChallan] ([BankChallanId], [DayId], [BankId], [ChallanNo], [ChallanDatetime], [isChallanSubmitedtoBank], [DepositSlip], [TotalAmount], [TotalAmountinword], [CreatedBy], [CreatedDatetime], [ModifiedBy], [ModifiedDatetime]) VALUES (31, 86, 2, N'20860', CAST(0x0000A83100FEE3EC AS DateTime), 0, NULL, CAST(0.00 AS Decimal(18, 2)), NULL, NULL, CAST(0x0000A83100FEE3EC AS DateTime), NULL, NULL)
INSERT [dbo].[BankChallan] ([BankChallanId], [DayId], [BankId], [ChallanNo], [ChallanDatetime], [isChallanSubmitedtoBank], [DepositSlip], [TotalAmount], [TotalAmountinword], [CreatedBy], [CreatedDatetime], [ModifiedBy], [ModifiedDatetime]) VALUES (32, 87, 2, N'20870', CAST(0x0000A8310100835A AS DateTime), 0, NULL, CAST(0.00 AS Decimal(18, 2)), NULL, NULL, CAST(0x0000A8310100835A AS DateTime), NULL, NULL)
INSERT [dbo].[BankChallan] ([BankChallanId], [DayId], [BankId], [ChallanNo], [ChallanDatetime], [isChallanSubmitedtoBank], [DepositSlip], [TotalAmount], [TotalAmountinword], [CreatedBy], [CreatedDatetime], [ModifiedBy], [ModifiedDatetime]) VALUES (33, 88, 2, N'20880', CAST(0x0000A83101027414 AS DateTime), 0, NULL, CAST(50.00 AS Decimal(18, 2)), NULL, NULL, CAST(0x0000A83101027414 AS DateTime), NULL, NULL)
INSERT [dbo].[BankChallan] ([BankChallanId], [DayId], [BankId], [ChallanNo], [ChallanDatetime], [isChallanSubmitedtoBank], [DepositSlip], [TotalAmount], [TotalAmountinword], [CreatedBy], [CreatedDatetime], [ModifiedBy], [ModifiedDatetime]) VALUES (34, 89, 2, N'20890', CAST(0x0000A8310102BD79 AS DateTime), 0, NULL, CAST(0.00 AS Decimal(18, 2)), NULL, NULL, CAST(0x0000A8310102BD79 AS DateTime), NULL, NULL)
INSERT [dbo].[BankChallan] ([BankChallanId], [DayId], [BankId], [ChallanNo], [ChallanDatetime], [isChallanSubmitedtoBank], [DepositSlip], [TotalAmount], [TotalAmountinword], [CreatedBy], [CreatedDatetime], [ModifiedBy], [ModifiedDatetime]) VALUES (35, 90, 2, N'20900', CAST(0x0000A8310102E906 AS DateTime), 0, NULL, CAST(0.00 AS Decimal(18, 2)), NULL, NULL, CAST(0x0000A8310102E906 AS DateTime), NULL, NULL)
INSERT [dbo].[BankChallan] ([BankChallanId], [DayId], [BankId], [ChallanNo], [ChallanDatetime], [isChallanSubmitedtoBank], [DepositSlip], [TotalAmount], [TotalAmountinword], [CreatedBy], [CreatedDatetime], [ModifiedBy], [ModifiedDatetime]) VALUES (36, 91, 2, N'20910', CAST(0x0000A8310102E909 AS DateTime), 0, NULL, CAST(0.00 AS Decimal(18, 2)), NULL, NULL, CAST(0x0000A8310102E909 AS DateTime), NULL, NULL)
INSERT [dbo].[BankChallan] ([BankChallanId], [DayId], [BankId], [ChallanNo], [ChallanDatetime], [isChallanSubmitedtoBank], [DepositSlip], [TotalAmount], [TotalAmountinword], [CreatedBy], [CreatedDatetime], [ModifiedBy], [ModifiedDatetime]) VALUES (37, 92, 2, N'20920', CAST(0x0000A831010302D2 AS DateTime), 1, N'bankchallan_f87235b5-251e-4f8e-83b1-1e1932b64ce2.txt', CAST(0.00 AS Decimal(18, 2)), NULL, NULL, CAST(0x0000A831010302D2 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[BankChallan] OFF
SET IDENTITY_INSERT [dbo].[BankChallanDetail] ON 

INSERT [dbo].[BankChallanDetail] ([BankChallenDetailsId], [BankChallanId], [TokenDetailId], [Fees]) VALUES (61, 26, 305, CAST(20.00 AS Decimal(18, 2)))
INSERT [dbo].[BankChallanDetail] ([BankChallenDetailsId], [BankChallanId], [TokenDetailId], [Fees]) VALUES (62, 26, 306, CAST(20.00 AS Decimal(18, 2)))
INSERT [dbo].[BankChallanDetail] ([BankChallenDetailsId], [BankChallanId], [TokenDetailId], [Fees]) VALUES (63, 26, 307, CAST(20.00 AS Decimal(18, 2)))
INSERT [dbo].[BankChallanDetail] ([BankChallenDetailsId], [BankChallanId], [TokenDetailId], [Fees]) VALUES (64, 26, 308, CAST(20.00 AS Decimal(18, 2)))
INSERT [dbo].[BankChallanDetail] ([BankChallenDetailsId], [BankChallanId], [TokenDetailId], [Fees]) VALUES (65, 26, 309, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[BankChallanDetail] ([BankChallenDetailsId], [BankChallanId], [TokenDetailId], [Fees]) VALUES (66, 26, 310, CAST(20.00 AS Decimal(18, 2)))
INSERT [dbo].[BankChallanDetail] ([BankChallenDetailsId], [BankChallanId], [TokenDetailId], [Fees]) VALUES (67, 26, 311, CAST(20.00 AS Decimal(18, 2)))
INSERT [dbo].[BankChallanDetail] ([BankChallenDetailsId], [BankChallanId], [TokenDetailId], [Fees]) VALUES (68, 26, 312, CAST(20.00 AS Decimal(18, 2)))
INSERT [dbo].[BankChallanDetail] ([BankChallenDetailsId], [BankChallanId], [TokenDetailId], [Fees]) VALUES (69, 26, 313, CAST(20.00 AS Decimal(18, 2)))
INSERT [dbo].[BankChallanDetail] ([BankChallenDetailsId], [BankChallanId], [TokenDetailId], [Fees]) VALUES (70, 26, 314, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[BankChallanDetail] ([BankChallenDetailsId], [BankChallanId], [TokenDetailId], [Fees]) VALUES (71, 26, 315, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[BankChallanDetail] ([BankChallenDetailsId], [BankChallanId], [TokenDetailId], [Fees]) VALUES (72, 26, 316, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[BankChallanDetail] ([BankChallenDetailsId], [BankChallanId], [TokenDetailId], [Fees]) VALUES (73, 26, 317, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[BankChallanDetail] ([BankChallenDetailsId], [BankChallanId], [TokenDetailId], [Fees]) VALUES (74, 26, 318, CAST(30.00 AS Decimal(18, 2)))
INSERT [dbo].[BankChallanDetail] ([BankChallenDetailsId], [BankChallanId], [TokenDetailId], [Fees]) VALUES (75, 26, 319, CAST(30.00 AS Decimal(18, 2)))
INSERT [dbo].[BankChallanDetail] ([BankChallenDetailsId], [BankChallanId], [TokenDetailId], [Fees]) VALUES (76, 26, 320, CAST(30.00 AS Decimal(18, 2)))
INSERT [dbo].[BankChallanDetail] ([BankChallenDetailsId], [BankChallanId], [TokenDetailId], [Fees]) VALUES (77, 26, 321, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[BankChallanDetail] ([BankChallenDetailsId], [BankChallanId], [TokenDetailId], [Fees]) VALUES (78, 27, 322, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[BankChallanDetail] ([BankChallenDetailsId], [BankChallanId], [TokenDetailId], [Fees]) VALUES (79, 27, 323, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[BankChallanDetail] ([BankChallenDetailsId], [BankChallanId], [TokenDetailId], [Fees]) VALUES (80, 27, 324, CAST(30.00 AS Decimal(18, 2)))
INSERT [dbo].[BankChallanDetail] ([BankChallenDetailsId], [BankChallanId], [TokenDetailId], [Fees]) VALUES (81, 27, 325, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[BankChallanDetail] ([BankChallenDetailsId], [BankChallanId], [TokenDetailId], [Fees]) VALUES (82, 27, 326, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[BankChallanDetail] ([BankChallenDetailsId], [BankChallanId], [TokenDetailId], [Fees]) VALUES (83, 27, 327, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[BankChallanDetail] ([BankChallenDetailsId], [BankChallanId], [TokenDetailId], [Fees]) VALUES (84, 32, 328, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[BankChallanDetail] ([BankChallenDetailsId], [BankChallanId], [TokenDetailId], [Fees]) VALUES (85, 33, 329, CAST(30.00 AS Decimal(18, 2)))
INSERT [dbo].[BankChallanDetail] ([BankChallenDetailsId], [BankChallanId], [TokenDetailId], [Fees]) VALUES (86, 33, 330, CAST(30.00 AS Decimal(18, 2)))
INSERT [dbo].[BankChallanDetail] ([BankChallenDetailsId], [BankChallanId], [TokenDetailId], [Fees]) VALUES (87, 33, 331, CAST(30.00 AS Decimal(18, 2)))
SET IDENTITY_INSERT [dbo].[BankChallanDetail] OFF
SET IDENTITY_INSERT [dbo].[CancelStatus] ON 

INSERT [dbo].[CancelStatus] ([CancelStatusId], [CancelStatus]) VALUES (1, N'Pending')
INSERT [dbo].[CancelStatus] ([CancelStatusId], [CancelStatus]) VALUES (2, N'Approved')
INSERT [dbo].[CancelStatus] ([CancelStatusId], [CancelStatus]) VALUES (3, N'Reject')
INSERT [dbo].[CancelStatus] ([CancelStatusId], [CancelStatus]) VALUES (4, N'Refund')
SET IDENTITY_INSERT [dbo].[CancelStatus] OFF
SET IDENTITY_INSERT [dbo].[Category] ON 

INSERT [dbo].[Category] ([CategoryId], [CategoryName], [Narration]) VALUES (2, N'Individual', N'Pass for Individual')
INSERT [dbo].[Category] ([CategoryId], [CategoryName], [Narration]) VALUES (3, N'MotorCycle', N'Commercial Pass')
INSERT [dbo].[Category] ([CategoryId], [CategoryName], [Narration]) VALUES (4, N'Truck', N'10 wheeler pass')
INSERT [dbo].[Category] ([CategoryId], [CategoryName], [Narration]) VALUES (8, N'Car', N'non-commercial pass')
SET IDENTITY_INSERT [dbo].[Category] OFF
SET IDENTITY_INSERT [dbo].[CategoryFees] ON 

INSERT [dbo].[CategoryFees] ([CategoryFeeId], [CategoryId], [Fees], [EffectiveDate], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, 2, CAST(10.00 AS Decimal(18, 2)), CAST(0x0000A81B00000000 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[CategoryFees] ([CategoryFeeId], [CategoryId], [Fees], [EffectiveDate], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, 3, CAST(30.00 AS Decimal(18, 2)), CAST(0x0000A81B00000000 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[CategoryFees] ([CategoryFeeId], [CategoryId], [Fees], [EffectiveDate], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, 4, CAST(20.00 AS Decimal(18, 2)), CAST(0x0000A81B00000000 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[CategoryFees] ([CategoryFeeId], [CategoryId], [Fees], [EffectiveDate], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (10, 8, CAST(10.00 AS Decimal(18, 2)), CAST(0x0000A81B00000000 AS DateTime), NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[CategoryFees] OFF
SET IDENTITY_INSERT [dbo].[Day] ON 

INSERT [dbo].[Day] ([DayId], [StartedBy], [StartDateTime], [EndingDateTime], [IsDayClosed], [FinalAmount], [IsBankChallanGenerated], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (81, 2, CAST(0x0000A83100E5D0B2 AS DateTime), CAST(0x0000A83100E70C6F AS DateTime), 1, CAST(210.00 AS Decimal(18, 2)), 1, 2, CAST(0x0000A83100E5D0B3 AS DateTime), NULL, NULL)
INSERT [dbo].[Day] ([DayId], [StartedBy], [StartDateTime], [EndingDateTime], [IsDayClosed], [FinalAmount], [IsBankChallanGenerated], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (82, 2, CAST(0x0000A83100E75878 AS DateTime), CAST(0x0000A83100E97C30 AS DateTime), 1, CAST(70.00 AS Decimal(18, 2)), 1, 2, CAST(0x0000A83100E75878 AS DateTime), NULL, NULL)
INSERT [dbo].[Day] ([DayId], [StartedBy], [StartDateTime], [EndingDateTime], [IsDayClosed], [FinalAmount], [IsBankChallanGenerated], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (83, 2, CAST(0x0000A83100E9C938 AS DateTime), CAST(0x0000A83100E9CB89 AS DateTime), 1, CAST(0.00 AS Decimal(18, 2)), 1, 2, CAST(0x0000A83100E9C938 AS DateTime), NULL, NULL)
INSERT [dbo].[Day] ([DayId], [StartedBy], [StartDateTime], [EndingDateTime], [IsDayClosed], [FinalAmount], [IsBankChallanGenerated], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (84, 2, CAST(0x0000A83100E9D631 AS DateTime), CAST(0x0000A83100EEC08B AS DateTime), 1, CAST(0.00 AS Decimal(18, 2)), 1, 2, CAST(0x0000A83100E9D631 AS DateTime), NULL, NULL)
INSERT [dbo].[Day] ([DayId], [StartedBy], [StartDateTime], [EndingDateTime], [IsDayClosed], [FinalAmount], [IsBankChallanGenerated], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (85, 2, CAST(0x0000A83100EEC5CA AS DateTime), CAST(0x0000A83100EEFEFB AS DateTime), 1, CAST(0.00 AS Decimal(18, 2)), 1, 2, CAST(0x0000A83100EEC5CA AS DateTime), NULL, NULL)
INSERT [dbo].[Day] ([DayId], [StartedBy], [StartDateTime], [EndingDateTime], [IsDayClosed], [FinalAmount], [IsBankChallanGenerated], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (86, 2, CAST(0x0000A83100EFEDA2 AS DateTime), CAST(0x0000A831010032A6 AS DateTime), 1, CAST(0.00 AS Decimal(18, 2)), 1, 2, CAST(0x0000A83100EFEDA2 AS DateTime), NULL, NULL)
INSERT [dbo].[Day] ([DayId], [StartedBy], [StartDateTime], [EndingDateTime], [IsDayClosed], [FinalAmount], [IsBankChallanGenerated], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (87, 2, CAST(0x0000A83101003643 AS DateTime), CAST(0x0000A8310100B263 AS DateTime), 1, CAST(10.00 AS Decimal(18, 2)), 1, 2, CAST(0x0000A83101003643 AS DateTime), NULL, NULL)
INSERT [dbo].[Day] ([DayId], [StartedBy], [StartDateTime], [EndingDateTime], [IsDayClosed], [FinalAmount], [IsBankChallanGenerated], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (88, 2, CAST(0x0000A8310101E1C9 AS DateTime), CAST(0x0000A83101024C63 AS DateTime), 1, CAST(50.00 AS Decimal(18, 2)), 1, 2, CAST(0x0000A8310101E1C9 AS DateTime), NULL, NULL)
INSERT [dbo].[Day] ([DayId], [StartedBy], [StartDateTime], [EndingDateTime], [IsDayClosed], [FinalAmount], [IsBankChallanGenerated], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (89, 2, CAST(0x0000A8310102B2C7 AS DateTime), CAST(0x0000A8310102B64E AS DateTime), 1, CAST(0.00 AS Decimal(18, 2)), 1, 2, CAST(0x0000A8310102B2C7 AS DateTime), NULL, NULL)
INSERT [dbo].[Day] ([DayId], [StartedBy], [StartDateTime], [EndingDateTime], [IsDayClosed], [FinalAmount], [IsBankChallanGenerated], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (90, 2, CAST(0x0000A8310102C1D3 AS DateTime), CAST(0x0000A8310102CF1D AS DateTime), 1, CAST(0.00 AS Decimal(18, 2)), 1, 2, CAST(0x0000A8310102C1D3 AS DateTime), NULL, NULL)
INSERT [dbo].[Day] ([DayId], [StartedBy], [StartDateTime], [EndingDateTime], [IsDayClosed], [FinalAmount], [IsBankChallanGenerated], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (91, 2, CAST(0x0000A8310102D7AD AS DateTime), CAST(0x0000A8310102F38E AS DateTime), 1, CAST(0.00 AS Decimal(18, 2)), 1, 2, CAST(0x0000A8310102D7AD AS DateTime), NULL, NULL)
INSERT [dbo].[Day] ([DayId], [StartedBy], [StartDateTime], [EndingDateTime], [IsDayClosed], [FinalAmount], [IsBankChallanGenerated], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (92, 2, CAST(0x0000A8310102F7C1 AS DateTime), NULL, 0, CAST(0.00 AS Decimal(18, 2)), 1, 2, CAST(0x0000A8310102F7C1 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[Day] OFF
SET IDENTITY_INSERT [dbo].[Operation] ON 

INSERT [dbo].[Operation] ([OperationId], [ShiftId], [OperationStartedBy], [OperationStartingDateTime], [OperationEndingDateTime], [TotalAmountinOperation], [OpeartionClosed], [CreatedBy], [CreatedDate], [ModifyBy], [ModifiedDate]) VALUES (58, 105, 4, CAST(0x0000A83100E5FA7B AS DateTime), CAST(0x0000A83100E6C7C4 AS DateTime), CAST(60.00 AS Decimal(18, 2)), 1, 4, CAST(0x0000A83100E5FA7B AS DateTime), NULL, NULL)
INSERT [dbo].[Operation] ([OperationId], [ShiftId], [OperationStartedBy], [OperationStartingDateTime], [OperationEndingDateTime], [TotalAmountinOperation], [OpeartionClosed], [CreatedBy], [CreatedDate], [ModifyBy], [ModifiedDate]) VALUES (59, 105, 3, CAST(0x0000A83100E60AAC AS DateTime), NULL, CAST(150.00 AS Decimal(18, 2)), 1, 3, CAST(0x0000A83100E60AAC AS DateTime), NULL, NULL)
INSERT [dbo].[Operation] ([OperationId], [ShiftId], [OperationStartedBy], [OperationStartingDateTime], [OperationEndingDateTime], [TotalAmountinOperation], [OpeartionClosed], [CreatedBy], [CreatedDate], [ModifyBy], [ModifiedDate]) VALUES (60, 106, 3, CAST(0x0000A83100E76167 AS DateTime), NULL, CAST(40.00 AS Decimal(18, 2)), 1, 3, CAST(0x0000A83100E76167 AS DateTime), NULL, NULL)
INSERT [dbo].[Operation] ([OperationId], [ShiftId], [OperationStartedBy], [OperationStartingDateTime], [OperationEndingDateTime], [TotalAmountinOperation], [OpeartionClosed], [CreatedBy], [CreatedDate], [ModifyBy], [ModifiedDate]) VALUES (61, 106, 4, CAST(0x0000A83100E84CB6 AS DateTime), NULL, CAST(0.00 AS Decimal(18, 2)), 1, 4, CAST(0x0000A83100E84CB6 AS DateTime), NULL, NULL)
INSERT [dbo].[Operation] ([OperationId], [ShiftId], [OperationStartedBy], [OperationStartingDateTime], [OperationEndingDateTime], [TotalAmountinOperation], [OpeartionClosed], [CreatedBy], [CreatedDate], [ModifyBy], [ModifiedDate]) VALUES (62, 107, 3, CAST(0x0000A83100E90872 AS DateTime), NULL, CAST(20.00 AS Decimal(18, 2)), 1, 3, CAST(0x0000A83100E90872 AS DateTime), NULL, NULL)
INSERT [dbo].[Operation] ([OperationId], [ShiftId], [OperationStartedBy], [OperationStartingDateTime], [OperationEndingDateTime], [TotalAmountinOperation], [OpeartionClosed], [CreatedBy], [CreatedDate], [ModifyBy], [ModifiedDate]) VALUES (63, 107, 4, CAST(0x0000A83100E93B88 AS DateTime), NULL, CAST(10.00 AS Decimal(18, 2)), 1, 4, CAST(0x0000A83100E93B88 AS DateTime), NULL, NULL)
INSERT [dbo].[Operation] ([OperationId], [ShiftId], [OperationStartedBy], [OperationStartingDateTime], [OperationEndingDateTime], [TotalAmountinOperation], [OpeartionClosed], [CreatedBy], [CreatedDate], [ModifyBy], [ModifiedDate]) VALUES (64, 109, 3, CAST(0x0000A83100E9E973 AS DateTime), NULL, CAST(0.00 AS Decimal(18, 2)), 1, 3, CAST(0x0000A83100E9E973 AS DateTime), NULL, NULL)
INSERT [dbo].[Operation] ([OperationId], [ShiftId], [OperationStartedBy], [OperationStartingDateTime], [OperationEndingDateTime], [TotalAmountinOperation], [OpeartionClosed], [CreatedBy], [CreatedDate], [ModifyBy], [ModifiedDate]) VALUES (65, 110, 3, CAST(0x0000A83100EED187 AS DateTime), NULL, CAST(0.00 AS Decimal(18, 2)), 1, 3, CAST(0x0000A83100EED187 AS DateTime), NULL, NULL)
INSERT [dbo].[Operation] ([OperationId], [ShiftId], [OperationStartedBy], [OperationStartingDateTime], [OperationEndingDateTime], [TotalAmountinOperation], [OpeartionClosed], [CreatedBy], [CreatedDate], [ModifyBy], [ModifiedDate]) VALUES (66, 111, 3, CAST(0x0000A83100EFFC09 AS DateTime), NULL, CAST(0.00 AS Decimal(18, 2)), 1, 3, CAST(0x0000A83100EFFC09 AS DateTime), NULL, NULL)
INSERT [dbo].[Operation] ([OperationId], [ShiftId], [OperationStartedBy], [OperationStartingDateTime], [OperationEndingDateTime], [TotalAmountinOperation], [OpeartionClosed], [CreatedBy], [CreatedDate], [ModifyBy], [ModifiedDate]) VALUES (67, 112, 3, CAST(0x0000A831010043FB AS DateTime), NULL, CAST(10.00 AS Decimal(18, 2)), 1, 3, CAST(0x0000A831010043FB AS DateTime), NULL, NULL)
INSERT [dbo].[Operation] ([OperationId], [ShiftId], [OperationStartedBy], [OperationStartingDateTime], [OperationEndingDateTime], [TotalAmountinOperation], [OpeartionClosed], [CreatedBy], [CreatedDate], [ModifyBy], [ModifiedDate]) VALUES (68, 113, 3, CAST(0x0000A83101020527 AS DateTime), NULL, CAST(50.00 AS Decimal(18, 2)), 1, 3, CAST(0x0000A83101020527 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[Operation] OFF
SET IDENTITY_INSERT [dbo].[PlaceOfVisit] ON 

INSERT [dbo].[PlaceOfVisit] ([PlaceOfVisitId], [PlaceOfVisit]) VALUES (1, N'Abc department')
INSERT [dbo].[PlaceOfVisit] ([PlaceOfVisitId], [PlaceOfVisit]) VALUES (2, N'department 2')
INSERT [dbo].[PlaceOfVisit] ([PlaceOfVisitId], [PlaceOfVisit]) VALUES (3, N'department 3')
INSERT [dbo].[PlaceOfVisit] ([PlaceOfVisitId], [PlaceOfVisit]) VALUES (4, N'department 4')
INSERT [dbo].[PlaceOfVisit] ([PlaceOfVisitId], [PlaceOfVisit]) VALUES (5, N'department 5')
SET IDENTITY_INSERT [dbo].[PlaceOfVisit] OFF
SET IDENTITY_INSERT [dbo].[PurposeOfVisit] ON 

INSERT [dbo].[PurposeOfVisit] ([PurposeOfVisitId], [PurposeOfVIsit]) VALUES (1, N'meeting')
INSERT [dbo].[PurposeOfVisit] ([PurposeOfVisitId], [PurposeOfVIsit]) VALUES (2, N'xxx')
INSERT [dbo].[PurposeOfVisit] ([PurposeOfVisitId], [PurposeOfVIsit]) VALUES (3, N'xxxx')
INSERT [dbo].[PurposeOfVisit] ([PurposeOfVisitId], [PurposeOfVIsit]) VALUES (4, N'asww')
INSERT [dbo].[PurposeOfVisit] ([PurposeOfVisitId], [PurposeOfVIsit]) VALUES (5, N'sadafaf')
SET IDENTITY_INSERT [dbo].[PurposeOfVisit] OFF
SET IDENTITY_INSERT [dbo].[Shift] ON 

INSERT [dbo].[Shift] ([ShiftId], [DayId], [ShiftStartedBy], [ShiftStartingDatetime], [ShiftEndingDatetime], [AmountToBeCollected], [ShiftClosed], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (105, 81, 2, CAST(0x0000A83100E5D0B2 AS DateTime), NULL, CAST(210.00 AS Decimal(18, 2)), 1, 2, CAST(0x0000A83100E5D0B4 AS DateTime), NULL, NULL)
INSERT [dbo].[Shift] ([ShiftId], [DayId], [ShiftStartedBy], [ShiftStartingDatetime], [ShiftEndingDatetime], [AmountToBeCollected], [ShiftClosed], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (106, 82, 2, CAST(0x0000A83100E75878 AS DateTime), NULL, CAST(40.00 AS Decimal(18, 2)), 1, 2, CAST(0x0000A83100E75878 AS DateTime), NULL, NULL)
INSERT [dbo].[Shift] ([ShiftId], [DayId], [ShiftStartedBy], [ShiftStartingDatetime], [ShiftEndingDatetime], [AmountToBeCollected], [ShiftClosed], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (107, 82, 2, CAST(0x0000A83100E8F0AB AS DateTime), NULL, CAST(30.00 AS Decimal(18, 2)), 1, 2, CAST(0x0000A83100E8F0AB AS DateTime), NULL, NULL)
INSERT [dbo].[Shift] ([ShiftId], [DayId], [ShiftStartedBy], [ShiftStartingDatetime], [ShiftEndingDatetime], [AmountToBeCollected], [ShiftClosed], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (108, 83, 2, CAST(0x0000A83100E9C938 AS DateTime), NULL, CAST(0.00 AS Decimal(18, 2)), 1, 2, CAST(0x0000A83100E9C938 AS DateTime), NULL, NULL)
INSERT [dbo].[Shift] ([ShiftId], [DayId], [ShiftStartedBy], [ShiftStartingDatetime], [ShiftEndingDatetime], [AmountToBeCollected], [ShiftClosed], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (109, 84, 2, CAST(0x0000A83100E9D631 AS DateTime), NULL, CAST(0.00 AS Decimal(18, 2)), 1, 2, CAST(0x0000A83100E9D631 AS DateTime), NULL, NULL)
INSERT [dbo].[Shift] ([ShiftId], [DayId], [ShiftStartedBy], [ShiftStartingDatetime], [ShiftEndingDatetime], [AmountToBeCollected], [ShiftClosed], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (110, 85, 2, CAST(0x0000A83100EEC5CA AS DateTime), NULL, CAST(0.00 AS Decimal(18, 2)), 1, 2, CAST(0x0000A83100EEC5CA AS DateTime), NULL, NULL)
INSERT [dbo].[Shift] ([ShiftId], [DayId], [ShiftStartedBy], [ShiftStartingDatetime], [ShiftEndingDatetime], [AmountToBeCollected], [ShiftClosed], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (111, 86, 2, CAST(0x0000A83100EFEDA2 AS DateTime), NULL, CAST(0.00 AS Decimal(18, 2)), 1, 2, CAST(0x0000A83100EFEDA2 AS DateTime), NULL, NULL)
INSERT [dbo].[Shift] ([ShiftId], [DayId], [ShiftStartedBy], [ShiftStartingDatetime], [ShiftEndingDatetime], [AmountToBeCollected], [ShiftClosed], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (112, 87, 2, CAST(0x0000A83101003643 AS DateTime), NULL, CAST(10.00 AS Decimal(18, 2)), 1, 2, CAST(0x0000A83101003643 AS DateTime), NULL, NULL)
INSERT [dbo].[Shift] ([ShiftId], [DayId], [ShiftStartedBy], [ShiftStartingDatetime], [ShiftEndingDatetime], [AmountToBeCollected], [ShiftClosed], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (113, 88, 2, CAST(0x0000A8310101E1C9 AS DateTime), NULL, CAST(50.00 AS Decimal(18, 2)), 1, 2, CAST(0x0000A8310101E1CB AS DateTime), NULL, NULL)
INSERT [dbo].[Shift] ([ShiftId], [DayId], [ShiftStartedBy], [ShiftStartingDatetime], [ShiftEndingDatetime], [AmountToBeCollected], [ShiftClosed], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (114, 89, 2, CAST(0x0000A8310102B2C7 AS DateTime), NULL, CAST(0.00 AS Decimal(18, 2)), 1, 2, CAST(0x0000A8310102B2C7 AS DateTime), NULL, NULL)
INSERT [dbo].[Shift] ([ShiftId], [DayId], [ShiftStartedBy], [ShiftStartingDatetime], [ShiftEndingDatetime], [AmountToBeCollected], [ShiftClosed], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (115, 90, 2, CAST(0x0000A8310102C1D3 AS DateTime), NULL, CAST(0.00 AS Decimal(18, 2)), 1, 2, CAST(0x0000A8310102C1D3 AS DateTime), NULL, NULL)
INSERT [dbo].[Shift] ([ShiftId], [DayId], [ShiftStartedBy], [ShiftStartingDatetime], [ShiftEndingDatetime], [AmountToBeCollected], [ShiftClosed], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (116, 91, 2, CAST(0x0000A8310102D7AD AS DateTime), NULL, CAST(0.00 AS Decimal(18, 2)), 1, 2, CAST(0x0000A8310102D7AD AS DateTime), NULL, NULL)
INSERT [dbo].[Shift] ([ShiftId], [DayId], [ShiftStartedBy], [ShiftStartingDatetime], [ShiftEndingDatetime], [AmountToBeCollected], [ShiftClosed], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (117, 92, 2, CAST(0x0000A8310102F7C1 AS DateTime), NULL, NULL, 0, 2, CAST(0x0000A8310102F7C1 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[Shift] OFF
SET IDENTITY_INSERT [dbo].[ShiftDetails] ON 

INSERT [dbo].[ShiftDetails] ([ShiftDetailId], [ShiftId], [UserLoggedIn], [UserLoggedInDateTime], [UserLoggedOut], [UserLoggedOutDateTime], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (147, 105, 4, CAST(0x0000A83100E5F5E7 AS DateTime), NULL, NULL, CAST(0x0000A83100E5F5E7 AS DateTime), 4, NULL, NULL)
INSERT [dbo].[ShiftDetails] ([ShiftDetailId], [ShiftId], [UserLoggedIn], [UserLoggedInDateTime], [UserLoggedOut], [UserLoggedOutDateTime], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (148, 105, 3, CAST(0x0000A83100E60905 AS DateTime), NULL, NULL, CAST(0x0000A83100E60905 AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ShiftDetails] ([ShiftDetailId], [ShiftId], [UserLoggedIn], [UserLoggedInDateTime], [UserLoggedOut], [UserLoggedOutDateTime], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (149, 106, 3, CAST(0x0000A83100E75FC6 AS DateTime), NULL, NULL, CAST(0x0000A83100E75FC6 AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ShiftDetails] ([ShiftDetailId], [ShiftId], [UserLoggedIn], [UserLoggedInDateTime], [UserLoggedOut], [UserLoggedOutDateTime], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (150, 106, 4, CAST(0x0000A83100E84B1E AS DateTime), NULL, NULL, CAST(0x0000A83100E84B1E AS DateTime), 4, NULL, NULL)
INSERT [dbo].[ShiftDetails] ([ShiftDetailId], [ShiftId], [UserLoggedIn], [UserLoggedInDateTime], [UserLoggedOut], [UserLoggedOutDateTime], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (151, 106, 3, CAST(0x0000A83100E85B0E AS DateTime), NULL, NULL, CAST(0x0000A83100E85B0E AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ShiftDetails] ([ShiftDetailId], [ShiftId], [UserLoggedIn], [UserLoggedInDateTime], [UserLoggedOut], [UserLoggedOutDateTime], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (152, 107, 3, CAST(0x0000A83100E906D9 AS DateTime), NULL, NULL, CAST(0x0000A83100E906D9 AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ShiftDetails] ([ShiftDetailId], [ShiftId], [UserLoggedIn], [UserLoggedInDateTime], [UserLoggedOut], [UserLoggedOutDateTime], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (153, 109, 3, CAST(0x0000A83100E9E771 AS DateTime), NULL, NULL, CAST(0x0000A83100E9E771 AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ShiftDetails] ([ShiftDetailId], [ShiftId], [UserLoggedIn], [UserLoggedInDateTime], [UserLoggedOut], [UserLoggedOutDateTime], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (154, 110, 3, CAST(0x0000A83100EECFC7 AS DateTime), NULL, NULL, CAST(0x0000A83100EECFC7 AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ShiftDetails] ([ShiftDetailId], [ShiftId], [UserLoggedIn], [UserLoggedInDateTime], [UserLoggedOut], [UserLoggedOutDateTime], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (155, 111, 3, CAST(0x0000A83100EFF64E AS DateTime), NULL, NULL, CAST(0x0000A83100EFF64E AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ShiftDetails] ([ShiftDetailId], [ShiftId], [UserLoggedIn], [UserLoggedInDateTime], [UserLoggedOut], [UserLoggedOutDateTime], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (156, 112, 3, CAST(0x0000A8310100426F AS DateTime), NULL, NULL, CAST(0x0000A8310100426F AS DateTime), 3, NULL, NULL)
INSERT [dbo].[ShiftDetails] ([ShiftDetailId], [ShiftId], [UserLoggedIn], [UserLoggedInDateTime], [UserLoggedOut], [UserLoggedOutDateTime], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (157, 113, 3, CAST(0x0000A8310101EE05 AS DateTime), NULL, NULL, CAST(0x0000A8310101EE05 AS DateTime), 3, NULL, NULL)
SET IDENTITY_INSERT [dbo].[ShiftDetails] OFF
SET IDENTITY_INSERT [dbo].[SystemConfig] ON 

INSERT [dbo].[SystemConfig] ([SystemConfigId], [SystemConfigKey], [SystemConfigValue]) VALUES (1, N'tokenCancellationTimeOut', N'60')
SET IDENTITY_INSERT [dbo].[SystemConfig] OFF
SET IDENTITY_INSERT [dbo].[Token] ON 

INSERT [dbo].[Token] ([TokenId], [OperationId], [IssueDateTime], [VehiclePlateNo], [CategoryFeesId], [TokenNo], [PurposeOfVisit], [PlaceOfVisitId], [Noofpassengers], [IsNicAvailable], [IsClosed], [closedDatetime], [GoAheadReasonWithoutNIC], [DatetimeCanceled], [Reason], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifedBy]) VALUES (154, 59, CAST(0x0000A83100E6443E AS DateTime), N'2343253', 3, N'2022499-0', 1, 4, 4, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Token] ([TokenId], [OperationId], [IssueDateTime], [VehiclePlateNo], [CategoryFeesId], [TokenNo], [PurposeOfVisit], [PlaceOfVisitId], [Noofpassengers], [IsNicAvailable], [IsClosed], [closedDatetime], [GoAheadReasonWithoutNIC], [DatetimeCanceled], [Reason], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifedBy]) VALUES (155, 59, CAST(0x0000A83100E64ECF AS DateTime), N'2343253', 1, N'2031517-0', 1, 5, 1, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Token] ([TokenId], [OperationId], [IssueDateTime], [VehiclePlateNo], [CategoryFeesId], [TokenNo], [PurposeOfVisit], [PlaceOfVisitId], [Noofpassengers], [IsNicAvailable], [IsClosed], [closedDatetime], [GoAheadReasonWithoutNIC], [DatetimeCanceled], [Reason], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifedBy]) VALUES (156, 59, CAST(0x0000A83100E66574 AS DateTime), N'2343253', 3, N'2050840-0', 3, 1, 4, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Token] ([TokenId], [OperationId], [IssueDateTime], [VehiclePlateNo], [CategoryFeesId], [TokenNo], [PurposeOfVisit], [PlaceOfVisitId], [Noofpassengers], [IsNicAvailable], [IsClosed], [closedDatetime], [GoAheadReasonWithoutNIC], [DatetimeCanceled], [Reason], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifedBy]) VALUES (157, 59, CAST(0x0000A83100E69374 AS DateTime), N'2343253', 10, N'203092-0', 2, 3, 4, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Token] ([TokenId], [OperationId], [IssueDateTime], [VehiclePlateNo], [CategoryFeesId], [TokenNo], [PurposeOfVisit], [PlaceOfVisitId], [Noofpassengers], [IsNicAvailable], [IsClosed], [closedDatetime], [GoAheadReasonWithoutNIC], [DatetimeCanceled], [Reason], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifedBy]) VALUES (158, 58, CAST(0x0000A83100E6AF92 AS DateTime), N'23532415132', 2, N'205486-0', 2, 4, 3, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Token] ([TokenId], [OperationId], [IssueDateTime], [VehiclePlateNo], [CategoryFeesId], [TokenNo], [PurposeOfVisit], [PlaceOfVisitId], [Noofpassengers], [IsNicAvailable], [IsClosed], [closedDatetime], [GoAheadReasonWithoutNIC], [DatetimeCanceled], [Reason], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifedBy]) VALUES (159, 58, CAST(0x0000A83100E6BA1E AS DateTime), N'23532415132', 1, N'20384-0', 2, 4, 1, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Token] ([TokenId], [OperationId], [IssueDateTime], [VehiclePlateNo], [CategoryFeesId], [TokenNo], [PurposeOfVisit], [PlaceOfVisitId], [Noofpassengers], [IsNicAvailable], [IsClosed], [closedDatetime], [GoAheadReasonWithoutNIC], [DatetimeCanceled], [Reason], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifedBy]) VALUES (160, 60, CAST(0x0000A83100E88710 AS DateTime), NULL, 1, N'2036424-0', 2, 5, 1, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Token] ([TokenId], [OperationId], [IssueDateTime], [VehiclePlateNo], [CategoryFeesId], [TokenNo], [PurposeOfVisit], [PlaceOfVisitId], [Noofpassengers], [IsNicAvailable], [IsClosed], [closedDatetime], [GoAheadReasonWithoutNIC], [DatetimeCanceled], [Reason], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifedBy]) VALUES (161, 60, CAST(0x0000A83100E89433 AS DateTime), NULL, 1, N'2047637-0', 1, 5, 1, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Token] ([TokenId], [OperationId], [IssueDateTime], [VehiclePlateNo], [CategoryFeesId], [TokenNo], [PurposeOfVisit], [PlaceOfVisitId], [Noofpassengers], [IsNicAvailable], [IsClosed], [closedDatetime], [GoAheadReasonWithoutNIC], [DatetimeCanceled], [Reason], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifedBy]) VALUES (162, 60, CAST(0x0000A83100E8B7A5 AS DateTime), NULL, 2, N'2017882-0', 1, 5, 1, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Token] ([TokenId], [OperationId], [IssueDateTime], [VehiclePlateNo], [CategoryFeesId], [TokenNo], [PurposeOfVisit], [PlaceOfVisitId], [Noofpassengers], [IsNicAvailable], [IsClosed], [closedDatetime], [GoAheadReasonWithoutNIC], [DatetimeCanceled], [Reason], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifedBy]) VALUES (163, 62, CAST(0x0000A83100E9137A AS DateTime), NULL, 1, N'2036246-0', 1, 5, 1, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Token] ([TokenId], [OperationId], [IssueDateTime], [VehiclePlateNo], [CategoryFeesId], [TokenNo], [PurposeOfVisit], [PlaceOfVisitId], [Noofpassengers], [IsNicAvailable], [IsClosed], [closedDatetime], [GoAheadReasonWithoutNIC], [DatetimeCanceled], [Reason], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifedBy]) VALUES (164, 62, CAST(0x0000A83100E91EFD AS DateTime), NULL, 1, N'204668-0', 5, 3, 1, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Token] ([TokenId], [OperationId], [IssueDateTime], [VehiclePlateNo], [CategoryFeesId], [TokenNo], [PurposeOfVisit], [PlaceOfVisitId], [Noofpassengers], [IsNicAvailable], [IsClosed], [closedDatetime], [GoAheadReasonWithoutNIC], [DatetimeCanceled], [Reason], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifedBy]) VALUES (165, 63, CAST(0x0000A83100E95394 AS DateTime), NULL, 1, N'2030944-0', 1, 2, 1, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Token] ([TokenId], [OperationId], [IssueDateTime], [VehiclePlateNo], [CategoryFeesId], [TokenNo], [PurposeOfVisit], [PlaceOfVisitId], [Noofpassengers], [IsNicAvailable], [IsClosed], [closedDatetime], [GoAheadReasonWithoutNIC], [DatetimeCanceled], [Reason], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifedBy]) VALUES (166, 67, CAST(0x0000A83101004D12 AS DateTime), NULL, 1, N'209820-0', 1, 5, 1, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Token] ([TokenId], [OperationId], [IssueDateTime], [VehiclePlateNo], [CategoryFeesId], [TokenNo], [PurposeOfVisit], [PlaceOfVisitId], [Noofpassengers], [IsNicAvailable], [IsClosed], [closedDatetime], [GoAheadReasonWithoutNIC], [DatetimeCanceled], [Reason], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifedBy]) VALUES (167, 68, CAST(0x0000A8310102165C AS DateTime), N'safwqr', 2, N'204041-0', 1, 5, 3, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Token] OFF
SET IDENTITY_INSERT [dbo].[TokenCancelledRequestDetails] ON 

INSERT [dbo].[TokenCancelledRequestDetails] ([TokenCancelledRequestDetail], [TokenDetailId], [TokenCancelRequestId]) VALUES (32, 320, 25)
SET IDENTITY_INSERT [dbo].[TokenCancelledRequestDetails] OFF
SET IDENTITY_INSERT [dbo].[TokenCancelRequest] ON 

INSERT [dbo].[TokenCancelRequest] ([TokenCancelRequestId], [TokenId], [NoofTickets], [CategoryId], [RefundedAmount], [DateTimeCancelled], [Reason], [CancelStatusId], [CreatedBy], [CreatedDate], [ModifedBy], [ModifiedDate]) VALUES (25, 158, 1, 3, CAST(10.00 AS Decimal(18, 2)), NULL, NULL, 4, 3, CAST(0x0000A83100E7DEA2 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[TokenCancelRequest] OFF
SET IDENTITY_INSERT [dbo].[TokenDetails] ON 

INSERT [dbo].[TokenDetails] ([TokenDetailsId], [TokenId], [TokenNo], [Name], [NIC], [IsDriver], [isCancel], [Fees]) VALUES (305, 154, N'2022499-0', NULL, NULL, 1, 0, CAST(20.00 AS Decimal(18, 2)))
INSERT [dbo].[TokenDetails] ([TokenDetailsId], [TokenId], [TokenNo], [Name], [NIC], [IsDriver], [isCancel], [Fees]) VALUES (306, 154, N'2022499-1', NULL, NULL, 0, 0, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[TokenDetails] ([TokenDetailsId], [TokenId], [TokenNo], [Name], [NIC], [IsDriver], [isCancel], [Fees]) VALUES (307, 154, N'2022499-2', N'fsaf', N'34654546', 0, 0, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[TokenDetails] ([TokenDetailsId], [TokenId], [TokenNo], [Name], [NIC], [IsDriver], [isCancel], [Fees]) VALUES (308, 154, N'2022499-3', N'sdagfsadg', N'745', 0, 0, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[TokenDetails] ([TokenDetailsId], [TokenId], [TokenNo], [Name], [NIC], [IsDriver], [isCancel], [Fees]) VALUES (309, 155, N'2031517-0', N'safsd', N'sdadfsgg', 1, 0, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[TokenDetails] ([TokenDetailsId], [TokenId], [TokenNo], [Name], [NIC], [IsDriver], [isCancel], [Fees]) VALUES (310, 156, N'2050840-0', NULL, NULL, 1, 0, CAST(20.00 AS Decimal(18, 2)))
INSERT [dbo].[TokenDetails] ([TokenDetailsId], [TokenId], [TokenNo], [Name], [NIC], [IsDriver], [isCancel], [Fees]) VALUES (311, 156, N'2050840-1', NULL, NULL, 0, 0, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[TokenDetails] ([TokenDetailsId], [TokenId], [TokenNo], [Name], [NIC], [IsDriver], [isCancel], [Fees]) VALUES (312, 156, N'2050840-2', N'gdsaf', N'346', 0, 0, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[TokenDetails] ([TokenDetailsId], [TokenId], [TokenNo], [Name], [NIC], [IsDriver], [isCancel], [Fees]) VALUES (313, 156, N'2050840-3', N'gdfag', N'4567457', 0, 0, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[TokenDetails] ([TokenDetailsId], [TokenId], [TokenNo], [Name], [NIC], [IsDriver], [isCancel], [Fees]) VALUES (314, 157, N'203092-0', NULL, NULL, 1, 0, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[TokenDetails] ([TokenDetailsId], [TokenId], [TokenNo], [Name], [NIC], [IsDriver], [isCancel], [Fees]) VALUES (315, 157, N'203092-1', NULL, NULL, 0, 0, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[TokenDetails] ([TokenDetailsId], [TokenId], [TokenNo], [Name], [NIC], [IsDriver], [isCancel], [Fees]) VALUES (316, 157, N'203092-2', N'fsa', N'645645', 0, 0, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[TokenDetails] ([TokenDetailsId], [TokenId], [TokenNo], [Name], [NIC], [IsDriver], [isCancel], [Fees]) VALUES (317, 157, N'203092-3', N'dfsdafsdf', N'6456', 0, 0, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[TokenDetails] ([TokenDetailsId], [TokenId], [TokenNo], [Name], [NIC], [IsDriver], [isCancel], [Fees]) VALUES (318, 158, N'205486-0', NULL, NULL, 1, 0, CAST(30.00 AS Decimal(18, 2)))
INSERT [dbo].[TokenDetails] ([TokenDetailsId], [TokenId], [TokenNo], [Name], [NIC], [IsDriver], [isCancel], [Fees]) VALUES (319, 158, N'205486-1', NULL, NULL, 0, 0, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[TokenDetails] ([TokenDetailsId], [TokenId], [TokenNo], [Name], [NIC], [IsDriver], [isCancel], [Fees]) VALUES (320, 158, N'205486-2', N'safsadsd', N'2345341', 0, 1, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[TokenDetails] ([TokenDetailsId], [TokenId], [TokenNo], [Name], [NIC], [IsDriver], [isCancel], [Fees]) VALUES (321, 159, N'20384-0', N'sdcsdf', N'2114', 1, 0, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[TokenDetails] ([TokenDetailsId], [TokenId], [TokenNo], [Name], [NIC], [IsDriver], [isCancel], [Fees]) VALUES (322, 160, N'2036424-0', N'asfda', N'sfasfsaf', 1, 0, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[TokenDetails] ([TokenDetailsId], [TokenId], [TokenNo], [Name], [NIC], [IsDriver], [isCancel], [Fees]) VALUES (323, 161, N'2047637-0', N'asfsaf', N'dsfweew', 1, 0, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[TokenDetails] ([TokenDetailsId], [TokenId], [TokenNo], [Name], [NIC], [IsDriver], [isCancel], [Fees]) VALUES (324, 162, N'2017882-0', NULL, NULL, 1, 0, CAST(30.00 AS Decimal(18, 2)))
INSERT [dbo].[TokenDetails] ([TokenDetailsId], [TokenId], [TokenNo], [Name], [NIC], [IsDriver], [isCancel], [Fees]) VALUES (325, 163, N'2036246-0', N'aDad', N'2e4234', 1, 0, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[TokenDetails] ([TokenDetailsId], [TokenId], [TokenNo], [Name], [NIC], [IsDriver], [isCancel], [Fees]) VALUES (326, 164, N'204668-0', NULL, NULL, 1, 0, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[TokenDetails] ([TokenDetailsId], [TokenId], [TokenNo], [Name], [NIC], [IsDriver], [isCancel], [Fees]) VALUES (327, 165, N'2030944-0', N'sadsaf', N'asfsaf', 1, 0, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[TokenDetails] ([TokenDetailsId], [TokenId], [TokenNo], [Name], [NIC], [IsDriver], [isCancel], [Fees]) VALUES (328, 166, N'209820-0', N'SADFAF', N'ASFASF', 1, 0, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[TokenDetails] ([TokenDetailsId], [TokenId], [TokenNo], [Name], [NIC], [IsDriver], [isCancel], [Fees]) VALUES (329, 167, N'204041-0', NULL, NULL, 1, 0, CAST(30.00 AS Decimal(18, 2)))
INSERT [dbo].[TokenDetails] ([TokenDetailsId], [TokenId], [TokenNo], [Name], [NIC], [IsDriver], [isCancel], [Fees]) VALUES (330, 167, N'204041-1', NULL, NULL, 0, 0, CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[TokenDetails] ([TokenDetailsId], [TokenId], [TokenNo], [Name], [NIC], [IsDriver], [isCancel], [Fees]) VALUES (331, 167, N'204041-2', N'asfawfa', N'235', 0, 0, CAST(10.00 AS Decimal(18, 2)))
SET IDENTITY_INSERT [dbo].[TokenDetails] OFF
SET IDENTITY_INSERT [dbo].[TokenTransaction] ON 

INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (126, 305, 59, CAST(20.00 AS Decimal(18, 2)), 1, 3, CAST(0x0000A83100E64443 AS DateTime))
INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (127, 306, 59, CAST(10.00 AS Decimal(18, 2)), 1, 3, CAST(0x0000A83100E64443 AS DateTime))
INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (128, 307, 59, CAST(10.00 AS Decimal(18, 2)), 1, 3, CAST(0x0000A83100E64443 AS DateTime))
INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (129, 308, 59, CAST(10.00 AS Decimal(18, 2)), 1, 3, CAST(0x0000A83100E64444 AS DateTime))
INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (130, 309, 59, CAST(10.00 AS Decimal(18, 2)), 1, 3, CAST(0x0000A83100E64ED0 AS DateTime))
INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (131, 310, 59, CAST(20.00 AS Decimal(18, 2)), 1, 3, CAST(0x0000A83100E66574 AS DateTime))
INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (132, 311, 59, CAST(10.00 AS Decimal(18, 2)), 1, 3, CAST(0x0000A83100E66574 AS DateTime))
INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (133, 312, 59, CAST(10.00 AS Decimal(18, 2)), 1, 3, CAST(0x0000A83100E66575 AS DateTime))
INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (134, 313, 59, CAST(10.00 AS Decimal(18, 2)), 1, 3, CAST(0x0000A83100E66575 AS DateTime))
INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (135, 314, 59, CAST(10.00 AS Decimal(18, 2)), 1, 3, CAST(0x0000A83100E69374 AS DateTime))
INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (136, 315, 59, CAST(10.00 AS Decimal(18, 2)), 1, 3, CAST(0x0000A83100E69374 AS DateTime))
INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (137, 316, 59, CAST(10.00 AS Decimal(18, 2)), 1, 3, CAST(0x0000A83100E69375 AS DateTime))
INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (138, 317, 59, CAST(10.00 AS Decimal(18, 2)), 1, 3, CAST(0x0000A83100E69375 AS DateTime))
INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (139, 318, 58, CAST(30.00 AS Decimal(18, 2)), 1, 4, CAST(0x0000A83100E6AF92 AS DateTime))
INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (140, 319, 58, CAST(10.00 AS Decimal(18, 2)), 1, 4, CAST(0x0000A83100E6AF92 AS DateTime))
INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (141, 320, 58, CAST(10.00 AS Decimal(18, 2)), 1, 4, CAST(0x0000A83100E6AF92 AS DateTime))
INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (142, 321, 58, CAST(10.00 AS Decimal(18, 2)), 1, 4, CAST(0x0000A83100E6BA1E AS DateTime))
INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (143, 320, 60, CAST(10.00 AS Decimal(18, 2)), 0, 3, CAST(0x0000A83100E863B2 AS DateTime))
INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (144, 322, 60, CAST(10.00 AS Decimal(18, 2)), 1, 3, CAST(0x0000A83100E88710 AS DateTime))
INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (145, 323, 60, CAST(10.00 AS Decimal(18, 2)), 1, 3, CAST(0x0000A83100E89434 AS DateTime))
INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (146, 324, 60, CAST(30.00 AS Decimal(18, 2)), 1, 3, CAST(0x0000A83100E8B7A5 AS DateTime))
INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (147, 325, 62, CAST(10.00 AS Decimal(18, 2)), 1, 3, CAST(0x0000A83100E9137A AS DateTime))
INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (148, 326, 62, CAST(10.00 AS Decimal(18, 2)), 1, 3, CAST(0x0000A83100E91EFD AS DateTime))
INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (149, 327, 63, CAST(10.00 AS Decimal(18, 2)), 1, 4, CAST(0x0000A83100E95394 AS DateTime))
INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (150, 328, 67, CAST(10.00 AS Decimal(18, 2)), 1, 3, CAST(0x0000A83101004D14 AS DateTime))
INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (151, 329, 68, CAST(30.00 AS Decimal(18, 2)), 1, 3, CAST(0x0000A83101021661 AS DateTime))
INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (152, 330, 68, CAST(10.00 AS Decimal(18, 2)), 1, 3, CAST(0x0000A83101021662 AS DateTime))
INSERT [dbo].[TokenTransaction] ([TokenTransactionID], [TokendetailId], [OperationId], [Amount], [isPayIn], [CreatedBy], [CreatedDate]) VALUES (153, 331, 68, CAST(10.00 AS Decimal(18, 2)), 1, 3, CAST(0x0000A83101021662 AS DateTime))
SET IDENTITY_INSERT [dbo].[TokenTransaction] OFF
ALTER TABLE [dbo].[AppEmployees] ADD  CONSTRAINT [DF_UserProfiles_UserProfileID]  DEFAULT (newid()) FOR [EmployeeGUID]
GO
ALTER TABLE [dbo].[AppModuleConfigs] ADD  CONSTRAINT [DF_ModuleConfigs_ConfigModuleID]  DEFAULT ((0)) FOR [ModuleID]
GO
ALTER TABLE [dbo].[AppModuleConfigs] ADD  CONSTRAINT [DF_AppModuleConfigs_isLock]  DEFAULT ((0)) FOR [isLock]
GO
ALTER TABLE [dbo].[AppModulePermissions] ADD  CONSTRAINT [DF_AppModuleMeta_isAvailable]  DEFAULT ((0)) FOR [isAvailable]
GO
ALTER TABLE [dbo].[AppModules] ADD  CONSTRAINT [DF_Modules_ShowInMenu]  DEFAULT ((0)) FOR [ShowInMenu]
GO
ALTER TABLE [dbo].[AppModules] ADD  CONSTRAINT [DF_AppModules_isPrimary]  DEFAULT ((0)) FOR [isPrimary]
GO
ALTER TABLE [dbo].[AppModules] ADD  CONSTRAINT [DF_Modules_Sort]  DEFAULT ((0)) FOR [Sort]
GO
ALTER TABLE [dbo].[AppModules] ADD  CONSTRAINT [DF_Modules_IsDisable]  DEFAULT ((0)) FOR [isDisable]
GO
ALTER TABLE [dbo].[AppModules] ADD  CONSTRAINT [DF_AppModules_HasFullAccess]  DEFAULT ((0)) FOR [HasFullAccess]
GO
ALTER TABLE [dbo].[AppModules] ADD  CONSTRAINT [DF_AppModules_HasCreate]  DEFAULT ((0)) FOR [HasCreate]
GO
ALTER TABLE [dbo].[AppModules] ADD  CONSTRAINT [DF_AppModules_HasEdit]  DEFAULT ((0)) FOR [HasEdit]
GO
ALTER TABLE [dbo].[AppModules] ADD  CONSTRAINT [DF_AppModules_HasView]  DEFAULT ((0)) FOR [HasView]
GO
ALTER TABLE [dbo].[AppModules] ADD  CONSTRAINT [DF_AppModules_HasDetail]  DEFAULT ((0)) FOR [HasDetail]
GO
ALTER TABLE [dbo].[AppRoles] ADD  CONSTRAINT [DF_Group_Priority]  DEFAULT ((1)) FOR [Priority]
GO
ALTER TABLE [dbo].[AppRoles] ADD  CONSTRAINT [DF_AppGroups_isLock]  DEFAULT ((1)) FOR [isEditable]
GO
ALTER TABLE [dbo].[AppRoles] ADD  CONSTRAINT [DF_Group_isDisable]  DEFAULT ((0)) FOR [isDisable]
GO
ALTER TABLE [dbo].[AppRolesModules] ADD  CONSTRAINT [DF_GroupModules_IsAllowed]  DEFAULT ((0)) FOR [IsAllowed]
GO
ALTER TABLE [dbo].[AppUserLogs] ADD  CONSTRAINT [DF_AppUserLogs_LoginDateTime]  DEFAULT (getdate()) FOR [LoginDateTime]
GO
ALTER TABLE [dbo].[AppUserMeta] ADD  CONSTRAINT [DF_AppUserMeta_isInternal]  DEFAULT ((0)) FOR [isInternal]
GO
ALTER TABLE [dbo].[AppUserPwdRequest] ADD  CONSTRAINT [DF_AppUserPwdRequest_isGenerated]  DEFAULT ((0)) FOR [isGenerated]
GO
ALTER TABLE [dbo].[AppUsers] ADD  CONSTRAINT [DF_Users_Userkey]  DEFAULT (newid()) FOR [UserGUID]
GO
ALTER TABLE [dbo].[AppUsers] ADD  CONSTRAINT [DF_Users_isEmployee]  DEFAULT ((1)) FOR [isEmployee]
GO
ALTER TABLE [dbo].[AppUsers] ADD  CONSTRAINT [DF_Users_LoginCount]  DEFAULT ((0)) FOR [LoginCount]
GO
ALTER TABLE [dbo].[AppUsers] ADD  CONSTRAINT [DF_AppUsers_isDisable1]  DEFAULT ((0)) FOR [isActive]
GO
ALTER TABLE [dbo].[AppUsers] ADD  CONSTRAINT [DF_Users_isActive]  DEFAULT ((0)) FOR [isDisable]
GO
ALTER TABLE [dbo].[BankChallan] ADD  CONSTRAINT [DF_BankChallan_isChallanSubmitedtoBank]  DEFAULT ((0)) FOR [isChallanSubmitedtoBank]
GO
ALTER TABLE [dbo].[Day] ADD  CONSTRAINT [DF_Day_DayClosed]  DEFAULT ((0)) FOR [IsDayClosed]
GO
ALTER TABLE [dbo].[Day] ADD  CONSTRAINT [DF_Day_IsBankChallanGenerated]  DEFAULT ((0)) FOR [IsBankChallanGenerated]
GO
ALTER TABLE [dbo].[Operation] ADD  CONSTRAINT [DF_Operation_OpeartionClosed]  DEFAULT ((0)) FOR [OpeartionClosed]
GO
ALTER TABLE [dbo].[Shift] ADD  CONSTRAINT [DF_Shift_ShiftClosed]  DEFAULT ((0)) FOR [ShiftClosed]
GO
ALTER TABLE [dbo].[Token] ADD  CONSTRAINT [DF_Token_IsNicAvailable]  DEFAULT ((0)) FOR [IsNicAvailable]
GO
ALTER TABLE [dbo].[TokenCancelRequest] ADD  CONSTRAINT [DF_TokenCancelRequest_IsApproved]  DEFAULT ((0)) FOR [CancelStatusId]
GO
ALTER TABLE [dbo].[TokenDetails] ADD  CONSTRAINT [DF_TokenDetails_isCancel]  DEFAULT ((0)) FOR [isCancel]
GO
ALTER TABLE [dbo].[TokenTransaction] ADD  CONSTRAINT [DF_TokenTransaction_isPayIn]  DEFAULT ((0)) FOR [isPayIn]
GO
ALTER TABLE [dbo].[AppEmployeeAgencies]  WITH CHECK ADD  CONSTRAINT [FK_AppEmployeeAgencies_AppEmployees] FOREIGN KEY([EmployeeID])
REFERENCES [dbo].[AppEmployees] ([EmployeeID])
GO
ALTER TABLE [dbo].[AppEmployeeAgencies] CHECK CONSTRAINT [FK_AppEmployeeAgencies_AppEmployees]
GO
ALTER TABLE [dbo].[AppEmployeeDepartments]  WITH CHECK ADD  CONSTRAINT [FK_AppEmployeeDepartments_AppEmployees] FOREIGN KEY([EmployeeID])
REFERENCES [dbo].[AppEmployees] ([EmployeeID])
GO
ALTER TABLE [dbo].[AppEmployeeDepartments] CHECK CONSTRAINT [FK_AppEmployeeDepartments_AppEmployees]
GO
ALTER TABLE [dbo].[AppEmployees]  WITH CHECK ADD  CONSTRAINT [FK_Employees_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[AppUsers] ([UserId])
GO
ALTER TABLE [dbo].[AppEmployees] CHECK CONSTRAINT [FK_Employees_Users]
GO
ALTER TABLE [dbo].[AppEmployeeServices]  WITH CHECK ADD  CONSTRAINT [FK_AppEmployeeServices_AppEmployees] FOREIGN KEY([EmployeeID])
REFERENCES [dbo].[AppEmployees] ([EmployeeID])
GO
ALTER TABLE [dbo].[AppEmployeeServices] CHECK CONSTRAINT [FK_AppEmployeeServices_AppEmployees]
GO
ALTER TABLE [dbo].[AppModuleConfigs]  WITH CHECK ADD  CONSTRAINT [FK_ModuleConfigs_Modules] FOREIGN KEY([ModuleID])
REFERENCES [dbo].[AppModules] ([ModuleID])
GO
ALTER TABLE [dbo].[AppModuleConfigs] CHECK CONSTRAINT [FK_ModuleConfigs_Modules]
GO
ALTER TABLE [dbo].[AppModulePermissions]  WITH CHECK ADD  CONSTRAINT [FK_AppModuleMeta_AppModules] FOREIGN KEY([ModuleID])
REFERENCES [dbo].[AppModules] ([ModuleID])
GO
ALTER TABLE [dbo].[AppModulePermissions] CHECK CONSTRAINT [FK_AppModuleMeta_AppModules]
GO
ALTER TABLE [dbo].[AppRolesModules]  WITH CHECK ADD  CONSTRAINT [FK_GroupFeature_Features] FOREIGN KEY([ModuleId])
REFERENCES [dbo].[AppModules] ([ModuleID])
GO
ALTER TABLE [dbo].[AppRolesModules] CHECK CONSTRAINT [FK_GroupFeature_Features]
GO
ALTER TABLE [dbo].[AppRolesModules]  WITH CHECK ADD  CONSTRAINT [FK_GroupFeature_Group] FOREIGN KEY([RolesId])
REFERENCES [dbo].[AppRoles] ([RoleId])
GO
ALTER TABLE [dbo].[AppRolesModules] CHECK CONSTRAINT [FK_GroupFeature_Group]
GO
ALTER TABLE [dbo].[AppUserLogs]  WITH CHECK ADD  CONSTRAINT [FK_AppUserLogs_AppUsers] FOREIGN KEY([UserId])
REFERENCES [dbo].[AppUsers] ([UserId])
GO
ALTER TABLE [dbo].[AppUserLogs] CHECK CONSTRAINT [FK_AppUserLogs_AppUsers]
GO
ALTER TABLE [dbo].[AppUserMeta]  WITH CHECK ADD  CONSTRAINT [FK_AppUserMeta_AppUsers] FOREIGN KEY([UserId])
REFERENCES [dbo].[AppUsers] ([UserId])
GO
ALTER TABLE [dbo].[AppUserMeta] CHECK CONSTRAINT [FK_AppUserMeta_AppUsers]
GO
ALTER TABLE [dbo].[AppUserPwdRequest]  WITH CHECK ADD  CONSTRAINT [FK_AppUserPwdRequest_AppUsers] FOREIGN KEY([UserId])
REFERENCES [dbo].[AppUsers] ([UserId])
GO
ALTER TABLE [dbo].[AppUserPwdRequest] CHECK CONSTRAINT [FK_AppUserPwdRequest_AppUsers]
GO
ALTER TABLE [dbo].[AppUsers]  WITH CHECK ADD  CONSTRAINT [FK_Users_Groups] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AppRoles] ([RoleId])
GO
ALTER TABLE [dbo].[AppUsers] CHECK CONSTRAINT [FK_Users_Groups]
GO
ALTER TABLE [dbo].[BankChallan]  WITH CHECK ADD  CONSTRAINT [FK_BankChallan_Day] FOREIGN KEY([DayId])
REFERENCES [dbo].[Day] ([DayId])
GO
ALTER TABLE [dbo].[BankChallan] CHECK CONSTRAINT [FK_BankChallan_Day]
GO
ALTER TABLE [dbo].[BankChallan]  WITH CHECK ADD  CONSTRAINT [FK_BankChallanDetails_Bank] FOREIGN KEY([BankId])
REFERENCES [dbo].[Bank] ([BankId])
GO
ALTER TABLE [dbo].[BankChallan] CHECK CONSTRAINT [FK_BankChallanDetails_Bank]
GO
ALTER TABLE [dbo].[BankChallanDetail]  WITH CHECK ADD  CONSTRAINT [FK_BankChallanDetail_BankId] FOREIGN KEY([BankChallanId])
REFERENCES [dbo].[BankChallan] ([BankChallanId])
GO
ALTER TABLE [dbo].[BankChallanDetail] CHECK CONSTRAINT [FK_BankChallanDetail_BankId]
GO
ALTER TABLE [dbo].[BankChallanDetail]  WITH CHECK ADD  CONSTRAINT [FK_BankChallanDetail_TokenDetails] FOREIGN KEY([TokenDetailId])
REFERENCES [dbo].[TokenDetails] ([TokenDetailsId])
GO
ALTER TABLE [dbo].[BankChallanDetail] CHECK CONSTRAINT [FK_BankChallanDetail_TokenDetails]
GO
ALTER TABLE [dbo].[CategoryFees]  WITH CHECK ADD  CONSTRAINT [FK_CategoryFees_Category] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Category] ([CategoryId])
GO
ALTER TABLE [dbo].[CategoryFees] CHECK CONSTRAINT [FK_CategoryFees_Category]
GO
ALTER TABLE [dbo].[Day]  WITH CHECK ADD  CONSTRAINT [FK_Day_AppUsers] FOREIGN KEY([StartedBy])
REFERENCES [dbo].[AppUsers] ([UserId])
GO
ALTER TABLE [dbo].[Day] CHECK CONSTRAINT [FK_Day_AppUsers]
GO
ALTER TABLE [dbo].[Operation]  WITH CHECK ADD  CONSTRAINT [FK_Operation_AppUsers] FOREIGN KEY([OperationStartedBy])
REFERENCES [dbo].[AppUsers] ([UserId])
GO
ALTER TABLE [dbo].[Operation] CHECK CONSTRAINT [FK_Operation_AppUsers]
GO
ALTER TABLE [dbo].[Operation]  WITH CHECK ADD  CONSTRAINT [FK_Shift_Operation] FOREIGN KEY([ShiftId])
REFERENCES [dbo].[Shift] ([ShiftId])
GO
ALTER TABLE [dbo].[Operation] CHECK CONSTRAINT [FK_Shift_Operation]
GO
ALTER TABLE [dbo].[Shift]  WITH CHECK ADD  CONSTRAINT [FK_Shift_AppUsers] FOREIGN KEY([ShiftStartedBy])
REFERENCES [dbo].[AppUsers] ([UserId])
GO
ALTER TABLE [dbo].[Shift] CHECK CONSTRAINT [FK_Shift_AppUsers]
GO
ALTER TABLE [dbo].[Shift]  WITH CHECK ADD  CONSTRAINT [FK_Shift_Day] FOREIGN KEY([DayId])
REFERENCES [dbo].[Day] ([DayId])
GO
ALTER TABLE [dbo].[Shift] CHECK CONSTRAINT [FK_Shift_Day]
GO
ALTER TABLE [dbo].[ShiftDetails]  WITH CHECK ADD  CONSTRAINT [FK_ShiftDetails_AppUsers] FOREIGN KEY([UserLoggedIn])
REFERENCES [dbo].[AppUsers] ([UserId])
GO
ALTER TABLE [dbo].[ShiftDetails] CHECK CONSTRAINT [FK_ShiftDetails_AppUsers]
GO
ALTER TABLE [dbo].[ShiftDetails]  WITH CHECK ADD  CONSTRAINT [FK_ShiftDetails_AppUsers1] FOREIGN KEY([UserLoggedOut])
REFERENCES [dbo].[AppUsers] ([UserId])
GO
ALTER TABLE [dbo].[ShiftDetails] CHECK CONSTRAINT [FK_ShiftDetails_AppUsers1]
GO
ALTER TABLE [dbo].[ShiftDetails]  WITH CHECK ADD  CONSTRAINT [FK_ShiftDetails_Shift] FOREIGN KEY([ShiftId])
REFERENCES [dbo].[Shift] ([ShiftId])
GO
ALTER TABLE [dbo].[ShiftDetails] CHECK CONSTRAINT [FK_ShiftDetails_Shift]
GO
ALTER TABLE [dbo].[Token]  WITH CHECK ADD  CONSTRAINT [FK_Token_CategoryFees] FOREIGN KEY([CategoryFeesId])
REFERENCES [dbo].[CategoryFees] ([CategoryFeeId])
GO
ALTER TABLE [dbo].[Token] CHECK CONSTRAINT [FK_Token_CategoryFees]
GO
ALTER TABLE [dbo].[Token]  WITH CHECK ADD  CONSTRAINT [FK_Token_Operation] FOREIGN KEY([OperationId])
REFERENCES [dbo].[Operation] ([OperationId])
GO
ALTER TABLE [dbo].[Token] CHECK CONSTRAINT [FK_Token_Operation]
GO
ALTER TABLE [dbo].[Token]  WITH CHECK ADD  CONSTRAINT [fkToken_PlaceOfVisit] FOREIGN KEY([PlaceOfVisitId])
REFERENCES [dbo].[PlaceOfVisit] ([PlaceOfVisitId])
GO
ALTER TABLE [dbo].[Token] CHECK CONSTRAINT [fkToken_PlaceOfVisit]
GO
ALTER TABLE [dbo].[Token]  WITH CHECK ADD  CONSTRAINT [fkToken_PurposeOfVisit] FOREIGN KEY([PurposeOfVisit])
REFERENCES [dbo].[PurposeOfVisit] ([PurposeOfVisitId])
GO
ALTER TABLE [dbo].[Token] CHECK CONSTRAINT [fkToken_PurposeOfVisit]
GO
ALTER TABLE [dbo].[TokenCancelledRequestDetails]  WITH CHECK ADD  CONSTRAINT [FK_TokenCancelledRequestDetails_TokenCancelRequest] FOREIGN KEY([TokenCancelRequestId])
REFERENCES [dbo].[TokenCancelRequest] ([TokenCancelRequestId])
GO
ALTER TABLE [dbo].[TokenCancelledRequestDetails] CHECK CONSTRAINT [FK_TokenCancelledRequestDetails_TokenCancelRequest]
GO
ALTER TABLE [dbo].[TokenCancelledRequestDetails]  WITH CHECK ADD  CONSTRAINT [FK_TokenCancelledRequestDetails_TokenDetails] FOREIGN KEY([TokenDetailId])
REFERENCES [dbo].[TokenDetails] ([TokenDetailsId])
GO
ALTER TABLE [dbo].[TokenCancelledRequestDetails] CHECK CONSTRAINT [FK_TokenCancelledRequestDetails_TokenDetails]
GO
ALTER TABLE [dbo].[TokenCancelRequest]  WITH CHECK ADD  CONSTRAINT [FK_TokenCancelRequest_AppUsers] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[AppUsers] ([UserId])
GO
ALTER TABLE [dbo].[TokenCancelRequest] CHECK CONSTRAINT [FK_TokenCancelRequest_AppUsers]
GO
ALTER TABLE [dbo].[TokenCancelRequest]  WITH CHECK ADD  CONSTRAINT [FK_TokenCancelRequest_CancelStatus] FOREIGN KEY([CancelStatusId])
REFERENCES [dbo].[CancelStatus] ([CancelStatusId])
GO
ALTER TABLE [dbo].[TokenCancelRequest] CHECK CONSTRAINT [FK_TokenCancelRequest_CancelStatus]
GO
ALTER TABLE [dbo].[TokenCancelRequest]  WITH CHECK ADD  CONSTRAINT [FK_TokenCancelRequest_Category] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Category] ([CategoryId])
GO
ALTER TABLE [dbo].[TokenCancelRequest] CHECK CONSTRAINT [FK_TokenCancelRequest_Category]
GO
ALTER TABLE [dbo].[TokenCancelRequest]  WITH CHECK ADD  CONSTRAINT [FK_TokenCancelRequest_TokenDetails] FOREIGN KEY([TokenId])
REFERENCES [dbo].[Token] ([TokenId])
GO
ALTER TABLE [dbo].[TokenCancelRequest] CHECK CONSTRAINT [FK_TokenCancelRequest_TokenDetails]
GO
ALTER TABLE [dbo].[TokenDetails]  WITH CHECK ADD  CONSTRAINT [FK_TokenDetails_Token] FOREIGN KEY([TokenId])
REFERENCES [dbo].[Token] ([TokenId])
GO
ALTER TABLE [dbo].[TokenDetails] CHECK CONSTRAINT [FK_TokenDetails_Token]
GO
ALTER TABLE [dbo].[TokenTransaction]  WITH CHECK ADD  CONSTRAINT [FK_TokenTransaction_Operation] FOREIGN KEY([OperationId])
REFERENCES [dbo].[Operation] ([OperationId])
GO
ALTER TABLE [dbo].[TokenTransaction] CHECK CONSTRAINT [FK_TokenTransaction_Operation]
GO
ALTER TABLE [dbo].[TokenTransaction]  WITH CHECK ADD  CONSTRAINT [FK_TokenTransaction_Token] FOREIGN KEY([TokendetailId])
REFERENCES [dbo].[TokenDetails] ([TokenDetailsId])
GO
ALTER TABLE [dbo].[TokenTransaction] CHECK CONSTRAINT [FK_TokenTransaction_Token]
GO
USE [master]
GO
ALTER DATABASE [VehicleDB] SET  READ_WRITE 
GO
